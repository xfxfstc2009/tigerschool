//
//  AppDelegate.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/12.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

