//
//  TSViewTool.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TSViewTool : NSObject

+(UITableView *)gwCreateTableViewRect:(CGRect)rect;                             // 1.创建一个tableView
+(UICollectionView *)gwCreateCollectionViewRect:(CGRect)rect;                   // 2.创建一个collectionview
+(UIScrollView *)gwCreateScrollViewWithRect:(CGRect)rect;                       // 3.创建一个scrollview
+(UILabel *)createLabelFont:(NSString *)font textColor:(NSString *)color;       // 4.创建一个textLabel；


@end
