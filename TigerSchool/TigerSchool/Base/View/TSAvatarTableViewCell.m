//
//  TSAvatarTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSAvatarTableViewCell.h"

@interface TSAvatarTableViewCell()
@property (nonatomic,strong)UIView *convertView;
@property (nonatomic,strong)TSImageView *arrowImgView;          /**< arrow image*/


@end

@implementation TSAvatarTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont systemFontOfCustomeSize:16.];
    self.fixedLabel.text = @"上传头像";
    [self addSubview:self.fixedLabel];
    
    self.convertView = [[UIView alloc]init];
    self.convertView.backgroundColor = [UIColor clearColor];
    self.convertView.frame = CGRectMake(kScreenBounds.size.width - TSFloat(48) - TSFloat(40), 0, TSFloat(40), TSFloat(40));
    self.convertView.layer.cornerRadius = self.convertView.size_width / 2.;
    self.convertView.clipsToBounds = YES;
    [self addSubview:self.convertView];
    
    self.avatarImgView = [[TSImageView alloc]init];
    self.avatarImgView.backgroundColor = [UIColor clearColor];
    [self.convertView addSubview:self.avatarImgView];
    
    self.arrowImgView = [[TSImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    [self addSubview:self.arrowImgView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    // 1. title
    CGSize titleSize = [self.fixedLabel.text sizeWithCalcFont:self.fixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.fixedLabel.font])];
    self.fixedLabel.frame = CGRectMake(TSFloat(11), 0, titleSize.width, transferCellHeight);
 
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - TSFloat(16) - TSFloat(9), (transferCellHeight - TSFloat(15)) / 2., TSFloat(9), TSFloat(15));
    
    self.convertView.frame = CGRectMake(self.arrowImgView.orgin_x - TSFloat(15) - TSFloat(42), TSFloat(7), TSFloat(42), TSFloat(42));
    self.convertView.center_y = self.fixedLabel.center_y;
}


-(void)setTransferAvatarUrl:(NSString *)transferAvatarUrl{
    _transferAvatarUrl = transferAvatarUrl;
    
    __weak typeof(self)weakSelf = self;
    [self.avatarImgView uploadImageWithURL:transferAvatarUrl placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [TSTool setTransferSingleAsset:image imgView:strongSelf.avatarImgView convertView:strongSelf.convertView];
    }];
}

-(void)setTransferAvatar:(UIImage *)transferAvatar{
    _transferAvatar = transferAvatar;
    self.avatarImgView.image = transferAvatar;
    [TSTool setTransferSingleAsset:transferAvatar imgView:self.avatarImgView convertView:self.convertView];
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = TSFloat(10);
    cellHeight += TSFloat(42);
    cellHeight += TSFloat(10);
    
    return cellHeight;
}


@end
