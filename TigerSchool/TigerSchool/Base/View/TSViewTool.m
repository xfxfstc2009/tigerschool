//
//  TSViewTool.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSViewTool.h"

@implementation TSViewTool

+(UITableView *)gwCreateTableViewRect:(CGRect)rect{
    UITableView *tableView = [[UITableView alloc] initWithFrame:rect style:UITableViewStylePlain];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.scrollEnabled = YES;
    return tableView;
}

+(UICollectionView *)gwCreateCollectionViewRect:(CGRect)rect{
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    UICollectionView *collectionView = [[UICollectionView alloc] initWithFrame:rect collectionViewLayout:flowLayout];
    collectionView.backgroundColor = [UIColor clearColor];
    collectionView.showsVerticalScrollIndicator = NO;
    collectionView.showsHorizontalScrollIndicator = NO;
    collectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight| UIViewAutoresizingFlexibleWidth;
    return collectionView;
}

+(UIScrollView *)gwCreateScrollViewWithRect:(CGRect)rect{
    UIScrollView *mainScrollView = [[UIScrollView alloc]init];
    mainScrollView.frame = rect;
    mainScrollView.backgroundColor = [UIColor clearColor];
    mainScrollView.pagingEnabled = YES;
    mainScrollView.alwaysBounceHorizontal = NO;
    mainScrollView.alwaysBounceVertical = NO;
    mainScrollView.showsHorizontalScrollIndicator = NO;
    mainScrollView.showsVerticalScrollIndicator = NO;
    //    mainScrollView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    return mainScrollView;
}


+(UILabel *)createLabelFont:(NSString *)font textColor:(NSString *)color{
    UILabel *label = [[UILabel alloc]init];
    label.backgroundColor = [UIColor clearColor];
    label.textColor = [UIColor colorWithCustomerName:color];
    label.font = [UIFont fontWithCustomerSizeName:font];
    
    
    return label;
}





@end
