//
//  GWButtonTableViewCell.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWButtonTableViewCell.h"


static char buttonKey;
@interface GWButtonTableViewCell()

@end

@implementation GWButtonTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        self.backgroundColor = [UIColor clearColor];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.button = [UIButton buttonWithType:UIButtonTypeCustom];
    self.button.titleLabel.font = [[UIFont systemFontOfCustomeSize:22.]boldFont];
    [self.button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.button buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &buttonKey);
        if (block){
            block();
        }
    }];
    [self addSubview:self.button];
}

-(void)setTransferTitle:(NSString *)transferTitle {
    _transferTitle = transferTitle;
    self.button.frame = CGRectMake(TSFloat(85) / 2., 0, kScreenBounds.size.width - TSFloat(85), self.transferCellHeight);
    [self.button setTitle:transferTitle forState:UIControlStateNormal];
}

-(void)setButtonType:(GWButtonTableViewCellType)buttonType{
    _buttonType = buttonType;
}

-(void)setButtonStatus:(BOOL)enable{
    if (self.buttonType == GWButtonTableViewCellTypeLayer){
        self.button.titleLabel.font = [UIFont systemFontOfCustomeSize:15.];
        self.button.layer.cornerRadius = MIN(self.button.size_width, self.button.size_height) / 2.;
        self.button.backgroundColor = [UIColor whiteColor];
        self.button.userInteractionEnabled = YES;
        self.button.clipsToBounds = YES;
        self.button.layer.borderColor = [UIColor hexChangeFloat:@"DCDCDC"].CGColor;
        [self.button setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
        self.button.layer.borderWidth = .5f;
    } else {
        if(enable){         // 可以进行点击
            self.button.userInteractionEnabled = YES;
            [self.button setBackgroundImage:[TSTool stretchImageWithName:@"icon_button_hlt"] forState:UIControlStateNormal];
        } else {
            self.button.userInteractionEnabled = NO;
            [self.button setBackgroundImage:[TSTool stretchImageWithName:@"icon_button_nor"] forState:UIControlStateNormal];
        }
    }
}

-(void)buttonClickManager:(void(^)())block{
    objc_setAssociatedObject(self, &buttonKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeight{
    return TSFloat(44);
}

@end
