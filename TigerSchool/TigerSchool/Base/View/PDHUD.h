//
//  PDHUD.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/31.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <WSProgressHUD/WSProgressHUD.h>

@interface PDHUD : WSProgressHUD


#pragma mark - 显示Text
+(void)showText:(NSString *)text;                                           /**< 显示文字*/
-(void)showText:(NSString *)text;

#pragma mark -Progress
+(void)showHUDProgress:(NSString *)text diary:(NSInteger)diary;                 /**< */

#pragma mark -Success
+(void)showHUDError:(NSString *)text;                           /**< 显示失败的HUD*/
+(void)showHUDSuccess:(NSString *)text;                         /**< 显示成功的HUD*/

#pragma mark - dismiss
+(void)dismissManager;                                              /**< 这是消失方法*/

+(void)showHUDBindingProgress:(NSString *)text diary:(NSInteger)diary; /**< */
+(void)showHUDBindingRoleDetailProgress:(NSString *)text diary:(NSInteger)diary;

@end
