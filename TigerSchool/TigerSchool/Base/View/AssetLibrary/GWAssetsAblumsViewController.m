//
//  GWAssetsAblumsViewController.m
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/18.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import "GWAssetsAblumsViewController.h"
#import "GWAblumCollectionViewCell.h"
@interface GWAssetsAblumsViewController()<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic,strong)UICollectionView *ablumCollectionView;

@end

@implementation GWAssetsAblumsViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self createCollectionView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"相册";
}

-(void)createCollectionView{
    if (!self.ablumCollectionView){
        UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
        self.ablumCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
        self.ablumCollectionView.backgroundColor = [UIColor clearColor];
        self.ablumCollectionView.showsVerticalScrollIndicator = NO;
        [self.ablumCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
        self.ablumCollectionView.delegate = self;
        self.ablumCollectionView.dataSource = self;
        self.ablumCollectionView.showsHorizontalScrollIndicator = NO;
        self.ablumCollectionView.scrollsToTop = YES;
        self.ablumCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
        [self.view addSubview:self.ablumCollectionView];
        
        [self.ablumCollectionView registerClass:[GWAblumCollectionViewCell class] forCellWithReuseIdentifier:@"GWAblumCollectionViewCell"];
    }
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.ablumsArr.count;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    GWAblumCollectionViewCell *cell = (GWAblumCollectionViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"GWAblumCollectionViewCell"  forIndexPath:indexPath];
    
    
    // 赋值
    ALAssetsGroup *ablum = (ALAssetsGroup *)[self.ablumsArr objectAtIndex:indexPath.row];

    cell.transferImage = [UIImage imageWithCGImage:ablum.posterImage];
    cell.ablumName = [ablum valueForProperty:ALAssetsGroupPropertyName];
    cell.numberOfAblum = [NSString stringWithFormat:@"%li",(long)[ablum numberOfAssets]];
    return cell;
 }

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return [GWAblumCollectionViewCell calculationCellSize];
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,10,0,10);
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGSize size = {kScreenBounds.size.width,TSFloat(20)};
    return size;
}

#pragma mark cell的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    ALAssetsGroup *selectedAblum = [self.ablumsArr objectAtIndex:indexPath.row];
    if (self.ablumSelectrdBlock){
        self.ablumSelectrdBlock(selectedAblum);
    }
    [self.navigationController popViewControllerAnimated:YES];
}



@end
