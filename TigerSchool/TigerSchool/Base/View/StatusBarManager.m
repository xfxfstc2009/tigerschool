//
//  StatusBarManager.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "StatusBarManager.h"
#import <WTStatusBar/WTStatusBar.h>

#ifndef dispatch_main_async_safe
#define dispatch_main_async_safe(block)\
if (strcmp(dispatch_queue_get_label(DISPATCH_CURRENT_QUEUE_LABEL), dispatch_queue_get_label(dispatch_get_main_queue())) == 0) {\
block();\
} else {\
dispatch_async(dispatch_get_main_queue(), block);\
}
#endif

@implementation StatusBarManager

+(void)statusBarWithClear{
    [WTStatusBar setBackgroundColor:UUBlack];
    [WTStatusBar clearStatusAnimated:YES];
}

+(void)statusShowWithString:(NSString *)string{
    if (IS_iPhoneX){
        [PDHUD showText:string];
    } else {
        [WTStatusBar setBackgroundColor:UUBlack];
        [WTStatusBar setStatusText:string animated:YES];
    }
}

+(void)statusHiddenWithString:(NSString *)string{
//    if (IS_iPhoneX){
        dispatch_main_async_safe(^{
            [PDHUD showText:string];
        });
//    } else {
//        [WTStatusBar setTextColor:UUWhite];
//        [WTStatusBar setBackgroundColor:[UIColor colorWithCustomerName:@"黑"]];
//        [WTStatusBar setStatusText:string timeout:3 animated:YES];
//    }
}

+(void)statusBarWithText:(NSString *)text progress:(CGFloat)progress{
    if (progress < 1.0){
        [WTStatusBar setProgressBarColor:UUBlue];
        [WTStatusBar setStatusText:text animated:YES];
        [WTStatusBar setProgress:progress animated:YES];
    } else {
        [WTStatusBar setProgressBarColor:UUBlue];
    }
}


+(void)testStatusBarWithText:(NSString *)text progress:(CGFloat)progress{
    [WTStatusBar setBackgroundColor:UUBlack];
    [WTStatusBar setProgress:progress animated:YES];
}


@end
