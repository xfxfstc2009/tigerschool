//
//  StatusBarManager.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "PDHUD.h"

@interface StatusBarManager : NSObject

+(void)statusShowWithString:(NSString *)string;
+(void)statusHiddenWithString:(NSString *)string;
+(void)statusBarWithText:(NSString *)text progress:(CGFloat)progress;
+(void)testStatusBarWithText:(NSString *)text progress:(CGFloat)progress;

@end
