//
//  TSImageView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef NS_ENUM(NSInteger ,PDImgType) {
    PDImgTypeNormal = 0,
    PDImgTypeAvatar = 1,
    PDImgTypeRoot = 3,              /**< 原图*/
};

@interface TSImageView : UIImageView

// 更新图片
-(void)uploadImageWithURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
-(void)uploadImageWithAvatarURL:(NSString *)urlString placeholder:(UIImage *)placeholder callback:(void(^)(UIImage *image))callbackBlock;
// 缓存数据
+(void)cleanImgCacheWithBlock:(void(^)())block;
+(NSString *)diskCount;

@end
