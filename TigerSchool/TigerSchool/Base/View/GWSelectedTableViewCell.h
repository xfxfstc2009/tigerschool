//
//  GWSelectedTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/17.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSBaseTableViewCell.h"

@interface GWSelectedTableViewCell : TSBaseTableViewCell
{
    BOOL			isChecked;
}
@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,strong)UIImage *iconImg;
+(CGFloat)calculationCellHeight;

-(void)setChecked:(BOOL)checked;

@end
