//
//  TSAvatarTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//
// 【头像的Cell】

#import "TSBaseTableViewCell.h"

@interface TSAvatarTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)TSImageView *avatarImgView;
@property (nonatomic,strong)UIImage *transferAvatar;
@property (nonatomic,copy)NSString *transferAvatarUrl;
@property (nonatomic,strong)UILabel *fixedLabel;

+(CGFloat)calculationCellHeight;


@end
