//
//  GWNormalTableViewCell.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 2016/12/16.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSBaseTableViewCell.h"

@interface GWNormalTableViewCell : TSBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;                  /**< 传入的标题*/
@property (nonatomic,strong)UIImage *transferIcon;                  /**< 传入的icon*/
@property (nonatomic,copy)NSString *transferDesc;                   /**< 传入的内容*/
@property (nonatomic,assign)BOOL transferHasArrow;                  /**< 是否箭头*/
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,assign)BOOL textAlignmentCenter;               /**< 是否设置中间*/
@property (nonatomic,strong)UIButton *titleButton;
@property (nonatomic,strong)UIButton *dymicButton;
-(void)actionClickWithBlock:(void(^)())block;
-(void)actionClickWithDymicBlock:(void(^)())block;
+(CGFloat)calculationCellHeight;

@end
