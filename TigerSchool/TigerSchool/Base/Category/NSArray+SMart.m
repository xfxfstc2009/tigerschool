//
//  NSArray+SMart.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/18.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "NSArray+SMart.h"

@implementation NSArray (SMart)


- (NSString *)mmh_JSONString
{
    NSError *error = nil;
    NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                   options:0
                                                     error:&error];
    if (error) {
        NSLog(@"ERROR: cannot convert array to JSON string: %@", self);
        return nil;
    }
    
    return [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
}

@end
