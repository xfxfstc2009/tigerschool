//
//  UIScrollView+Customise.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/11/3.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIScrollView (Customise)<UIScrollViewDelegate>

+(UIScrollView *)createScrollViewScrollEndBlock:(void (^)(UIScrollView *scrollView))block;

@end
