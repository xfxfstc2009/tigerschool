//
//  NSString+NSMutableAttributedString.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/3.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (NSMutableAttributedString)

-(NSAttributedString *)stringWithParagraphlineSpeace:(CGFloat)lineSpacing
                                           textColor:(UIColor *)textcolor
                                            textFont:(UIFont *)font ;

-(CGFloat)getSpaceLabelHeightwithSpeace:(CGFloat)lineSpeace withFont:(UIFont*)font withWidth:(CGFloat)width ;
@end
