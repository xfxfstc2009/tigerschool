//
//  UILabel+PDCategory.h
//  PandaKing
//
//  Created by Cranz on 16/9/14.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UILabel (PDCategory)
@property (nonatomic, assign) CGSize constraintSize;

-(void)animationWithScrollToValue:(NSInteger)toValue;
- (CGSize)size;
@end
