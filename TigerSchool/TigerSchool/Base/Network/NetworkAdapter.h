//
//  NetworkAdapter.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//
// 网络适配器
#import <Foundation/Foundation.h>

#define PDBizErrorDomain @"PDBizErrorDomain"

typedef void (^FetchCompletionHandler) (BOOL isSucceeded,id responseObject, NSError *error);

@protocol PDNetworkAdapterDelegate <NSObject>

@optional
-(void)pandaNotLoginNeedLoginManager;                           // 没有登录，需要登录
-(void)pandaNotLoginNeedSMSLogin;                               // 从来没有登录过，需要验证码登录
-(void)socketDidBackData:(id)responseObject;
@end

@protocol TSNetworkAdapterLoginErrorDelegate <NSObject>
@optional
-(void)tigerschoolNotLoginNeedSMSLogin;                               // 从来没有登录过，需要验证码登录
@end

@interface NetworkAdapter : NSObject

+(instancetype)sharedAdapter;                                                               /**< 单例*/

#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block;                                                          /**< 短链接*/
-(void)fetchWithGetPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block;

#pragma mark - 长链接
@property (nonatomic,weak)id<PDNetworkAdapterDelegate> delegate;                        /**< */
@property (nonatomic,weak)id<TSNetworkAdapterLoginErrorDelegate> loginErrorDelegate;    /**< 登陆异常回调*/

+ (NSString*)dictionaryToJson:(NSDictionary *)dic;



@end
