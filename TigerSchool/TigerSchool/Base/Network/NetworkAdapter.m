//
//  NetworkAdapter.m
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

// 网络适配器
#import "NetworkAdapter.h"
#import "NetworkEngine.h"
#import "SBJSON.h"


@implementation NetworkAdapter

+(instancetype)sharedAdapter{
    static NetworkAdapter *_sharedAdapter = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAdapter = [[NetworkAdapter alloc] init];
    });
    return _sharedAdapter;
}

#pragma mark - 短链接
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block{
    __weak typeof(self)weakSelf = self;

    NetworkEngine *networkEngine;
   
    if (JAVA_Port.length){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@",JAVA_Host,JAVA_Port]]];
    } else {
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",JAVA_Host]]];
    }

    [networkEngine fetchWithPath:path requestParams:requestParams responseObjectClass:responseObjectClass succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            if ([responseObject isKindOfClass:[NSDictionary class]]){           // 字典类别
                // 判断是否请求成功
                NSNumber *errorNumber = [responseObject objectForKey:@"errCode"];
                if (errorNumber.integerValue == 200){           // 请求成功
                    if (responseObjectClass == nil){
                        NSDictionary *dic = [responseObject objectForKey:@"data"];
                        block(YES,dic,nil);
                        return;
                    }
                    
                    FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:[responseObject objectForKey:@"data"]];
                    block(YES,responseModelObject,nil);
                    
                } else if (errorNumber.integerValue == 500){            // 重新登录
                    if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(pandaNotLoginNeedLoginManager)]){
                        [[NetworkAdapter sharedAdapter].delegate pandaNotLoginNeedLoginManager];
                    }
                } else {                                        // 业务错误
//                    [StatusBarManager statusBarHidenWithText:@"服务器好像出了点问题"];
                    id errInfo = [responseObject objectForKey:@"errMsg"];
                    if ([errInfo isKindOfClass:[NSString class]]){
                        NSString *errorInfo = [responseObject objectForKey:@"errMsg"];
                        if (!errorInfo.length){
                            errorInfo = @"系统错误，请联系管理员";
                        }
                        NSDictionary *dict = @{NSLocalizedDescriptionKey: errorInfo};
                        NSError *bizError = [NSError errorWithDomain:PDBizErrorDomain code:errorNumber.integerValue userInfo:dict];
//                        [PDHUD showHUDError:errorInfo];
                        
                        block(NO,responseObject,bizError);
                        return;
                    } else {
                        return;
                    }
                }
            }
        }
    }];
}

#pragma mark - 短链接
-(void)fetchWithGetPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block{
    __weak typeof(self)weakSelf = self;
    
    NetworkEngine *networkEngine;
    
    if (JAVA_Port.length){
        networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@:%@",JAVA_Host,JAVA_Port]]];
    } else {
        if ([JAVA_Host isEqualToString:@"apipre.tigerschool.cn"]){
            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"https://%@",JAVA_Host]]];
        } else {
            networkEngine = [[NetworkEngine alloc]initWithBaseURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",JAVA_Host]]];
        }
    }
    
    [networkEngine fetchWithGetPath:path requestParams:requestParams responseObjectClass:responseObjectClass succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            if ([responseObject isKindOfClass:[NSDictionary class]]){           // 字典类别
                // 判断是否请求成功
                NSNumber *errorNumber = [responseObject objectForKey:@"status"];
                if (errorNumber.integerValue == 200){           // 请求成功
                    if (responseObjectClass == nil){
                        NSDictionary *dic = [responseObject objectForKey:@"data"];
                        block(YES,dic,nil);
                        return;
                    }
                    
                    // 1. 判断是否有数组
                    NSMutableDictionary *dataDic = [NSMutableDictionary dictionary];
                    
                    NSDictionary *tempResponseObject = (NSDictionary *)responseObject;
                    
                    if ([tempResponseObject.allKeys containsObject:@"items"]){
                        NSArray *itemsArr = [tempResponseObject objectForKey:@"items"];
                        NSDictionary *tempItemsDic = @{@"items":itemsArr};
                        [dataDic setValue:tempItemsDic forKey:@"data"];
                    }
                    
                    if ([tempResponseObject.allKeys containsObject:@"data"]){
                        [dataDic setObject:[responseObject objectForKey:@"data"] forKey:@"data"];
                    }
                    
                    
                    FetchModel *responseModelObject = (FetchModel *)[[responseObjectClass alloc] initWithJSONDict:[dataDic objectForKey:@"data"]];
                    block(YES,responseModelObject,nil);
                    
                } else if (errorNumber.integerValue == 501){            // 重新登录
                    if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(pandaNotLoginNeedSMSLogin)]){
                        [[NetworkAdapter sharedAdapter].delegate pandaNotLoginNeedSMSLogin];
                    }
                } else {                                        // 业务错误
                    id errInfo = [responseObject objectForKey:@"msg"];
                    if ([errInfo isKindOfClass:[NSString class]]){
                        NSString *errorInfo = [responseObject objectForKey:@"msg"];
                        if (!errorInfo.length){
                            errorInfo = @"系统错误，请联系管理员";
                        }
                        NSDictionary *dict = @{NSLocalizedDescriptionKey: errorInfo};
                        NSError *bizError = [NSError errorWithDomain:PDBizErrorDomain code:errorNumber.integerValue userInfo:dict];

                        if (errorNumber.integerValue == 500 && ([path isEqualToString:login_checkregister] || [path isEqualToString:teach_materiallimit] || [path isEqualToString:login_changepwd] || [path isEqualToString:home_searchDetail] || [path isEqualToString:login_forgetpwd])){     // 判断是否存在手机号。
                            block(NO,responseObject,bizError);
                            return;
                        }
                        if (errorNumber.integerValue == 505){
                            if ([AccountModel sharedAccountModel].has505 == NO){
                                [[UIAlertView alertViewWithTitle:nil message:errorInfo buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                    if (!weakSelf){
                                        return ;
                                    }
                                    if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(pandaNotLoginNeedLoginManager)]){
                                        [[NetworkAdapter sharedAdapter].delegate pandaNotLoginNeedLoginManager];
                                    }
                                }]show];
                                [AccountModel sharedAccountModel].has505 = YES;
                            }
                            return;
                        }
                        if (errorNumber.integerValue == 100001 && [path isEqualToString:teach_iosaddpayorder]){
                            block(NO,responseObject,bizError);
                            return;
                        }
                        
                        [[UIAlertView alertViewWithTitle:nil message:errorInfo buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                            if (!weakSelf){
                                return ;
                            }
                            if (errorNumber.integerValue == 300){           // 表示第一次登陆用验证码登陆
                                if ([NetworkAdapter sharedAdapter].loginErrorDelegate && [[NetworkAdapter sharedAdapter].loginErrorDelegate respondsToSelector:@selector(tigerschoolNotLoginNeedSMSLogin)]){
                                    [[NetworkAdapter sharedAdapter].loginErrorDelegate tigerschoolNotLoginNeedSMSLogin];
                                }
                            } else {
//                                if ([NetworkAdapter sharedAdapter].delegate && [[NetworkAdapter sharedAdapter].delegate respondsToSelector:@selector(pandaNotLoginNeedLoginManager)]){
//                                    [[NetworkAdapter sharedAdapter].delegate pandaNotLoginNeedLoginManager];
//                                }
                            }
                        }]show];
                        
                        block(NO,responseObject,bizError);
                        return;
                    } else {
                        return;
                    }
                }
            }
        }
    }];
}


- (NSString*)dictionaryToJson:(NSDictionary *)dic{
    NSError *parseError = nil;
    
    SBJSON *jsonSDK = [[SBJSON alloc] init];
    NSString *jsonStr = [jsonSDK stringWithObject:dic error:&parseError];
    return jsonStr;
}


+ (NSString*)dictionaryToJson:(NSDictionary *)dic{
    NSError *parseError = nil;

    SBJSON *jsonSDK = [[SBJSON alloc] init];
    NSString *jsonStr = [jsonSDK stringWithObject:dic error:&parseError];
    return jsonStr;
}


@end

