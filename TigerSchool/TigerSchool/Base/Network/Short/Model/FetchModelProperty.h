//
//  FetchModelProperty.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#import <Foundation/Foundation.h>

/*
 相关知识请参见Runtime文档
 Type Encodings https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtTypeEncodings.html#//apple_ref/doc/uid/TP40008048-CH100-SW1
 Property Type String https://developer.apple.com/library/mac/documentation/Cocoa/Conceptual/ObjCRuntimeGuide/Articles/ocrtPropertyIntrospection.html#//apple_ref/doc/uid/TP40008048-CH101-SW6
 */

typedef NS_ENUM(NSUInteger, FetchModelPropertyValueType) {
    ClassPropertyValueTypeNone = 0,
    ClassPropertyTypeChar,
    ClassPropertyTypeInt,
    ClassPropertyTypeShort,
    ClassPropertyTypeLong,
    ClassPropertyTypeLongLong,
    ClassPropertyTypeUnsignedChar,
    ClassPropertyTypeUnsignedInt,
    ClassPropertyTypeUnsignedShort,
    ClassPropertyTypeUnsignedLong,
    ClassPropertyTypeUnsignedLongLong,
    ClassPropertyTypeFloat,
    ClassPropertyTypeDouble,
    ClassPropertyTypeBool,
    ClassPropertyTypeVoid,
    ClassPropertyTypeCharString,
    ClassPropertyTypeObject,
    ClassPropertyTypeClassObject,
    ClassPropertyTypeSelector,
    ClassPropertyTypeArray,
    ClassPropertyTypeStruct,
    ClassPropertyTypeUnion,
    ClassPropertyTypeBitField,
    ClassPropertyTypePointer,
    ClassPropertyTypeUnknow
};

@interface FetchModelProperty : NSObject
@property (nonatomic, copy) NSString *name;

@property (nonatomic, assign) FetchModelPropertyValueType valueType;
@property (nonatomic, copy) NSString *typeName;
@property (nonatomic, assign) Class objectClass;
@property (nonatomic, strong) NSArray *objectProtocols;
@property (nonatomic, assign) BOOL isReadonly;

- (id)initWithName:(NSString *)name typeString:(NSString *)typeString;

@end
