//
//  FetchFileModel.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/21.
//  Copyright © 2016年 BasicPod. All rights reserved.
//
//
#import <Foundation/Foundation.h>

@interface FetchFileModel : NSObject

@property (nonatomic,strong)id uploadImage;
@property (nonatomic,copy)NSString *keyName;

@end
