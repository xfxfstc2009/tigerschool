//
//  NetworkEngine.h
//  Basic
//
//  Created by 裴烨烽 on 16/5/23.
//  Copyright © 2016年 BasicPod. All rights reserved.
//

#import <AFNetworking/AFNetworking.h>

typedef void (^FetchCompletionHandler) (BOOL isSucceeded,id responseObject, NSError *error);

@interface NetworkEngine : AFHTTPSessionManager

// 【POST】
-(void)fetchWithPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block;

// 【Get】
-(void)fetchWithGetPath:(NSString *)path requestParams:(NSDictionary *)requestParams responseObjectClass:(Class)responseObjectClass succeededBlock:(FetchCompletionHandler)block;

-(instancetype)initWithHttpsBaseURL:(NSURL *)url ;

@end
