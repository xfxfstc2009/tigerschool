//
//  URLConstance.h
//  PandaGaming
//
//  Created by 裴烨烽 on 2017/10/30.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#ifndef URLConstance_h
#define URLConstance_h

// 【接口环境】
#ifdef DEBUG        // 测试apitest.tigerschool.cn
#define JAVA_Host @"apipre.tigerschool.cn"
#define JAVA_Port @""
#else               // 线上apipre.tigerschool.cn
#define JAVA_Host @"apipre.tigerschool.cn"
#define JAVA_Port @""
#endif

// 【配置】
static NSString *const ErrorDomain = @"ErrorDomain";
static BOOL NetLogAlert = YES;                                                  /**< 网络输出*/
static NSInteger TimeoutInterval = 15;                                          /**< 短链接失效时间*/

// 【长链接】
static BOOL isHeartbeatPacket = NO;                                            /**< 是否心跳包*/
static BOOL isSocketLogAlert = NO;                                             /**< 是否socket*/

#ifdef DEBUG
#define PDSocketDebug
#endif

#ifdef PDSocketDebug
#define PDSocketLog(format, ...) NSLog(format, ## __VA_ARGS__)
#else
#define PDSocketLog(format, ...)
#endif


///////////////////////////////////////////////////////////////////////接口链接//////////////////////////////////////////////////////////////////////

// 【其他】
static NSString *other_version = @"version";

// 【登录】
static NSString *login_smsCode = @"sendcode?phone=18768189840";
static NSString *login_login = @"login";
static NSString *login_getuserlevel = @"getuserlevel";
static NSString *login_logout = @"logout";
static NSString *login_forgetpwd = @"forgetpwd";
static NSString *login_checkcode = @"checkcode";
static NSString *login_changepwd = @"changepwd";
static NSString *login_getuserinfo = @"getuserinfo";
static NSString *login_changenickname = @"changenickname";
static NSString *login_changeheadimg = @"changeheadimg";
static NSString *login_sendcode = @"sendcode";
static NSString *login_register = @"register";
static NSString *login_checklogin = @"checklogin";
static NSString *login_checkregister = @"checkregister";
// 【首页】
static NSString *home_banner = @"getbanner";
static NSString *home_typeall = @"typeall";
static NSString *courselist = @"courselist";
static NSString *home_typelimit = @"typelimit";
static NSString *home_coursedesc = @"coursedesc";
static NSString *home_courselimit = @"courselimit";
static NSString *home_searchlist = @"searchlist";                   /**< 获取搜索列表历史*/
static NSString *home_searchDetail = @"search";                     /**< 获取搜索内容*/
static NSString *home_coursedetail = @"coursedetail";
static NSString *home_subexam = @"subexam";                         /**< 提交自测信息*/
// 【工具】
static NSString *teach_root = @"toolindex";
static NSString *teach_bopdesc = @"bopdesc";
static NSString *teach_bopdetail = @"bopdetail";
static NSString *teach_bopitemdetail = @"bopitemdetail";
static NSString *teach_materialtype = @"materialtype";                /**< 素材分类*/
static NSString *teach_materiallimit = @"materiallimit";              /**< 素材分页*/
static NSString *teach_watchlog = @"watchlog";
static NSString *teach_ispay = @"ispay";                              /**< 是否支付*/
static NSString *teach_ioscheckcoupon = @"ioscheckcoupon";            /**< 判断是否可用*/
static NSString *teach_iosaddpayorder = @"iosaddpayorder";            /**< 预支付*/
static NSString *teach_iosnotify = @"iosnotify";                      /**< 支付回调*/
static NSString *teach_courseprocess = @"courseprocess";              /**< 支付回调*/




// 【个人中心】
static NSString *center_about = @"about";
static NSString *center_mytools = @"mytools";
static NSString *center_mycourse = @"mycourse";
static NSString *center_getsigndata = @"getsigndata";
#endif /* URLConstance_h */
