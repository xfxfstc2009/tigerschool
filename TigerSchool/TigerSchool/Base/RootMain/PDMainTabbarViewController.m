//
//  PDMainTabbarViewController.m
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import "PDMainTabbarViewController.h"

@interface PDMainTabbarViewController ()

@end

@implementation PDMainTabbarViewController

+(instancetype)sharedController{
    static PDMainTabbarViewController *_sharedController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        NSArray *iconArr = @[@"icon_tabbar_class_hlt",@"icon_tabbar_tool_hlt",@"icon_tabbar_mine_hlt"];
        _sharedController = [[PDMainTabbarViewController alloc] initWithTabIconNames:iconArr];
        _sharedController.selectedColor = [UIColor clearColor];
        _sharedController.buttonsBackgroundColor = [UIColor whiteColor];
        _sharedController.separatorLineVisible = YES;
        _sharedController.separatorLineColor = [UIColor hexChangeFloat:@"DCDCDC"];
        _sharedController.selectionIndicatorHeight = .5f;

        // 1. 首页
        HomeRootViewController *homeRootViewController = [HomeRootViewController sharedCenterController];
        UINavigationController *homeRootNav = [[UINavigationController alloc] initWithRootViewController:homeRootViewController];;
        homeRootNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        homeRootNav.navigationBar.layer.shadowOpacity = .2f;
        homeRootNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [_sharedController setViewController:homeRootNav atIndex:0];

        // 2. 发现
        TeachRootViewController *findRootViewController = [[TeachRootViewController alloc]init];
        UINavigationController *findRootNav = [[UINavigationController alloc] initWithRootViewController:findRootViewController];;
        findRootNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        findRootNav.navigationBar.layer.shadowOpacity = .2f;
        findRootNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [_sharedController setViewController:findRootNav atIndex:1];

        // 消息
        CenterRootViewController *messageRootmainViewController = [CenterRootViewController sharedCenterController];
        UINavigationController *messageRootNav = [[UINavigationController alloc] initWithRootViewController:messageRootmainViewController];;
        messageRootNav.navigationBar.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
        messageRootNav.navigationBar.layer.shadowOpacity = .2f;
        messageRootNav.navigationBar.layer.shadowOffset = CGSizeMake(.2f, .2f);
        [_sharedController setViewController:messageRootNav atIndex:2];
        
        
        [_sharedController actionClickTabBarItemsBlock:^(NSInteger index) {
            if (index == 0){
                [TSMTAManager MTAevent:MTAType_Home_Root str:@""];
            } else if (index == 1){
                [TSMTAManager MTAevent:MTAType_Home_Teach str:@""];
            } else if (index == 2){
                [TSMTAManager MTAevent:MTAType_Home_Center str:@""];
            }
        }];
    });

    return _sharedController;
}

-(CGFloat)tabBarHeight{
    if ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO){
        return  49 + 34;
    } else {
        return  49 ;
    }
}

-(CGFloat)statusBarHeight{
    if ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO){
        return  44;
    } else {
        return  20;
    }
}

-(CGFloat)navBarHeight{
    if ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO){
        return  88;
    } else {
        return  64;
    }
}

#pragma mark - 审核标记
-(void)sendRequestToGetShenheWithBlock:(void(^)())block{
//    __weak typeof(self)weakSelf = self;
//    NSDictionary *params = @{@"version":[TSTool appVersion]};
//    [[NetworkAdapter sharedAdapter] fetchWithPath:appShenhe requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
//        if (!
//            weakSelf){
//            return ;
//        }
//        if (isSucceeded){
//            PDAppleShenheModel *shenheModel = (PDAppleShenheModel *)responseObject;
//            [AccountModel sharedAccountModel].lifeIsShenhe = shenheModel.enable;
//        #ifdef DEBUG            // 测试
//            [AccountModel sharedAccountModel].lifeIsShenhe = YES;
//        #else               // 线上
//        #endif
//        }
//        if (block){
//            block();
//        }
//    }];
}

-(void)shenheManager{
    
}



@end
