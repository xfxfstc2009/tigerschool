//
//  PDMainTabbarViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2017/5/16.
//  Copyright © 2017年 PandaOL. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ESTabBarController.h"
#import "HomeRootViewController.h"
#import "TeachRootViewController.h"
#import "CenterRootViewController.h"

@interface PDMainTabbarViewController : ESTabBarController

@property (nonatomic,assign)CGFloat tabBarHeight;
@property (nonatomic,assign)CGFloat statusBarHeight;
@property (nonatomic,assign)CGFloat navBarHeight;
+(instancetype)sharedController;

-(void)shenheManager;

@end
