//
//  TSLoginDataInfoModel.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/21.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSLoginDataInfoModel.h"

@implementation TSLoginDataInfoModel

-(NSString *)headimgurl{
    if (_headimgurl.length){
        return _headimgurl;
    } else if ([TSTool userDefaultGetWithKey:@"userAvatar"].length){
        return [TSTool userDefaultGetWithKey:@"userAvatar"];
    } else {
        return @"";
    }
}

@end
