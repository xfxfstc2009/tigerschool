//
//  TSLoginDataInfoModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/21.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@interface TSLoginDataInfoModel : FetchModel

@property (nonatomic,copy)NSString *accountid;
@property (nonatomic,copy)NSString *baiecode;
@property (nonatomic,copy)NSString *baiegroup;
@property (nonatomic,copy)NSString *baielevel;
@property (nonatomic,copy)NSString *baiename;
@property (nonatomic,copy)NSString *headimgurl;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)BOOL isleader;
@property (nonatomic,copy)NSString *nickname;
@property (nonatomic,copy)NSString *imaccount;
@property (nonatomic,copy)NSString *imtoken;
@property (nonatomic,assign)NSInteger level;
@property (nonatomic,copy)NSString *token;

@end
