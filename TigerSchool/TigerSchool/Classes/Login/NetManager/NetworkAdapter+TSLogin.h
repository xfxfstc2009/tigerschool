//
//  NetworkAdapter+TSLogin.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "NetworkAdapter.h"
#import "TSLoginDataInfoModel.h"

@interface NetworkAdapter (TSLogin)
#pragma mark - 登录
-(void)loginWithFetchManager:(NSString *)phone pwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 验证码登录
-(void)loginWithFetchSMSManager:(NSString *)phone smscode:(NSString *)smscode block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 下发验证码
-(void)loginWithSendSmsCode:(NSString *)phone block:(void(^)(BOOL successed,NSError *error))block;
#pragma mark - 获取用户信息
-(void)loginWithGetUserInfoBlock:(void(^)(TSLoginDataInfoModel *singleModel))block;
#pragma mark - 用户退出登录
-(void)loginWithLogoutMangaerBlock:(void(^)())block;
#pragma mark - 判断手机是否注册
-(void)loginWithCheckRegister:(NSString *)phone block:(void(^)(BOOL isSuccessed,NSError *error))block;
#pragma mark - 验证手机验证码
-(void)loginCheckCode:(NSString *)phone code:(NSString *)code block:(void(^)(BOOL isSuccessed))block;
#pragma mark - 用户注册
-(void)loginWithRegister:(NSString *)phone code:(NSString *)code nickName:(NSString *)nickName headerImg:(NSString *)headerImg pwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed,TSLoginDataInfoModel *loginModel))block;
#pragma mark - 修改用户头像地址
-(void)loginWithChangeUserAvatar:(NSString *)avatar block:(void(^)(NSString *avatarUrl))block;
#pragma mark - 修改用户昵称
-(void)loginWithChangeUserNick:(NSString *)nickName block:(void(^)(NSString *nick))block;
#pragma mark - 忘记密码
-(void)loginWithResetPwdPhone:(NSString *)phone code:(NSString *)code pwd:(NSString *)pwd block:(void(^)(BOOL isSucceeded))block;
#pragma mark - 更换密码
-(void)loginChangePwd:(NSString *)oldPassword newPassword:(NSString *)newPassword block:(void(^)(BOOL isSucceeded, id responseObject, NSError *error))block;
@end
