//
//  NetworkAdapter+TSLogin.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "NetworkAdapter+TSLogin.h"
#import "TSLoginModel.h"

@implementation NetworkAdapter (TSLogin)

#pragma mark - 登录
-(void)loginWithFetchManager:(NSString *)phone pwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed))block{
    NSString *error;
    if (!phone.length){
        error = @"请输入手机号";
    } else if (!pwd.length){
        error = @"请输入密码";
    }
    
    if (error.length){
        [StatusBarManager statusHiddenWithString:error];
        return;
    }
    
    NSString *md5Pwd = [MD5Manager md5:pwd];
//    if ([phone isEqualToString:@"10000000000"]){
//        md5Pwd = pwd;
//    }
    NSDictionary *params = @{@"phone":phone,@"logintype":@"2",@"pwd":md5Pwd};
    // 密码登录
    [self loginManagerWithParams:params phone:phone pwd:pwd block:block];
}

#pragma mark  验证码登录
-(void)loginWithFetchSMSManager:(NSString *)phone smscode:(NSString *)smscode block:(void(^)(BOOL isSuccessed))block{
    NSString *error;
    if (!phone.length){
        error = @"请输入手机号";
    } else if (!smscode.length){
        error = @"请输入验证码";
    }
    
    if (error.length){
        [StatusBarManager statusHiddenWithString:error];
        return;
    }
    
    NSDictionary *params = @{@"phone":phone,@"logintype":@"1",@"code":smscode};
    // 验证码登录
    [self loginManagerWithParams:params phone:phone pwd:@"" block:block];
}

-(void)loginManagerWithParams:(NSDictionary *)params phone:(NSString *)phone pwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed))block{
    NSString *userToken = [TSTool userDefaultGetWithKey:@"token"];
    __weak typeof(self)weakSelf = self;
    if (userToken.length && [AccountModel sharedAccountModel].has505 == NO){
        [self loginWithGetUserInfoBlock:^(TSLoginDataInfoModel *singleModel) {
            if (!weakSelf){
                return ;
            }

            [AccountModel sharedAccountModel].userInfoModel = singleModel;
            
            [AccountModel sharedAccountModel].level = singleModel.level;
            [AccountModel sharedAccountModel].token = userToken;
            [AccountModel sharedAccountModel].accountId = singleModel.accountid;
            
            // 保存动画图片
            if (singleModel.headimgurl.length){
                [TSTool userDefaulteWithKey:@"userAvatar" Obj:singleModel.headimgurl];
            }
            // 保存动画名称
            if (singleModel.nickname.length){
                [TSTool userDefaulteWithKey:@"nickname" Obj:singleModel.nickname];
            }
            
            // 登录IMToken
            [TSIMManager loginManagerWithToken:[AccountModel sharedAccountModel].userKeyInfo.imtoken];
            
            if (block){
                block(YES);
            }
        }];
        return;
    }


    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_login requestParams:params responseObjectClass:[TSLoginModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            TSLoginModel *loginModel = (TSLoginModel *)responseObject;
            [AccountModel sharedAccountModel].userKeyInfo = loginModel;
            
            [AccountModel sharedAccountModel].level = loginModel.level;
            [AccountModel sharedAccountModel].token = loginModel.token;
            [AccountModel sharedAccountModel].accountId = loginModel.ID;
            
            // 保存token
            [TSTool userDefaulteWithKey:@"token" Obj:loginModel.token];
            [TSTool userDefaulteWithKey:@"phone" Obj:phone];
            [TSTool userDefaulteWithKey:@"userid" Obj:loginModel.ID];
            if (pwd.length){
                [TSTool userDefaulteWithKey:@"pwd" Obj:pwd];
            }
            
            NSString *phone = [TSTool userDefaultGetWithKey:@"phone"];
            [AccountModel sharedAccountModel].phone = phone;
            // 保存app表示已经登录过
            [TSTool userDefaulteWithKey:User_First_Setup Obj:User_First_Setup];
            
            [strongSelf loginWithGetUserInfoBlock:^(TSLoginDataInfoModel *singleModel) {
                [AccountModel sharedAccountModel].userInfoModel = singleModel;
                
                // 保存动画图片
                if (singleModel.headimgurl.length){
                    [TSTool userDefaulteWithKey:@"userAvatar" Obj:singleModel.headimgurl];
                }
                // 保存动画名称
                if (singleModel.nickname.length){
                    [TSTool userDefaulteWithKey:@"nickname" Obj:singleModel.nickname];
                }
                
                // 登录IMToken
                [TSIMManager loginManagerWithToken:[AccountModel sharedAccountModel].userKeyInfo.imtoken];
                
                if (block){
                    block(YES);
                }
            }];
        } else {
            [StatusBarManager statusHiddenWithString:error.localizedDescription];
            return;
        }
    }];
}


#pragma mark - 获取用户等级
-(void)sendRequestToGetUserLevelInfo{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"":@""};
    [[NetworkAdapter sharedAdapter] fetchWithPath:login_getuserlevel requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            
        }
    }];
}

#pragma mark - 用户退出登录
-(void)loginWithLogoutMangaerBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"":@""};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_logout requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [AccountModel sharedAccountModel].level = -1;
            [AccountModel sharedAccountModel].token = @"";
            [AccountModel sharedAccountModel].accountId = @"";
            
            // 保存token
            [TSTool userDefaulteWithKey:@"token" Obj:@""];
            [TSTool userDefaulteWithKey:@"phone" Obj:@""];
            [TSTool userDefaulteWithKey:@"userid" Obj:@""];
            [TSTool userDefaulteWithKey:@"pwd" Obj:@""];
            [AccountModel sharedAccountModel].phone = @"";
            [StatusBarManager statusHiddenWithString:@"账户退出成功"];
            if (block){
                block();
            }
        } else {
            [StatusBarManager statusHiddenWithString:[NSString stringWithFormat:@"账户退出失败:%@",error.localizedDescription]];
            return;
        }
    }];
}

#pragma mark - 忘记密码
-(void)loginWithResetPwdPhone:(NSString *)phone code:(NSString *)code pwd:(NSString *)pwd block:(void(^)(BOOL isSucceeded))block{
    __weak typeof(self)weakSelf = self;
    NSString *md5Pwd = [MD5Manager md5:pwd];
    NSDictionary *params = @{@"phone":phone,@"code":code,@"pwd":md5Pwd};
    
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_forgetpwd requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [TSTool userDefaulteWithKey:@"pwd" Obj:md5Pwd];
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
    }];
}

#pragma mark - 获取当前版本号
-(void)otherWithGetCurrentVersion{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"platformid":@"2",@"channelcode":@"1000000"};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:other_version requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            
        }
    }];
}

#pragma mark - 判断手机是否注册
-(void)loginWithCheckRegister:(NSString *)phone block:(void(^)(BOOL isSuccessed,NSError *error))block{
    NSString *error = @"";
    if(![TSTool validateMobile:phone]){
        error = @"请确认手机号";
    }
    if (error.length){
        [StatusBarManager statusHiddenWithString:error];
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"phone":phone};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_checkregister requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES,nil);
            }
        } else {
            if ([error.localizedDescription isEqualToString:@"手机号已注册"]){
                if (block){
                    block(YES,error);
                }
            } else {
                if (block){
                    block(NO,error);
                }
                [StatusBarManager statusHiddenWithString:error.localizedDescription];
            }

            return;
        }
    }];
}

#pragma mark - 下发验证码
-(void)loginWithSendSmsCode:(NSString *)phone block:(void(^)(BOOL successed,NSError *error))block{
    __weak typeof(self)weakSelf = self;
    if (!phone.length){
        [StatusBarManager statusHiddenWithString:@"请输入手机号"];
        return;
    }
    
    NSString *sign = [NSString stringWithFormat:@"%@%@tigerschool",phone,[AccountModel sharedAccountModel].keychain];
    NSString *newSign = [sign md5String];
    NSDictionary *params = @{@"phone":phone,@"sign":newSign};
    
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_sendcode requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [StatusBarManager statusHiddenWithString:@"验证码下发成功"];
            if (block){
                block(YES,nil);
            }
        } else {
            [StatusBarManager statusHiddenWithString:[NSString stringWithFormat:@"验证码下发失败:%@",error.localizedDescription]];
            if (block){
                block(NO,error);
            }
        }
    }];
}

#pragma mark - 验证手机验证码
-(void)loginCheckCode:(NSString *)phone code:(NSString *)code block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    NSString *error = @"";
    if (!phone.length){
        error = @"请输入手机号";
    } else if (!code.length){
        error = @"请输入手机收到的验证码";
    }
    if (error.length){
        [StatusBarManager statusHiddenWithString:error];
        return;
    }
    
    NSDictionary *params = @{@"phone":phone,@"code":code};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_checkcode requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        } else {
            [StatusBarManager statusHiddenWithString:[NSString stringWithFormat:@"验证失败：%@",error.localizedDescription]];
            return;
        }
    }];
}

#pragma mark - 更换密码
-(void)loginChangePwd:(NSString *)oldPassword newPassword:(NSString *)newPassword block:(void(^)(BOOL isSucceeded, id responseObject, NSError *error))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"old":oldPassword,@"newpwd":newPassword};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_changepwd requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block(isSucceeded,responseObject,error);
        }
    }];
}

#pragma mark - 获取用户信息
-(void)loginWithGetUserInfoBlock:(void(^)(TSLoginDataInfoModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_getuserinfo requestParams:nil responseObjectClass:[TSLoginDataInfoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSLoginDataInfoModel *singleModle = (TSLoginDataInfoModel *)responseObject;
            
            
            block(singleModle);
        }
    }];
}

#pragma mark - 修改用户昵称
-(void)loginWithChangeUserNick:(NSString *)nickName block:(void(^)(NSString *nick))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"nickname":nickName};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_changenickname requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            [AccountModel sharedAccountModel].userInfoModel.nickname = nickName;
            [TSTool userDefaulteWithKey:@"nickname" Obj:nickName];
            if (block){
                block(nickName);
            }
        } else {
            [StatusBarManager statusHiddenWithString:error.localizedDescription];
            return;
        }
    }];
}

#pragma mark - 修改用户头像地址
-(void)loginWithChangeUserAvatar:(NSString *)avatar block:(void(^)(NSString *avatarUrl))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"headimg":avatar};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_changeheadimg requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            [TSTool userDefaulteWithKey:@"userAvatar" Obj:avatar];
            if (block){
                block(avatar);
            }
        } else {
            [StatusBarManager statusHiddenWithString:[NSString stringWithFormat:@"上传失败,%@",error.localizedDescription]];
            return;
        }
    }];
}



#pragma mark - 用户注册
-(void)loginWithRegister:(NSString *)phone code:(NSString *)code nickName:(NSString *)nickName headerImg:(NSString *)headerImg pwd:(NSString *)pwd block:(void(^)(BOOL isSuccessed,TSLoginDataInfoModel *loginModel))block{
    NSString *error = @"";
    if (!phone.length){
        error = @"请输入手机号";
    } else if (!code.length){
        error = @"请输入验证码";
    } else if (!nickName.length){
        error = @"请输入昵称";
    } else if (!pwd.length){
        error = @"请输入密码";
    }
    
    if (error.length){
        [StatusBarManager statusHiddenWithString:error];
        return;
    }
    __weak typeof(self)weakSelf = self;
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:phone forKey:@"phone"];
    [params setObject:code forKey:@"code"];
    [params setObject:nickName forKey:@"nickname"];
    NSString *md5Pwd = [MD5Manager md5:pwd];
    [params setObject:md5Pwd forKey:@"pwd"];
    if (headerImg.length){
        [params setObject:headerImg forKey:@"headimg"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_register requestParams:params responseObjectClass:[TSLoginDataInfoModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSLoginDataInfoModel *singleModel =(TSLoginDataInfoModel *)responseObject;
            [AccountModel sharedAccountModel].userInfoModel = singleModel;
            if (block){
                block(YES,singleModel);
            }
        } else {
            [StatusBarManager statusHiddenWithString:error.localizedDescription];
        }
    }];
}

#pragma mark - 判断是否有设备登录
-(void)loginWithChecklogin:(NSString *)phone block:(void(^)())block{
    NSDictionary *params = @{@"phone":phone};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:login_checklogin requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        
    }];
}



@end
