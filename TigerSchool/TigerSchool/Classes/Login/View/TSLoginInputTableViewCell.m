//
//  TSLoginInputTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSLoginInputTableViewCell.h"
#import "PDCountDownButton.h"

static char actionClickWithSMSBtnKey;
@interface TSLoginInputTableViewCell()<UITextFieldDelegate>
@property (nonatomic,strong)UILabel *titleLabel;                    /**< 标题*/
@property (nonatomic,strong)UIButton *actionShowPwdButton;          /**< 是否显示密码*/
@property (nonatomic,strong)UIView *lineView;                       /**< 下划线*/
@property (nonatomic,strong)UIButton *eyeBtn;                       /**< 眼睛按钮*/
@property (nonatomic,strong)PDCountDownButton *daojishiBtn;         /**< 倒计时按钮*/

@end

@implementation TSLoginInputTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [TSViewTool createLabelFont:@"副标题" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    // 2. 创建输入框
    self.inputTextField = [[UITextField alloc]init];
    self.inputTextField.backgroundColor = [UIColor clearColor];
    self.inputTextField.userInteractionEnabled = YES;
    self.inputTextField.delegate = self;
    self.inputTextField.font = [[UIFont systemFontOfCustomeSize:19.]boldFont];
    [self.inputTextField addTarget:self action:@selector(textFieldWillChange) forControlEvents:UIControlEventEditingChanged];
    [self addSubview:self.inputTextField];
    
    // 3. 倒计时
    __weak typeof(self)weakSelf = self;
    self.daojishiBtn = [[PDCountDownButton alloc]initWithFrame:CGRectMake(kScreenBounds.size.width - TSFloat(20) - TSFloat(90), 0, TSFloat(90), TSFloat(34)) daojishi:60 withBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;

        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithSMSBtnKey);
        if(block){
            block();
        }
    }];
    [self addSubview:self.daojishiBtn];
    
    // 4. line
    self.lineView = [[UIView alloc]init];
    self.lineView.backgroundColor = [UIColor hexChangeFloat:@"DCDCDC"];
    [self addSubview:self.lineView];
    
    // 5. 添加眼睛
    self.eyeBtn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.eyeBtn.backgroundColor = [UIColor clearColor];
    [self.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwdhidden"] forState:UIControlStateNormal];
    self.eyeBtn.selected = NO;
    [self.eyeBtn buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        self.eyeBtn.selected = !self.eyeBtn.selected;
        if (weakSelf.eyeBtn.selected){
            [weakSelf.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwdshow"] forState:UIControlStateNormal];
            self.inputTextField.secureTextEntry = NO;
        } else {
            [weakSelf.eyeBtn setImage:[UIImage imageNamed:@"icon_login_pwdhidden"] forState:UIControlStateNormal];
            self.inputTextField.secureTextEntry = YES;
        }
    }];
    [self addSubview:self.eyeBtn];
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}


-(void)setTransferType:(LoginInputType)transferType{
    _transferType = transferType;
    // title
    if (transferType == LoginInputTypePhone){
        self.titleLabel.text = @"手机号";
        self.inputTextField.placeholder = @"请输入手机号码";
        self.daojishiBtn.hidden = YES;
        self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    } else if (transferType == LoginInputTypeSMS){
        self.titleLabel.text = @"验证码";
        self.inputTextField.placeholder = @"请输入手机验证码";
        self.daojishiBtn.hidden = NO;
        self.inputTextField.keyboardType = UIKeyboardTypeNumberPad;
    } else if (transferType == LoginInputTypePwd){
        self.titleLabel.text = @"密码";
        self.inputTextField.placeholder = @"至少六位";
        self.daojishiBtn.hidden = YES;
        self.inputTextField.clearButtonMode = UITextFieldViewModeAlways;
        self.inputTextField.keyboardType = UIKeyboardTypeEmailAddress;
        self.inputTextField.secureTextEntry = YES;
    } else if (transferType == LoginInputTypeNick){
        self.titleLabel.text = @"昵称";
        self.inputTextField.placeholder = @"请输入昵称";
        self.daojishiBtn.hidden = YES;
        self.inputTextField.keyboardType = UIKeyboardTypeDefault;
        self.inputTextField.secureTextEntry = NO;
    }
    
    // 1.title
    self.titleLabel.frame = CGRectMake(TSFloat(40), 0, kScreenBounds.size.width - 2 * TSFloat(40), [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    // 2. input
    self.inputTextField.frame = CGRectMake(TSFloat(40), CGRectGetMaxY(self.titleLabel.frame) + TSFloat(11), kScreenBounds.size.width - 2 * TSFloat(40), TSFloat(44));
    
    // 3.line
    self.lineView.frame = CGRectMake(TSFloat(40), CGRectGetMaxY(self.inputTextField.frame) + TSFloat(3), kScreenBounds.size.width - 2 * TSFloat(40), .5f);
    
    // 4. 倒计时
    self.daojishiBtn.frame = CGRectMake(kScreenBounds.size.width - TSFloat(42) - self.daojishiBtn.size_width, self.transferCellHeight - TSFloat(10) - self.daojishiBtn.size_height, self.daojishiBtn.size_width, self.daojishiBtn.size_height);
    self.daojishiBtn.center_y = self.inputTextField.center_y;
    
    if (transferType == LoginInputTypePwd){
        self.inputTextField.size_width -= (TSFloat(44) + TSFloat(11));
        self.eyeBtn.frame = CGRectMake(CGRectGetMaxX(self.inputTextField.frame) + TSFloat(11), TSFloat(11), TSFloat(44), TSFloat(44));
        self.eyeBtn.center_y = self.inputTextField.center_y;
        self.eyeBtn.hidden = NO;
    } else {
        self.eyeBtn.hidden = YES;
        self.inputTextField.frame = CGRectMake(TSFloat(40), CGRectGetMaxY(self.titleLabel.frame) + TSFloat(11), kScreenBounds.size.width - 2 * TSFloat(40), TSFloat(44));
    }
}

#pragma mark - UITextFieldDelegate
-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    if ([string isEqualToString:@"\n"]){
        return YES;
    }
    NSString * toBeString = [textField.text stringByReplacingCharactersInRange:range withString:string]; //得到输入框的内容
    if (self.transferType == LoginInputTypePwd){                // 用户
        if (toBeString.length > 18){
            textField.text = [toBeString substringToIndex:18];
            return NO;
        }
//        if (toBeString.length >= 5 && toBeString.length <= 20){
//            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
//                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
//            }
//        } else {
//            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
//                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:NO];
//            }
//        }
    } else if (self.transferType == LoginInputTypePhone){           // 手机号
        if (toBeString.length > 11){
            textField.text = [toBeString substringToIndex:11];
            return NO;
        }
//        if (toBeString.length == 11 && [TSTool validateMobile:toBeString]){
//            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
//                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
//            }
//        } else {
//            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
//                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:NO];
//            }
//        }
    } else if (self.transferType == LoginInputTypeSMS){  // 验证码
        if (toBeString.length > 4){
            textField.text = [toBeString substringToIndex:4];
            return NO;
        }
//        else if (toBeString.length == 4){
//            if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
//                [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:YES];
//            }
//        }
    } else if (self.transferType == LoginInputTypeNick){
        if (toBeString.length > 15){
            textField.text = [toBeString substringToIndex:15];
            return NO;
        }
    }
    return YES;
}

-(void)textFieldWillChange{
    BOOL success = NO;
    if (self.transferType == LoginInputTypePhone){           // 【手机号】
        if ((self.inputTextField.text.length == 11 && ([TSTool validateMobile:self.inputTextField.text])) || [self.inputTextField.text isEqualToString:@"10000000000"]){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == LoginInputTypePwd){              // 【密码】
        if (self.inputTextField.text.length >= 6 && self.inputTextField.text.length <= 18){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == LoginInputTypeSMS){          // 【验证码】
        if (self.inputTextField.text.length == 4){
            success = YES;
        } else {
            success = NO;
        }
    } else if (self.transferType == LoginInputTypeNick){        // 【昵称】
        if (self.inputTextField.text.length >= 2){
            success = YES;
        } else {
            success = NO;
        }
    }
    if (self.delegate && [self.delegate respondsToSelector:@selector(inputTextConfirm:textField:status:)]){
        [self.delegate inputTextConfirm:self.transferType textField:self.inputTextField status:success];
    }
}


-(void)startTimer{
    [self.daojishiBtn startTimer];
}

-(void)actionClickWithSMSBtn:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithSMSBtnKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"副标题"]];
    cellHeight += TSFloat(11);
    cellHeight += TSFloat(44);
    cellHeight += TSFloat(11);
    return cellHeight;
}


@end
