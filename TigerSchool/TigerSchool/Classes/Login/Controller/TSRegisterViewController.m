//
//  TSRegisterViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSRegisterViewController.h"
#import "TSLoginInputTableViewCell.h"
#import "TSRegisterUserInfoViewController.h"
#import "NetworkAdapter+TSLogin.h"
#import "TSCenterSettingViewController.h"


@interface TSRegisterViewController ()<UITableViewDelegate,UITableViewDataSource,TSLoginInputTableViewCellDelegate> {
    UITextField *phoneTextField;
    UITextField *pwdTextField;
    UITextField *smsTextField;
    
    BOOL phoneTextFieldEnable;
    BOOL pwdTextFieldEnable;
    BOOL smsTextFieldEnable;
    
    GWButtonTableViewCell *phoneCell1;
    GWButtonTableViewCell *phoneCell2;
    
    NSString *tempInputPhoneNumber;
}
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UITableView *registerTableView;
@property (nonatomic,strong)UITableView *registerTableView2;
@property (nonatomic,strong)NSArray *registerArr1;              /**< 页面1的注册*/
@property (nonatomic,strong)NSArray *registerArr2;              /**< 页面1的注册*/
@end

@implementation TSRegisterViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (phoneTextField){
        [phoneTextField becomeFirstResponder];
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createScrollView];
    [self createTableView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
    self.leftButton = [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_back"] barHltImage:[UIImage imageNamed:@"icon_main_back"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.mainScrollView.contentOffset.x == 0){
            [strongSelf.navigationController popToRootViewControllerAnimated:YES];
        } else {
            strongSelf->smsTextField.text = @"";
            strongSelf->pwdTextField.text = @"";
            [strongSelf.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
        }
    }];
    if(self.transferRegisterType == RegisterTypeRegister){
        self.barMainTitle = @"注册";
    } else if (self.transferRegisterType == RegisterTypeReset){
        self.barMainTitle = @"重置密码";
    }
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.registerArr1 = @[@[@"手机号"],@[@"下一步"]];
    self.registerArr2 = @[@[@"已发送"],@[@"验证码"],@[@"密码"],@[@"注册"]];
}

#pragma mark - createScrollView
-(void)createScrollView{
    if (!self.mainScrollView){
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            
        }];
        self.mainScrollView.scrollEnabled = NO;
        self.mainScrollView.frame = self.view.bounds;
        self.mainScrollView.contentSize = CGSizeMake(2 * kScreenBounds.size.width, kScreenBounds.size.height);
        [self.view addSubview:self.mainScrollView];
    }
}

#pragma mark - createTableView
-(void)createTableView{
    if (!self.registerTableView){
        self.registerTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.registerTableView.dataSource = self;
        self.registerTableView.delegate = self;
        [self.mainScrollView addSubview:self.registerTableView];
    }
    if (!self.registerTableView2){
        self.registerTableView2 = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.registerTableView2.orgin_x = kScreenBounds.size.width;
        self.registerTableView2.dataSource = self;
        self.registerTableView2.delegate = self;
        [self.mainScrollView addSubview:self.registerTableView2];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.registerTableView){
        return self.registerArr1.count;
    } else if (tableView == self.registerTableView2){
        return self.registerArr2.count;
    }
    return 0;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *tempArr;
    if (tableView == self.registerTableView){
        tempArr = self.registerArr1;
    } else if (tableView == self.registerTableView2){
        tempArr = self.registerArr2;
    }
    NSArray *sectionOfArr = [tempArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (tableView == self.registerTableView){
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"手机号" sourceArr:self.registerArr1]){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            TSLoginInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[TSLoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.delegate = self;
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferType = LoginInputTypePhone;
            phoneTextField = cellWithRowOne.inputTextField;
            return cellWithRowOne;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"下一步" sourceArr:self.registerArr1]){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferTitle = @"下一步";
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            __weak typeof(self)weakSelf = self;
            [cellWithRowTwo buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [TSMTAManager MTAevent:MTAType_Login_Register_Reg_Next str:@""];          // 埋点
                [strongSelf actionClickCheckPhoneNumber:strongSelf->phoneTextField.text];
            }];
            phoneCell1 = cellWithRowTwo;
            return cellWithRowTwo;
        }
    } else {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"已发送" sourceArr:self.registerArr2]){
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            GWNormalTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFour.transferCellHeight = cellHeight;
            NSString *newStr = [tempInputPhoneNumber substringFromIndex:(tempInputPhoneNumber.length - 4)];
            cellWithRowFour.transferTitle = [NSString stringWithFormat:@"已向手机尾号为%@的号码发送验证码",newStr];
            cellWithRowFour.titleLabel.font = [UIFont systemFontOfCustomeSize:14.];
            cellWithRowFour.titleLabel.textColor = [UIColor hexChangeFloat:@"999999"];
            cellWithRowFour.titleLabel.orgin_x = TSFloat(39);
            
            return cellWithRowFour;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"验证码" sourceArr:self.registerArr2] ||
                   indexPath.section == [self cellIndexPathSectionWithcellData:@"密码" sourceArr:self.registerArr2]){
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            TSLoginInputTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[TSLoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                cellWithRowFiv.delegate = self;
                cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            __weak typeof(self)weakSelf = self;
            [cellWithRowFiv actionClickWithSMSBtn:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf sendRequestToGetSMSCode:strongSelf->tempInputPhoneNumber];
            }];
            if (indexPath.section == [self cellIndexPathSectionWithcellData:@"验证码" sourceArr:self.registerArr2] ){
                cellWithRowFiv.transferType = LoginInputTypeSMS;
                smsTextField = cellWithRowFiv.inputTextField;
            } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"密码" sourceArr:self.registerArr2]){
                cellWithRowFiv.transferType = LoginInputTypePwd;
                pwdTextField = cellWithRowFiv.inputTextField;
            }
            return cellWithRowFiv;
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"注册" sourceArr:self.registerArr2]){
            static NSString *cellIdentifyWithRowSex = @"cellIdentifyWithRowSex";
            GWButtonTableViewCell *cellWithRowSex = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSex];
            if (!cellWithRowSex){
                cellWithRowSex = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSex];
                cellWithRowSex.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSex.transferCellHeight = cellHeight;
            if(self.transferRegisterType == RegisterTypeRegister){
                cellWithRowSex.transferTitle = @"注册";
            } else {
                cellWithRowSex.transferTitle = @"重置密码";
            }
            
            phoneCell2 = cellWithRowSex;
            __weak typeof(self)weakSelf = self;
            [cellWithRowSex buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                if (strongSelf.transferRegisterType == RegisterTypeRegister){
                    [strongSelf actionCheckToRegister2];
                } else if (strongSelf.transferRegisterType == RegisterTypeReset){
                    [strongSelf actionCheckToResetPwd];
                }
            }];
            return cellWithRowSex;
        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (tableView == self.registerTableView){
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"手机号" sourceArr:self.registerArr1]){
            return [TSLoginInputTableViewCell calculationCellHeight];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"下一步" sourceArr:self.registerArr1]){
            return [GWButtonTableViewCell calculationCellHeight];
        }
    } else {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"已发送" sourceArr:self.registerArr2]){
            return [GWNormalTableViewCell calculationCellHeight];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"验证码" sourceArr:self.registerArr2] ||
                   indexPath.section == [self cellIndexPathSectionWithcellData:@"密码" sourceArr:self.registerArr2]){
            return [TSLoginInputTableViewCell calculationCellHeight];
        } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"注册" sourceArr:self.registerArr2]){
            return [GWButtonTableViewCell calculationCellHeight];
        }
    }
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if(tableView == self.registerTableView){
        if (section == 0){
            return TSFloat(80);
        } else {
            return TSFloat(60);
        }
    } else {
        if (section == 0){
            return TSFloat(20);
        } else if (section == [self cellIndexPathSectionWithcellData:@"注册" sourceArr:self.registerArr2]){
            return TSFloat(60);
        } else {
            return TSFloat(11);
        }
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)inputTextConfirm:(LoginInputType)type textField:(UITextField *)textField status:(BOOL)status{
    if (type == LoginInputTypePhone && textField == phoneTextField){
        if (status){
            phoneTextFieldEnable = YES;
        } else {
            phoneTextFieldEnable = NO;
        }
        if (phoneTextFieldEnable){
            [phoneCell1 setButtonStatus:YES];
        } else {
            [phoneCell1 setButtonStatus:NO];
        }
    } else if (type == LoginInputTypeSMS && textField == smsTextField){
        if (status){
            smsTextFieldEnable = YES;
        } else {
            smsTextFieldEnable = NO;
        }
        if (pwdTextFieldEnable && smsTextFieldEnable){
            [phoneCell2 setButtonStatus:YES];
        } else {
            [phoneCell2 setButtonStatus:NO];
        }
    } else if (type == LoginInputTypePwd && textField == pwdTextField){
        if (status){
            pwdTextFieldEnable = YES;
        } else {
            pwdTextFieldEnable = NO;
        }
        if (pwdTextFieldEnable && smsTextFieldEnable){
            [phoneCell2 setButtonStatus:YES];
        } else {
            [phoneCell2 setButtonStatus:NO];
        }
    }
}

#pragma mark - 确认手机号
-(void)actionClickCheckPhoneNumber:(NSString *)phoneNumber{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginWithCheckRegister:phoneNumber block:^(BOOL isSuccessed, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.transferRegisterType == RegisterTypeRegister){
            if (isSuccessed && (error == nil)){
                // 1. 记录当前输入的手机号
                strongSelf-> tempInputPhoneNumber = phoneNumber;
                // 2. 更新第二个界面的已发送信息
                NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"已发送" sourceArr:strongSelf.registerArr2];
                NSInteger section = [strongSelf cellIndexPathRowWithcellData:@"已发送" sourceArr:strongSelf.registerArr2];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
                [strongSelf.registerTableView2 reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
                // 3. 发送验证码
                [strongSelf sendRequestToGetSMSCode:phoneNumber];
                
                // 4.键盘释放
                if ([strongSelf->phoneTextField isFirstResponder]){
                    [strongSelf->phoneTextField resignFirstResponder];
                }
                
                [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * 1, 0) animated:YES];
            } else {
                [StatusBarManager statusHiddenWithString:[NSString stringWithFormat:@"%@",error.localizedDescription]];
                return;
            }
        } else if (strongSelf.transferRegisterType == RegisterTypeReset){
            if (isSuccessed && (error == nil)){
                [StatusBarManager statusHiddenWithString:[NSString stringWithFormat:@"%@",@"手机号码未注册"]];
                return;
            } else {
                // 1. 记录当前输入的手机号
                strongSelf-> tempInputPhoneNumber = phoneNumber;
                // 2. 更新第二个界面的已发送信息
                NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"已发送" sourceArr:strongSelf.registerArr2];
                NSInteger section = [strongSelf cellIndexPathRowWithcellData:@"已发送" sourceArr:strongSelf.registerArr2];
                NSIndexPath *indexPath = [NSIndexPath indexPathForRow:row inSection:section];
                [strongSelf.registerTableView2 reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
                
                // 3. 发送验证码
                [strongSelf sendRequestToGetSMSCode:phoneNumber];
                
                // 4.键盘释放
                if ([strongSelf->phoneTextField isFirstResponder]){
                    [strongSelf->phoneTextField resignFirstResponder];
                }
                
                [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * 1, 0) animated:YES];
            }
        }
        
    }];
}

#pragma mark - 发送验证码
-(void)sendRequestToGetSMSCode:(NSString *)code{
    __weak typeof(self)weakSelf = self;
    if (self.transferRegisterType == RegisterTypeRegister){
        [TSMTAManager MTAevent:MTAType_Login_Register_Reg_YZM str:code];          // 埋点
    } else if (self.transferRegisterType == RegisterTypeReset){
        [TSMTAManager MTAevent:MTAType_Login_ResetPwdGetSMS str:code];          // 埋点
    }
    
    [[NetworkAdapter sharedAdapter] loginWithSendSmsCode:code block:^(BOOL successed, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            NSInteger smsRow = [strongSelf cellIndexPathRowWithcellData:@"验证码" sourceArr:strongSelf.registerArr2];
            NSInteger smsSection = [strongSelf cellIndexPathSectionWithcellData:@"验证码" sourceArr:strongSelf.registerArr2];
            TSLoginInputTableViewCell *cell = (TSLoginInputTableViewCell *)[strongSelf.registerTableView2 cellForRowAtIndexPath:[NSIndexPath indexPathForRow:smsRow inSection:smsSection]];
            [cell startTimer];
        } 
    }];
}

#pragma mark - 注册
-(void)actionCheckToRegister2{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginCheckCode:phoneTextField.text code:smsTextField.text block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        TSRegisterUserInfoViewController *registerViewController = [[TSRegisterUserInfoViewController alloc]init];
        registerViewController.transferSMSCode = strongSelf->smsTextField.text;
        registerViewController.transferPwd = strongSelf->pwdTextField.text;
        registerViewController.transferPhoneNumber = strongSelf ->tempInputPhoneNumber;
        [strongSelf.navigationController pushViewController:registerViewController animated:YES];
    }];
}

#pragma mark - 忘记密码
-(void)actionCheckToResetPwd{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginWithResetPwdPhone:phoneTextField.text code:smsTextField.text pwd:pwdTextField.text block:^(BOOL isSucceeded) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 1. 记录当前输入的手机号
        strongSelf-> tempInputPhoneNumber = strongSelf->phoneTextField.text;
        
        if (isSucceeded){
            [TSTool userDefaulteWithKey:@"pwd" Obj:pwdTextField.text];
            [[UIAlertView alertViewWithTitle:@"修改密码成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                for (UIViewController *viewController in strongSelf.navigationController.childViewControllers){
                    if ([viewController isKindOfClass:[LoginViewController class]]){
                        [strongSelf.navigationController popToViewController:viewController animated:YES];
                    }
                    if ([viewController isKindOfClass:[TSCenterSettingViewController class]]){
                        [strongSelf.navigationController popToViewController:viewController animated:YES];
                    }
                }
            }]show];
        }
    }];
}
@end
