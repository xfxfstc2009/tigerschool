//
//  TSRegisterUserInfoViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSRegisterUserInfoViewController.h"
#import "TSLoginInputTableViewCell.h"
#import "NetworkAdapter+TSLogin.h"

@interface TSRegisterUserInfoViewController ()<UITableViewDelegate,UITableViewDataSource,TSLoginInputTableViewCellDelegate>{
    UIImage *avatarImg;
    UITextField *nickTextField;
    GWButtonTableViewCell *tempButtonCell;
    BOOL LoginInputTypeNickEnable;
    NSString *avatarfileUrl;
}
@property (nonatomic,strong)UITableView *userInfoTableView;
@property (nonatomic,strong)NSArray *userInfoArr;
@end

@implementation TSRegisterUserInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.userInfoArr = @[@[@"头像"],@[@"昵称",@"信息"],@[@"注册"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.userInfoTableView){
        self.userInfoTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.userInfoTableView.dataSource = self;
        self.userInfoTableView.delegate = self;
        [self.view addSubview:self.userInfoTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.userInfoArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.userInfoArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.userInfoArr]){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        TSAvatarTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if(!cellWithRowOne){
            cellWithRowOne = [[TSAvatarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.transferCellHeight = cellHeight;
        cellWithRowOne.transferAvatar = avatarImg?avatarImg: [UIImage imageNamed:@"img_login_userAvatar_nor"];
        cellWithRowOne.fixedLabel.text = @"上传头像";
        cellWithRowOne.fixedLabel.orgin_x = TSFloat(40);
        return cellWithRowOne;
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"昵称" sourceArr:self.userInfoArr]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            TSLoginInputTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[TSLoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowTwo.delegate = self;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferType = LoginInputTypeNick;
            nickTextField = cellWithRowTwo.inputTextField;
            return cellWithRowTwo;
        } else {
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            GWNormalTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowThr.transferCellHeight = cellHeight;
            cellWithRowThr.transferTitle = @"昵称请控制在4-30个字符，支持中英文、数字、横线和下划线";
            cellWithRowThr.textAlignmentCenter = YES;
            cellWithRowThr.titleLabel.font = [UIFont systemFontOfCustomeSize:11.];
            cellWithRowThr.titleLabel.textColor = [UIColor hexChangeFloat:@"666666"];
            return cellWithRowThr;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"注册" sourceArr:self.userInfoArr]){
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        GWButtonTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowFour.buttonType = GWButtonTableViewCellTypeNormal;
        cellWithRowFour.transferCellHeight = cellHeight;
        cellWithRowFour.transferTitle = @"注册";
        tempButtonCell = cellWithRowFour;
        [cellWithRowFour setButtonStatus:NO];
        __weak typeof(self)weakSelf = self;
        [cellWithRowFour buttonClickManager:^{
            if(!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToRegister];
        }];
        return cellWithRowFour;
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.userInfoArr]){
        GWAssetsLibraryViewController *assetViewController = [[GWAssetsLibraryViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [assetViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf->avatarImg = [selectedImgArr lastObject];
            [strongSelf.userInfoTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            // 上传头像
            [strongSelf uploadAvatar];
        }];
        [self.navigationController pushViewController:assetViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.userInfoArr]){
        return [TSAvatarTableViewCell calculationCellHeight];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"昵称" sourceArr:self.userInfoArr]){
        if (indexPath.row == 0){
            return [TSLoginInputTableViewCell calculationCellHeight];
        } else {
            return [GWNormalTableViewCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"注册" sourceArr:self.userInfoArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    }
    return 44;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.userInfoArr]){
        return TSFloat(3);
    } else if (section == [self cellIndexPathSectionWithcellData:@"昵称" sourceArr:self.userInfoArr]){
        return TSFloat(30);
    } else if (section == [self cellIndexPathSectionWithcellData:@"注册" sourceArr:self.userInfoArr]){
        return TSFloat(20);
    }
    return 44;
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)inputTextConfirm:(LoginInputType)type textField:(UITextField *)textField status:(BOOL)status{
    if (type == LoginInputTypeNick && textField == nickTextField){
        LoginInputTypeNickEnable = status;
        if(LoginInputTypeNickEnable){
            [tempButtonCell setButtonStatus:YES];
        } else {
            [tempButtonCell setButtonStatus:NO];
        }
    }
}


#pragma mark - register
-(void)sendRequestToRegister{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]loginWithRegister:self.transferPhoneNumber code:self.transferSMSCode nickName:nickTextField.text headerImg:avatarfileUrl pwd:self.transferPwd block:^(BOOL isSuccessed, TSLoginDataInfoModel *loginModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [strongSelf registSuccess];
        }
    }];
}

-(void)registSuccess{
    __weak typeof(self)weakSelf = self;
    [[UIAlertView alertViewWithTitle:@"注册成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[NetworkAdapter sharedAdapter] loginWithFetchManager:strongSelf.transferPhoneNumber pwd:strongSelf.transferPwd block:^(BOOL isSuccessed) {
            [[HomeRootViewController sharedCenterController] homeRootInterfaceManager];
            [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
        }];
    }]show];
}

#pragma mark - 上传头像
-(void)uploadAvatar{
    OSSSingleFileModel *fileModel = [[OSSSingleFileModel alloc]init];
    fileModel.objcImg = avatarImg;
    NSMutableArray<OSSSingleFileModel> *avararMutableArr = [[NSMutableArray<OSSSingleFileModel> alloc]init];
    [avararMutableArr addObject:fileModel];
    
    __weak typeof(self)weakSelf = self;
    [StatusBarManager statusShowWithString:@"正在上传头像"];
    [[AliOSSManager sharedOssManager] uploadFileWithImgArr:avararMutableArr uploadType:ImageUsingTypeUser compressionRatio:.7f block:^(NSArray *fileUrlArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [StatusBarManager statusHiddenWithString:@"头像上传成功"];
        strongSelf->avatarfileUrl = [fileUrlArr lastObject];
        
        // 修改按钮状态
        if(strongSelf->LoginInputTypeNickEnable && strongSelf->avatarfileUrl.length){
            [strongSelf->tempButtonCell setButtonStatus:YES];
        } else {
            [strongSelf->tempButtonCell setButtonStatus:NO];
        }
    }];
}

@end
