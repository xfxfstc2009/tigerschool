//
//  LoginViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"

@interface LoginViewController : AbstractViewController

-(void)loginSuccessWithManagerBlock:(void(^)(BOOL isSuccessed))block;

@end
