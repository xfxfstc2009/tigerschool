//
//  TSRegisterUserInfoViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"

@interface TSRegisterUserInfoViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferPhoneNumber;        /**< 传递过来的手机号*/
@property (nonatomic,copy)NSString *transferSMSCode;            /**< 传递过来的手机验证码*/
@property (nonatomic,copy)NSString *transferPwd;               /**< 传递过来的密码*/


@end
