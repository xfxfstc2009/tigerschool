//
//  TSLoginChangePwdViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/25.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSLoginChangePwdViewController.h"
#import "NetworkAdapter+TSLogin.h"
#import "TSRegisterViewController.h"

@interface TSLoginChangePwdViewController ()<UITableViewDelegate,UITableViewDataSource>
{
    UITextField *inputTextField1;
    UITextField *inputTextField2;
    UITextField *inputTextField3;
}
@property (nonatomic,strong)UITableView *changeTableView;
@property (nonatomic,strong)NSArray *changeArr;
@property (nonatomic,strong)UIButton *resetButton;
@end

@implementation TSLoginChangePwdViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self createResetButton];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"修改密码";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.changeArr = @[@[@"原密码",@"新密码",@"确认密码"],@[@"保存"]];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.changeTableView){
        self.changeTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.changeTableView.dataSource = self;
        self.changeTableView.delegate = self;
        [self.view addSubview:self.changeTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.changeArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.changeArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        GWInputTextFieldTableViewCell *cellWithRowone = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowone){
            cellWithRowone = [[GWInputTextFieldTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowone.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowone.transferCellHeight = cellHeight;
        cellWithRowone.inputTextField.clearButtonMode = UITextFieldViewModeAlways;
        cellWithRowone.inputTextField.keyboardType = UIKeyboardTypeEmailAddress;
        cellWithRowone.inputTextField.secureTextEntry = YES;

        __weak typeof(self)weakSelf = self;
        if (indexPath.row == 0){
            inputTextField1 = cellWithRowone.inputTextField;
            cellWithRowone.transferTitle = @"原密码";
            cellWithRowone.transferPlaceholeder = @"请输入原密码";
        } else if (indexPath.row == 1){
            inputTextField2 = cellWithRowone.inputTextField;
            cellWithRowone.transferTitle = @"新密码";
            cellWithRowone.transferPlaceholeder = @"请输入新密码";
        } else if (indexPath.row == 2){
            inputTextField3 = cellWithRowone.inputTextField;
            cellWithRowone.transferTitle = @"确认密码";
            cellWithRowone.transferPlaceholeder = @"请输入确认新密码";
        }
        [cellWithRowone textFieldDidChangeBlock:^(NSString *info) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf verificationWithInfoManager];
        }];
        return cellWithRowone;
    } else {
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        GWButtonTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferCellHeight = cellHeight;
        cellWithRowTwo.transferTitle = @"确认修改";
        __weak typeof(self)weakSelf = self;
        [cellWithRowTwo buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf actionClickToChangePwd];
        }];
        return cellWithRowTwo;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return TSFloat(11);
    } else {
        return TSFloat(45);
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return TSFloat(60);
    } else {
        return 44;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        if ([indexPath row] == [[self.changeArr objectAtIndex:indexPath.section] count] - 1) {
            [cell addSeparatorLineWithType:SeparatorTypeBottom];
        } else {
            [cell addSeparatorLineWithType:SeparatorTypeMiddle];
        }
    }
}

-(void)verificationWithInfoManager{
    NSInteger section = [self cellIndexPathSectionWithcellData:@"保存" sourceArr:self.changeArr];
    NSInteger row = [self cellIndexPathRowWithcellData:@"保存" sourceArr:self.changeArr];
    GWButtonTableViewCell *btnCell = [self.changeTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:row inSection:section]];;
    if (inputTextField1.text.length && inputTextField2.text.length &&inputTextField3.text && ([inputTextField2.text isEqualToString:inputTextField3.text])){
        [btnCell setButtonStatus:YES];
    } else {
        [btnCell setButtonStatus:NO];
    }
}

-(void)createResetButton{
    self.resetButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.resetButton setTitle:@"忘记密码？" forState:UIControlStateNormal];
    self.resetButton.titleLabel.font = [UIFont systemFontOfCustomeSize:15.];
    [self.resetButton setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.resetButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        TSRegisterViewController *resetPwdViewController = [[TSRegisterViewController alloc]init];
        resetPwdViewController.transferRegisterType = RegisterTypeReset;
        [strongSelf.navigationController pushViewController:resetPwdViewController animated:YES];
    }];
    self.resetButton.frame = CGRectMake((kScreenBounds.size.width - TSFloat(200)) / 2., kScreenBounds.size.height - 88 - 40 - 60, TSFloat(200), 44);
    [self.view addSubview:self.resetButton];
}

-(void)actionClickToChangePwd{
    NSString *error = @"";
    if (![inputTextField2.text isEqualToString:inputTextField3.text]){
        error = @"2次输入的心密码不统一，请核对后再试";
    }
    if (error.length){
        [StatusBarManager statusHiddenWithString:error];
        return;
    }
    
    __weak typeof(self)weakSelf = self;
    NSString *oldPwd = [MD5Manager md5:inputTextField1.text];
    NSString *newPwd = [MD5Manager md5:inputTextField2.text];
    
    [[NetworkAdapter sharedAdapter] loginChangePwd:oldPwd newPassword:newPwd block:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            [strongSelf.navigationController popViewControllerAnimated:YES];
            // 保存pwd
            [TSTool userDefaulteWithKey:@"pwd" Obj:strongSelf->inputTextField2.text];
            
            [StatusBarManager statusHiddenWithString:@"修改成功"];
        } else {
            [StatusBarManager statusHiddenWithString:[NSString stringWithFormat:@"%@",error.localizedDescription]];
        }
    }];
}



@end
