//
//  TSRegisterViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,RegisterType){
    RegisterTypeRegister,               /**< 注册*/
    RegisterTypeReset,                  /**< 重置密码*/
};

@interface TSRegisterViewController : AbstractViewController

@property (nonatomic,assign)RegisterType transferRegisterType;

@end
