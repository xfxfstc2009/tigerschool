//
//  TSLoginSuccessAnimationViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/21.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSLoginSuccessAnimationViewController.h"
#import "TNSexyImageUploadProgress.h"
@interface TSLoginSuccessAnimationViewController ()
@property (nonatomic,strong)TSImageView *launchImageView;
@property (nonatomic,strong)TSImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong,nullable)TNSexyImageUploadProgress *imageUploadProgress;             /**< progress*/
@property (nonatomic,strong)UITapGestureRecognizer *tapGesture;
@property (nonatomic,assign)BOOL hasTap;
@end

@implementation TSLoginSuccessAnimationViewController

-(void)dealloc{
    NSLog(@"123");
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.view addSubview:self.launchImageView];
    [self.view bringSubviewToFront:self.launchImageView];
    self.view.backgroundColor = BACKGROUND_VIEW_COLOR;
    [self createView];
}

#pragma mark - createView
-(void)createView{
    // 4. 创建手势
    self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(animationStopTapManager)];
    [self.view addGestureRecognizer:self.tapGesture];
    
    [self showAnimation];
}


-(void)showAnimation{
    __weak typeof(self)weakSelf = self;
    if ([TSTool userDefaultGetWithKey:@"userAvatar"].length){
        if (![AccountModel sharedAccountModel].userInfoModel){
            [AccountModel sharedAccountModel].userInfoModel = [[TSLoginDataInfoModel alloc]init];
        }

        [AccountModel sharedAccountModel].userInfoModel.headimgurl = [TSTool userDefaultGetWithKey:@"userAvatar"];
        [self.avatarImgView uploadImageWithURL:[TSTool userDefaultGetWithKey:@"userAvatar"] placeholder:nil callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.view addSubview:self.avatarImgView];
            [strongSelf.view addSubview:strongSelf.titleLabel];
            strongSelf.avatarImgView.image = [TSTool imageByComposingImage:image withMaskImage:[UIImage imageNamed:@"round"]];
            [strongSelf showAnimation1WithImg:image];
        }];
    } else {
        [self.view addSubview:self.avatarImgView];
        [self.view addSubview:self.titleLabel];
        [self showAnimation1WithImg:[UIImage imageNamed:@"icon_music_avatar_nor"]];
    }
}

-(void)showAnimation1WithImg:(UIImage *)image{
    if (!self.imageUploadProgress){
        self.imageUploadProgress = [[TNSexyImageUploadProgress alloc] init];
    }
    
    self.imageUploadProgress.radius = TSFloat(70);
    self.imageUploadProgress.progressBorderThickness = 3;
    self.imageUploadProgress.trackColor = [UIColor blackColor];
    self.imageUploadProgress.progressColor = [UIColor blackColor];
    self.imageUploadProgress.imageToUpload = image;
    [self.imageUploadProgress showWithView:self.view];
    
    [self performSelector:@selector(showSliderWithAnimation) withObject:nil afterDelay:.5f];
}

#pragma mark - 显示
-(void)showSliderWithAnimation{
    [self performSelector:@selector(loginSuccessAnimationWithProView:) withObject:self.imageUploadProgress afterDelay:.3f];
}

#pragma mark - Animation Login
-(void)loginSuccessAnimationWithProView:(TNSexyImageUploadProgress *)progressView{
    
    // 3. 创建一个副本图像
    UIImageView *animationImageView = [[UIImageView alloc]init];
    UIImage *newRoundImg = self.avatarImgView.image;
    animationImageView.image = newRoundImg;
    animationImageView.frame = progressView.imageView.frame;
    [self.view addSubview:animationImageView];

    [progressView outroAnimation];
    [UIView animateWithDuration:.9  delay:.5 options:UIViewAnimationOptionBeginFromCurrentState animations:^{
        animationImageView.frame = self.avatarImgView.frame;
    } completion:^(BOOL finished) {
        self.avatarImgView.hidden = NO;
        [animationImageView removeFromSuperview];
        [self nickLabelAnimation];
    }];
}

-(void)nickLabelAnimation{
    [UIView animateWithDuration:.3f animations:^{
        self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatarImgView.frame) + TSFloat(40), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.titleLabel.alpha = 1;
    } completion:^(BOOL finished) {
        if (self.hasTap){
            return ;
        }
        [self animationDismissManager];
    }];
}

-(void)animationStopTapManager{
    self.avatarImgView.transform = CGAffineTransformMakeScale(.7f, .7f);
    self.hasTap = YES;
    [self animationDismissManager];
    
}

-(void)animationDismissManager{
    UIWindow *keywindow = [[UIApplication sharedApplication] keyWindow];
    [UIView mdDeflateTransitionFromView:self.view toView:keywindow.rootViewController.view originalPoint:self.avatarImgView.center duration:.9f completion:^{
        [[HomeRootViewController sharedCenterController]homeRootInterfaceManager];
        [self.view removeFromSuperview];
    }];
}



-(TSImageView *)avatarImgView{
    if (!_avatarImgView){
        _avatarImgView = [[TSImageView alloc]init];
        _avatarImgView.frame = CGRectMake((kScreenBounds.size.width - TSFloat(81)) / 2., TSFloat(153), TSFloat(81), TSFloat(81));
        _avatarImgView.image = [UIImage imageNamed:@"icon_music_avatar_nor"];
        _avatarImgView.hidden = YES;
    }
    return _avatarImgView;
}

-(UILabel *)titleLabel{
    if (!_titleLabel){
        // title
        _titleLabel = [TSViewTool createLabelFont:@"大标题" textColor:@"黑"];
        _titleLabel.text = [NSString stringWithFormat:@"欢迎回来 %@",@"亲爱的同学"];
        _titleLabel.font = [UIFont systemFontOfCustomeSize:20.];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.avatarImgView.frame) + TSFloat(100), kScreenBounds.size.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
        _titleLabel.alpha = 0;
        if ([TSTool userDefaultGetWithKey:@"nickname"].length){
            _titleLabel.text = [NSString stringWithFormat:@"欢迎回来 %@",[TSTool userDefaultGetWithKey:@"nickname"]];
            
            if (![AccountModel sharedAccountModel].userInfoModel){
                [AccountModel sharedAccountModel].userInfoModel = [[TSLoginDataInfoModel alloc]init];
            }
            [AccountModel sharedAccountModel].userInfoModel.nickname = [TSTool userDefaultGetWithKey:@"nickname"];
        }
    }
    return _titleLabel;
}

@end
