//
//  LoginViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "LoginViewController.h"

#import "NetworkAdapter+TSLogin.h"
#import "TSLoginInputTableViewCell.h"

#import "TSNewFeatureViewController.h"
#import "TSRegisterViewController.h"

static char loginSuccessWithManagerBlockKey;
@interface LoginViewController ()<UITableViewDelegate,UITableViewDataSource,TSLoginInputTableViewCellDelegate,TSNetworkAdapterLoginErrorDelegate> {
    UITextField *phoneTextField1;
    UITextField *phoneTextField2;
    UITextField *passwordTextField;
    UITextField *smsTextField;
    GWButtonTableViewCell *pwdLoginButtonCell;
    GWButtonTableViewCell *smsLoginButtonCell;
    BOOL phoneTextField1Enable;
    BOOL phoneTextField2Enable;
    BOOL passwordTextFieldEnable;
    BOOL smsTextFieldEnable;
}
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)UIScrollView *backgroundScrollView;
@property (nonatomic,strong)UITableView *pwdLoginTableView;     /**< 密码登录*/
@property (nonatomic,strong)UITableView *smsLoginTableView;
@property (nonatomic,strong)NSArray *pwdArr;
@property (nonatomic,strong)NSArray *smsArr;

@end

@implementation LoginViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createScrollView];
    [NetworkAdapter sharedAdapter].loginErrorDelegate = self;
}

#pragma mark - pageSetting
-(void)pageSetting{
    __weak typeof(self)weakSelf = self;
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"密码登录",@"验证码登录"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.backgroundScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
    }];
    self.segmentList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    [self.segmentList setTitleFont:[[UIFont systemFontOfCustomeSize:20.]boldFont] forState:UIControlStateNormal];
    self.segmentList.frame = CGRectMake(TSFloat(30) / 2., 0, kScreenBounds.size.width - TSFloat(50), TSFloat(50));
    [self.view addSubview:self.segmentList];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.pwdArr = @[@[@"手机号"],@[@"密码",@"忘记密码"],@[@"按钮"]];
    self.smsArr = @[@[@"手机号"],@[@"验证码"],@[@"按钮"]];
}

#pragma mark - createScrollView
-(void)createScrollView{
    __weak typeof(self)weakSelf = self;
    self.backgroundScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger indexPage = scrollView.contentOffset.x / kScreenBounds.size.width;
        [strongSelf.segmentList setSelectedButtonIndex:indexPage animated:YES];
    }];
    self.backgroundScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, self.view.size_height - CGRectGetMaxY(self.segmentList.frame));
    self.backgroundScrollView.scrollEnabled = NO;
    self.backgroundScrollView.contentSize = CGSizeMake(2 * kScreenBounds.size.width, self.backgroundScrollView.size_height);
    [self.view addSubview:self.backgroundScrollView];
    
    // 创建tableView
    [self createTableView];
}

#pragma mark - createTableView
-(void)createTableView {
    if (!self.pwdLoginTableView){
        self.pwdLoginTableView = [TSViewTool gwCreateTableViewRect:self.backgroundScrollView.bounds];
        self.pwdLoginTableView.dataSource = self;
        self.pwdLoginTableView.delegate = self;
        [self.backgroundScrollView addSubview:self.pwdLoginTableView];
    }
    if (!self.smsLoginTableView){
        self.smsLoginTableView = [TSViewTool gwCreateTableViewRect:self.backgroundScrollView.bounds];
        self.smsLoginTableView.orgin_x = kScreenBounds.size.width;
        self.smsLoginTableView.dataSource = self;
        self.smsLoginTableView.delegate = self;
        [self.backgroundScrollView addSubview:self.smsLoginTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    if (tableView == self.pwdLoginTableView){
        return self.pwdArr.count;
    } else {
        return self.smsArr.count;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *infoArr;
    if (tableView == self.pwdLoginTableView){
        infoArr = self.pwdArr;
    } else {
        infoArr = self.smsArr;
    }
    NSArray *sectionOfArr = [infoArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    
    if(tableView == self.pwdLoginTableView){
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.pwdArr]){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowThr.transferCellHeight = cellHeight;
            cellWithRowThr.transferTitle = @"登 录";
            [cellWithRowThr setButtonStatus:NO];
            pwdLoginButtonCell = cellWithRowThr;
            __weak typeof(self)weakSelf = self;
            [cellWithRowThr buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [TSMTAManager MTAevent:MTAType_Login_PWDLogin str:@""];          // 埋点
                [strongSelf loginManagerWithPhoneNumber:strongSelf->phoneTextField1.text pwd:strongSelf->passwordTextField.text];
            }];
            return cellWithRowThr;
        } else {
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"忘记密码" sourceArr:self.pwdArr]){
                static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
                GWNormalTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
                if (!cellWithRowFiv){
                    cellWithRowFiv = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                    cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                __weak typeof(self)weakSelf = self;
                [cellWithRowFiv actionClickWithBlock:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [TSMTAManager MTAevent:MTAType_Login_ResetPwd str:@""];          // 埋点
                    TSRegisterViewController *resetVC = [[TSRegisterViewController alloc]init];
                    resetVC.transferRegisterType = RegisterTypeReset;
                    [strongSelf.navigationController pushViewController:resetVC animated:YES];
                }];
                
                cellWithRowFiv.transferCellHeight = cellHeight;
                cellWithRowFiv.transferTitle = @"忘记密码？";
                cellWithRowFiv.titleButton.hidden = NO;
                cellWithRowFiv.titleLabel.textColor = [UIColor hexChangeFloat:@"666666"];
                cellWithRowFiv.titleLabel.orgin_x = TSFloat(42);
                return cellWithRowFiv;
            } else {
                static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
                TSLoginInputTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
                if (!cellWithRowFour){
                    cellWithRowFour = [[TSLoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                    cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
                    cellWithRowFour.delegate = self;
                }
                cellWithRowFour.transferCellHeight = cellHeight;
                if (indexPath.section == [self cellIndexPathSectionWithcellData:@"手机号" sourceArr:self.pwdArr]){
                    cellWithRowFour.transferType = LoginInputTypePhone;
                    phoneTextField1 = cellWithRowFour.inputTextField;
                } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"密码" sourceArr:self.pwdArr]){
                    cellWithRowFour.transferType = LoginInputTypePwd;
                    passwordTextField = cellWithRowFour.inputTextField;
                }
                
                return cellWithRowFour;
            }
        }
    } else {
        if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:self.smsArr]){
            static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
            GWButtonTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
            if (!cellWithRowFiv){
                cellWithRowFiv = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            }
            cellWithRowFiv.transferCellHeight = cellHeight;
            cellWithRowFiv.transferTitle = @"登 录";
            [cellWithRowFiv setButtonStatus:NO];
            smsLoginButtonCell = cellWithRowFiv;
            __weak typeof(self)weakSelf = self;
            [cellWithRowFiv buttonClickManager:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [TSMTAManager MTAevent:MTAType_Login_SMSLogin str:@""];          // 埋点
                [strongSelf loginManagerWithSMSPhoneNumber:strongSelf->phoneTextField2.text smsCode:strongSelf->smsTextField.text];
            }];
            return cellWithRowFiv;
        } else {
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            TSLoginInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[TSLoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.delegate = self;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            if (indexPath.section == [self cellIndexPathSectionWithcellData:@"手机号" sourceArr:self.smsArr]){
                cellWithRowOne.transferType = LoginInputTypePhone;
                phoneTextField2 = cellWithRowOne.inputTextField;
            } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"验证码" sourceArr:self.smsArr]){
                cellWithRowOne.transferType = LoginInputTypeSMS;
                smsTextField = cellWithRowOne.inputTextField;
                __weak typeof(self)weakSelf = self;
                [cellWithRowOne actionClickWithSMSBtn:^{
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    [strongSelf sendRequestToGetSMSCode:strongSelf->phoneTextField2.text];
                }];
            }
            return cellWithRowOne;
        }
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSArray *infoArr;
    if (tableView == self.pwdLoginTableView){
        infoArr = self.pwdArr;
    } else if (tableView == self.smsLoginTableView){
        infoArr = self.smsArr;
    }
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"按钮" sourceArr:infoArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"按钮" sourceArr:infoArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else {
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"忘记密码" sourceArr:self.pwdArr]){
            return [GWNormalTableViewCell calculationCellHeight];
        }
        return [TSLoginInputTableViewCell calculationCellHeight];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return TSFloat(30);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)inputTextConfirm:(LoginInputType)type textField:(UITextField *)textField status:(BOOL)status{
    if (type == LoginInputTypePhone && textField == phoneTextField1){
        if (status == YES){
            phoneTextField1Enable = YES;
        } else {                // 不通过
            phoneTextField1Enable = NO;
        }
        
        if (phoneTextField1Enable && passwordTextFieldEnable){
            [pwdLoginButtonCell setButtonStatus:YES];
        } else {
            [pwdLoginButtonCell setButtonStatus:NO];
        }
        
    } else if (type == LoginInputTypePwd && textField == passwordTextField){
        if (status == YES){
            passwordTextFieldEnable = YES;
        } else {
            passwordTextFieldEnable = NO;
        }
        
        if (phoneTextField1Enable && passwordTextFieldEnable){
            [pwdLoginButtonCell setButtonStatus:YES];
        } else {
            [pwdLoginButtonCell setButtonStatus:NO];
        }
        
    } else if (type == LoginInputTypeSMS && textField == smsTextField){
        if (status == YES){
            smsTextFieldEnable = YES;
        } else {
            smsTextFieldEnable = NO;
        }
        
        if (phoneTextField2Enable && smsTextFieldEnable){
            [smsLoginButtonCell setButtonStatus:YES];
        } else {
            [smsLoginButtonCell setButtonStatus:NO];
        }
    } else if (type == LoginInputTypePhone && textField == phoneTextField2){
        if (status == YES){
            phoneTextField2Enable = YES;
        } else {
            phoneTextField2Enable = NO;
        }
        
        if (phoneTextField2Enable && smsTextFieldEnable){
            [smsLoginButtonCell setButtonStatus:YES];
        } else {
            [smsLoginButtonCell setButtonStatus:NO];
        }
    }
}



#pragma mark - Interface
#pragma mark 登录
-(void)loginManagerWithPhoneNumber:(NSString *)phone pwd:(NSString *)password {
    [[NetworkAdapter sharedAdapter] loginWithFetchManager:phone pwd:password block:^(BOOL isSuccessed) {
        if (isSuccessed){
            void(^block)(BOOL isSuccessed) = objc_getAssociatedObject(self, &loginSuccessWithManagerBlockKey);
            if (block){
                block(YES);
            }
            [self dismissViewControllerAnimated:YES completion:^{
         
            }];
        }
    }];
}

#pragma mark 验证码登录
-(void)loginManagerWithSMSPhoneNumber:(NSString *)phone smsCode:(NSString *)smsCode {
    [[NetworkAdapter sharedAdapter]loginWithFetchSMSManager:phone smscode:smsCode block:^(BOOL isSuccessed) {
        if (isSuccessed){
            [self dismissViewControllerAnimated:YES completion:^{
                void(^block)(BOOL isSuccessed) = objc_getAssociatedObject(self, &loginSuccessWithManagerBlockKey);
                if (block){
                    block(YES);
                }
            }];
        }
    }];
}


-(void)loginSuccessWithManagerBlock:(void(^)(BOOL isSuccessed))block{
    objc_setAssociatedObject(self, &loginSuccessWithManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 发送验证码
-(void)sendRequestToGetSMSCode:(NSString *)code{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginWithSendSmsCode:code block:^(BOOL successed, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            NSInteger smsRow = [strongSelf cellIndexPathRowWithcellData:@"验证码" sourceArr:self.smsArr];
            NSInteger smsSection = [strongSelf cellIndexPathSectionWithcellData:@"验证码" sourceArr:self.smsArr];
            TSLoginInputTableViewCell *cell = (TSLoginInputTableViewCell *)[strongSelf.smsLoginTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:smsRow inSection:smsSection]];
            [cell startTimer];
        }
    }];
}

-(void)tigerschoolNotLoginNeedSMSLogin{
    // 1.segment移动
    [self.segmentList setSelectedButtonIndex:1 animated:YES];
    // 2. scrollview移动
    [self.backgroundScrollView setContentOffset:CGPointMake(kScreenBounds.size.width, 0) animated:YES];
    // 3.修改手机号
    phoneTextField2.text = phoneTextField1.text;
    phoneTextField1.text = @"";
    smsTextField.text = @"";
    passwordTextField.text = @"";
    [pwdLoginButtonCell setButtonStatus:NO];
    // 4.
    phoneTextField2Enable = phoneTextField1Enable;
    [self sendRequestToGetSMSCode:phoneTextField2.text];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if ([phoneTextField1 isFirstResponder]){
        [phoneTextField1 resignFirstResponder];
    } else if ([phoneTextField2 isFirstResponder]){
        [phoneTextField2 resignFirstResponder];
    } else if ([passwordTextField isFirstResponder]){
        [passwordTextField resignFirstResponder];
    } else if ([smsTextField isFirstResponder]){
        [smsTextField resignFirstResponder];
    }
}

@end
