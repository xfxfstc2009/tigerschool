//
//  TSCenterMyToolTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/25.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSMaterialSegmentModel.h"
@interface TSCenterMyToolTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)TSImageView *imgView;
@property (nonatomic,strong)TSMaterialSegmentModel *transferSingleModel;

@end
