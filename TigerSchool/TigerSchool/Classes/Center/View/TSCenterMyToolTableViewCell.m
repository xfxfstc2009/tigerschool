//
//  TSCenterMyToolTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/25.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSCenterMyToolTableViewCell.h"


@interface TSCenterMyToolTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation TSCenterMyToolTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[TSImageView alloc]init];
    self.imgView.backgroundColor = UURandomColor;
    [self addSubview:self.imgView];
    
    self.titleLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"黑"];
    self.titleLabel.font = [UIFont systemFontOfCustomeSize:15.];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
}

-(void)setTransferSingleModel:(TSMaterialSegmentModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    // 1. img
    self.imgView.frame = CGRectMake(TSFloat(16), TSFloat(20), TSFloat(110), TSFloat(77));
    [self.imgView uploadImageWithURL:transferSingleModel.img placeholder:nil callback:NULL];
    
    // 2. title
    self.titleLabel.text = transferSingleModel.name;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - CGRectGetMaxX(self.imgView.frame) - 2 * TSFloat(11), CGFLOAT_MAX)];
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.imgView.frame) + TSFloat(11), 0, kScreenBounds.size.width - CGRectGetMaxX(self.imgView.frame) - 2 * TSFloat(11), 2 * [NSString contentofHeightWithFont:self.titleLabel.font]);
    } else {
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.imgView.frame) + TSFloat(11), 0, kScreenBounds.size.width - CGRectGetMaxX(self.imgView.frame) - 2 * TSFloat(11), 1 * [NSString contentofHeightWithFont:self.titleLabel.font]);
    }
    self.titleLabel.center_y = [TSCenterMyToolTableViewCell calculationCellHeight] / 2.;
}

+(CGFloat)calculationCellHeight{
    return TSFloat(117);
}

@end
