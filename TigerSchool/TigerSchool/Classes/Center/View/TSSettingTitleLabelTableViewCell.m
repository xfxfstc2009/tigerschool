//
//  TSSettingTitleLabelTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/25.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSSettingTitleLabelTableViewCell.h"

@interface TSSettingTitleLabelTableViewCell();
@property (nonatomic,strong)UILabel *titleLabel;

@end
@implementation TSSettingTitleLabelTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 2. 创建标题
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [[UIFont systemFontOfCustomeSize:23.] boldFont];
    self.titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    self.titleLabel.frame = CGRectMake(TSFloat(18), TSFloat(11), kScreenBounds.size.width - 2 *TSFloat(18), [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.text = @"设置";
    [self addSubview:self.titleLabel];
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += TSFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[[UIFont systemFontOfCustomeSize:23.] boldFont]];
    cellHeight += TSFloat(11);
    return cellHeight;
}
@end
