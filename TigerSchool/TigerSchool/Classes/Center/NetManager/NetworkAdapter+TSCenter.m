//
//  NetworkAdapter+TSCenter.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "NetworkAdapter+TSCenter.h"

@implementation NetworkAdapter (TSCenter)

// 1, 获取关于我们
-(void)centerWithAboutMeBlock:(void(^)(TSCenterSignCheckModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:center_about requestParams:nil responseObjectClass:[TSCenterSignCheckModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
        if (isSucceeded){
            TSCenterSignCheckModel *singleModel = (TSCenterSignCheckModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}

// 获取我的工具
-(void)centerWithMyToosWithBlock:(void(^)(BOOL isSuccessed,NSError *errorInfo,TSMaterialSegmentListModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:center_mytools requestParams:nil responseObjectClass:[TSMaterialSegmentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if(!weakSelf){
            return ;
        }
        if(isSucceeded){
            TSMaterialSegmentListModel *singleModel = (TSMaterialSegmentListModel *)responseObject;
            if (block){
                block(YES,error,singleModel);
            }
        } else {
            if (block){
                block(NO,error,nil);
            }
        }
    }];
}

// 获取我的观看历史
-(void)centerWithWatchHistoryWithPage:(NSInteger)page withBlcok:(void(^)(BOOL isSuccessed,NSError *errorInfo,TSHomeLiveListModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"type":@"2",@"page":@(page),@"limit":@"20"};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:center_mycourse requestParams:params responseObjectClass:[TSHomeLiveListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeLiveListModel *singleModle = (TSHomeLiveListModel *)responseObject;
            if (block){
                block(YES,nil,singleModle);
            }
        } else {
            if(block){
                block(NO,error,nil);
            }
        }
    }];
}

// 获取我的购买课程
-(void)centerWithWatchBuyClassWithPage:(NSInteger)page withBlcok:(void(^)(BOOL isSuccessed,NSError *errorInfo,TSHomeLiveListModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    
    NSDictionary *params = @{@"type":@"1",@"page":@(page),@"limit":@"20"};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:center_mycourse requestParams:params responseObjectClass:[TSHomeLiveListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeLiveListModel *singleModle = (TSHomeLiveListModel *)responseObject;
            if (block){
                block(YES,nil,singleModle);
            }
        } else {
            if(block){
                block(NO,error,nil);
            }
        }
    }];
}

#pragma mark - 获取签名
-(void)centerGetSignCheckWithBlcok:(void(^)(TSCenterSignCheckModel *singleModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:center_getsigndata requestParams:nil responseObjectClass:[TSCenterSignCheckModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }

        if (isSucceeded){
            TSCenterSignCheckModel *singleModel = (TSCenterSignCheckModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}



@end

