//
//  NetworkAdapter+TSCenter.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "NetworkAdapter.h"
#import "TSMaterialSegmentModel.h"
#import "TSHomeLiveModel.h"
#import "TSCenterSignCheckModel.h"

@interface NetworkAdapter (TSCenter)

// 1, 获取关于我们
-(void)centerWithAboutMeBlock:(void(^)(TSCenterSignCheckModel *singleModel))block;
// 获取我的工具
-(void)centerWithMyToosWithBlock:(void(^)(BOOL isSuccessed,NSError *errorInfo,TSMaterialSegmentListModel *singleModel))block;
// 获取我的观看历史
-(void)centerWithWatchHistoryWithPage:(NSInteger)page withBlcok:(void(^)(BOOL isSuccessed,NSError *errorInfo,TSHomeLiveListModel *singleModel))block;
// 获取我的购买课程
-(void)centerWithWatchBuyClassWithPage:(NSInteger)page withBlcok:(void(^)(BOOL isSuccessed,NSError *errorInfo,TSHomeLiveListModel *singleModel))block;
#pragma mark - 获取签名
-(void)centerGetSignCheckWithBlcok:(void(^)(TSCenterSignCheckModel *singleModel))block;

@end
