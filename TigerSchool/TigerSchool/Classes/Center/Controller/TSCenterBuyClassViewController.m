//
//  TSCenterBuyClassViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSCenterBuyClassViewController.h"
#import "NetworkAdapter+TSCenter.h"
#import "TSHomeTeachSingleCell.h"
#import "TSHomeClassDetailViewController.h"

@interface TSCenterBuyClassViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *classTableView;
@property (nonatomic,strong)NSMutableArray *classMutableArr;
@end

@implementation TSCenterBuyClassViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}


- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfoWithPage:1];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"已购课程";
}

#pragma mark - history
-(void)arrayWithInit{
    self.classMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.classTableView){
        self.classTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.classTableView.dataSource = self;
        self.classTableView.delegate = self;
        [self.view addSubview:self.classTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.classMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
    TSHomeTeachSingleCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
    if (!cellWithRowThr){
        cellWithRowThr = [[TSHomeTeachSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    TSHomeLiveModel *singleModel = [self.classMutableArr objectAtIndex:indexPath.row];
    cellWithRowThr.transferHomeLiveModel = singleModel;
    
    return cellWithRowThr;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TSHomeClassDetailViewController *homeClassDetailViewController = [[TSHomeClassDetailViewController alloc]init];
    TSHomeLiveModel *singleModel = [self.classMutableArr objectAtIndex:indexPath.row];
    homeClassDetailViewController.transferCourseId = singleModel.ID;
    homeClassDetailViewController.transferPrice = singleModel.price;
    homeClassDetailViewController.hasCancelPanDismiss = YES;
    [self.navigationController pushViewController:homeClassDetailViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [TSHomeTeachSingleCell calculationCellHeight];
}



#pragma mark - Interface
-(void)sendRequestToGetInfoWithPage:(NSInteger)page{
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] centerWithWatchBuyClassWithPage:page withBlcok:^(BOOL isSuccessed, NSError *errorInfo, TSHomeLiveListModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [strongSelf.classMutableArr addObjectsFromArray:singleModel.items];
            [strongSelf.classTableView reloadData];
            if (strongSelf.classMutableArr.count){
                [strongSelf.classTableView dismissPrompt];
            } else {
                [strongSelf.classTableView showPrompt:@"当前没有数据哦~" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:^{
                    
                }];
            }
        } else {
            [strongSelf.classTableView showPrompt:errorInfo.localizedDescription withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:^{
                
            }];
        }
    }];
}

@end
