//
//  TSCenterSettingViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSCenterSettingViewController.h"
#import "NetworkAdapter+TSLogin.h"
#import "TSSettingTitleLabelTableViewCell.h"
#import "NetworkAdapter+TSLogin.h"
#import "TSCenterSettingChangeViewController.h"
#import "TSLoginChangePwdViewController.h"
#import "VPImageCropperViewController.h"

@interface TSCenterSettingViewController ()<UITableViewDelegate,UITableViewDataSource,VPImageCropperDelegate>{
    UIImage *avatar;
    GWButtonTableViewCell *tempCellWithRowThr;
}
@property (nonatomic,strong)UITableView *settingTableView;
@property (nonatomic,strong)NSArray *settingArr;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation TSCenterSettingViewController

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if ([[AccountModel sharedAccountModel] hasLoggedIn]){
        if (tempCellWithRowThr){
            [tempCellWithRowThr setButtonStatus:YES];
        }
    } else {
        if (tempCellWithRowThr){
            [tempCellWithRowThr setButtonStatus:NO];
        }
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self arrayWithInit];
    [self createTableView];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.settingArr = @[@[@"设置"],@[@"头像",@"昵称",@"密码修改",@"账户ID"],@[@"退出登录"]];
}

#pragma mark - createView
-(void)createTableView{
    if (!self.settingTableView){
        self.settingTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.settingTableView.dataSource = self;
        self.settingTableView.delegate = self;
        [self.view addSubview:self.settingTableView];
    }
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.settingArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.settingArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"设置" sourceArr:self.settingArr]){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        TSSettingTitleLabelTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if(!cellWithRowZero){
            cellWithRowZero = [[TSSettingTitleLabelTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        
        return cellWithRowZero;
    }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.settingArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.settingArr]){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            TSAvatarTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if(!cellWithRowOne){
                cellWithRowOne = [[TSAvatarTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.fixedLabel.text = @"头像";
            cellWithRowOne.fixedLabel.orgin_x = TSFloat(18);
            
            cellWithRowOne.transferAvatarUrl = [AccountModel sharedAccountModel].userInfoModel.headimgurl;
            
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if(!cellWithRowTwo){
                cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.titleLabel.font = [UIFont systemFontOfCustomeSize:15.];
            cellWithRowTwo.titleLabel.orgin_x = TSFloat(18);
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"昵称" sourceArr:self.settingArr]){
                cellWithRowTwo.transferTitle = @"昵称";
                cellWithRowTwo.transferDesc = [AccountModel sharedAccountModel].userInfoModel.nickname;
                cellWithRowTwo.transferHasArrow = YES;
                cellWithRowTwo.dymicLabel.font = [UIFont systemFontOfCustomeSize:15.];
                cellWithRowTwo.dymicLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"密码修改" sourceArr:self.settingArr]){
                cellWithRowTwo.transferTitle = @"密码修改";
                cellWithRowTwo.transferHasArrow = YES;
                cellWithRowTwo.dymicLabel.font = [UIFont systemFontOfCustomeSize:15.];
                cellWithRowTwo.dymicLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"账户ID" sourceArr:self.settingArr]){
                cellWithRowTwo.transferTitle = @"账户ID";
                cellWithRowTwo.transferDesc = [AccountModel sharedAccountModel].accountId;
                cellWithRowTwo.transferHasArrow = NO;
                cellWithRowTwo.dymicLabel.font = [UIFont systemFontOfCustomeSize:14.];
                cellWithRowTwo.dymicLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
            }
            
            return cellWithRowTwo;
        }
    } else {
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if(!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        }
        cellWithRowThr.buttonType = GWButtonTableViewCellTypeLayer;
        tempCellWithRowThr = cellWithRowThr;
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"退出登录";
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [[UIAlertView alertViewWithTitle:@"提示" message:@"是否确认退出" buttonTitles:@[@"确认退出",@"不退出"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (buttonIndex == 0){
                    [strongSelf logOutManager];
                }
            }]show];
        }];
        return cellWithRowThr;
    }
}
- (void)imageCropper:(VPImageCropperViewController *)cropperViewController didFinished:(UIImage *)editedImage{
    __weak typeof(self)weakSelf = self;
    [cropperViewController dismissViewControllerAnimated:YES completion:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->avatar = editedImage;
        //     上传头像
        [strongSelf sendRequestToUpdateAvatar];
    }];
}
- (void)imageCropperDidCancel:(VPImageCropperViewController *)cropperViewController{
    [cropperViewController dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row == [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.settingArr] &&indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.settingArr]){
        GWAssetsLibraryViewController *assetViewController = [[GWAssetsLibraryViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [assetViewController selectImageArrayFromImagePickerWithMaxSelected:1 andBlock:^(NSArray *selectedImgArr) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                VPImageCropperViewController *vc = [[VPImageCropperViewController alloc]initWithImage:[selectedImgArr lastObject] cropFrame:CGRectMake(0, (kScreenBounds.size.height - kScreenBounds.size.width) / 2., kScreenBounds.size.width, kScreenBounds.size.width) limitScaleRatio:3];
                vc.delegate = strongSelf;
                [strongSelf presentViewController:vc animated:YES completion:NULL];
            });
        }];
        [self.navigationController pushViewController:assetViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"昵称" sourceArr:self.settingArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"昵称" sourceArr:self.settingArr]){
        TSCenterSettingChangeViewController *centerChangeViewController = [[TSCenterSettingChangeViewController alloc]init];
        __weak typeof(self)weakSelf = self;
        [centerChangeViewController nickNameChangedBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger section = [strongSelf cellIndexPathSectionWithcellData:@"昵称" sourceArr:strongSelf.settingArr];
            NSInteger row = [strongSelf cellIndexPathRowWithcellData:@"昵称" sourceArr:strongSelf.settingArr];
            [strongSelf.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationNone];
        }];
        [self.navigationController pushViewController:centerChangeViewController animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"密码修改" sourceArr:self.settingArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"密码修改" sourceArr:self.settingArr]){
        TSLoginChangePwdViewController *changePwdViewController = [[TSLoginChangePwdViewController alloc]init];
        [self.navigationController pushViewController:changePwdViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"设置" sourceArr:self.settingArr]){
        return [TSSettingTitleLabelTableViewCell calculationCellHeight];
    }else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.settingArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.settingArr]){
            return [TSAvatarTableViewCell calculationCellHeight];
        } else {
            return [GWNormalTableViewCell calculationCellHeight];
        }
    }
        return 50;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == 0){
        return TSFloat(10);
    } else if (section == 1){
        return TSFloat(0);
    } else {
        return TSFloat(52);
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SeparatorType separatorType = SeparatorTypeMiddle;
    if (indexPath.section > 0 && indexPath.section != [self cellIndexPathSectionWithcellData:@"退出登录" sourceArr:self.settingArr]){
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeMiddle;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
        } else if ([indexPath row] == [[self.settingArr objectAtIndex:indexPath.section] count] - 1) {
            separatorType = SeparatorTypeBottom;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
        } else {
            separatorType = SeparatorTypeMiddle;
            [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
        }
        if ([[self.settingArr objectAtIndex:indexPath.section] count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
    }
}

#pragma mark - 接口
-(void)sendRequestToUpdateAvatar{
    __weak typeof(self)weakSelf = self;
    
    OSSSingleFileModel *fileModel = [[OSSSingleFileModel alloc]init];
    fileModel.objcImg = avatar;
    NSMutableArray *avatarMutableArr = [NSMutableArray array];
    [avatarMutableArr addObject:fileModel];
    [StatusBarManager statusShowWithString:@"正在上传头像"];
    [[AliOSSManager sharedOssManager] uploadFileWithImgArr:[avatarMutableArr copy] uploadType:ImageUsingTypeUser compressionRatio:.7f block:^(NSArray *fileUrlArr) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSString *avatarUrl = [fileUrlArr lastObject];
        [strongSelf uploadRequestToUploadInterface:avatarUrl];
    }];
}

-(void)uploadRequestToUploadInterface:(NSString *)avatar{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginWithChangeUserAvatar:avatar block:^(NSString *avatarUrl) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [StatusBarManager statusHiddenWithString:@"头像上传成功"];
        [AccountModel sharedAccountModel].userInfoModel.headimgurl = avatarUrl;
        NSInteger section = [self cellIndexPathSectionWithcellData:@"头像" sourceArr:self.settingArr];
        NSInteger row = [self cellIndexPathRowWithcellData:@"头像" sourceArr:self.settingArr];
        [strongSelf.settingTableView reloadRowsAtIndexPaths:@[[NSIndexPath indexPathForRow:row inSection:section]] withRowAnimation:UITableViewRowAnimationNone];
        
        // 刷新首页的头像
        [[CenterRootViewController sharedCenterController] reloadAvatar];
    }];
}

-(void)logOutManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginWithLogoutMangaerBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf reLoginManager];
    }];
}

#pragma mark - 重新登录
-(void)reLoginManager{
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.settingTableView reloadData];
    }];
}

         
         
@end
