//
//  CenterRootViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/12.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "CenterRootViewController.h"
#import "KMScrollingHeaderView.h"
#import "TSCenterBuyClassViewController.h"
#import "TSCenterWatchHistoryViewController.h"
#import "TSCenterMyToolViewController.h"
#import "TSCenterAboutViewController.h"
#import "TSCenterSettingViewController.h"
#import "TSIMConversationViewController.h"
#import "NetworkAdapter+TSCenter.h"
#import "TSTestViewController.h"


@interface CenterRootViewController()<UITableViewDelegate,UITableViewDataSource,KMScrollingHeaderViewDelegate>{
    TSCenterSignCheckModel *tempSingleModel;
}
@property (nonatomic,strong)NSArray *centerArr;
@property (nonatomic,strong) KMScrollingHeaderView* scrollingHeaderView;            /**< 头部*/
@property (nonatomic,strong)TSImageView *avatar;                            /**< 头像*/
@property (nonatomic,strong)UILabel *nameLabel;
@property (nonatomic,strong)UILabel *slogenLabel;

@end

@implementation CenterRootViewController

+(instancetype)sharedCenterController{
    static CenterRootViewController *_sharedCenterController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedCenterController = [[CenterRootViewController alloc] init];
    });
    return _sharedCenterController;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self reloadNickName];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self setupDetailsPageView];
    [self setupNavbarButtons];
    [self sendRequestToGetSign];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"个人中心";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.centerArr = @[@[@"已购课程",@"观看历史",@"我的工具",@"关于我们",@"设置"]];
}

- (void)setupDetailsPageView {
    self.scrollingHeaderView = [[KMScrollingHeaderView alloc]initWithFrame:self.view.bounds];
    self.scrollingHeaderView.backgroundColor = [UIColor whiteColor];
    self.scrollingHeaderView.tableView.dataSource = self;
    self.scrollingHeaderView.tableView.delegate = self;
    self.scrollingHeaderView.delegate = self;
    
    self.scrollingHeaderView.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.scrollingHeaderView.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.scrollingHeaderView.tableView.showsVerticalScrollIndicator = YES;
    self.scrollingHeaderView.tableView.backgroundColor = [UIColor whiteColor];
    
    self.scrollingHeaderView.tableView.separatorColor = [UIColor whiteColor];
    self.scrollingHeaderView.headerImageViewContentMode = UIViewContentModeScaleToFill;
    [self.view addSubview:self.scrollingHeaderView];
    
    self.scrollingHeaderView.navbarViewFadingOffset = TSFloat(200);
    
    [self.scrollingHeaderView reloadScrollingHeader];
}

-(void)setupNavbarButtons{
    self.scrollingHeaderView.navbarView = nil;
}

#pragma mark - KMScrollingHeaderViewDelegate
- (void)detailsPage:(KMScrollingHeaderView *)detailsPageView headerImageView:(TSImageView *)imageView{
    [self createHeaderView:imageView];
}

#pragma mark - 创建头部信息
-(void)createHeaderView:(UIView *)bgView{
    self.avatar = [[TSImageView alloc]init];
    self.avatar.frame = CGRectMake(kScreenBounds.size.width - TSFloat(40) - TSFloat(71), TSFloat(173) / 2., TSFloat(71), TSFloat(71));
    self.avatar.backgroundColor = [UIColor clearColor];
    self.avatar.clipsToBounds = YES;
    self.avatar.layer.cornerRadius = self.avatar.size_width / 2.;
    self.avatar.backgroundColor = UURandomColor;
    
    [self.avatar uploadImageWithURL:[AccountModel sharedAccountModel].userInfoModel.headimgurl placeholder:nil callback:NULL];
    [bgView addSubview:self.avatar];
    
    // 2. 创建名字
    self.nameLabel = [[UILabel alloc]init];
    self.nameLabel.frame = CGRectMake(TSFloat(17), TSFloat(192) / 2., kScreenBounds.size.width - 2 * TSFloat(17), [NSString contentofHeightWithFont:[[UIFont systemFontOfCustomeSize:23.]boldFont]]);
    self.nameLabel.textColor = [UIColor colorWithCustomerName:@"灰"];
    self.nameLabel.font = [[UIFont systemFontOfCustomeSize:23.]boldFont];
    self.nameLabel.text = [AccountModel sharedAccountModel].userInfoModel.nickname;
    CGSize nameSize = [TSTool makeSizeWithLabel:self.nameLabel];
    self.nameLabel.frame = CGRectMake(TSFloat(17) * 2, TSFloat(86), nameSize.width, nameSize.height);
    [bgView addSubview:self.nameLabel];
    
    // 3. 创建slogen
    self.slogenLabel = [TSViewTool createLabelFont:@"标题" textColor:@"黑"];
    self.slogenLabel.frame = CGRectMake(self.nameLabel.orgin_x, CGRectGetMaxY(self.nameLabel.frame) + TSFloat(20), kScreenBounds.size.width - 2 * TSFloat(50), [NSString contentofHeightWithFont:self.slogenLabel.font]);
    self.slogenLabel.text = @"借人之智，完善自己";
    [bgView addSubview:self.slogenLabel];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.centerArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionofArr = [self.centerArr objectAtIndex:section];
    return sectionofArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdengifyWithRowOne = @"cellIdengifyWithRowOne";
    GWNormalTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowOne];
    if(!cellWithRowOne){
        cellWithRowOne = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferCellHeight = cellHeight;
    cellWithRowOne.transferTitle = [[self.centerArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
    if (indexPath.row == 0){            // 已购课程
        cellWithRowOne.transferIcon = [UIImage imageNamed:@"icon_center_class"];
    } else if (indexPath.row == 1){     // 观看历史
        cellWithRowOne.transferIcon = [UIImage imageNamed:@"icon_center_history"];
    } else if (indexPath.row == 2){     // 我的工具
        cellWithRowOne.transferIcon = [UIImage imageNamed:@"icon_center_tool"];
    } else if (indexPath.row == 3){     // 关于我们
        cellWithRowOne.transferIcon = [UIImage imageNamed:@"icon_center_about"];
    } else if (indexPath.row == 4){     // 设置
        cellWithRowOne.transferIcon = [UIImage imageNamed:@"icon_center_setting"];
    }
    cellWithRowOne.transferHasArrow = YES;
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return TSFloat(64);
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([indexPath row] == [[self.centerArr objectAtIndex:indexPath.section] count] - 1) {
        [cell addSeparatorLineWithTypeWithAres:SeparatorTypeBottom andUseing:@"center"];
    } else {
        [cell addSeparatorLineWithTypeWithAres:SeparatorTypeMiddle andUseing:@"center"];
    }
}



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){    // 已购课程
        [TSMTAManager MTAevent:MTAType_Center_ygkc str:@""];                 // 埋点
        TSCenterBuyClassViewController *buyClassViewController = [[TSCenterBuyClassViewController alloc]init];
        [self.navigationController pushViewController:buyClassViewController animated:YES];
    } else if (indexPath.row == 1){ // 观看历史
        [TSMTAManager MTAevent:MTAType_Center_gkls str:@""];                 // 埋点
        TSCenterWatchHistoryViewController *watchHistoryViewController = [[TSCenterWatchHistoryViewController alloc]init];
        [self.navigationController pushViewController:watchHistoryViewController animated:YES];
    } else if (indexPath.row == 2){ // 我的工具
        [TSMTAManager MTAevent:MTAType_Center_wdgj str:@""];                 // 埋点
        TSCenterMyToolViewController *myToolViewController = [[TSCenterMyToolViewController alloc]init];
        [self.navigationController pushViewController:myToolViewController animated:YES];
    } else if (indexPath.row == 3){ // 关于我们
        [TSMTAManager MTAevent:MTAType_Center_gywm str:@""];                 // 埋点
        TSCenterAboutViewController *aboutMeViewController = [[TSCenterAboutViewController alloc]init];
        [self.navigationController pushViewController:aboutMeViewController animated:YES];
    } else if (indexPath.row == 4){ // 设置
        [TSMTAManager MTAevent:MTAType_Center_Setting str:@""];                 // 埋点
        TSCenterSettingViewController *centerSettingViewController = [[TSCenterSettingViewController alloc]init];
        [self.navigationController pushViewController:centerSettingViewController animated:YES];
    }
}

#pragma mark - sendRequestToGetSign
-(void)sendRequestToGetSign{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerGetSignCheckWithBlcok:^(TSCenterSignCheckModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.slogenLabel.text = singleModel.value;
    }];
}

-(void)reloadAvatar{
    [self.avatar uploadImageWithURL:[AccountModel sharedAccountModel].userInfoModel.headimgurl placeholder:nil callback:NULL];
}

-(void)reloadNickName{
    NSString *name = [AccountModel sharedAccountModel].userInfoModel.nickname;
    self.nameLabel.text = name;
    CGSize nameSize = [TSTool makeSizeWithLabel:self.nameLabel];
    self.nameLabel.frame = CGRectMake(TSFloat(17) * 2, TSFloat(86), nameSize.width, nameSize.height);
}

@end
