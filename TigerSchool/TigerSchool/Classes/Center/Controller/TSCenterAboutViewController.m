//
//  TSCenterAboutViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSCenterAboutViewController.h"
#import "NetworkAdapter+TSCenter.h"

@interface TSCenterAboutViewController ()<PDWebViewDelegate>
@property (nonatomic,strong)PDWebView *mainWebView;
@property (nonatomic,strong)TSImageView *iconImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@end

@implementation TSCenterAboutViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
    [self sendRequestToGetInterface];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"关于我们";
}

#pragma mark - createView
-(void)createView{
    self.iconImgView = [[TSImageView alloc]init];
    self.iconImgView.image = [UIImage imageNamed:@"AppIcon"];
    self.iconImgView.backgroundColor = UURandomColor;
    self.iconImgView.frame = CGRectMake(TSFloat(40), TSFloat(40), TSFloat(60), TSFloat(60));
    self.iconImgView.layer.cornerRadius = TSFloat(10);
    self.iconImgView.clipsToBounds = YES;
    [self.view addSubview:self.iconImgView];
    
    self.titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.font = [[UIFont systemFontOfCustomeSize:25] boldFont];
    self.titleLabel.text = @"老虎大学";
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.iconImgView.frame) + TSFloat(20), self.iconImgView.orgin_y, kScreenBounds.size.width - self.iconImgView.orgin_x - CGRectGetMaxX(self.iconImgView.frame) - TSFloat(20), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self.view addSubview:self.titleLabel];

    self.dymicLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.dymicLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.iconImgView.frame) - [NSString contentofHeightWithFont:self.dymicLabel.font], self.titleLabel.size_width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    [self.view addSubview:self.dymicLabel];
    
    self.mainWebView = [[PDWebView alloc]init];
    self.mainWebView.frame = CGRectMake(self.iconImgView.orgin_x, CGRectGetMaxY(self.iconImgView.frame) + TSFloat(30), kScreenBounds.size.width - 2 * self.iconImgView.orgin_x, kScreenBounds.size.height - CGRectGetMaxY(self.iconImgView.frame) - [PDMainTabbarViewController sharedController].tabBarHeight);
    self.mainWebView.barTintColor = UURandomColor;
    self.mainWebView.wkWebView.scrollView.showsHorizontalScrollIndicator = NO;
    self.mainWebView.wkWebView.scrollView.showsVerticalScrollIndicator = NO;
    [self.view addSubview:self.mainWebView];
}

#pragma mark - Interface
- (void)sendRequestToGetInterface{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerWithAboutMeBlock:^(TSCenterSignCheckModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.mainWebView loadHTMLString:singleModel.value];
        strongSelf.dymicLabel.text = singleModel.version;
    }];
}




@end
