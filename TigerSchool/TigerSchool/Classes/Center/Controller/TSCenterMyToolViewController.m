//
//  TSCenterMyToolViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSCenterMyToolViewController.h"
#import "NetworkAdapter+TSCenter.h"
#import "TSCenterMyToolTableViewCell.h"
#import "TSTeachBopDetailViewController.h"

@interface TSCenterMyToolViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *myToolTableView;
@property (nonatomic,strong)NSMutableArray *myToolMutableArr;
@end

@implementation TSCenterMyToolViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetMyTool];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"我的工具";
}

-(void)arrayWithInit{
    self.myToolMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.myToolTableView){
        self.myToolTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.myToolTableView.dataSource = self;
        self.myToolTableView.delegate = self;
        [self.view addSubview:self.myToolTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.myToolTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetMyTool];
    }];
    [self.myToolTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetMyTool];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.myToolMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    TSCenterMyToolTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[TSCenterMyToolTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    cellWithRowOne.transferSingleModel = [self.myToolMutableArr objectAtIndex:indexPath.row];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TSTeachBopDetailViewController *teachViewController = [[TSTeachBopDetailViewController alloc]init];
    TSMaterialSegmentModel *singleModel = [self.myToolMutableArr objectAtIndex:indexPath.row];
    teachViewController.transferBopDetailId = singleModel.ID;
    [self.navigationController pushViewController:teachViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [TSCenterMyToolTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    SeparatorType separatorType = SeparatorTypeMiddle;
    if ( [indexPath row] == 0) {
        separatorType = SeparatorTypeHead;
    } else if ([indexPath row] == [self.myToolMutableArr count] - 1) {
        separatorType = SeparatorTypeBottom;
    } else {
        separatorType = SeparatorTypeMiddle;
    }
    if ([self.myToolMutableArr  count] == 1) {
        separatorType = SeparatorTypeSingle;
    }
    [cell addSeparatorLineWithType:separatorType];
}

-(void)sendRequestToGetMyTool{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerWithMyToosWithBlock:^(BOOL isSuccessed, NSError *errorInfo, TSMaterialSegmentListModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            if (strongSelf.myToolTableView.isXiaLa){
                [strongSelf.myToolMutableArr removeAllObjects];
            }
            
            if (singleModel.items.count){
                [strongSelf.myToolMutableArr addObjectsFromArray:singleModel.items];
                [strongSelf.musicTopView dismissPrompt];
            } else {
                [strongSelf.myToolTableView showPrompt:@"当前没有数据哟~" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:^{
                    [strongSelf sendRequestToGetMyTool];
                }];
            }
            [strongSelf.myToolTableView reloadData];
        } else {
            [strongSelf.myToolTableView showPrompt:errorInfo.localizedDescription withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:^{
                [strongSelf sendRequestToGetMyTool];
            }];
        }
        [strongSelf.myToolTableView stopPullToRefresh];
        [strongSelf.myToolTableView stopFinishScrollingRefresh];
    }];
}
@end
