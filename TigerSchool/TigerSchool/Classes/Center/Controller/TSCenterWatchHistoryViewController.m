//
//  TSCenterWatchHistoryViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSCenterWatchHistoryViewController.h"
#import "NetworkAdapter+TSCenter.h"
#import "TSHomeTeachSingleCell.h"
#import "TSHomeClassDetailViewController.h"

@interface TSCenterWatchHistoryViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UITableView *historyTableView;
@property (nonatomic,strong)NSMutableArray *historyMutableArr;
@end

@implementation TSCenterWatchHistoryViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfoWithPage:1];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"观看历史";
}

#pragma mark - history
-(void)arrayWithInit{
    self.historyMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.historyTableView){
        self.historyTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.historyTableView.dataSource = self;
        self.historyTableView.delegate = self;
        [self.view addSubview:self.historyTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.historyMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
    TSHomeTeachSingleCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
    if (!cellWithRowThr){
        cellWithRowThr = [[TSHomeTeachSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
        cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    TSHomeLiveModel *singleModel = [self.historyMutableArr objectAtIndex:indexPath.row];
    cellWithRowThr.transferHomeLiveModel = singleModel;
    
    return cellWithRowThr;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    TSHomeClassDetailViewController *homeClassDetailViewController = [[TSHomeClassDetailViewController alloc]init];
    TSHomeLiveModel *singleModel = [self.historyMutableArr objectAtIndex:indexPath.row];
    homeClassDetailViewController.transferCourseId = singleModel.ID;
    homeClassDetailViewController.transferPrice = singleModel.price;
    homeClassDetailViewController.hasCancelPanDismiss = YES;
    [self.navigationController pushViewController:homeClassDetailViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [TSHomeTeachSingleCell calculationCellHeight];
}

#pragma mark - Interface
-(void)sendRequestToGetInfoWithPage:(NSInteger)page{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] centerWithWatchHistoryWithPage:page withBlcok:^(BOOL isSuccessed, NSError *errorInfo, TSHomeLiveListModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [strongSelf.historyMutableArr addObjectsFromArray:singleModel.items];
            [strongSelf.historyTableView reloadData];
            if (strongSelf.historyMutableArr.count){
                [strongSelf.historyTableView dismissPrompt];
            } else {
                [strongSelf.historyTableView showPrompt:@"当前没有数据哦~" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:^{
                    
                }];
            }
        } else {
            [strongSelf.historyTableView showPrompt:errorInfo.localizedDescription withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:^{

            }];
        }
    }];
}

@end
