//
//  TSCenterSettingChangeViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/25.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSCenterSettingChangeViewController.h"
#import "TSLoginInputTableViewCell.h"
#import "NetworkAdapter+TSLogin.h"

static char nickNameChangedBlockKey;
@interface TSCenterSettingChangeViewController ()<UITableViewDelegate,UITableViewDataSource,TSLoginInputTableViewCellDelegate>{
    UITextField *nickTextField;
    GWButtonTableViewCell *tempCellWithRowThr;
    NSString *nickInfo;
}
@property (nonatomic,strong)UITableView *changeTableView;
@property (nonatomic,strong)NSArray *changeArr;
@end

@implementation TSCenterSettingChangeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if ([AccountModel sharedAccountModel].userInfoModel.nickname.length){
        [tempCellWithRowThr setButtonStatus:YES];
    } else {
        [tempCellWithRowThr setButtonStatus:NO];
    }
    if (nickTextField){
        [nickTextField becomeFirstResponder];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"昵称";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.changeArr = @[@[@"修改昵称",@"昵称fixed"],@[@"保存"]];
    nickInfo = [AccountModel sharedAccountModel].userInfoModel.nickname;
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.changeTableView){
        self.changeTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.changeTableView.dataSource = self;
        self.changeTableView.delegate = self;
        [self.view addSubview:self.changeTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.changeArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionofArr = [self.changeArr objectAtIndex:section];
    return sectionofArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"修改昵称" sourceArr:self.changeArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"修改昵称" sourceArr:self.changeArr]){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            TSLoginInputTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[TSLoginInputTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
                cellWithRowOne.delegate = self;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferType = LoginInputTypeNick;
            nickTextField = cellWithRowOne.inputTextField;
            cellWithRowOne.inputTextField.text = nickInfo;
            
            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            cellWithRowTwo.transferTitle = @"昵称请控制在4-30个字符，支持中英文、数字、横线和下划线";
            cellWithRowTwo.titleLabel.font = [UIFont systemFontOfCustomeSize:11.];
            cellWithRowTwo.titleLabel.textColor = [UIColor hexChangeFloat:@"666666"];
            cellWithRowTwo.textAlignmentCenter = YES;
            return cellWithRowTwo;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"保存" sourceArr:self.changeArr]){
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        GWButtonTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[GWButtonTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowThr.transferCellHeight = cellHeight;
        cellWithRowThr.transferTitle = @"保 存";
        tempCellWithRowThr = cellWithRowThr;
        __weak typeof(self)weakSelf = self;
        [cellWithRowThr buttonClickManager:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf sendRequestToChangeNick];
        }];
        return cellWithRowThr;
    } else {
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        UITableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowFour;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"修改昵称" sourceArr:self.changeArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"修改昵称" sourceArr:self.changeArr]){
            return [TSLoginInputTableViewCell calculationCellHeight];
        } else {
            return [GWNormalTableViewCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"保存" sourceArr:self.changeArr]){
        return [GWButtonTableViewCell calculationCellHeight];
    } else {
        return 44;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"修改昵称" sourceArr:self.changeArr]){
        return TSFloat(40);
    } else if (section == [self cellIndexPathSectionWithcellData:@"保存" sourceArr:self.changeArr]){
        return TSFloat(20);
    } else {
        return 44;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(void)inputTextConfirm:(LoginInputType)type textField:(UITextField *)textField status:(BOOL)status{
    if (type == LoginInputTypeNick && textField == nickTextField){
        nickInfo = textField.text;
        if (status == YES){
            [tempCellWithRowThr setButtonStatus:YES];
        } else {                // 不通过
            [tempCellWithRowThr setButtonStatus:NO];
        }
    }
}

#pragma mark - sendRequestToInfo
-(void)sendRequestToChangeNick{
    if ([nickTextField isFirstResponder]){
        [nickTextField resignFirstResponder];
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] loginWithChangeUserNick:nickInfo block:^(NSString *nick) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [StatusBarManager statusHiddenWithString:@"昵称修改成功"];
        // 刷新首页昵称
        [AccountModel sharedAccountModel].userInfoModel.nickname = nick;
        [[CenterRootViewController sharedCenterController] reloadNickName];
        // 进行返回
        void(^block)() = objc_getAssociatedObject(strongSelf, &nickNameChangedBlockKey);
        if (block){
            block();
        }
        [strongSelf.navigationController popViewControllerAnimated:YES];
        
    }];
    
}


-(void)nickNameChangedBlock:(void(^)())block{
    objc_setAssociatedObject(self, &nickNameChangedBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}
@end
