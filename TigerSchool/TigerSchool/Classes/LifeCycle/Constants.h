//
//  Constants.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/12.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

// 【USER Detault】
#define USERDEFAULTS     [NSUserDefaults standardUserDefaults]
#define NOTIFICENTER     [NSNotificationCenter defaultCenter]

// 【System】
#define IOS_Version [UIDevice currentDevice].systemVersion.floatValue
#define IS_IOS7_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 6.99)
#define IS_IOS8_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 7.99)
#define IS_IOS9_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 8.99)
#define IS_IOS10_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 9.99)
#define IS_IOS11_LATER   ([UIDevice currentDevice].systemVersion.floatValue > 10.99)
#define IS_iPhoneX ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? CGSizeEqualToSize(CGSizeMake(1125, 2436), [[UIScreen mainScreen] currentMode].size) : NO)

// 【屏幕尺寸】
#define kScreenBounds               [[UIScreen mainScreen] bounds]

// 【Style】
#define RGB(r, g, b,a)    [UIColor colorWithRed:(r)/255. green:(g)/255. blue:(b)/255. alpha:a]
#define BACKGROUND_VIEW_COLOR        RGB(239, 239, 244,1)               // 主题色
#define NAVBAR_COLOR RGB(255, 255, 255,1)                               // navBar

// 【nav】
#define BAR_BUTTON_FONT       [UIFont systemFontOfSize:14.]
#define BAR_MAIN_TITLE_FONT   [UIFont boldSystemFontOfSize:18.]
#define BAR_SUB_TITLE_FONT    [UIFont systemFontOfSize:12.]
#define BAR_TITLE_PADDING_TOP -3.
#define BAR_TITLE_MAX_WIDTH   TSFloat(230)

// 【log】
// 【Log 方式】
#ifdef DEBUG
#   define PDLog(fmt, ...) NSLog((@"%s [Line %d] " fmt), __PRETTY_FUNCTION__, __LINE__, ##__VA_ARGS__);
#else
#   define PDLog(...)
#endif

// 【高德地图】
#define mapIdentify @"995c2d5470ac90fd6b71a216e83aa48d"
#define beeMapIdentify @"f1725b00611c5667e42a56b49fb2a51b"

// 【百川聊天】
#define AliChattingIdentify @"23330943"                                     // 百川chattingAPI
#define AliPush @"3448665"
#define AliIMKey @"24808439"
#define AliIMSecret @"6702362ca6456c28eaf27fdb95337c6f"

// 【阿里推送】
#define AliPushKey @"24789645"
#define AliPushAppSecret @"d8acac3995b0c2e3daabca2e3a83ef31"

// 【是否layer】
#define View_Layer @"View_Layer"                    // 是否边框

// 【debug状态下的地址】
#define testNet_address @"testNet_address"          // 端口号
#define testNet_port @"testNet_port"                //port

// 【用户自定义配置】
#define mainStyleColor @"mainStyleColor"
#define UserDefaultCustomerType    @"CustomerType"                             // 游客类型
#define UserDefaultUserToken @"UserDefaultUserToken"
#define UserDefaultCustomerMemberId @"CustomerMemberId"
#define UserDetailtCustomeSocketAddress @"UserDetailtCustomeSocketAddress"      // socket的地址
#define UserDetailtCustomeSocketPort @"UserDetailtCustomeSocketPort"            // socket的端口

// 【三方分享】
#define WeChatAppID         @"wxab8268b11e980213"
#define WeChatAppSecret     @"5cb483dfe8b12681cfd522d03f549ced"

#define QQAppID             @"1105501406"
#define QQAppAppSecret      @"a0zBvC0I5boEIliE"

#define WeiBoAPPID          @"1938399542"
#define WeiBoAppSecret      @"b888b2779298f00b98aa6276f8cb3bdc"
#define WeiBoRedirectUri    @"http://www.pandaol.com"

// 【阿里OSS】
#define ossAccessKey    @"LTAIv73GnkoNH4H8"
#define ossSecretKey @"Ke6puYI0auzUfFdxHlMg6VpRBvRQVu"
#define ossEndpoint   @"http://oss-cn-hangzhou.aliyuncs.com"
#define ossBucketName @"ts-test2017"
#define aliyunOSS_BaseURL @"http://testoss.tigerschool.cn/"

// 【融云】
#define rongyun_Key  @"25wehl3u2s8pw"
#define rongyun_Secret  @"3CvCa6HhPoPmTs"


// 【分页每页数量】
static NSInteger cutPageNum = 10;


// 【app配置】
#define User_Choose_Game @"User_Choose_Game"
#define TestNet_Log @"testNet_Log"

// 【userDefault】
#define User_First_Setup @"User_First_Setup"



#endif /* Constants_h */
