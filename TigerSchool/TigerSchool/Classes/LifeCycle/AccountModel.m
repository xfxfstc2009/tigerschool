//
//  AccountModel.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/12.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AccountModel.h"
#import "KeychainUUID.h"

@implementation AccountModel

+(instancetype)sharedAccountModel{
    static AccountModel *_sharedAccountModel = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAccountModel = [[AccountModel alloc] init];
    });
    return _sharedAccountModel;
}

-(BOOL)hasLoggedIn{
    if ([AccountModel sharedAccountModel].token.length){
        return YES;
    } else {
        return NO;
    }
}

-(BOOL)hasMemberLogined{
    if ([AccountModel sharedAccountModel].token.length){
        return YES;
    } else if ([TSTool userDefaultGetWithKey:@"token"].length){
        return YES;
    } else {
        return NO;
    }
}



#pragma mark - 新手引导
-(void)guestSuccessWithType:(GuestType)guestType block:(void(^)())block{
    NSString *type = @"";
    if (guestType == GuestTypeHomeDetail){
        type = @"GuestTypeHomeDetail";
    }
    // 1. 保存到本地
    [TSTool userDefaulteWithKey:type Obj:@"yes"];
    // 2. 存储到内存
    if (guestType == GuestTypeHomeDetail){
        [AccountModel sharedAccountModel].homeDetail = YES;
    }
    // 3. block
    if (block){
        block();
    }
}

-(BOOL)homeDetail{
    if ([TSTool userDefaultGetWithKey:@"GuestTypeHomeDetail"].length){
        return YES;
    } else {
        return _homeDetail;
    }
}

-(NSString *)keychain{
    KeychainUUID *keychain = [[KeychainUUID alloc] init];
    id data = [keychain readUDID];
    
#ifdef DEBUG        // 测试
    return data;
#else               // 线上
    return data;
#endif
}

-(NSString *)token{
    NSString *userDefaultToken = [TSTool userDefaultGetWithKey:@"token"];
    if (userDefaultToken.length){
        return userDefaultToken;
    } else {
        return _token;
    }
}

-(NSString *)accountId{
    if ([TSTool userDefaultGetWithKey:@"userid"].length){
        return [TSTool userDefaultGetWithKey:@"userid"];
    } else {
        return _accountId;
    }
}
@end
