//
//  AccountModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/12.
//  Copyright © 2018年 百e国际. All rights reserved.
//
// 【账户-生命周期】


#import <Foundation/Foundation.h>
#import "TSLoginDataInfoModel.h"
#import "TSLoginModel.h"

typedef NS_ENUM(NSInteger,GuestType) {
    GuestTypeHomeDetail,                 /**< 课程详情*/
};

@interface AccountModel : NSObject

+(instancetype)sharedAccountModel;

@property (nonatomic,copy)NSString *token;                          /**< token*/
@property (nonatomic,assign)NSInteger level;
@property (nonatomic,copy)NSString *phone;
@property (nonatomic,copy)NSString *accountId;
@property (nonatomic,strong)TSLoginDataInfoModel *userInfoModel;
@property (nonatomic,strong)TSLoginModel *userKeyInfo;
// 【socket】
@property (nonatomic,copy)NSString *socketHost;                     /**< socket IP*/
@property (nonatomic,assign)NSInteger socketPort;                   /**< socket Port*/
@property (nonatomic,copy)NSString *keychain;


@property (nonatomic,assign)BOOL has505;                            /**< 用来操作是否505并且仅弹出一次error*/


-(BOOL)hasLoggedIn;
-(BOOL)hasMemberLogined;                        // 判断是否曾今登录过


#pragma mark - ------------------ 新手引导 -------------

@property (nonatomic,assign)BOOL homeDetail;                /**< 课程*/

-(void)guestSuccessWithType:(GuestType)guestType block:(void(^)())block;

@end
