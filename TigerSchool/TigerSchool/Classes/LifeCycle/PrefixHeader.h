//
//  PrefixHeader.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/12.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#ifndef PrefixHeader_h
#define PrefixHeader_h


#import "Enum.h"
#import <UIKit/UIKit.h>
#import <objc/runtime.h>                        // 【runtime】
// third pod
#import "NetworkAdapter.h"                      // 网络
#import <Lottie/Lottie.h>                       // 【动画】

#import "PDSideMenu.h"
// constance
#import "Constants.h"                           // 【基本设置类】

#import "ViewRootConstance.h"                   // 【View Base】
#import "TSTool.h"                              // 【Tool】
#import "CategoryStaticConstans.h"              // 【Category Base】
#import "AccountModel.h"                        // 【账号信息】
#import "FetchModel.h"                          // 【fetchModel】
#import "AbstractViewController.h"              // 【基础控制器】
#import "RESideMenu.h"                          // 【侧边栏】
#import "LoginViewController.h"                 // 【登录】
#import "PDMainTabbarViewController.h"          // 【tab】
#import "PDWebViewController.h"
#import "StatusBarManager.h"
#import "TSNewFeatureViewController.h"
#import "GWAssetsLibraryViewController.h"
#import "TSAlertView.h"                         //  【alert】
#import "AliOSSManager.h"
#import "TSIMManager.h"
#import "TSIMConversationViewController.h"
#import "GWMusicPlayerMethod.h"
#import "TSTeachBopClassViewController.h"
#import "CenterRootViewController.h"
#import "TSLoginSuccessAnimationViewController.h"
#import "TSMTAManager.h"                                    /**< 埋点*/









//////// 表示音乐播放完毕
static NSString *const GWMusicNotification = @"GWMusicNotification";                                                // 音乐通知
static NSString *const PDAppdelegateWillResignNotification = @"PDAppdelegateWillResignNotification";                // 将要进入后台通知




#endif /* PrefixHeader_h */
