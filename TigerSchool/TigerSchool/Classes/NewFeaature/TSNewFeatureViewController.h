//
//  TSNewFeatureViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"

@interface TSNewFeatureViewController : UIViewController

-(void)actionNewFeatureLoginManagerBlock:(void(^)(BOOL isSuccessed))block;

@end
