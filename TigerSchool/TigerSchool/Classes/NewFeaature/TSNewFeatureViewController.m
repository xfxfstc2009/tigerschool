//
//  TSNewFeatureViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSNewFeatureViewController.h"
#import "LoginViewController.h"
#import "TSRegisterViewController.h"

static char actionNewFeatureLoginManagerBlockKey;
@interface TSNewFeatureViewController ()

@property (nonatomic,strong)TSImageView *backgroundImageView;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)UIView *pageView1;
@property (nonatomic,strong)UIView *pageView2;
@property (nonatomic,strong)UIView *pageView3;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *slogenLabel;
@property (nonatomic,strong)UIButton *registerButton;
@property (nonatomic,strong)UIButton *loginButton;
@property (nonatomic,strong)NSTimer *timer;
@property (nonatomic,strong)UIPageControl *pageControl;
@end

@implementation TSNewFeatureViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createPageTimer];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.backgroundImageView = [[TSImageView alloc]init];
    self.backgroundImageView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:self.backgroundImageView];
    if (IS_iPhoneX){
        self.backgroundImageView.image = [UIImage imageNamed:@"bg_newfeature_normal_x.png"];
    } else {
        self.backgroundImageView.image = [UIImage imageNamed:@"bg_newfeature_normal.png"];
    }
    self.backgroundImageView.frame = kScreenBounds;
    
    // 2.
    __weak typeof(self)weakSelf = self;
    self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        NSInteger page = strongSelf.mainScrollView.contentOffset.x / strongSelf.mainScrollView.bounds.size.width;
        strongSelf.pageControl.currentPage = page;
    }];
    self.mainScrollView.backgroundColor = [UIColor clearColor];
    self.mainScrollView.frame = kScreenBounds;
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 3, kScreenBounds.size.height);
    [self.view addSubview:self.mainScrollView];
    
    // 1. 创建3个页面
    self.pageView1 = [self createPageViewWithTitle:@"学习" slogen:@"经常不断地学习\n你就什么都知道\n你知道越多你就越有力量"];
    self.pageView1.orgin_x = 0;
    [self.mainScrollView addSubview:self.pageView1];
    self.pageView2 = [self createPageViewWithTitle:@"行动" slogen:@"如果你无法简洁的\n表达你的想法\n那只能说明你还不够了解它"];
    self.pageView2.orgin_x = kScreenBounds.size.width;
    [self.mainScrollView addSubview:self.pageView2];
    self.pageView3 = [self createPageViewWithTitle:@"成长" slogen:@"借人之智完善自己\n学最好的别人\n做最好的自己"];
    self.pageView3.orgin_x = 2 * kScreenBounds.size.width;
    [self.mainScrollView addSubview:self.pageView3];

    
    // 4.
    self.registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.registerButton setTitle:@"注册" forState:UIControlStateNormal];
    [self.registerButton setTitleColor:[UIColor colorWithCustomerName:@"白"] forState:UIControlStateNormal];
    [self.registerButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [TSMTAManager MTAevent:MTAType_Wel_Register str:@""];          // 埋点
        TSRegisterViewController *registerViewController = [[TSRegisterViewController alloc]init];
        [strongSelf.navigationController pushViewController:registerViewController animated:YES];
    }];
    
    self.registerButton.layer.cornerRadius = TSFloat(2);
    self.registerButton.frame = CGRectMake(TSFloat(13), kScreenBounds.size.height - TSFloat(75) - TSFloat(44), TSFloat(170), TSFloat(44));
    [self.registerButton setBackgroundImage:[TSTool createImageWithColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.3f] frame:self.registerButton.bounds] forState:UIControlStateNormal];
    self.registerButton.clipsToBounds = YES;
    
    [self.view addSubview:self.registerButton];
    
    // 登录
    self.loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.loginButton setTitle:@"登录" forState:UIControlStateNormal];
    [self.loginButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.loginButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [TSMTAManager MTAevent:MTAType_Wel_Login str:@""];          // 埋点
        LoginViewController *loginViewController = [[LoginViewController alloc]init];
        [loginViewController loginSuccessWithManagerBlock:^(BOOL isSuccessed) {
            void(^block)(BOOL isSuccess) = objc_getAssociatedObject(strongSelf, &actionNewFeatureLoginManagerBlockKey);
            if (block){
                block(isSuccessed);
            }
        }];
        [strongSelf.navigationController pushViewController:loginViewController animated:YES];
    }];
    self.loginButton.backgroundColor = [UIColor whiteColor];
    self.loginButton.layer.cornerRadius = TSFloat(2);
    self.loginButton.clipsToBounds = YES;
    self.loginButton.frame = CGRectMake(kScreenBounds.size.width - TSFloat(13) - self.registerButton.size_width, kScreenBounds.size.height - TSFloat(75) - TSFloat(44), TSFloat(170), TSFloat(44));
    [self.view addSubview:self.loginButton];
    
    // pageControl
    self.pageControl = [[UIPageControl alloc]init];
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = 3;
    self.pageControl.frame = CGRectMake(0, self.loginButton.orgin_y - TSFloat(35), kScreenBounds.size.width, 20);
    [self.pageControl setValue:[UIImage imageNamed:@"img_page_hlt"] forKeyPath:@"_currentPageImage"];
    [self.pageControl setValue:[UIImage imageNamed:@"img_page_nor"] forKeyPath:@"_pageImage"];
    [self.view addSubview:self.pageControl];
}


-(UIView *)createPageViewWithTitle:(NSString *)title slogen:(NSString *)slogen{
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor clearColor];
    bgView.frame = CGRectMake(0, 0, kScreenBounds.size.height, CGRectGetMaxY(self.pageControl.frame) - TSFloat(90));
    bgView.backgroundColor = UURandomColor;
    
    // 1.创建标题
    UILabel *titleLabel = [TSViewTool createLabelFont:@"标题" textColor:@"白"];
    titleLabel.text = title;
    titleLabel.font = [[UIFont systemFontOfCustomeSize:80.]boldFont];
    CGSize titleSize = [TSTool makeSizeWithLabel:titleLabel];
    titleLabel.frame = CGRectMake(TSFloat(25), TSFloat(450) / 2., titleSize.width, titleSize.height);
    [bgView addSubview:titleLabel];
    
    // 2. 创建slogen
    UILabel *slogenLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"白"];
    slogenLabel.text = slogen;
    slogenLabel.backgroundColor = [UIColor clearColor];
    slogenLabel.font = [UIFont systemFontOfCustomeSize:23.];
    slogenLabel.numberOfLines = 0;
    CGSize slogenSize = [slogenLabel.text sizeWithCalcFont:slogenLabel.font constrainedToSize:CGSizeMake(kScreenBounds.size.width - TSFloat(50), CGFLOAT_MAX)];
    slogenLabel.frame = CGRectMake(titleLabel.orgin_x, CGRectGetMaxY(titleLabel.frame) + TSFloat(55), kScreenBounds.size.width - TSFloat(50), slogenSize.height);
    [bgView addSubview:slogenLabel];
    
    return bgView;
}

-(void)createPageTimer{
    self.timer = [NSTimer timerWithTimeInterval:3.0f target:self selector:@selector(timerChanged) userInfo:nil repeats:YES];
    [[NSRunLoop currentRunLoop]addTimer:self.timer forMode:NSRunLoopCommonModes];

}

-(void)timerChanged{
    int page  = (self.pageControl.currentPage + 1) % 3;
    self.pageControl.currentPage = page;
    [self pageChanged:self.pageControl];
}

-(void)pageChanged:(UIPageControl *)pageControl{
    if (pageControl.currentPage >= pageControl.numberOfPages){
        pageControl.currentPage = 0;
    }
    CGFloat x = (pageControl.currentPage) * kScreenBounds.size.width;
    [self.mainScrollView setContentOffset:CGPointMake(x, 0) animated:YES];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    double page = self.mainScrollView.contentOffset.x / self.mainScrollView.bounds.size.width;
    self.pageControl.currentPage = page;
    
    if (page== - 1) {
        self.pageControl.currentPage = 2;// 序号0 最后1页
    } else if (page == 2) {
        self.pageControl.currentPage = 0; // 最后+1,循环第1页
        [self.mainScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView{
    [self.timer invalidate];
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate{
    [self createPageTimer];
}

-(void)actionNewFeatureLoginManagerBlock:(void(^)(BOOL isSuccessed))block{
    objc_setAssociatedObject(self, &actionNewFeatureLoginManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
