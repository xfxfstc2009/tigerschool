//
//  TSIMConversationViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/26.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSIMConversationViewController.h"

@interface TSIMConversationViewController ()

@end

@implementation TSIMConversationViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [[PDMainTabbarViewController sharedController] setBarHidden:YES animated:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = UURandomColor;
    [self pageSetting];
}

-(void)superViewManager:(UIView *)view{
//    self.conversationMessageCollectionView.frame = view.bounds;
    self.view.frame = view.bounds;
}

#pragma mark pageSetting
-(void)pageSetting{
    self.title = @"聊天室";
}



@end
