//
//  TSLyricParser.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/24.
//  Copyright © 2018年 百e国际. All rights reserved.
//
// 歌词解析
#import <Foundation/Foundation.h>
#import "TSLyricModel.h"

@interface TSLyricParser : NSObject

/**
 歌词解析
 
 @param url 歌词的url
 @return 包含歌词模型的数组
 */
+ (NSArray *)lyricParserWithUrl:(NSString *)url;

/**
 解析歌词
 
 @param url 歌词的url
 @param isDelBlank 是否去掉空白行歌词
 @return 包含歌词模型的数组
 */
+ (NSArray *)lyricParserWithUrl:(NSString *)url isDelBlank:(BOOL)isDelBlank;

/**
 歌词解析
 
 @param str 所有歌词的字符串
 @return 包含歌词模型的数组
 */
+ (NSArray *)lyricParserWithStr:(NSString *)str;

@end
