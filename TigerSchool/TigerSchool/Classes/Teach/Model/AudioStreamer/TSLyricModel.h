//
//  TSLyricModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/24.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@interface TSLyricModel : FetchModel

/** 毫秒 */
@property (nonatomic, assign) NSTimeInterval msTime;
/** 秒 */
@property (nonatomic, assign) NSTimeInterval secTime;
/** 时间字符串 */
@property (nonatomic, copy) NSString *timeString;
/** 歌词内容 */
@property (nonatomic, copy) NSString *content;

@property (nonatomic,assign)NSTimeInterval time;

@property (nonatomic,assign)CGFloat playbackTimeInSeconds;

@property (nonatomic,assign)CGFloat position;

@end
