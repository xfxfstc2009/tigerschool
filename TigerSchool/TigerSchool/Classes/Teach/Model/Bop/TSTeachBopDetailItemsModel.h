//
//  TSTeachBopDetailItemsModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@protocol TSTeachBopDetailItemsModel <NSObject>

@end

@interface TSTeachBopDetailItemsModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *img;
@property (nonatomic,assign)BOOL iswatch;           /**< 是否观看*/
@property (nonatomic,copy)NSString *name;
@property (nonatomic,assign)NSInteger score;        /**< 分数*/
@property (nonatomic,assign)BOOL unlock;            /**< 是否锁定*/

// temp
@property (nonatomic,assign)BOOL hasAnimation;
@property (nonatomic,copy)NSString *transferCourseId;
@end

@interface TSTeachBopDetailItemsRootModel : FetchModel

@property (nonatomic,strong)NSArray<TSTeachBopDetailItemsModel> *detailitems;
@property (nonatomic,assign)CGFloat process;            /**<进度*/
@property (nonatomic,assign)CGFloat score;              /**< 分数*/

@end
