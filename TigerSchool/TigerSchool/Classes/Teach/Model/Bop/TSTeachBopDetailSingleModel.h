//
//  TSTeachBopDetailSingleModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@interface TSTeachBopDetailSingleModel : FetchModel

@property (nonatomic,copy)NSString *cou_desc;
@property (nonatomic,copy)NSString *img;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,assign)CGFloat price;

// temp
@property (nonatomic,assign)CGFloat currentPrice;
@property (nonatomic,copy)NSString *tempCourId;             /**< 课程编号*/
@end
