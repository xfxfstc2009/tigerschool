//
//  TSTeachBopHasPayModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/18.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@interface TSTeachBopHasPayModel : FetchModel

@property (nonatomic,assign)BOOL isfree;
@property (nonatomic,assign)BOOL ispay;

@end
