//
//  TSTeachBopResultModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/2.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@interface TSTeachBopResultInfoModel : FetchModel
@property (nonatomic,strong)NSArray *word;
@end

@interface TSTeachBopResultModel : FetchModel

@property (nonatomic,copy)NSString *corpus_no;
@property (nonatomic,copy)NSString*err_no;
@property (nonatomic,strong)TSTeachBopResultInfoModel *result;
@property (nonatomic,copy)NSString *sn;
@property (nonatomic,copy)NSString *voice_energy;
@end

@interface TSTeachBopResultMainModel : FetchModel

@property (nonatomic,strong)NSArray *results_recognition;           /**< 识别结果*/
@property (nonatomic,strong)TSTeachBopResultModel *origin_result;   /**< 最初的结果*/

@end





////////// bop用户进行阅读的内容 ///////////
typedef NS_ENUM(NSInteger,BopBeiyibeiType) {
    BopBeiyibeiTypeNor = 0,             /**< bop 没背过*/
    BopBeiyibeiTypeSuccess,             /**< bop 成功*/
    BopBeiyibeiTypeFail,                /**< bop 失败*/
};

@interface TSTeachBopReadedInfo : FetchModel
@property (nonatomic,assign)NSInteger infoIndex;            /**< 内容的标记*/
@property (nonatomic,copy)NSString *mainInfo;               /**< 主内容*/
@property (nonatomic,copy)NSString *userInputInfo;          /**< 用户输入的内容*/
@property (nonatomic,assign)BopBeiyibeiType bopLrcType;     /**< bop类型*/

@property (nonatomic,copy)NSString *mainTempInfo;               /**< 去除掉空格与,。的主内容*/
@property (nonatomic,copy)NSString *mainTempInfoArrow;          /**< 用*代替的文案 */
@property (nonatomic,assign)BOOL hasRead;                       /**< 判断是否背诵过*/
@property (nonatomic,strong)NSArray *needMainTagArr;
@end



