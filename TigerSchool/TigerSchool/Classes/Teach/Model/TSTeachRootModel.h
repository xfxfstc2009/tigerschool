//
//  TSTeachRootModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"
#import "TSTeachMaterialSingleView.h"

@protocol TSTeachRootSingleModel <NSObject>

@end

@interface TSTeachRootSingleModel : FetchModel

@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *img;
@property (nonatomic,copy)NSString *ID;

@end

@interface TSTeachRootModel : FetchModel
@property (nonatomic,strong)NSArray<TSTeachMaterialSingleModel> *material;
@property (nonatomic,strong)NSArray<TSTeachRootSingleModel> *bop;

@end

