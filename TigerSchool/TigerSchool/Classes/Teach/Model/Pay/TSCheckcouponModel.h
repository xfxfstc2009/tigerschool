//
//  TSCheckcouponModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@interface TSCheckcouponModel : FetchModel

@property (nonatomic,assign)CGFloat newprice;           /**< 优惠码使用后的价格*/

@end



// 提交预付款
@interface TSAddWillPayOrderModel : FetchModel

@property (nonatomic,assign)CGFloat price;                /**< 优惠码使用后的价格*/
@property (nonatomic,copy)NSString *outtradeno;           /**< 优惠码使用后的价格*/
@property (nonatomic,copy)NSString *identify;             /**< 优惠码使用后的价格*/
@property (nonatomic,copy)NSString *transactionid;        /**< 交易流水号*/
@end

