//
//  TSTeachBopResultModel.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/2.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachBopResultModel.h"

@implementation TSTeachBopResultModel

@end

@implementation TSTeachBopResultInfoModel

@end

@implementation TSTeachBopResultMainModel

@end

@implementation TSTeachBopReadedInfo

-(void)setMainInfo:(NSString *)mainInfo{
    _mainInfo = mainInfo;
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"@／：:：<>《》；（）¥「」＂、[]{}#%-*+=_\\|~＜＞$?^?'@#$%^&*()_+' .。，,"];
    NSString * hmutStr = [[mainInfo componentsSeparatedByCharactersInSet: doNotWant]componentsJoinedByString: @""];
    
    _mainTempInfo = hmutStr;
}

-(NSString *)mainTempInfoArrow{
    NSString *arrowStr = @"";
    for (int i = 0 ; i < self.mainInfo.length;i++){
        arrowStr = [arrowStr stringByAppendingString:@"*"];
    }
    return arrowStr;
}

@end

