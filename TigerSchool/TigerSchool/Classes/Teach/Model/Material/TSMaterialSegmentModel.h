//
//  TSMaterialSegmentModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"


@protocol TSMaterialSegmentModel <NSObject>

@end

@interface TSMaterialSegmentModel : FetchModel

@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *img;
@property (nonatomic,assign)NSInteger type;         /**< 内容类型*/
@property (nonatomic,copy)NSString *url;            /**< 内容类型*/


// temp
@property (nonatomic,copy)NSString *tableIndex;

@end

@interface TSMaterialSegmentListModel : FetchModel

@property (nonatomic,strong)NSArray<TSMaterialSegmentModel> *items;

@end
