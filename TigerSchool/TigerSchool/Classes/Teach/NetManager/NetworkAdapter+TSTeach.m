//
//  NetworkAdapter+TSTeach.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "NetworkAdapter+TSTeach.h"
#import "TSTeachBopHasPayModel.h"

@implementation NetworkAdapter (TSTeach)

-(void)teachWithFetchRootWithBlock:(void(^)(TSTeachRootModel *teachRootModel))block{
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_root requestParams:nil responseObjectClass:[TSTeachRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSTeachRootModel *singleModel = (TSTeachRootModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}

-(void)teachBopDetailWithId:(NSString *)courseId block:(void(^)(TSTeachBopDetailSingleModel *bopDetailModel))block{
    if (!courseId.length){
        NSLog(@"err");
        return;
    }
    NSDictionary *params = @{@"courseid":courseId};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_bopdesc requestParams:params  responseObjectClass:[TSTeachBopDetailSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSTeachBopDetailSingleModel *singleModel = (TSTeachBopDetailSingleModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}

// 【BOP课程进度分数】
-(void)teachBopWithCourseId:(NSString *)courseId block:(void(^)(TSTeachBopDetailItemsRootModel *detailModel))block{
    if (!courseId.length){
        return;
    }
    NSDictionary *params = @{@"courseid":courseId};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_bopdetail requestParams:params responseObjectClass:[TSTeachBopDetailItemsRootModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        TSTeachBopDetailItemsRootModel *singleModel = (TSTeachBopDetailItemsRootModel *)responseObject;
        if(block){
            block(singleModel);
        }
    }];
}

// 【获取bop详情】
-(void)teachBopDetailWithDetailId:(NSString *)detailId block:(void(^)(TSBopItemDetailListModel *singleRootModel))block{
    NSDictionary *params = @{@"detailid":detailId};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_bopitemdetail requestParams:params responseObjectClass:[TSBopItemDetailListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSBopItemDetailListModel *singleModel = (TSBopItemDetailListModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}

// 【获取工具首页】
-(void)teachToolHomePageWithBlock:(void(^)(TSMaterialSegmentListModel *materialSingleModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_materialtype requestParams:nil responseObjectClass:[TSMaterialSegmentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSMaterialSegmentListModel *singleModel = (TSMaterialSegmentListModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}

// 【素材分页】
-(void)teachToolLimit:(NSString *)mid page:(NSInteger)page block:(void(^)(BOOL isSuccessed,NSError *error,TSMaterialSegmentListModel *toolListModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"mid":mid,@"page":@(page),@"limit":@(10)};
    
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_materiallimit requestParams:params responseObjectClass:[TSMaterialSegmentListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSMaterialSegmentListModel *singleModel = (TSMaterialSegmentListModel *)responseObject;
            if (block){
                block(YES,nil,singleModel);
            }
        } else {
            if (block){
                block(NO,error,nil);
            }
        }
    }];
}

// 【提交用户看完某小节】
-(void)teachToolWatchLog:(NSString *)detailid courseid:(NSString *)courseid itemid:(NSString *)itemid score:(NSInteger)score str:(NSString *)str recite:(BOOL)recite block:(void(^)(BOOL isSuccessed))block{
    
    NSString *error = @"";
    if (!detailid.length){
        error = @"当前没有小节ID";
    } else if (!courseid.length){
        error = @"当前没有课程ID";
    } 
    if (error.length){
        [StatusBarManager statusHiddenWithString:error];
        return;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (detailid.length){
        [params setValue:detailid forKey:@"detailid"];
    }
    if (courseid.length){
        [params setValue:courseid forKey:@"courseid"];
    }
    if (itemid.length){
        [params setValue:itemid forKey:@"itemid"];
    }
    if (score != -1){
        [params setValue:@(score) forKey:@"score"];
    }
    if (str.length){
        [params setValue:str forKey:@"str"];
    }
    if (recite == YES){
        [params setValue:@(recite) forKey:@"recite"];
    }
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_watchlog requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        block(isSucceeded);
    }];
}

#pragma mark - 判断是否已支付
-(void)teachBopHasPayId:(NSString *)courseid block:(void(^)(TSTeachBopHasPayModel *model))block{
    if (!courseid.length){
        [StatusBarManager statusHiddenWithString:@"没获取到课程ID"];
        return;
    }
    NSDictionary *params = @{@"courseid":courseid};
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_ispay requestParams:params responseObjectClass:[TSTeachBopHasPayModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSTeachBopHasPayModel *hasPayModel = (TSTeachBopHasPayModel *)responseObject;
            if(block){
                block(hasPayModel);
            }
        }
    }];
}

#pragma mark - 验证优惠券是否可用
-(void)teachCheckCouponHasUsedCoupon:(NSString *)coupon courseId:(NSString *)courseId block:(void(^)(BOOL isSuccessed, CGFloat newPrice))block{
    
    __weak typeof(self)weakSelf = self;
    NSString *error = @"";
    if (!coupon.length){
        error = @"请输入优惠券";
    } else if (!courseId.length){
        error = @"请输入课程ID";
    }
    if (error.length){
        [StatusBarManager statusHiddenWithString:error];
        return;
    }
    NSDictionary *params = @{@"coupon":coupon,@"courseid":courseId};
    
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_ioscheckcoupon requestParams:params responseObjectClass:[TSCheckcouponModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSCheckcouponModel *singleModel = (TSCheckcouponModel *)responseObject;
            if (block){
                block(YES,singleModel.newprice);
            }
        } else {
            if (block){
                block(NO,0);
            }
        }
    }];
}

#pragma mark 预支付 提交
-(void)teachAddpayorderCourseId:(NSString *)courseId coupon:(NSString *)coupon block:(void(^)(TSAddWillPayOrderModel *model))block{
    __weak typeof(self)weakSelf = self;
    
    NSString *err = @"";
    if (!courseId.length){
        err = @"请输入课程号";
    }
    
    if (err.length){
        [StatusBarManager statusHiddenWithString:err];
        return ;
    }
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:courseId forKey:@"courseid"];
    if (coupon.length){
        [params setObject:coupon forKey:@"coupon"];
    }
    
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_iosaddpayorder requestParams:params responseObjectClass:[TSAddWillPayOrderModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSAddWillPayOrderModel *model = (TSAddWillPayOrderModel *)responseObject;
            if(block){
                block(model);
            }
        } else {
            TSAddWillPayOrderModel *model = [[TSAddWillPayOrderModel alloc]init];
            model.price = 8.0;
            model.outtradeno = [NSDate getCurrentTimeWithFileName];
            model.identify = @"com.tigerschool.class18";
            model.transactionid = [NSDate getCurrentTimeWithFileName];
            if(block){
                block(model);
            }
        }
    }];
}

#pragma mark - 进行回调
-(void)teachPayBackDevice:(NSString *)device outtradeno:(NSString *)outtradeno transactionid:(NSString *)transactionid userid:(NSString *)userid block:(void(^)(BOOL isSuccessed))block{
    __weak typeof(self)weakSelf = self;
    
    NSString *mainSign = @"";
    mainSign = [mainSign stringByAppendingString:device];
    mainSign = [mainSign stringByAppendingString:outtradeno];
    mainSign = [mainSign stringByAppendingString:transactionid];
    mainSign = [mainSign stringByAppendingString:userid];
    mainSign = [mainSign stringByAppendingString:@"tigerschoolnotify"];
    mainSign = [mainSign md5String];
    NSDictionary *params = @{@"device":device,@"outtradeno":outtradeno,@"transactionid":transactionid,@"userid":userid,@"sign":mainSign};
    
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_iosnotify requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            if (block){
                block(YES);
            }
        }
    }];
}

-(void)teachPlayerCourseprocess:(NSString *)courseId detailId:(NSString *)detailId process:(NSInteger)process{
    __weak typeof(self)weakSelf = self;
    NSDictionary*params = @{@"courseid":courseId,@"detailid":detailId,@"process":@(process)};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:teach_courseprocess requestParams:params responseObjectClass:nil succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        
    }];
}

@end
