//
//  NetworkAdapter+TSTeach.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "NetworkAdapter.h"
#import "TSTeachRootModel.h"
#import "TSTeachBopDetailSingleModel.h"
#import "TSTeachBopDetailItemsModel.h"
#import "TSBopItemDetailModel.h"
#import "TSMaterialSegmentModel.h"
#import "TSTeachBopHasPayModel.h"
#import "TSCheckcouponModel.h"              // 优惠券是否ke'yong
@interface NetworkAdapter (TSTeach)

-(void)teachWithFetchRootWithBlock:(void(^)(TSTeachRootModel *teachRootModel))block;

// 【获取bop详情】
-(void)teachBopDetailWithId:(NSString *)courseId block:(void(^)(TSTeachBopDetailSingleModel *bopDetailModel))block;
// 【BOP课程进度分数】
-(void)teachBopWithCourseId:(NSString *)courseId block:(void(^)(TSTeachBopDetailItemsRootModel *detailModel))block;
// 【获取bop详情】
-(void)teachBopDetailWithDetailId:(NSString *)detailId block:(void(^)(TSBopItemDetailListModel *singleRootModel))block;
// 【获取工具首页】
-(void)teachToolHomePageWithBlock:(void(^)(TSMaterialSegmentListModel *materialSingleModel))block;
// 【素材分页】
-(void)teachToolLimit:(NSString *)mid page:(NSInteger)page block:(void(^)(BOOL isSuccessed,NSError *error,TSMaterialSegmentListModel *toolListModel))block;
// 【提交用户看完某小节】
-(void)teachToolWatchLog:(NSString *)detailid courseid:(NSString *)courseid itemid:(NSString *)itemid score:(NSInteger)score str:(NSString *)str recite:(BOOL)recite block:(void(^)(BOOL isSuccessed))block;
// 【判断是否支付】
-(void)teachBopHasPayId:(NSString *)courseid block:(void(^)(TSTeachBopHasPayModel *model))block;
// 【验证优惠券是否可用】
-(void)teachCheckCouponHasUsedCoupon:(NSString *)coupon courseId:(NSString *)courseId block:(void(^)(BOOL isSuccessed, CGFloat newPrice))block;
// 【预支付 提交】
-(void)teachAddpayorderCourseId:(NSString *)courseId coupon:(NSString *)coupon block:(void(^)(TSAddWillPayOrderModel *model))block;
// 【进行回调】
-(void)teachPayBackDevice:(NSString *)device outtradeno:(NSString *)outtradeno transactionid:(NSString *)transactionid userid:(NSString *)userid block:(void(^)(BOOL isSuccessed))block;
// 【进行刷新】
-(void)teachPlayerCourseprocess:(NSString *)courseId detailId:(NSString *)detailId process:(NSInteger)process;
@end
