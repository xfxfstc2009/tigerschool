//
//  TSTeachBopClassBeiSongSuccessViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/2.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachBopClassBeiSongSuccessViewController.h"
#import "TSBopBeiyibeiSingleTableViewCell.h"
#import "TSTeachBopEndView.h"
#import "NetworkAdapter+TSTeach.h"

static char reBackWithBlockKey;
@interface TSTeachBopClassBeiSongSuccessViewController ()<UITableViewDelegate,UITableViewDataSource>
@property (nonatomic,strong)UIView *bottomView;
@property (nonatomic,strong)UIButton *rebutton;
@property (nonatomic,strong)UIButton *successButton;
@property (nonatomic,strong)UITableView *bopEndTableView;
@property (nonatomic,strong)NSMutableArray *infoMainArr;
@property (nonatomic,strong)TSTeachBopEndView *numberView;                 /**< 分数view*/
@property (nonatomic,strong)UIButton *leftButton;

@end

@implementation TSTeachBopClassBeiSongSuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createBottomView];
    [self buttonDymicManager];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"测评结果";
    self.leftButton = [self leftBarButtonWithTitle:@"" barNorImage:nil barHltImage:nil action:NULL];
    self.leftButton.hidden = YES;
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.infoMainArr = [NSMutableArray array];
    [self.infoMainArr addObjectsFromArray:self.transferNeedInfoModel];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    // 计算分数
    [self calculationWithScoreManager];
}

-(void)calculationWithScoreManager{
    NSInteger hasKey = 0;
    NSInteger keyCount = 0;
    NSMutableArray *tempKeyMutableArr = [NSMutableArray array];
    for (int i = 0 ; i < self.infoMainArr.count;i++){
        TSTeachBopReadedInfo *resultModel = [self.infoMainArr objectAtIndex:i];
        keyCount += resultModel.mainInfo.length;
        
        if (!tempKeyMutableArr.count){
            [tempKeyMutableArr addObjectsFromArray:resultModel.needMainTagArr];
        }
        for (int j = 0 ; j < resultModel.needMainTagArr.count;j++){
            NSString *key = [resultModel.needMainTagArr objectAtIndex:j];
            if(([resultModel.mainInfo rangeOfString:key].location !=NSNotFound)) {
                hasKey++;
            }
        }
    }

    NSInteger score = hasKey * 1.f / keyCount * 100;
    [self.numberView showAnimationWithNumber:score];
    
    // 2. 提交接口
    NSString *resultStr = @"";
    for (int i = 0 ; i < tempKeyMutableArr.count;i++){
        NSString *key = [tempKeyMutableArr objectAtIndex:i];
        resultStr = [resultStr stringByAppendingString:key];
    }
    if (!resultStr.length){
        resultStr = @",";
    }
    [self sendRequestToUploadInfo:score str:resultStr];
}

#pragma mark -
#pragma mark - createBottomView
-(void)createBottomView{
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    self.bottomView.frame = CGRectMake(TSFloat(25), TSFloat(26), kScreenBounds.size.width - 2 * TSFloat(25), self.view.size_height - TSFloat(20) * 2 - 88 - 50 - 100);
    self.bottomView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.bottomView.layer.shadowOpacity = .2f;
    self.bottomView.layer.shadowOffset = CGSizeMake(.2f, .2f);
    [self.view addSubview:self.bottomView];
    
    [self createTableView];
    
    // 创建完成按钮
    self.rebutton = [UIButton buttonWithType:UIButtonTypeCustom];
    __weak typeof(self)weakSelf = self;
    [self.rebutton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [TSMTAManager MTAevent:MTAType_Tool_Bop_ReBeiyibei str:self.transferDetailId];                 // 埋点
        void(^block)() = objc_getAssociatedObject(strongSelf, &reBackWithBlockKey);
        if (block){
            block();
        }
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.rebutton setTitle:@"重新背诵" forState:UIControlStateNormal];
    [self.rebutton setTitleColor:[UIColor colorWithCustomerName:@"红"] forState:UIControlStateNormal];
    [self.view addSubview:self.rebutton];
    
    // 创建重新按钮
    self.successButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.successButton setBackgroundImage:[TSTool stretchImageWithName:@"icon_button_hlt"] forState:UIControlStateNormal];
    [self.successButton setTitle:@"完成" forState:UIControlStateNormal];
    [self.view addSubview:self.successButton];
    
    CGFloat margin = TSFloat(25);
    CGFloat btn_width = (self.bottomView.size_width - margin) / 2.;
    self.rebutton.frame = CGRectMake(self.bottomView.orgin_x, CGRectGetMaxY(self.bottomView.frame) + TSFloat(16), btn_width, TSFloat(44));
    self.successButton.frame = CGRectMake(CGRectGetMaxX(self.bottomView.frame) - self.rebutton.size_width, self.rebutton.orgin_y, self.rebutton.size_width, self.rebutton.size_height);
    
    self.rebutton.layer.cornerRadius = MIN(self.rebutton.size_width, self.rebutton.size_height) / 2.;
    self.rebutton.layer.borderColor = [UIColor colorWithCustomerName:@"红"].CGColor;
    self.rebutton.clipsToBounds = YES;
    self.rebutton.layer.borderWidth = .5f;

    // 创建分数背景
    self.numberView = [[TSTeachBopEndView alloc]initWithFrame:CGRectMake(0, TSFloat(20), kScreenBounds.size.width, TSFloat(57))];
    [self.view addSubview:self.numberView];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.bopEndTableView){
        self.bopEndTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.bopEndTableView.frame = CGRectMake(TSFloat(25), TSFloat(80), self.bottomView.size_width - 2 * TSFloat(25), self.bottomView.size_height - TSFloat(80) - TSFloat(50));
        self.bopEndTableView.dataSource = self;
        self.bopEndTableView.delegate = self;
        [self.bottomView addSubview:self.bopEndTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.infoMainArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    TSBopBeiyibeiSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[TSBopBeiyibeiSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    TSTeachBopReadedInfo *resultModel = [self.infoMainArr objectAtIndex:indexPath.section];
    resultModel.needMainTagArr = self.transferEcodeArr;
    cellWithRowOne.transferBopEndResultModel = resultModel;

    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    TSTeachBopReadedInfo *singleModel = [self.infoMainArr objectAtIndex:indexPath.section];
    return [TSBopBeiyibeiSingleTableViewCell calculationCellHeightWithModel:singleModel];
}


#pragma mark - 重新背诵
-(void)reBackWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &reBackWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

#pragma mark - 完成方法
-(void)successManager{
    for (UIViewController *controler in self.navigationController.childViewControllers){
        if ([controler isKindOfClass:[TSTeachDetailViewController class]]){
            [self.navigationController popToViewController:controler animated:YES];
            return;
        }
    }
    
    [self.navigationController dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - 显示动画
-(void)createScoreView{
    
}

#pragma mark - interface提交
-(void)sendRequestToUploadInfo:(NSInteger)score str:(NSString *)str{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachToolWatchLog:self.transferDetailId courseid:self.transferCourseId itemid:self.transferItemId score:score str:@"," recite:1 block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
    }];
}

-(void)buttonDymicManager{
    NSInteger index = 0;
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < self.transferInfoItemsArr.count;i++){
        TSBopItemDetailModel *detailModel = [self.transferInfoItemsArr objectAtIndex:i];
        if ([detailModel.ID isEqualToString:self.transferItemId]){
            index = i;
        }
    }
    
    index++;
    if (index > self.transferInfoItemsArr.count - 1){
        [self.successButton setTitle:@"完成" forState:UIControlStateNormal];
        [self.successButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf successManager];
        }];
        return;
    } else {
        [self.successButton setTitle:@"下一节" forState:UIControlStateNormal];
        
        [self.successButton buttonWithBlock:^(UIButton *button) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            for (UIViewController *controller in strongSelf.navigationController.childViewControllers){
                if ([controller isKindOfClass:[TSTeachBopClassViewController class]]){
                    [TSMTAManager MTAevent:MTAType_Tool_Bop_Next str:strongSelf.transferDetailId];                 // 埋点
                    
                    TSTeachBopClassViewController *mainController = (TSTeachBopClassViewController *)controller;
                    [mainController playItemWithIndex:index];
                    [strongSelf.navigationController popToViewController:mainController animated:YES];
                }
            }
        }];
    }
//
//     TSBopItemDetailModel *detailModel = [self.transferInfoItemsArr objectAtIndex:index];
//
//    if (!detailModel.isrecite || !detailModel.iswatch || detailModel.score == 0){
//        [self.successButton setTitle:@"下一节" forState:UIControlStateNormal];
//
//        [self.successButton buttonWithBlock:^(UIButton *button) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            for (UIViewController *controller in strongSelf.navigationController.childViewControllers){
//                if ([controller isKindOfClass:[TSTeachBopClassViewController class]]){
//                    TSTeachBopClassViewController *mainController = (TSTeachBopClassViewController *)controller;
//                    [mainController playItemWithIndex:index];
//                    [strongSelf.navigationController popToViewController:mainController animated:YES];
//                }
//            }
//        }];
//    } else {            // 完成
//        [self.successButton setTitle:@"完成" forState:UIControlStateNormal];
//        [self.successButton buttonWithBlock:^(UIButton *button) {
//            if (!weakSelf){
//                return ;
//            }
//            __strong typeof(weakSelf)strongSelf = weakSelf;
//            [strongSelf successManager];
//        }];
//    }
    
    
    
}


@end
