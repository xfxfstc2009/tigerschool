//
//  TSTeachBopClassViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,TSTeachBopClassType) {
    TSTeachBopClassTypeLisen,             /**< 听一听*/
    TSTeachBopClassTypeSpeak,             /**< 说一说*/
    TSTeachBopClassTypeRead,              /**< 读一读*/
};

@interface TSTeachBopClassViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferDetailId;
@property (nonatomic,copy)NSString *transferCourseId;
@property (nonatomic,assign)TSTeachBopClassType transferType;
@property (nonatomic,assign)BOOL hasLastItem;                   /**< 判断是否是最后一个，如果是最后一个的话，就不强制听完*/

-(void)playItemWithIndex:(NSInteger)index;
@end
