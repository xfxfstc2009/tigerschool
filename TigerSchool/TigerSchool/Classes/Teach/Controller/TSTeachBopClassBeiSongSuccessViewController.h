//
//  TSTeachBopClassBeiSongSuccessViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/2.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"
#import "TSTeachBopResultModel.h"
#import "TSTeachDetailViewController.h"
@class TSTeachDetailViewController;
@interface TSTeachBopClassBeiSongSuccessViewController : AbstractViewController

@property (nonatomic,strong)NSArray *transferNeedInfoModel;
@property (nonatomic,strong)NSArray *transferEcodeArr;


@property (nonatomic,copy)NSString *transferCourseId;
@property (nonatomic,copy)NSString *transferItemId;
@property (nonatomic,copy)NSString *transferDetailId;

@property (nonatomic,strong)NSArray *transferInfoItemsArr;

-(void)reBackWithBlock:(void(^)())block;                // 重新背诵

@end
