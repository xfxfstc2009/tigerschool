//
//  TSTeachDetailViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachDetailViewController.h"
#import "TSTeachDetailViewTopView.h"
#import "NetworkAdapter+TSTeach.h"
#import "TSBopDetailViewTableViewCell.h"
#import "TSTeachBopClassViewController.h"
#import "TSMusicPopImgView.h"

@interface TSTeachDetailViewController ()<UITableViewDelegate,UITableViewDataSource>{
    TSTeachBopDetailItemsRootModel *detailRootModel;
    BOOL hasShowAllAnimation;
}
@property (nonatomic,strong)UITableView *bopDetailTableView;
@property (nonatomic,strong)NSMutableArray *bopDetailMutableArr;
@property (nonatomic,strong)TSTeachDetailViewTopView *topView;

@end

@implementation TSTeachDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTopView];
    [self createTableView];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self sendRequestGetInfoDetail];
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    hasShowAllAnimation = NO;
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"Bop课程";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:@"课程介绍" barNorImage:nil barHltImage:nil action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [TSMTAManager MTAevent:MTAType_Tool_BopDetail str:@""];                 // 埋点
        
        PDWebViewController *webViewController = [[PDWebViewController alloc]init];
        [webViewController webDirectedWithBopWebHTMLWithDetailId:strongSelf.transferCourseId];
        [strongSelf.navigationController pushViewController:webViewController animated:YES];
        
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.bopDetailMutableArr = [NSMutableArray array];
    hasShowAllAnimation = YES;
}

#pragma mark - createTopView
-(void)createTopView{
    self.topView = [[TSTeachDetailViewTopView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, TSFloat(114))];
    self.topView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.topView.layer.shadowOpacity = .2f;
    self.topView.layer.shadowOffset = CGSizeMake(.2f, .2f);
    [self.view addSubview:self.topView];
}

#pragma mark - UITableView
-(void)createTableView{
    self.bopDetailTableView = [TSViewTool gwCreateTableViewRect:CGRectMake(0, CGRectGetMaxY(self.topView.frame) + TSFloat(3), kScreenBounds.size.width, self.view.size_height - CGRectGetMaxY(self.topView.frame) - TSFloat(3))];
    self.bopDetailTableView.dataSource = self;
    self.bopDetailTableView.delegate = self;
    [self.view addSubview:self.bopDetailTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.bopDetailMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    TSBopDetailViewTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[TSBopDetailViewTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
    }
    cellWithRowOne.transferBopDetailModel = [self.bopDetailMutableArr objectAtIndex:indexPath.row];
    [cellWithRowOne loadContentWithIndex:indexPath.row + 1];
    __weak typeof(self)weakSelf = self;
    [cellWithRowOne readyButtonActionClickBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        TSTeachBopDetailItemsModel *singleModel = [strongSelf.bopDetailMutableArr objectAtIndex:indexPath.row];
        
        TSTeachBopClassViewController *bopClassViewController = [[TSTeachBopClassViewController alloc]init];
        bopClassViewController.transferDetailId = singleModel.ID;
        bopClassViewController.transferCourseId = strongSelf.transferCourseId;
        [TSMusicPopImgView sharedMusicView].bopSingleModel = singleModel;
        [TSMusicPopImgView sharedMusicView].bopSingleModel.transferCourseId = strongSelf.transferCourseId;
        if (singleModel == [strongSelf.bopDetailMutableArr lastObject]){
            bopClassViewController.hasLastItem = YES;
        }
        [strongSelf.navigationController pushViewController:bopClassViewController animated:YES];
    }];
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [TSBopDetailViewTableViewCell calculationCellHeight];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    // 移除窗口视图
    [[GWMusicPlayerMethod sharedLocationManager] actionStopAndRemoveFromWindow];
    
    TSTeachBopDetailItemsModel *singleModel = [self.bopDetailMutableArr objectAtIndex:indexPath.row];
    
    if (singleModel.iswatch){           // 表示可以跳转
        TSTeachBopClassViewController *bopClassViewController = [[TSTeachBopClassViewController alloc]init];
        bopClassViewController.transferDetailId = singleModel.ID;
        bopClassViewController.transferCourseId = self.transferCourseId;
        [TSMusicPopImgView sharedMusicView].bopSingleModel = singleModel;
        [TSMusicPopImgView sharedMusicView].bopSingleModel.transferCourseId = self.transferCourseId;
        [self.navigationController pushViewController:bopClassViewController animated:YES];
        
        [TSMTAManager MTAevent:MTAType_Tool_BopClass str:singleModel.ID];                 // 埋点
    } else {
        if (singleModel.unlock){                // 表示可以跳转
            TSTeachBopClassViewController *bopClassViewController = [[TSTeachBopClassViewController alloc]init];
            bopClassViewController.transferDetailId = singleModel.ID;
            bopClassViewController.transferCourseId = self.transferCourseId;
            [TSMusicPopImgView sharedMusicView].bopSingleModel = singleModel;
            [TSMusicPopImgView sharedMusicView].bopSingleModel.transferCourseId = self.transferCourseId;
            [self.navigationController pushViewController:bopClassViewController animated:YES];
            [TSMTAManager MTAevent:MTAType_Tool_BopClass str:singleModel.ID];                 // 埋点
        } else {
            [StatusBarManager statusHiddenWithString:@"本小节暂未解锁"];
            return;
        }
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.bopDetailTableView) {
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType = SeparatorTypeBottom;
        } else if ([indexPath row] == [self.bopDetailMutableArr count] - 1) {
            separatorType = SeparatorTypeBottom;
        } else {
            separatorType = SeparatorTypeBottom;
        }
        if ([self.bopDetailMutableArr  count] == 1) {
            separatorType = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithType:separatorType];
    }
    
    // 动画
    static CGFloat initialDelay = 0.2f;
    static CGFloat stutter = 0.06f;
    TSTeachBopDetailItemsModel *singleModel = [self.bopDetailMutableArr objectAtIndex:indexPath.row];
    if (!singleModel.hasAnimation && hasShowAllAnimation == YES){
        TSBopDetailViewTableViewCell *musicInfoCell = (TSBopDetailViewTableViewCell *)cell;
        [musicInfoCell startAnimationWithDelay:initialDelay + ((indexPath.row) * stutter)];
        singleModel.hasAnimation = YES;
    }
}




-(void)sendRequestGetInfoDetail{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachBopWithCourseId:self.transferCourseId block:^(TSTeachBopDetailItemsRootModel *detailModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->detailRootModel = detailModel;
        if (strongSelf.bopDetailMutableArr.count){
            [strongSelf.bopDetailMutableArr removeAllObjects];
        }
        // top
        strongSelf.topView.transferDetailModel = detailModel;
        // 刷新tableView
        [strongSelf.bopDetailMutableArr addObjectsFromArray:detailModel.detailitems];
        [strongSelf.bopDetailTableView reloadData];
    }];
}
@end
