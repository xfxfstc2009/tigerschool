//
//  TSTeachBopClassViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachBopClassViewController.h"
#import "NetworkAdapter+TSTeach.h"

#import "GWMusicPlayerMethod.h"

#import "TSBopClassLrcTableViewCell.h"                  // 歌词
#import "TSLyricParser.h"                               // 歌词
#import "TSTeachBopClassBeiyibeiViewController.h"       // 背诵
#import "TSTeachDetailViewTopPlayerView.h"              // pop的播放
#import "GWMusicPlayerMethod.h"
#import "IQAudioRecorderViewController.h"
#import "TSTeachBopEndView.h"


@interface TSTeachBopClassViewController ()<UITableViewDelegate,UITableViewDataSource,GWMusicSingleModelDelegate>{
    TSBopItemDetailModel *singleModel;
    BOOL hasFinished;               // 是否完成过
    BOOL hasPushed;
}
@property (nonatomic,strong)UIView *bottomView;
@property (nonatomic,strong)UITableView *bopClassTableView;
@property (nonatomic,strong)NSArray *lyricList;             // 歌词数组
@property (nonatomic,assign) NSInteger currentLcrIndex; //当前歌词所在行
@property (nonatomic,assign) BOOL isDrag;

// itemsArr
@property (nonatomic,strong)NSMutableArray *itemsMutableArr;

// 背诵
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)UIButton *leftButton;

// action
@property (nonatomic,strong)TSTeachDetailViewTopPlayerView *actionPlayerView;
@property (nonatomic,strong)UIPageControl *pageControl;

@property (nonatomic,strong)TSTeachBopEndView *numberView;

@end

@implementation TSTeachBopClassViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
    if (hasPushed){
        [self playItemWithIndex:self.pageControl.currentPage];
        hasPushed = NO;
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createPageControl];
    [self arrayWithInit];
    [self createBottomView];                // 1. 创建底部的view
    [self sendRequestToGetInfo];
    [self createNumberView];                // 创建分数view
    // 添加观察者
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(musicStatusNotification:) name:GWMusicNotification object:nil];
    // 进行判断
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.view.backgroundColor = RGB(241, 241, 241, 1);
    self.barMainTitle = @"Bop课程";
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.backgroundColor = [UIColor clearColor];
    self.leftButton.frame = CGRectMake(TSFloat(19), TSFloat(30) + TSFloat(40), TSFloat(44), TSFloat(44));
    __weak typeof(self)weaKSelf = self;
    [self.leftButton setImage:[UIImage imageNamed:@"icon_main_back"] forState:UIControlStateNormal];
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weaKSelf){
            return ;
        }
        __strong typeof(weaKSelf)strongSelf = weaKSelf;
        if (strongSelf.navigationController.childViewControllers.count == 1){
            [strongSelf.navigationController dismissViewControllerAnimated:YES completion:NULL];
            return;
        } else {
            [strongSelf.navigationController popViewControllerAnimated:YES];
        }
    }];
    [self.view addSubview:self.leftButton];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.itemsMutableArr = [NSMutableArray array];
    hasPushed = NO;
}

#pragma mark - createBottomView
-(void)createBottomView{
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    self.bottomView.frame = CGRectMake(TSFloat(25), CGRectGetMaxY(self.leftButton.frame) + TSFloat(45), kScreenBounds.size.width - 2 * TSFloat(25), self.view.size_height - TSFloat(20) * 2 - 88 - 50 - CGRectGetMaxY(self.leftButton.frame));
    self.bottomView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.bottomView.layer.shadowOpacity = .2f;
    self.bottomView.layer.shadowOffset = CGSizeMake(.2f, .2f);
    [self.view addSubview:self.bottomView];
   
    // 创建播放按钮
    [self createLeftPlayerButton];
    // 创建歌词
    [self createTableView];                 // 2.
    
    // 3. 创建按钮
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.frame = CGRectMake((kScreenBounds.size.width - TSFloat(150)) / 2., CGRectGetMaxY(self.bottomView.frame) + TSFloat(11), TSFloat(150), TSFloat(44));
    [self.actionButton setBackgroundImage:[TSTool stretchImageWithName:@"icon_button_hlt"] forState:UIControlStateNormal];
    [self.actionButton setTitle:@"背一背" forState:UIControlStateNormal];
    self.actionButton.hidden = YES;
    self.actionButton.alpha = 0;
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 关闭声音
        [[GWMusicPlayerMethod sharedLocationManager].streamer stop];
        // 按钮关闭
        strongSelf.actionPlayerView.leftButton.buttonState = iQiYiPlayButtonStatePlay;
        // 移除window视图
        [[GWMusicPlayerMethod sharedLocationManager] actionStopAndRemoveFromWindow];
        // 进行跳转
        TSTeachBopClassBeiyibeiViewController *beiyibeiVC = [[TSTeachBopClassBeiyibeiViewController alloc]init];
        beiyibeiVC.transferLrcArr = strongSelf.lyricList;
        beiyibeiVC.transferCourseId = strongSelf.transferCourseId;
        beiyibeiVC.transferDetailId = strongSelf.transferDetailId;
        beiyibeiVC.transferItemId = strongSelf->singleModel.ID;
        beiyibeiVC.transferInfoItemsArr = strongSelf.itemsMutableArr;
        strongSelf->hasPushed = YES;
        [strongSelf.navigationController pushViewController:beiyibeiVC animated:YES];
        
        // 埋点
        [TSMTAManager MTAevent:MTAType_Tool_Bop_Beiyibei str:@""];                 // 埋点
    }];
    [self.view addSubview:self.actionButton];
}

#pragma mark - createView
-(void)createTableView{
    self.bopClassTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
    self.bopClassTableView.frame = CGRectMake(TSFloat(25), TSFloat(80), self.bottomView.size_width - 2 * TSFloat(25), self.bottomView.size_height - TSFloat(80) - TSFloat(50));
    self.bopClassTableView.dataSource = self;
    self.bopClassTableView.delegate = self;
    [self.bottomView addSubview:self.bopClassTableView];
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.lyricList.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    TSBopClassLrcTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[TSBopClassLrcTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferLrcModel = [self.lyricList objectAtIndex:indexPath.row];
    if (indexPath.row == self.currentLcrIndex) {
        [cellWithRowOne reloadCellForSelect:YES];
    }else{
        [cellWithRowOne reloadCellForSelect:NO];
    }
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [TSBopClassLrcTableViewCell calculationCellHeightWithText: [self.lyricList objectAtIndex:indexPath.row]];
}


-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachBopDetailWithDetailId:self.transferDetailId block:^(TSBopItemDetailListModel *singleRootModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.itemsMutableArr addObjectsFromArray:singleRootModel.items];
        
        // 2. 添加pageControl
        strongSelf.pageControl.numberOfPages = strongSelf.itemsMutableArr.count;
        
        // 3. 播放内容
        [strongSelf playItemWithIndex:0];
        
        // 4. 判断
        [strongSelf jiexiManager];
        
        // 5. 判断是否最后一个执行方法
        [strongSelf hasLastItemManager];
    }];
}

#pragma mark - 创建audioManager
-(void)createAudioManager:(NSArray<GWMusicSingleModel> *)musicList{
    [[GWMusicPlayerMethod sharedLocationManager] playWithMusicList:musicList playIndex:0];
    [GWMusicPlayerMethod sharedLocationManager].delegate = self;
    if (self.actionPlayerView){
        self.actionPlayerView.leftButton.buttonState = iQiYiPlayButtonStatePause;
    }
}

-(void)musicProgress:(NSTimeInterval)currentprogress duration:(NSTimeInterval)duration{
    //更新歌词
    [self updateMusicLrcForRowWithCurrentTime:currentprogress * 1000];
}


//逐行更新歌词
- (void)updateMusicLrcForRowWithCurrentTime:(NSTimeInterval)currentTime {
    for (int i = 0; i < self.lyricList.count; i ++) {
        TSLyricModel *model = [self.lyricList objectAtIndex:i];
        
        NSInteger next = i + 1;
        
        TSLyricModel *nextLrcModel = nil;
        if (next < self.lyricList.count) {
            nextLrcModel = self.lyricList[next];
        }
        
        if (self.currentLcrIndex != i && currentTime >= model.time) {
            BOOL show = NO;
            if (nextLrcModel) {
                if (currentTime < nextLrcModel.time) {
                    show = YES;
                }
            } else{
                show = YES;
            }
            
            if (show) {
                NSIndexPath *currentIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
                NSIndexPath *previousIndexPath = [NSIndexPath indexPathForRow:self.currentLcrIndex inSection:0];
                
                self.currentLcrIndex = i;
                
                TSBopClassLrcTableViewCell *currentCell = [self.bopClassTableView cellForRowAtIndexPath:currentIndexPath];
                TSBopClassLrcTableViewCell *previousCell = [self.bopClassTableView cellForRowAtIndexPath:previousIndexPath];
                
                //设置当前行的状态
                [currentCell reloadCellForSelect:YES];
                //取消上一行的选中状态
                [previousCell reloadCellForSelect:NO];
                
                if (!self.isDrag) {
                    [self.bopClassTableView scrollToRowAtIndexPath:currentIndexPath atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
                }
            }
        }
        
        if (self.currentLcrIndex == i) {
            TSBopClassLrcTableViewCell *cell = [self.bopClassTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:i inSection:0]];
            
            CGFloat totalTime = 0;
            if (nextLrcModel) {
                totalTime = nextLrcModel.time - model.time;
            }else{
                totalTime = [GWMusicPlayerMethod sharedLocationManager].streamer.duration - model.time;
            }
            CGFloat progressTime = currentTime - model.time;
            cell.lrcLabel.progress = progressTime / totalTime;
        }
    }
}

#pragma mark == UIScrollViewDelegate
- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.isDrag = YES;
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    self.isDrag = NO;
    [self.bopClassTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.currentLcrIndex inSection:0] atScrollPosition:UITableViewScrollPositionMiddle animated:YES];
}


#pragma mark - 创建左侧的播放按钮
-(void)createLeftPlayerButton{
    self.actionPlayerView = [[TSTeachDetailViewTopPlayerView alloc]initWithFrame:CGRectMake(TSFloat(19), TSFloat(21), self.bottomView.size_width - 2 * TSFloat(19), TSFloat(37))];
    __weak typeof(self)weakself = self;
    [self.actionPlayerView playerButtonActionClickBlock:^(iQiYiPlayButtonState status) {
        if (!weakself){
            return ;
        }
        __strong typeof(weakself)strongSelf = weakself;
        if (strongSelf->hasFinished && [GWMusicPlayerMethod sharedLocationManager].streamer.status != DOUAudioStreamerPlaying){
            [[GWMusicPlayerMethod sharedLocationManager] actionNextWithBlock:NULL];
            strongSelf->hasFinished = NO;
        } else {
            [[GWMusicPlayerMethod sharedLocationManager] actionPlayPause];
        }
    }];
    [self.bottomView addSubview:self.actionPlayerView];
}

-(void)musicStatusNotification:(NSNotification *)note{
    if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPlaying){ // 正在播放中
        
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPaused){   // 暂停
        
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerIdle){   // 等待任务
        
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerFinished){   // 完成
        [self lisenEndManager];
    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerBuffering){   //  缓冲

    } else if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerError){   //  缓冲
        
    }
}

#pragma mark - 表示听完的方法
-(void)lisenEndManager{
    // 0.完成按钮
    hasFinished = YES;
    // 1. 按钮换成待播放
    self.actionPlayerView.leftButton.buttonState = iQiYiPlayButtonStatePlay;
    // 2.背一背按钮显示
    self.actionButton.hidden = NO;
    [UIView animateWithDuration:.5f animations:^{
        self.actionButton.alpha = 1;
    } completion:^(BOOL finished) {
        self.actionButton.alpha = 1;
        self.actionButton.hidden = NO;
    }];
    // 3. 提交已经听完
    if (!singleModel.isrecite){
        [self sendRequestToUploadInfo];
    }
}

#pragma mark - interface提交
-(void)sendRequestToUploadInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachToolWatchLog:self.transferDetailId courseid:self.transferCourseId itemid:singleModel.ID score:-1 str:@"" recite:0 block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        if (isSuccessed){
            [StatusBarManager statusHiddenWithString:@"本小节已学习完毕"];
        }
    }];
}

#pragma mark  创建pageControl
-(void)createPageControl{
    if (!self.pageControl){
        self.pageControl = [[UIPageControl alloc]init];
        self.pageControl.frame = CGRectMake(CGRectGetMaxX(self.leftButton.frame) + TSFloat(30), 0, kScreenBounds.size.width - 2 * (CGRectGetMaxX(self.leftButton.frame) + TSFloat(30)), 50);
        self.pageControl.center_y = self.leftButton.center_y;
        self.pageControl.backgroundColor = [UIColor clearColor];
        [self.pageControl addTarget:self action:@selector(pageTurn:) forControlEvents:UIControlEventValueChanged];  //用户点击UIPageControl的响应函数
        [self.pageControl setValue:[UIImage imageNamed:@"xingxing_hong"] forKeyPath:@"_currentPageImage"];
        [self.pageControl setValue:[UIImage imageNamed:@"xingxing_hui"] forKeyPath:@"_pageImage"];
        [self.view addSubview:self.pageControl];
    }
}

- (void)pageTurn:(UIPageControl*)sender {
    [self playItemWithIndex:sender.currentPage];
}

-(void)playItemWithIndex:(NSInteger)index{
    TSBopItemDetailModel *itemsDetailModel = [self.itemsMutableArr objectAtIndex:index];
    singleModel = itemsDetailModel;
    
    // 2. pageControl
    self.pageControl.currentPage = index;
    hasPushed = NO;
    // 4. 分数
    if (itemsDetailModel.isrecite){          // 表示已经背完
        // 1. 显示分数
        [self numberViewWithShowWithScore:singleModel.score];

        // 2. 按钮显示
        self.actionButton.hidden = NO;
        [UIView animateWithDuration:.5f animations:^{
            self.actionButton.alpha = 1;
        } completion:^(BOOL finished) {
            self.actionButton.alpha = 1;
            self.actionButton.hidden = NO;
        }];
        // 3. 当前声音进行暂停
    } else {
        [UIView animateWithDuration:.5f animations:^{
            self.actionButton.alpha = 0;
        } completion:^(BOOL finished) {
            self.actionButton.alpha = 0;
            self.actionButton.hidden = YES;
        }];
        // 隐藏数据
        [self numberViewWithShowWithScore:singleModel.score];
    }
    
    GWMusicSingleModel *musicSingleModel = [[GWMusicSingleModel alloc]init];
    musicSingleModel.img = @"123";
    musicSingleModel.lrc = itemsDetailModel.url;
    musicSingleModel.music = itemsDetailModel.voice;
    musicSingleModel.name = @"Bop课程";
    
    // 解析歌词
    self.lyricList = [TSLyricParser lyricParserWithUrl:musicSingleModel.lrc isDelBlank:YES];
    
    if (self.lyricList.count == 0) {
        NSLog(@"123");
    }else {
        [self.bopClassTableView reloadData];
    }
    
    NSMutableArray<GWMusicSingleModel> *musicListArr = [NSMutableArray<GWMusicSingleModel> array];
    [musicListArr addObject:musicSingleModel];
    [self createAudioManager:musicListArr];
}


#pragma mark - 判断
-(void)createNumberView{
    // 创建分数背景
    if (!self.numberView){
        self.numberView = [[TSTeachBopEndView alloc]initWithFrame:CGRectMake(kScreenBounds.size.width / 2.,  self.bottomView.orgin_y, kScreenBounds.size.width / 2., TSFloat(57))];
    }
    
    [self.view addSubview:self.numberView];
}

-(void)numberViewWithShowWithScore:(NSInteger)score{
    if (score != 0){
        [self.numberView showAnimationWithNumber:score];
    } else {
        [self.numberView hidenAnimationWithNumber:score];
    }
}

-(void)jiexiManager{
    BOOL hasReadEnd = YES;
    for (int i = 0 ; i < self.itemsMutableArr.count;i++){
        TSBopItemDetailModel *detailModel = [self.itemsMutableArr objectAtIndex:i];
        if (!detailModel.isrecite){
            hasReadEnd = NO;
        }
    }
    
    self.pageControl.userInteractionEnabled = hasReadEnd;
}

#pragma mark - 判断是否最后一个，如果是最后一个的话，就不强制听完
-(void)hasLastItemManager{
    if (self.hasLastItem){
        [self lisenEndManager];
    }
}

@end
