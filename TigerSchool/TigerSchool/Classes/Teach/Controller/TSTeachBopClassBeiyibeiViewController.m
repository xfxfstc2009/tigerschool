//
//  TSTeachBopClassBeiyibeiViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/24.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachBopClassBeiyibeiViewController.h"
#import "TSBaiduSoundsManager.h"
#import "TSBopBeiyibeiSingleTableViewCell.h"
#import "TSTeachBopResultModel.h"
#import "TSLyricModel.h"
#import "TSTeachBopClassBeiSongSuccessViewController.h"
#import "TSBopSoundsWaveView.h"
#import <AVFoundation/AVFoundation.h>


@interface TSTeachBopClassBeiyibeiViewController ()<TSBaiduSoundsManagerDelegate,UITableViewDelegate,UITableViewDataSource,AVAudioRecorderDelegate>{
    TSTeachBopResultMainModel *tempResponseModelObject;
    NSMutableArray *baiduEcodeMutableArr;           // 百度识别的内容
    CADisplayLink *meterUpdateDisplayLink;
    AVAudioRecorder *_audioRecorder;                // 录音
    NSString *_recordingFilePath;
}
@property (nonatomic,assign)NSInteger userReadIndex;
@property (nonatomic,strong)TSBaiduSoundsManager *manager;
@property (nonatomic,strong)UIView *bottomView;
@property (nonatomic,strong)UITableView *bopTableView;
@property (nonatomic,strong)NSMutableArray *infoMainArr;
@property (nonatomic,strong)NSMutableArray *resultArr;
// 用来判断是否阅读完毕
@property (nonatomic,strong)NSTimer *readTimer;
@property (nonatomic,assign)BOOL hasReadEnd;
// 完成按钮
@property (nonatomic,strong)UIButton *resultButton;
// 创建模式
@property (nonatomic,strong)UIButton *typeButton;
// 创建
@property (nonatomic,strong)TSBopSoundsWaveView *waveView;
// 当前的背诵模式
@property (nonatomic,assign)BeiSongType transferBeisongType;
//
@property (nonatomic,strong)UIButton *leftButton;

@end

@implementation TSTeachBopClassBeiyibeiViewController

-(void)dealloc{
    NSLog(@"123");
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
    [self startUpdatingMeter];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self releaseSounds];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self soundsManagerWithInit];
    [self arrayWithInit];
    [self createBottomView];
    [self createRecorder];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.infoMainArr = [NSMutableArray array];
    self.resultArr = [NSMutableArray array];
    baiduEcodeMutableArr = [NSMutableArray array];
    self.userReadIndex = 0;
    // 数据整理
    [self dataWithInit];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"背诵";
    self.transferBeisongType = BeiSongTypeAllBei;
    __weak typeof(self)weakSelf = self;
    self.leftButton = [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_back"] barHltImage:[UIImage imageNamed:@"icon_main_back"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"你确认要退出吗" message:nil buttonTitles:@[@"确定",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 0){
                [strongSelf.navigationController popViewControllerAnimated:YES];
            }
        }]show];
    }];
}

#pragma mark - createBottomView
-(void)createBottomView{ 
    self.bottomView = [[UIView alloc]init];
    self.bottomView.backgroundColor = [UIColor whiteColor];
    self.bottomView.frame = CGRectMake(TSFloat(25), TSFloat(26), kScreenBounds.size.width - 2 * TSFloat(25), self.view.size_height - TSFloat(20) * 2 - 88 - 50 - 100);
    self.bottomView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.bottomView.layer.shadowOpacity = .2f;
    self.bottomView.layer.shadowOffset = CGSizeMake(.2f, .2f);
    [self.view addSubview:self.bottomView];
    
    if (self.transferBeisongType == BeiSongTypeAllBei){
      [self.bottomView addSubview:[self allBeisongManager:self.bottomView]];
    } else {
        [self createTableView];
    }
    
    // 创建完成按钮
    self.resultButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.resultButton setBackgroundImage:[TSTool stretchImageWithName:@"icon_button_hlt"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [self.resultButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf beisongVCTransferManger];
    }];
    self.resultButton.frame = CGRectMake((kScreenBounds.size.width - TSFloat(150)) / 2., CGRectGetMaxY(self.bottomView.frame) + TSFloat(20), TSFloat(150), TSFloat(44));
    [self.resultButton setTitle:@"完成" forState:UIControlStateNormal];
    
    [self.view addSubview:self.resultButton];
    
    // 创建模式状态按钮
    self.typeButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.typeButton.frame = CGRectMake(self.bottomView.size_width - TSFloat(50), 0, TSFloat(50), TSFloat(50));
    self.typeButton.backgroundColor = UURandomColor;
    [self.typeButton setTitle:@"类型" forState:UIControlStateNormal];
    [self.bottomView addSubview:self.typeButton];
    [self.typeButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        NSLog(@"123");
    }];
    
    // 创建waveView
    self.waveView = [[TSBopSoundsWaveView alloc]initWithFrame:CGRectMake(0, self.bottomView.size_height - 250, self.bottomView.size_width, 250)];
    
    [self.bottomView addSubview:self.waveView];
    
    self.typeButton.hidden = YES;
}


-(void)soundsManagerWithInit{
    self.manager = [[TSBaiduSoundsManager alloc]init];
    self.manager.delegate = self;
    [self.manager soundsWithInit];
    
    [self.manager actionToRead];
}

-(void)soundsCallBackWithModelInfo:(TSTeachBopResultMainModel *)responseModelObject{
    // 1. 第一步，进行数据保存
    tempResponseModelObject = responseModelObject;
    // 2. 是否已读设为未读
    self.hasReadEnd = NO;
    // 3. 第二步，开启计时器
    [self startTimer];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.bopTableView){
        self.bopTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.bopTableView.frame = CGRectMake(TSFloat(25), TSFloat(50), self.bottomView.size_width - 2 * TSFloat(25), self.bottomView.size_height - TSFloat(50) - TSFloat(100));
        self.bopTableView.dataSource = self;
        self.bopTableView.delegate = self;
        [self.bottomView addSubview:self.bopTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.infoMainArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    TSBopBeiyibeiSingleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[TSBopBeiyibeiSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowOne.transferCellHeight = cellHeight;
    cellWithRowOne.transferBopResultModel = [self.infoMainArr objectAtIndex:indexPath.section];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    TSTeachBopReadedInfo *transferBopResultModel = [self.infoMainArr objectAtIndex:indexPath.section];
    return [TSBopBeiyibeiSingleTableViewCell calculationCellHeightWithModel:transferBopResultModel];
}


#pragma mark - 数据整理
-(void)dataWithInit{
    for (int i = 0 ;i < self.transferLrcArr.count;i++){
        TSTeachBopReadedInfo *model = [[TSTeachBopReadedInfo alloc]init];
        model.infoIndex = i;
        TSLyricModel *infoStr = [self.transferLrcArr objectAtIndex:i];
        model.mainInfo = infoStr.content;
        [self.infoMainArr addObject:model];
    }
}

#pragma mark - 计时器，用来计算阅读时间
-(void)startTimer{
    [self jiexiManager];
//    if (!self.readTimer){
//        self.readTimer = [NSTimer scheduledTimerWithTimeInterval:1.f target:self selector:@selector(daojishiManager) userInfo:nil repeats:YES];
//    }
}

-(void)stopTimer{
    if (self.readTimer){
        [self.readTimer invalidate];
        self.readTimer = nil;
    }
}

-(void)daojishiManager{
    if (self.hasReadEnd){           // 已读完
        [self jiexiManager];
    } else {                        // 没有读完
        self.hasReadEnd = YES;
    }
}

-(void)jiexiManager{
//    if (!self.hasReadEnd){           // 没读完
//        return;
//    } else {                         // 读完了
//        // 读完，关闭计时器
//        [self stopTimer];
    
        // 2. 进行识别
        // 2.1 第一步 获取当前需要解析的文案
//        TSTeachBopReadedInfo *needInfoModel = [self.infoMainArr objectAtIndex:self.userReadIndex];
//        // 2.2 第二步 讲当前需要解析的文案进行拆分
//        NSMutableArray *needChaifenArr = [NSMutableArray arrayWithCapacity:0];
//        [needInfoModel.mainTempInfo enumerateSubstringsInRange:NSMakeRange(0, needInfoModel.mainTempInfo.length) options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
//            [needChaifenArr addObject:substring];
//        }];
//        needInfoModel.needMainTagArr = needChaifenArr;
    
        // 2.3 第三步，进行百度识别语音的匹配拆分
        NSMutableArray *baiduBackMutableArr = [NSMutableArray array];
    [baiduBackMutableArr addObjectsFromArray:@[@"百",@"e",@"国",@"际",@",",@"。",@"，",@"、",@"；",@"?",@"？",@":",@";",@"",@"/"]];
        for (NSString *baiduSingleInfo in tempResponseModelObject.results_recognition){
            [baiduSingleInfo enumerateSubstringsInRange:NSMakeRange(0, baiduSingleInfo.length) options:NSStringEnumerationByComposedCharacterSequences usingBlock:^(NSString *substring, NSRange substringRange, NSRange enclosingRange, BOOL *stop) {
                if (![baiduBackMutableArr containsObject:substring]){
                    [baiduBackMutableArr addObject:substring];
                }
            }];
        }
        
        // 第四步 寻找当前的cell
        TSBopBeiyibeiSingleTableViewCell *cellWithRowOne = [self.bopTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:self.userReadIndex]];
        cellWithRowOne.transferBaiduEcodeArr = baiduBackMutableArr;
        
        [baiduEcodeMutableArr removeAllObjects];
        [baiduEcodeMutableArr addObjectsFromArray:baiduBackMutableArr];
        
        self.userReadIndex ++;
//    }
}

#pragma mark - 全局背诵
-(UIView *)allBeisongManager:(UIView *)bgView{
    UIView *allBeisongBgView = [[UIView alloc]init];
    allBeisongBgView.backgroundColor = [UIColor clearColor];
    
    // 1. 创建view
    TSImageView *soundsImgView = [[TSImageView alloc]init];
    soundsImgView.backgroundColor = [UIColor clearColor];
    soundsImgView.image = [UIImage imageNamed:@"ic_lrc_record.gif"];
    soundsImgView.frame = CGRectMake(TSFloat(30), TSFloat(28), TSFloat(16), TSFloat(23));
    [allBeisongBgView addSubview:soundsImgView];
    
    // 2. 创建录音中
    UILabel *luyinzhongLabel = [TSViewTool createLabelFont:@"小正文" textColor:@"黑"];
    luyinzhongLabel.text = @"录音中";
    CGSize luyinSize = [TSTool makeSizeWithLabel:luyinzhongLabel];
    [allBeisongBgView addSubview:luyinzhongLabel];
    luyinzhongLabel.frame = CGRectMake(CGRectGetMaxX(soundsImgView.frame) + TSFloat(11), 0, luyinSize.width, [NSString contentofHeightWithFont:luyinzhongLabel.font]);
    luyinzhongLabel.center_y = soundsImgView.center_y;
    
    // 3. 创建fixedLabel
    UILabel *fixedLabel = [TSViewTool createLabelFont:@"" textColor:@"黑"];
    fixedLabel.textColor = [UIColor hexChangeFloat:@"6D7984"];
    fixedLabel.font = [UIFont systemFontOfCustomeSize:18];
    [allBeisongBgView addSubview:fixedLabel];
    fixedLabel.text = @"每一次大声开口，每一次坚持练习，都会让我离成功更近一步。";
    fixedLabel.numberOfLines = 0;
    CGSize fixedSize = [fixedLabel.text sizeWithCalcFont:fixedLabel.font constrainedToSize:CGSizeMake(bgView.size_width - 2 * TSFloat(24), CGFLOAT_MAX)];
    fixedLabel.frame = CGRectMake(TSFloat(24), CGRectGetMaxY(soundsImgView.frame) + TSFloat(32), bgView.size_width - 2 * TSFloat(24), fixedSize.height);
    
    
    return allBeisongBgView;
}


-(void)beisongVCTransferManger{
    TSTeachBopClassBeiSongSuccessViewController *infoVC = [[TSTeachBopClassBeiSongSuccessViewController alloc]init];
    infoVC.hasCancelPanDismiss = YES;
    infoVC.transferNeedInfoModel = self.infoMainArr;
    infoVC.transferItemId = self.transferItemId;
    infoVC.transferCourseId = self.transferCourseId;
    infoVC.transferDetailId = self.transferDetailId;
    infoVC.transferEcodeArr = baiduEcodeMutableArr;
    __weak typeof(self)weakSelf = self;
    [infoVC reBackWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf createRecorder];
        [strongSelf soundsManagerWithInit];
    }];
    infoVC.transferInfoItemsArr = self.transferInfoItemsArr;
    

    [self.navigationController pushViewController:infoVC animated:YES];
}

-(void)releaseSounds{
    if (_audioRecorder){
        _audioRecorder.delegate = nil;
        [_audioRecorder stop];
        _audioRecorder = nil;
    }
    
    if (self.manager){
        [self.manager actionToStop];
        
        self.manager.delegate = nil;
        self.manager = nil;
    }
    [meterUpdateDisplayLink invalidate];
}

-(void)startUpdatingMeter{
    [meterUpdateDisplayLink invalidate];
    meterUpdateDisplayLink = [CADisplayLink displayLinkWithTarget:self selector:@selector(updateMeters)];
    [meterUpdateDisplayLink addToRunLoop:[NSRunLoop currentRunLoop] forMode:NSRunLoopCommonModes];
}

-(void)updateMeters{
    if (_audioRecorder.isRecording){
        [_audioRecorder updateMeters];
        
        CGFloat normalizedValue = pow (10, [_audioRecorder averagePowerForChannel:0] / 20);
        
        [self.waveView waveAnimationWithFloat:normalizedValue];
    } else {
        [self.waveView waveAnimationWithFloat:0];
    }
}

#pragma mark - 创建录音
-(void)createRecorder{
    
    NSString *globallyUniqueString = [NSProcessInfo processInfo].globallyUniqueString;
    _recordingFilePath = [NSTemporaryDirectory() stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.m4a",globallyUniqueString]];
    
    NSMutableDictionary *recordSettings = [[NSMutableDictionary alloc] init];
    recordSettings[AVFormatIDKey] = @(kAudioFormatAppleLossless);
    
    _audioRecorder = [[AVAudioRecorder alloc] initWithURL:[NSURL fileURLWithPath:_recordingFilePath] settings:recordSettings error:nil];
    _audioRecorder.delegate = self;
    _audioRecorder.meteringEnabled = YES;
    [_audioRecorder record];
}

-(CGFloat)getRandomNumber{
    return (arc4random() % 100) / 100.;
}
@end
