//
//  TSTeachBopClassBeiyibeiViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/24.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"

typedef NS_ENUM(NSInteger,BeiSongType) {
    BeiSongTypeGenBei,                  /**< 跟背模式*/
    BeiSongTypeAllBei,                  /**< 背诵模式*/
};

@interface TSTeachBopClassBeiyibeiViewController : AbstractViewController

@property (nonatomic,strong)NSArray *transferInfoItemsArr;

@property (nonatomic,strong)NSArray *transferLrcArr;                /**< 传递过去的lrc文件数组*/

@property (nonatomic,copy)NSString *transferCourseId;
@property (nonatomic,copy)NSString *transferItemId;
@property (nonatomic,copy)NSString *transferDetailId;

@end
