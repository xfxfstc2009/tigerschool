//
//  TSTeachAllMaterialViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachAllMaterialViewController.h"
#import "NetworkAdapter+TSTeach.h"
#import "TSCenterMyToolTableViewCell.h"
#import "TSTeachMaterialDetailViewController.h"
#import "TSHomeSearchListViewController.h"


#define kOriginalScrollView     @"assetOriginalScrollView"
#define kOriginalImageView      @"assetOriginalImageView"
#define kOriginalMaskImageView  @"assetOriginalMaskImageView"
#define kOriginalNaviBar        @"assetOriginalNaviBar"
#define kSelectionLabKey        @"assetSelectionLabKey"
#define kSelectionImageKey      @"assetSelectionOImageKey"

static char originalFrameKey;

@interface TSTeachAllMaterialViewController ()<HTHorizontalSelectionListDataSource,HTHorizontalSelectionListDelegate,UIScrollViewDelegate,UITableViewDataSource,UITableViewDelegate>{
    TSMaterialSegmentListModel *tempMaterialSingleModel;
    CGFloat _yOffset;
}
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;         /**< segment*/
@property (nonatomic,strong)UIScrollView *tableViewMainScrollView;          /**< scrollView*/


@property (nonatomic,strong)NSMutableArray *segmentMutableArr;              /**< segment数组*/
@property (nonatomic,strong)NSMutableDictionary *tableViewMutableDic;       /**< 列表的字典*/
@property (nonatomic,strong)NSMutableDictionary *dataSourceMutableDic;      /**< 数据源字典*/
@property (nonatomic,strong)NSMutableArray *currentMutableArr;              /**< 当前的数组*/
@property (nonatomic,strong)NSMutableDictionary *pointYMutableDic;          /**< 高度*/

@property (nonatomic,strong)UIScrollView *headerScrollView;                 /**< */
@property (nonatomic,strong)UIView *headerView;
@property (nonatomic,assign)CGFloat pointY;

@end

@implementation TSTeachAllMaterialViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];                               // 1. 数据初始化
    [self createHeaderView];                            // 3.
    [self createBaseScrollView];                        // 2. 创建底部的scrollView
    
    
    __weak typeof(self)weakSelf = self;
    [weakSelf sendRequestToGetMaterialSegmentListWithBlock:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.transferSingleModel){
            for (int i = 0 ; i < strongSelf.segmentMutableArr.count;i++){
                TSMaterialSegmentModel *singleModel = [strongSelf.segmentMutableArr objectAtIndex:i];
                if ([singleModel.name isEqualToString:strongSelf.transferSingleModel.name]){
                    [strongSelf.segmentList setSelectedButtonIndex:i animated:YES];
                    [strongSelf hasTableAndManagerWithIndex:strongSelf.segmentList.selectedButtonIndex];
                    [strongSelf.tableViewMainScrollView setContentOffset:CGPointMake(i * kScreenBounds.size.width, 0) animated:YES];
                    return;
                }
            }
            
        } else {
            [strongSelf hasTableAndManagerWithIndex:strongSelf.segmentList.selectedButtonIndex];
        }
    }];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"全部素材";
    __weak typeof(self)weakSelf = self;
    [self rightBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_source_search"] barHltImage:[UIImage imageNamed:@"icon_source_search"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        TSHomeSearchListViewController *searchVC = [[TSHomeSearchListViewController alloc]init];
        [strongSelf.navigationController pushViewController:searchVC animated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.segmentMutableArr = [NSMutableArray array];                        // 1. segment
    
    self.tableViewMutableDic = [NSMutableDictionary dictionary];            // 2. tableView
    self.dataSourceMutableDic = [NSMutableDictionary dictionary];           // 3. 数据源
    self.pointYMutableDic = [NSMutableDictionary dictionary];               // 4. point Y
    self.currentMutableArr = [NSMutableArray array];                        // 5. 当前数据源
    
}

#pragma mark 创建头部的view
-(void)createHeaderView{
    // 3. 创建segment
    [self createSegment];
}

-(void)createTableHeadView:(UITableView *)tableView{
    
    UIView *tableHeaderView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, TSFloat(44))];
    tableHeaderView.backgroundColor = [UIColor clearColor];
    tableView.showsVerticalScrollIndicator = NO;
    tableView.tableHeaderView = tableHeaderView;
}

#pragma mark - Segment
-(void)createSegment{
    self.segmentList = [[HTHorizontalSelectionList alloc]initWithFrame: CGRectMake(0, 0, kScreenBounds.size.width, TSFloat(44))];
    self.segmentList.delegate = self;
    self.segmentList.dataSource = self;
    [self.segmentList setTitleColor:[UIColor colorWithCustomerName:@"黑"] forState:UIControlStateNormal];
    self.segmentList.selectionIndicatorColor = [UIColor colorWithCustomerName:@"红"];
    self.segmentList.bottomTrimColor = [UIColor clearColor];
    [self.segmentList setTitleColor:[UIColor grayColor] forState:UIControlStateHighlighted];
    self.segmentList.backgroundColor = [UIColor whiteColor];
//    self.segmentList.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
//    self.segmentList.layer.shadowOpacity = 1.0;
//    self.segmentList.layer.shadowOffset = CGSizeMake(.8f, .8f);
    [self.view addSubview:self.segmentList];
}

#pragma mark - HTHorizontalSelectionListDataSource
- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
    return self.segmentMutableArr.count;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index {
   TSMaterialSegmentModel *segmentItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
    return segmentItemSingleModel.name;
}

#pragma mark - HTHorizontalSelectionListDelegate
- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index {
    [self.tableViewMainScrollView setContentOffset:CGPointMake(index * kScreenBounds.size.width, 0) animated:YES];
    [self hasTableAndManagerWithIndex:index];
}

#pragma makr - 临时创建一个tableView
-(void)hasTableAndManagerWithIndex:(NSInteger)index{
    // 2. 判断当前是否存在tableView
    if (![self.tableViewMutableDic.allKeys containsObject:[NSString stringWithFormat:@"%li",(long)index]]){            // 如果不存在
        if (self.currentMutableArr.count){
            [self.currentMutableArr removeAllObjects];
        }
        UITableView *tableView = [self createTableView];                            // 创建tableView
        tableView.orgin_x = index *kScreenBounds.size.width;                        // 修改frame
//        CGFloat currentPointY = 0;
//        if (self.pointY > TSFloat(44) + 64){
//            currentPointY = TSFloat(200);
//        } else {
//            currentPointY = self.pointY;
//        }
//
//        [tableView setContentOffset:CGPointMake(0, currentPointY)];                   //
        [self.tableViewMutableDic setObject:tableView forKey:[NSString stringWithFormat:@"%li",(long)index]];
        
        // 2. 走接口
        __weak typeof(self)weakSelf = self;
        TSMaterialSegmentModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
        studyItemSingleModel.tableIndex = [NSString stringWithFormat:@"%li",(long)index];
        [weakSelf sendRequestToGetInfoWithID:studyItemSingleModel isHorizontal:NO];
    } else {            // 已经存在
//        UITableView *currentTableView = (UITableView *)[self.tableViewMutableDic objectForKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
//        CGFloat currentPointY = 0;
//        if (self.pointY > TSFloat(44) + 64){
//            NSLog(@"self.pointY====>%.2f",self.pointY);
//            NSLog(@"currentTableView.contentOffset.y====>%.2f",currentTableView.contentOffset.y);
//            currentPointY = MIN(self.pointY, currentTableView.contentOffset.y) ;
//            if (currentPointY > 0 && currentPointY < TSFloat(200)){
//                currentPointY = TSFloat(200);
//            }
//            if (currentTableView.contentOffset.y == 0){
//                currentPointY = TSFloat(200);
//            }
//
//        } else {
//            if (self.pointY > TSFloat(200)){
//                currentPointY = TSFloat(200);
//            } else {
//                currentPointY = self.pointY;
//            }
//        }
        
//        [currentTableView setContentOffset:CGPointMake(0, currentPointY) animated:YES];
        
        // 2. 走接口
        __weak typeof(self)weakSelf = self;
        TSMaterialSegmentModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:index];
        [weakSelf sendRequestToGetInfoWithID:studyItemSingleModel isHorizontal:YES];
    }
}

#pragma mark - createScrollView
-(void)createBaseScrollView{
    if (!self.tableViewMainScrollView){
        self.tableViewMainScrollView = [[UIScrollView alloc]init];
        self.tableViewMainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, self.view.size_height - self.segmentList.size_height - [PDMainTabbarViewController sharedController].tabBarHeight);
        self.tableViewMainScrollView.delegate = self;
        self.tableViewMainScrollView.backgroundColor = [UIColor clearColor];
        self.tableViewMainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width , self.tableViewMainScrollView.size_height);
        self.tableViewMainScrollView.pagingEnabled = YES;
        self.tableViewMainScrollView.alwaysBounceVertical = NO;
        self.tableViewMainScrollView.showsVerticalScrollIndicator = NO;
        self.tableViewMainScrollView.showsHorizontalScrollIndicator = NO;
        self.tableViewMainScrollView.alwaysBounceHorizontal = NO;
        
        [self.view addSubview:self.tableViewMainScrollView];
    }
}

#pragma mark - createTableView
-(UITableView *)createTableView{
    UITableView *tableView= [[UITableView alloc] initWithFrame:self.tableViewMainScrollView.bounds style:UITableViewStylePlain];
    tableView.delegate = self;
    tableView.orgin_y = 0;
    tableView.size_height = self.tableViewMainScrollView.size_height - self.tabBarController.tabBar.size_height - 64;
    tableView.dataSource = self;
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    tableView.showsVerticalScrollIndicator = YES;
    tableView.backgroundColor = [UIColor clearColor];
    [self.tableViewMainScrollView addSubview:tableView];
    
    __weak typeof(self)weakSelf = self;
    
    TSMaterialSegmentModel *studyItemSingleModel = [self.segmentMutableArr objectAtIndex:self.segmentList.selectedButtonIndex];
    // 1. 下拉刷新
    [tableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithID:studyItemSingleModel isHorizontal:NO];
    }];
    
    [tableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetInfoWithID:studyItemSingleModel isHorizontal:NO];
    }];
    
    return tableView;
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.currentMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdengifyWithRowOne = @"cellIdentifyWithRowOne";
    TSCenterMyToolTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdengifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[TSCenterMyToolTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdengifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        cellWithRowOne.backgroundColor = [UIColor clearColor];
    }
    cellWithRowOne.transferCellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    // 1. 获取对应的tableView
    //    for (int i = 0 ;i < self.tableViewMutableDic.allKeys.count;i++){
    TSMaterialSegmentModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    
    if (tableView == currentTableView){         // 获取当前的model
        
        // 1.获取当前tableView的key
        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.ID];
        TSMaterialSegmentModel *studyInfoSingleModel = [currentArr objectAtIndex:indexPath.row];
        cellWithRowOne.transferSingleModel = studyInfoSingleModel;
    }
    //    }
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    // 1. 获取对应的tableView
    TSMaterialSegmentModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    
    if (tableView == currentTableView){         // 获取当前的model
        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.ID];
        TSMaterialSegmentModel *studyInfoSingleModel = [currentArr objectAtIndex:indexPath.row];
        if (studyInfoSingleModel.type == 1){
            TSCenterMyToolTableViewCell *cellWithRowOne = (TSCenterMyToolTableViewCell *)[tableView cellForRowAtIndexPath:indexPath];
            [self showBigImageManagerWithSelectedAsset:studyInfoSingleModel cell:cellWithRowOne];
            return;
        } else if (studyInfoSingleModel.type == 2){
            TSTeachMaterialDetailViewController *materialDetailViewController = [[TSTeachMaterialDetailViewController alloc]init];
            materialDetailViewController.transferStudyInfoSingleModel = studyInfoSingleModel;
            [self.navigationController pushViewController:materialDetailViewController animated:YES];
        }
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    TSMaterialSegmentModel *itemSingleModel = [self.segmentMutableArr objectAtIndex: self.segmentList.selectedButtonIndex];
    
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    
    if (tableView == currentTableView){         // 获取当前的model
        
        // 1.获取当前tableView的key
//        NSArray *currentArr = [self.dataSourceMutableDic objectForKey:itemSingleModel.ID];
//        TSMaterialSegmentModel *studyInfoSingleModel = [currentArr objectAtIndex:indexPath.row];
        return [TSCenterMyToolTableViewCell calculationCellHeight];
    }
    return 44;
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
//    if (indexPath.row > 0){
        SeparatorType separatorType = SeparatorTypeMiddle;
        if ( [indexPath row] == 0) {
            separatorType  = SeparatorTypeMiddle;
        } else if ([indexPath row] == [self.currentMutableArr count] - 1) {
            separatorType  = SeparatorTypeMiddle;
        } else {
            separatorType  = SeparatorTypeMiddle;
        }
        if ([self.currentMutableArr count] == 1) {
            separatorType  = SeparatorTypeSingle;
        }
        [cell addSeparatorLineWithTypeWithAres:separatorType andUseing:@"center"];
//    }
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.tableViewMainScrollView){
        CGPoint point = scrollView.contentOffset;
        NSInteger page = point.x / kScreenBounds.size.width;
        [self.segmentList setSelectedButtonIndex:page animated:YES];
        
        [self hasTableAndManagerWithIndex:page];
        
        return;
    }
    [self setTableViewContentOffsetWithTag:scrollView.tag contentOffset:scrollView.contentOffset.y];
    
    // 记录当前tableView的contentOfSet
    [self rememberPoint];
}

#pragma mark 记录当前的point
-(void)rememberPoint{
    UITableView *currentTableView = (UITableView *)[self.tableViewMutableDic objectForKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
    [self.pointYMutableDic setObject:@(currentTableView.contentOffset.y) forKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
}


-(void)scrollViewDidScroll:(UIScrollView *)scrollView{
    CGFloat offsetY = scrollView.contentOffset.y;
    if ([scrollView isEqual:self.tableViewMainScrollView]) {
        
        return;
    }
    
    
    self.pointY = offsetY;
    
    CGFloat scale = 1.0;
    // 放大
    if (offsetY < 0) {
        
    } else if (offsetY > 0) { // 缩小
        scale = MAX(0.45, 1 - offsetY / self.headerScrollView.size_height);
        
        //        _userHead.transform = CGAffineTransformMakeScale(scale, scale);
    }
    
    if (scrollView.contentOffset.y > TSFloat(200)) {
        self.headerView.center = CGPointMake(self.headerView.center.x, _yOffset - TSFloat(200));
        return;
    }
    CGFloat h = _yOffset - self.pointY;
    self.headerView.center = CGPointMake(self.headerView.center.x, h);
}


#pragma mark 完成拖拽
-(void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if ([scrollView isEqual:self.tableViewMainScrollView]) {
        return;
    }
    [self setTableViewContentOffsetWithTag:scrollView.tag contentOffset:scrollView.contentOffset.y];
    
    
    // 完成拖拽，记录当前的contentOfSet
    [self rememberPoint];
}

//设置tableView的偏移量
-(void)setTableViewContentOffsetWithTag:(NSInteger)tag contentOffset:(CGFloat)offset{
    
    CGFloat tableViewOffset = offset;
    
    if (offset > TSFloat(44) + 64){
        tableViewOffset = TSFloat(44) + 64;
    }
    
    // 1. 获取tableView
    NSInteger index = self.segmentList.selectedButtonIndex;
    // 2. 获取当前的tableView
    TSMaterialSegmentModel *itemSingleModel = [self.segmentMutableArr objectAtIndex:index];
    //    NSString *key = [self.tableViewMutableDic.allKeys objectAtIndex:index];
    UITableView *currentTableView = [self.tableViewMutableDic objectForKey:itemSingleModel.tableIndex];
    [currentTableView setContentOffset:CGPointMake(0,currentTableView.contentOffset.y) animated:NO];
    NSLog(@"contentOffset_y=====>%.2f",currentTableView.contentOffset.y);
    NSLog(@"height=====>%.2f",currentTableView.contentSize.height);
}

#pragma mark - 【接口】


#pragma mark - 获取当前的item
-(void)sendRequestToGetInfoWithID:(TSMaterialSegmentModel *)singleModel isHorizontal:(BOOL)horizontal{
    if (horizontal){
        return;
    }

    //1. 找到当前的tableView
    UITableView *currentTableView = (UITableView *)[self.tableViewMutableDic objectForKey:[NSString stringWithFormat:@"%li",(long)self.segmentList.selectedButtonIndex]];
    
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachToolLimit:singleModel.ID page:currentTableView.currentPage block:^(BOOL isSuccessed, NSError *error, TSMaterialSegmentListModel *toolListModel) {
        if (!weakSelf){
            return;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            // 2. 判断是否下啦
            if (currentTableView.isXiaLa){
                [strongSelf.dataSourceMutableDic removeObjectForKey:singleModel.ID];
            }
            // 3. 判断当前是否存在这样的数据源
            NSMutableArray *appendingMutableArr = [NSMutableArray array];
            if (![strongSelf.dataSourceMutableDic.allKeys containsObject:singleModel.ID]){          // 不存在
                [strongSelf.dataSourceMutableDic setObject:toolListModel.items forKey:singleModel.ID];
            } else {                                                                        // 存在
                // 1. 获取上一个数据源
                NSArray *lastArr = [strongSelf.dataSourceMutableDic objectForKey:singleModel.ID];
                // 2. 加入上一个数据源
                [appendingMutableArr addObjectsFromArray:lastArr];
                // 3. 加入新的数据源
                [appendingMutableArr addObjectsFromArray:toolListModel.items];
                // 4. 加入信息
                [strongSelf.dataSourceMutableDic setObject:appendingMutableArr forKey:singleModel.ID];
            }
            
            // 4. 获取当前的数据源
            NSArray *currentDataSourceArr = [strongSelf.dataSourceMutableDic objectForKey:singleModel.ID];
            
            if (strongSelf.currentMutableArr.count){
                [strongSelf.currentMutableArr removeAllObjects];
            }
            [strongSelf.currentMutableArr addObjectsFromArray:currentDataSourceArr];
            
            // dismiss
            [currentTableView stopPullToRefresh];
            [currentTableView stopFinishScrollingRefresh];
            
            // placeholder
            [currentTableView reloadData];
            if (!strongSelf.currentMutableArr.count){
                [currentTableView showPrompt:[NSString stringWithFormat:@"没有关于【%@】的信息",singleModel.name] withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            } else {
                [currentTableView dismissPrompt];
            }
            
            [currentTableView stopPullToRefresh];
            [currentTableView stopFinishScrollingRefresh];
        } else {
            if(error.code != 500){
                [currentTableView showPrompt:[NSString stringWithFormat:@"没有关于【%@】的信息",singleModel.name] withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
            }
            [currentTableView stopPullToRefresh];
            [currentTableView stopFinishScrollingRefresh];
        }
    }];
}

// 获取所有的segment
-(void)sendRequestToGetMaterialSegmentListWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachToolHomePageWithBlock:^(TSMaterialSegmentListModel *materialSingleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->tempMaterialSingleModel = materialSingleModel;
        [strongSelf.segmentMutableArr addObjectsFromArray:materialSingleModel.items];
        
        if (self.segmentMutableArr.count > 4){
            self.segmentList.isNotScroll = NO;
        } else {
            self.segmentList.isNotScroll = YES;
        }
        
        // 重要
        strongSelf.tableViewMainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * strongSelf.segmentMutableArr.count , strongSelf.tableViewMainScrollView.size_height);
        [strongSelf.segmentList reloadData];
        
        if(block){
            block();
        }
    }];
}









#pragma mark - OtherManager
-(void)showBigImageManagerWithSelectedAsset:(TSMaterialSegmentModel *)singleModel cell:(TSCenterMyToolTableViewCell *)cell{
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollView];
    if (originalScrollView){
        return;
    }
    CGRect convertItemFrame = [cell convertRect:cell.imgView.frame toView:self.view.window];
    originalScrollView = [[UIScrollView alloc] initWithFrame:convertItemFrame];
    originalScrollView.stringTag = kOriginalScrollView;
    originalScrollView.backgroundColor = [UIColor blackColor];
    originalScrollView.showsHorizontalScrollIndicator = NO;
    originalScrollView.showsVerticalScrollIndicator = NO;
    originalScrollView.layer.zPosition = MAXFLOAT;
    originalScrollView.delegate = self;
    originalScrollView.layer.masksToBounds = YES;
    originalScrollView.userInteractionEnabled = YES;
    originalScrollView.maximumZoomScale=2.0;
    originalScrollView.minimumZoomScale=0.5;
    originalScrollView.bounces = YES;
    originalScrollView.bouncesZoom = YES;
    
    UITapGestureRecognizer * tapForHideKeyBoard = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(actionDismissOriginalPhoto)];
    [originalScrollView addGestureRecognizer:tapForHideKeyBoard];
    objc_setAssociatedObject(originalScrollView, &originalFrameKey, [NSValue valueWithCGRect:convertItemFrame], OBJC_ASSOCIATION_RETAIN);
    
    TSImageView *originalImageView = [[TSImageView alloc] initWithFrame:originalScrollView.bounds];
    originalImageView.stringTag = kOriginalImageView;
    __weak typeof(self)weakSelf = self;
    [originalImageView uploadImageWithURL:singleModel.url placeholder:nil callback:^(UIImage *originalImage) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        originalImage = [UIImage scaleDown:originalImage withSize:CGSizeMake(kScreenBounds.size.width, kScreenBounds.size.width*originalImage.size.height/originalImage.size.width)];
        originalImageView.image = originalImage;
        
        
        // 设置比例
        CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
        CGFloat screenRadio = CGRectGetWidth(strongSelf.view.window.frame)/CGRectGetHeight(self.view.window.frame);
        if (imageRadio >= screenRadio) {
            CGFloat currentImageHeight = CGRectGetWidth(strongSelf.view.window.frame)/imageRadio;
            originalScrollView.maximumZoomScale = CGRectGetHeight(strongSelf.view.window.frame)/currentImageHeight;
        } else {
            CGFloat currentImageWidth = CGRectGetWidth(strongSelf.view.window.frame)*imageRadio;
            originalScrollView.maximumZoomScale = CGRectGetWidth(strongSelf.view.window.frame)/currentImageWidth;
        }
        originalScrollView.minimumZoomScale = 1.;
        // 开启动画
        [UIView animateWithDuration:.5f delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            originalScrollView.frame = strongSelf.navigationController.view.bounds;
            originalImageView.frame = strongSelf.navigationController.view.bounds;
        } completion:^(BOOL finished) {
            
        }];
        
        originalImageView.contentMode = UIViewContentModeScaleAspectFit;
        [originalScrollView addSubview:originalImageView];
        [strongSelf.view.window addSubview:originalScrollView];
    }];
    
}


-(void)actionDismissOriginalPhoto{
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollView];
    UIImageView *originalImageView = (UIImageView *)[originalScrollView viewWithStringTag:kOriginalImageView];
    CGRect originalFrame = (CGRect)[objc_getAssociatedObject(originalScrollView, &originalFrameKey)CGRectValue];
    
    [UIView animateWithDuration:.5 animations:^{
        originalScrollView.frame = originalFrame;
        originalScrollView.layer.cornerRadius = 5;
        originalScrollView.alpha = 0.;
        originalImageView.frame = CGRectMake(0, 0, CGRectGetWidth(originalFrame), CGRectGetHeight(originalFrame));
    } completion:^(BOOL finished) {
        [originalScrollView removeFromSuperview];
    }];
}

// 缩放
-(UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    UIScrollView *originalScrollView = (UIScrollView *)[self.view.window viewWithStringTag:kOriginalScrollView];
    UIImageView *originalImageView = (UIImageView *)[originalScrollView viewWithStringTag:kOriginalImageView];
    return originalImageView;
}




@end
