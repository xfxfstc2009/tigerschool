//
//  TSTeachBopDetailViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachBopDetailViewController.h"
#import "NetworkAdapter+TSTeach.h"
#import "TSTeachDetailViewController.h"
#import "TSPayManager.h"


@interface TSTeachBopDetailViewController () {
    TSTeachBopDetailSingleModel *bopDetailModel;
    TSTeachBopHasPayModel *hasPayModel;
}
@property (nonatomic,strong)PDWebView *webView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)TSImageView *bannerImgView;
@property (nonatomic,strong)UIView *bottomView;
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)UIButton *leftButton;
@end

@implementation TSTeachBopDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self createView];
    [self sendRequestToGetHasPay];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"Bop课程详情";
}

#pragma -createView
-(void)createView{
    self.bottomView = [[UIView alloc]init];
    if (IS_iPhoneX){
        self.bottomView.frame = CGRectMake(0, self.view.size_height - TSFloat(70), kScreenBounds.size.width, TSFloat(70));
    } else {
        self.bottomView.frame = CGRectMake(0, self.view.size_height - TSFloat(50), kScreenBounds.size.width, TSFloat(50));
    }
    self.bottomView.layer.shadowColor = [[UIColor lightGrayColor] CGColor];
    self.bottomView.layer.shadowOpacity = -.2f;
    self.bottomView.layer.shadowOffset = CGSizeMake(-.2f, -.2f);
    [self.view addSubview:self.bottomView];
    
    // 1.button
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.actionButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.actionButton.frame = self.bottomView.bounds;
    self.actionButton.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    self.actionButton.titleLabel.font = [[UIFont systemFontOfCustomeSize:18.]boldFont];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf->hasPayModel.isfree){            // 表示免费的
            TSTeachDetailViewController *detailViewController = [[TSTeachDetailViewController alloc]init];
            detailViewController.transferCourseId = strongSelf.transferBopDetailId;
            [strongSelf.navigationController pushViewController:detailViewController animated:YES];
        } else {
            if (strongSelf->hasPayModel.ispay){
                [TSMTAManager MTAevent:MTAType_Tool_BopInClass str:strongSelf.transferBopDetailId];                 // 埋点

                TSTeachDetailViewController *detailViewController = [[TSTeachDetailViewController alloc]init];
                detailViewController.transferCourseId = strongSelf.transferBopDetailId;
                [strongSelf.navigationController pushViewController:detailViewController animated:YES];
            } else {
                [TSMTAManager MTAevent:MTAType_Tool_BopBuy str:strongSelf.transferBopDetailId];                 // 埋点
                [strongSelf showAlertWithPayManager];
            }
        }
    }];
    [self.bottomView addSubview:self.actionButton];

    self.webView = [[PDWebView alloc]init];
    self.webView.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.bottomView.orgin_y - 0);
    self.webView.backgroundColor =  [UIColor clearColor];
    [self.view addSubview:self.webView];
    
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(TSFloat(11), TSFloat(11) + 44, TSFloat(50), TSFloat(50));
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
    [self.leftButton setImage:[UIImage imageNamed:@"icon_back_kecheng"] forState:UIControlStateNormal];
    [self.view addSubview:self.leftButton];
}

#pragma mark -

-(void)sendRequestToGetInfo:(TSTeachBopHasPayModel *)hasPayModel{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachBopDetailWithId:self.transferBopDetailId block:^(TSTeachBopDetailSingleModel *bopDetailModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->bopDetailModel = bopDetailModel;
        // 1.img
        [strongSelf.bannerImgView uploadImageWithURL:bopDetailModel.img placeholder:nil callback:NULL];
        // 2. html
        [strongSelf.webView loadHTMLString:bopDetailModel.cou_desc];
        // 3. title
        strongSelf.titleLabel.text = bopDetailModel.title;
        // 5.判断是否支付过
        if (hasPayModel.isfree){            // 表示免费的
            [strongSelf.actionButton setTitle:@"进入课程(免费)" forState:UIControlStateNormal];
        } else {
            if (hasPayModel.ispay){
                [strongSelf.actionButton setTitle:@"进入课程" forState:UIControlStateNormal];
            } else {
                // 4. 按钮
                NSString *priceStr = [NSString stringWithFormat:@"￥%.2f",bopDetailModel.price];
                [strongSelf.actionButton setTitle:priceStr forState:UIControlStateNormal];
            }
        }
    }];
}

-(void)sendRequestToGetHasPay{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachBopHasPayId:self.transferBopDetailId block:^(TSTeachBopHasPayModel *model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->hasPayModel = model;
        [strongSelf sendRequestToGetInfo:model];
    }];
}

-(void)showAlertWithPayManager{
    bopDetailModel.tempCourId = self.transferBopDetailId;
    
    __weak typeof(self)weakSelf = self;
    [[TSAlertView sharedAlertView] showAlertWithType:AlertTypePay model:bopDetailModel block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = self;
        if (isSuccessed){
            strongSelf->hasPayModel.ispay = YES;
            
            if (strongSelf->hasPayModel.isfree){            // 表示免费的
                [strongSelf.actionButton setTitle:@"进入课程(免费)" forState:UIControlStateNormal];
            } else {
                if (strongSelf->hasPayModel.ispay){
                    [strongSelf.actionButton setTitle:@"进入课程" forState:UIControlStateNormal];
                } else {
                    // 4. 按钮
                    NSString *priceStr = [NSString stringWithFormat:@"￥%.2f",strongSelf->bopDetailModel.price];
                    [strongSelf.actionButton setTitle:priceStr forState:UIControlStateNormal];
                }
            }
            
            [[UIAlertView alertViewWithTitle:@"支付成功" message:nil buttonTitles:@[@"确定"] callBlock:NULL]show];
        }
    }];
}



@end
