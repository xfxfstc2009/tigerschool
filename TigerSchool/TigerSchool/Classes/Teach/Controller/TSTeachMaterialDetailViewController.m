//
//  TSTeachMaterialDetailViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/10.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachMaterialDetailViewController.h"
#import "TSHomeClassDetailPlayerImgView.h"

@interface TSTeachMaterialDetailViewController ()

@property (nonatomic,strong)TSHomeClassDetailPlayerImgView *playerImgView;
@property (nonatomic,strong)UIView *bgView;

@end

@implementation TSTeachMaterialDetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor colorWithCustomerName:@"黑"];
    [self createBgView];
    [self createPlayerView];
    
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

#pragma mark - createView
-(void)createBgView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor blackColor];
    self.bgView.frame = kScreenBounds;
    [self.view addSubview:self.bgView];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self.bgView addGestureRecognizer:tapGesture];
}


-(void)createPlayerView{
    // 1. player
    self.playerImgView = [[TSHomeClassDetailPlayerImgView alloc]initWithFrame:CGRectMake(0, (kScreenBounds.size.height - TSFloat(230)) / 2., kScreenBounds.size.width, TSFloat(230))];
    self.playerImgView.selectedMaterialModel = self.transferStudyInfoSingleModel;

    __weak typeof(self)weakSelf = self;
    [self.playerImgView actionClickToPlayerWithSucai:^(TSMaterialSegmentModel *selectedCourseDetailModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPlaying){        // 正在播放中
            [[GWMusicPlayerMethod sharedLocationManager] actionStopAndRemoveFromWindow];
        }
        
        [strongSelf.playerImgView actionPlayer:selectedCourseDetailModel];
    }];
    
    [self.view addSubview:self.playerImgView];
}

-(void)tapManager{
    if (self.playerImgView){
        [self.playerImgView resetPlayerView];
    }
    [self.navigationController popViewControllerAnimated:YES];
}



@end
