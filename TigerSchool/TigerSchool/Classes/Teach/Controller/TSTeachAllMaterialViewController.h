//
//  TSTeachAllMaterialViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"
#import "TSTeachMaterialSingleView.h"

@interface TSTeachAllMaterialViewController : AbstractViewController

@property (nonatomic,strong)TSTeachMaterialSingleModel *transferSingleModel;

@end
