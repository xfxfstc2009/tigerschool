//
//  TeachRootViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/12.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TeachRootViewController.h"
#import "TSTeachMaterialCell.h"
#import "NetworkAdapter+TSTeach.h"
#import "TSTeachRootTitleTableViewCell.h"
#import "TSTeachBopTopTableViewCell.h"
#import "TSTeachAllMaterialViewController.h"                // 全部课程
#import "TSTeachBopClassViewController.h"
#import "TSTeachBopDetailViewController.h"                  // bop 课程详情
#import "TSHomeSearchListViewController.h"
#import "TSTeachMaterialRootSingleTableViewCell.h"
#import "TSTeachDetailViewController.h"

@interface TeachRootViewController()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>{
    TSTeachRootModel *teachRootModel;
}
@property (nonatomic,strong)UITableView *teachTableView;
@property (nonatomic,strong)NSMutableArray *teachMutableArr;
@property (nonatomic,strong)NSMutableArray *materialArr;
@property (nonatomic,strong)UISearchBar *searchBar;

@end

@implementation TeachRootViewController

-(void)viewDidLoad{
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self setBarButtonItem];
    [self sendRequestToGetTeachInfo];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"bop训练";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.teachMutableArr = [NSMutableArray array];
    self.materialArr = [NSMutableArray array];
    [self.materialArr addObject:@"素材fixed"];
    [self.teachMutableArr addObjectsFromArray: @[@[@"bop训练fixed",@"bop训练"]]];
    [self.teachMutableArr addObject:self.materialArr];
    
}

-(void)createTableView{
    if (!self.teachTableView){
        self.teachTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.teachTableView.delegate = self;
        self.teachTableView.dataSource = self;
        [self.view addSubview:self.teachTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.teachTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetTeachInfo];
    }];
}

#pragma mark - UITableViewDateSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.teachMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionofArr = [self.teachMutableArr objectAtIndex:section];
    return sectionofArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            TSTeachRootTitleTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[TSTeachRootTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferTitle = @"Bop训练";
            
            return cellWithRowOne;
            
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            TSTeachBopTopTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[TSTeachBopTopTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            TSTeachRootSingleModel *singleModel = [teachRootModel.bop lastObject];
            cellWithRowTwo.transferSingleModel = singleModel;
            return cellWithRowTwo;
        }
    } else if (indexPath.section == 1){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithThr = @"cellIdentifyWithThr";
            TSTeachRootTitleTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[TSTeachRootTitleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowThr.transferCellHeight = cellHeight;
            cellWithRowThr.transferTitle = @"素材精选";
            cellWithRowThr.transferDesc = @"查看全部";
            cellWithRowThr.dymicButton.hidden = NO;
            __weak typeof(self)weakSelf = self;
            [cellWithRowThr actionClickWithDymicBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                TSTeachAllMaterialViewController *allMaterialViewController = [[TSTeachAllMaterialViewController alloc]init];
                [strongSelf.navigationController pushViewController:allMaterialViewController animated:YES];
                [TSMTAManager MTAevent:MTAType_Tool_Sucai_All str:@""];                 // 埋点
            }];
            
            cellWithRowThr.dymicLabel.textColor = [UIColor hexChangeFloat:@"999999"];
            cellWithRowThr.transferHasArrow = YES;
            return cellWithRowThr;
            
        } else {
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            TSTeachMaterialRootSingleTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[TSTeachMaterialRootSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            TSTeachMaterialSingleModel *singleModel = [[self.teachMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            cellWithRowFour.transferSingleModel = singleModel;
            return cellWithRowFour;
        }
    }
    return nil;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        if (indexPath.row == 0){
            return [TSTeachRootTitleTableViewCell calculationCellHeight];
        } else {
            return [TSTeachBopTopTableViewCell calculationCellHeight];
        }
    } else {
        if (indexPath.row == 0){
            return [TSTeachRootTitleTableViewCell calculationCellHeight];
        } else {
           return  [TSTeachMaterialRootSingleTableViewCell calculationCellHeight];
        }
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 0){
        TSTeachRootSingleModel *singleModel = [teachRootModel.bop lastObject];
        [self bopHasPayWithId:singleModel.ID];
    } else {
        if (indexPath.row == 0){

        } else {
            TSTeachMaterialSingleModel *singleModel = [[self.teachMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            TSTeachAllMaterialViewController *allMaterialViewController = [[TSTeachAllMaterialViewController alloc]init];
            allMaterialViewController.transferSingleModel = singleModel;
            [TSMTAManager MTAevent:MTAType_Tool_Sucai_Type str:singleModel.name];                 // 埋点
            [self.navigationController pushViewController:allMaterialViewController animated:YES];
        }
    }
}

#pragma mark - 创建搜索框
- (void)setBarButtonItem {
    [self.navigationItem setHidesBackButton:YES];
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(TSFloat(5), 7, kScreenBounds.size.width - 2 * TSFloat(5), 40)];
    titleView.backgroundColor = [UIColor whiteColor];
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(titleView.frame) - 15, titleView.size_height)];
    searchBar.placeholder = @"课程/案例/素材";
    searchBar.backgroundImage = [TSTool createImageWithColor:[UIColor clearColor] frame:titleView.bounds];
    searchBar.delegate = self;
    searchBar.showsCancelButton = NO;
    UITextField *searchTextField = [searchBar valueForKey:@"_searchField"];
    searchTextField.font = [UIFont systemFontOfSize:15];
    searchTextField.backgroundColor = [UIColor colorWithRed:234/255.0 green:235/255.0 blue:237/255.0 alpha:1];
    searchBar.backgroundColor = [UIColor whiteColor];
    [titleView addSubview:searchBar];
    self.searchBar = searchBar;
    self.navigationItem.titleView = titleView;
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    TSHomeSearchListViewController *searchListController = [[TSHomeSearchListViewController alloc]init];
    [self.navigationController pushViewController:searchListController animated:YES];
    [TSMTAManager MTAevent:MTAType_Tool_BopSearch str:@""];                 // 埋点
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    NSLog(@"SearchButton");
}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"%@",searchText);
}


#pragma mark - Interface
-(void)sendRequestToGetTeachInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachWithFetchRootWithBlock:^(TSTeachRootModel *teachModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->teachRootModel = teachModel;
        [strongSelf.materialArr removeAllObjects];
        [strongSelf.materialArr addObject:@"素材精选"];
        [strongSelf.materialArr addObjectsFromArray:teachModel.material];
        [strongSelf.teachTableView reloadData];
        [strongSelf.teachTableView stopPullToRefresh];
        
    }];
}

#pragma mark - 判断bop是否支付
-(void)bopHasPayWithId:(NSString *)detailId{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachBopHasPayId:detailId block:^(TSTeachBopHasPayModel *hasPayModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (hasPayModel.isfree){            // 表示免费的
            TSTeachDetailViewController *teachDetailViewController = [[TSTeachDetailViewController alloc]init];
            teachDetailViewController.transferCourseId = detailId;
            [strongSelf.navigationController pushViewController:teachDetailViewController animated:YES];
        } else {
            if (hasPayModel.ispay){
                TSTeachDetailViewController *teachDetailViewController = [[TSTeachDetailViewController alloc]init];
                teachDetailViewController.transferCourseId = detailId;
                [strongSelf.navigationController pushViewController:teachDetailViewController animated:YES];
            } else {
                TSTeachBopDetailViewController *bopClassViewController = [[TSTeachBopDetailViewController alloc]init];
                bopClassViewController.transferBopDetailId = detailId;
                [self.navigationController pushViewController:bopClassViewController animated:YES];
            }
        }
    }];
}


@end
