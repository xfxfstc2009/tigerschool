//
//  TSTeachBopTopTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachBopTopTableViewCell.h"

@interface TSTeachBopTopTableViewCell()
@property (nonatomic,strong)TSImageView *bopBackgroundImgView;
@property(nonatomic,strong)TSImageView *bopImageView;
@property(nonatomic,strong)TSImageView *alphaImageView;

@end

@implementation TSTeachBopTopTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bopBackgroundImgView = [[TSImageView alloc]init];
    self.bopBackgroundImgView.backgroundColor = [UIColor clearColor];
    self.bopBackgroundImgView.image = [TSTool stretchImageWithName:@"img_teach_shadow"];
    [self addSubview:self.bopBackgroundImgView];
    
    self.bopImageView = [[TSImageView alloc]init];
    self.bopImageView.backgroundColor = UURandomColor;
    [self.bopBackgroundImgView addSubview:self.bopImageView];
//    self.bopImageView.hidden = YES;
    
    self.alphaImageView = [[TSImageView alloc]init];
    self.alphaImageView.backgroundColor = [UIColor clearColor];
    self.alphaImageView.hidden = YES;
    [self addSubview:self.alphaImageView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    self.bopBackgroundImgView.frame = CGRectMake(0, 0, kScreenBounds.size.width , TSFloat(480) / 2. + 2 * TSFloat(16));
    //
    self.bopImageView.frame = CGRectMake(TSFloat(16), TSFloat(16), kScreenBounds.size.width - 2 * TSFloat(16), TSFloat(480)/ 2.);
    self.bopImageView.layer.cornerRadius = TSFloat(7);
    self.bopImageView.clipsToBounds = YES;
    self.alphaImageView.frame = self.bopImageView.frame;
    self.alphaImageView.image = [TSTool stretchImageWithName:@"icon_home_tags_alpha"];
}

-(void)setTransferSingleModel:(TSTeachRootSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    [self.bopImageView uploadImageWithURL:transferSingleModel.img placeholder:nil callback:NULL];
}

+(CGFloat)calculationCellHeight{
    return TSFloat(480) / 2. + 2 * TSFloat(16);
}


@end
