//
//  TSTeachBopEndView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/17.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachBopEndView.h"
#import <pop/POP.h>

@interface TSTeachBopEndView()
@property (nonatomic,strong)TSImageView *scroeBgView;
@property (nonatomic,strong)UILabel *scoreLabell;
@end

@implementation TSTeachBopEndView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.scroeBgView = [[TSImageView alloc]init];
    self.scroeBgView.backgroundColor = [UIColor clearColor];
    self.scroeBgView.image = [UIImage imageNamed:@"img_score"];
    self.scroeBgView.frame = CGRectMake(self.size_width, 0, TSFloat(84), TSFloat(57));
    [self addSubview:self.scroeBgView];
    
    // 2. 创建分数
    self.scoreLabell = [TSViewTool createLabelFont:@"小正文" textColor:@"橙"];
    self.scoreLabell.frame = CGRectMake(TSFloat(20), self.scroeBgView.size_height - TSFloat(40), self.scroeBgView.size_width - TSFloat(20), TSFloat(40));
    self.scoreLabell.adjustsFontSizeToFitWidth = YES;
    self.scoreLabell.font = [self.scoreLabell.font boldFont];
    self.scoreLabell.textAlignment = NSTextAlignmentCenter;
    self.scoreLabell.font = [UIFont systemFontOfCustomeSize:27];
    self.scoreLabell.text = @"0分";
    self.scoreLabell.textColor = [UIColor hexChangeFloat:@"F9A401"];
    [self.scroeBgView addSubview:self.scoreLabell];
}

-(void)showAnimationWithNumber:(NSInteger)score{
    [UIView animateWithDuration:.5f animations:^{
        self.scroeBgView.orgin_x = self.size_width - self.scroeBgView.size_width;
    } completion:^(BOOL finished) {
        self.scroeBgView.orgin_x = self.size_width - self.scroeBgView.size_width;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self animationWithInfoWithScore:score];
        });
    }];
}

-(void)hidenAnimationWithNumber:(NSInteger)score{
    [UIView animateWithDuration:.5f animations:^{
        self.scroeBgView.orgin_x = self.size_width;
    } completion:^(BOOL finished) {
        self.scroeBgView.orgin_x = self.size_width;
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.5f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [self animationWithInfoWithScore:score];
        });
    }];
}

-(void)animationWithInfoWithScore:(NSInteger)score{
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1.3f;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            [obj setText:[NSString stringWithFormat:@"%li分",(long)values[0]]];
        };
        
        // dynamics threshold
        prop.threshold = 1;
    }];
    
    anim.property = prop;
    
    anim.fromValue = @(0);
    anim.toValue = @(score);
    
    [self.scoreLabell pop_addAnimation:anim forKey:@"counting"];
}


@end
