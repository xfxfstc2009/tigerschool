//
//  TSTeachDetailViewTopView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSTeachBopDetailItemsModel.h"

@interface TSTeachDetailViewTopView : UIView

@property (nonatomic,strong)TSTeachBopDetailItemsRootModel *transferDetailModel;

@end
