//
//  TSBopBeiyibeiSingleTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/2.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSTeachBopResultModel.h"
#import "TSBopClassLrcTableViewCell.h"

@interface TSBopBeiyibeiSingleTableViewCell : TSBaseTableViewCell
@property (nonatomic,strong)TSTeachBopReadedInfo *transferBopResultModel;
@property (nonatomic,strong)NSArray *transferBaiduEcodeArr;

@property (nonatomic,strong)TSTeachBopReadedInfo *transferBopEndResultModel;

+(CGFloat)calculationCellHeightWithModel:(TSTeachBopReadedInfo *)model;

// 成功动画
-(void)animationWithSuccess;
-(void)animationWithFail;

@end
