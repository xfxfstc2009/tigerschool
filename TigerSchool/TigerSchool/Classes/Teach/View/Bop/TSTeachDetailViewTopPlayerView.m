//
//  TSTeachDetailViewTopPlayerView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/30.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachDetailViewTopPlayerView.h"

static char playerButtonActionClickBlockKey;
@interface TSTeachDetailViewTopPlayerView()
@property (nonatomic,strong)UILabel *actionLabel;
@end

@implementation TSTeachDetailViewTopPlayerView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    //创建播放按钮，需要初始化一个状态，即显示暂停还是播放状态
    self.leftButton = [[iQiYiPlayButton alloc] initWithFrame:CGRectMake(0, 0,20 ,20) state:iQiYiPlayButtonStatePause];
    
    [self addSubview:self.leftButton];
    self.leftButton.center_y = self.size_height / 2.;
    __weak typeof(self)weakSelf = self;
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf playerButtonType];
        void(^block)(iQiYiPlayButtonState) = objc_getAssociatedObject(strongSelf, &playerButtonActionClickBlockKey);
        if (block){
            block(strongSelf.leftButton.buttonState);
        }
    }];
    
    self.actionLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    self.actionLabel.textColor = [UIColor hexChangeFloat:@"999999"];
    self.actionLabel.font = [UIFont systemFontOfCustomeSize:13.];
    [self addSubview:self.actionLabel];
    self.actionLabel.text = @"请跟读";
    CGSize actionLabelSize = [TSTool makeSizeWithLabel:self.actionLabel];
    self.actionLabel.frame = CGRectMake(CGRectGetMaxX(self.leftButton.frame) + TSFloat(7), 0, actionLabelSize.width, [NSString contentofHeightWithFont:self.actionLabel.font]);
    self.actionLabel.center_y = self.leftButton.center_y;
}

-(void)playerButtonType{
    //通过判断当前状态 切换显示状态
    if (self.leftButton.buttonState == iQiYiPlayButtonStatePause) {
        self.leftButton.buttonState = iQiYiPlayButtonStatePlay;
    }else {
        self.leftButton.buttonState = iQiYiPlayButtonStatePause;
    }
}

-(void)playerButtonActionClickBlock:(void(^)(iQiYiPlayButtonState))block{
    objc_setAssociatedObject(self, &playerButtonActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
