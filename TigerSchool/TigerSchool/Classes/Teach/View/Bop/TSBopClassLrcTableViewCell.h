//
//  TSBopClassLrcTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/24.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSLyricModel.h"

// 歌词的label
@interface TSBopClassLrcTableViewCellLabel : UILabel
//进度
@property (nonatomic,assign) CGFloat progress;

@end

@interface TSBopClassLrcTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)TSLyricModel *transferLrcModel;         /**< 传递过来歌词信息*/

@property (nonatomic,strong)TSBopClassLrcTableViewCellLabel *lrcLabel;
- (void)reloadCellForSelect:(BOOL)select ;

+(CGFloat)calculationCellHeightWithText:(TSLyricModel *)transferLrcModel;

@end
