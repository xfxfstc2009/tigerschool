//
//  TSBopItemDetailModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"


@protocol TSBopItemDetailModel <NSObject>

@end

@interface TSBopItemDetailModel : FetchModel

@property (nonatomic,copy)NSString *ID;         /**< 小节ID*/
@property (nonatomic,assign)BOOL isrecite;      /**< 是否背完*/
@property (nonatomic,assign)BOOL iswatch;       /**< 是否听完*/
@property (nonatomic,copy)NSString *keyword;    /**< 背诵关键字提醒*/
@property (nonatomic,assign)NSInteger score;    /**< 分数 背完展示分数*/
@property (nonatomic,copy)NSString *str;        /**< 背完产生的错误str*/
@property (nonatomic,copy)NSString *url;        /**< lrc 路径*/
@property (nonatomic,copy)NSString *voice;      /**< 音频地址*/

@end

@interface TSBopItemDetailListModel : FetchModel

@property (nonatomic,strong)NSArray<TSBopItemDetailModel> *items;

@end
