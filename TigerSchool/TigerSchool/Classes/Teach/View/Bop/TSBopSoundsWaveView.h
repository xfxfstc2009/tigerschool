//
//  TSBopSoundsWaveView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/7.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSBopSoundsWaveView : UIView

-(void)waveAnimationWithFloat:(CGFloat)animation;

@end
