//
//  TSTeachDetailViewTopPlayerView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/30.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iQiYiPlayButton.h"

@interface TSTeachDetailViewTopPlayerView : UIView
@property (nonatomic,strong)iQiYiPlayButton *leftButton;

-(void)playerButtonActionClickBlock:(void(^)(iQiYiPlayButtonState))block;

@end
