//
//  TSTeachDetailViewTopView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachDetailViewTopView.h"
#import "GWSlider.h"
#import <pop/POP.h>
@interface TSTeachDetailViewTopView()<GWSliderDataSource>
@property (nonatomic,strong)GWSlider *slider;
@property (nonatomic,strong)TSImageView *imgBgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@end

@implementation TSTeachDetailViewTopView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.imgBgView = [[TSImageView alloc]init];
    self.imgBgView.image = [UIImage imageNamed:@"bg_bopdetail"];
    self.imgBgView.frame = self.bounds;
    [self addSubview:self.imgBgView];
    
    self.titleLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    self.titleLabel.text = @"总进度";
    CGSize titleSize = [TSTool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(TSFloat(25) / 2., TSFloat(136) / 2., titleSize.width, titleSize.height);
    [self addSubview:self.titleLabel];
    
    
    // 3. 创建进度条
    self.slider = [[GWSlider alloc]initWithFrame:CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + TSFloat(10), TSFloat(58), kScreenBounds.size.width - 2 * (CGRectGetMaxX(self.titleLabel.frame) + TSFloat(10)) , 50)];
    self.slider.center_y = self.titleLabel.center_y;
    self.slider.popUpViewCornerRadius = 0.6f;
    [self.slider setMaxFractionDigitsDisplayed:0];
    self.slider.popUpViewAnimatedColors = @[RGB(253, 186, 69, 1),RGB(248, 125, 74, 1)];
    self.slider.userInteractionEnabled = NO;
    self.slider.font = [UIFont fontWithName:@"GillSans-Bold" size:22];
    self.slider.textColor = [UIColor colorWithHue:0.55 saturation:1.0 brightness:0.5 alpha:1];
    [self.slider addTarget:self action:@selector(playProgressChange:) forControlEvents:UIControlEventValueChanged];
    [self.slider addTarget:self action:@selector(playProgressChangeEnd:) forControlEvents:UIControlEventTouchUpInside];
    self.slider.maximumTrackTintColor = [UIColor hexChangeFloat:@"FFF4C4"];
    self.slider.thumbTintColor = [UIColor clearColor];
    [self addSubview:self.slider];
    
    self.dymicLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    NSString *strDymic = @"123%";
    CGSize size = [strDymic sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    self.dymicLabel.frame = CGRectMake(kScreenBounds.size.width - TSFloat(25) / 2. - size.width, 0, kScreenBounds.size.width - TSFloat(25) / 2. - size.width, [NSString contentofHeightWithFont:self.dymicLabel.font]);
    [self addSubview:self.dymicLabel];
    self.dymicLabel.center_y = self.titleLabel.center_y;
    
}

-(void)smartAminationWithFloat:(CGFloat)score{
    POPBasicAnimation *anim = [POPBasicAnimation animation];
    anim.duration = 1;
    anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
    POPAnimatableProperty * prop = [POPAnimatableProperty propertyWithName:@"count" initializer:^(POPMutableAnimatableProperty *prop){
        prop.readBlock = ^(id obj, CGFloat values[]) {
            values[0] = [[obj description] floatValue];
        };
        prop.writeBlock = ^(id obj, const CGFloat values[]) {
            [obj setText:[NSString stringWithFormat:@"%li%%",(long)values[0]]];
        };
        // dynamics threshold
        prop.threshold = 1;
    }];

    anim.property = prop;

    anim.fromValue = @(0);
    anim.toValue = @(score);

    [self.dymicLabel pop_addAnimation:anim forKey:@"counting"];
}

#pragma mark - Action
// 4. 进度条显示
- (void)playProgressChange:(GWSlider *)slider {
 
}

-(void)playProgressChangeEnd:(GWSlider *)slider{
     [self.slider setValue:slider.value animated:YES];
}

- (NSString *)slider:(GWSlider *)slider stringForValue:(float)value{
    return @"234";
}

-(void)setTransferDetailModel:(TSTeachBopDetailItemsRootModel *)transferDetailModel{
    _transferDetailModel = transferDetailModel;
    [self smartAminationWithFloat:transferDetailModel.process];
    [self.slider setValue:(transferDetailModel.process / 100.) animated:YES];
}

@end
