//
//  TSBopDetailViewTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSTeachBopDetailItemsModel.h"

@interface TSBopDetailViewTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)TSTeachBopDetailItemsModel *transferBopDetailModel;

-(void)loadContentWithIndex:(NSInteger)index;

- (void)startAnimationWithDelay:(CGFloat)delayTime ;            // 动画

-(void)readyButtonActionClickBlock:(void(^)())block;

@end
