//
//  TSBopSoundsWaveView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/7.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBopSoundsWaveView.h"
#import "SCSiriWaveformView.h"

@interface TSBopSoundsWaveView()

@property (nonatomic,strong)SCSiriWaveformView *waveView;

@end

@implementation TSBopSoundsWaveView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.waveView = [[SCSiriWaveformView alloc] initWithFrame:self.bounds];
    self.waveView.alpha = 1;
    self.waveView.numberOfWaves = 10;
    self.waveView.density = 1.f;
    self.waveView.frequency = 2.;
    self.waveView.waveColor = [UIColor colorWithCustomerName:@"红"];
    self.waveView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.waveView];
}

-(void)waveAnimationWithFloat:(CGFloat)animation{
    [self.waveView updateWithLevel:animation];
}


@end
