//
//  TSBopDetailViewTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBopDetailViewTableViewCell.h"
#import <pop/POP.h>
#import "StringAttributeHelper.h"

static char readyButtonActionClickBlockKey;
@interface TSBopDetailViewTableViewCell()
@property (nonatomic,strong)TSImageView *avatarImgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *scoreLabel;
@property (nonatomic,strong)UILabel *avgFixedLabel;
@property (nonatomic,strong)TSImageView *arrowImgView;
@property (nonatomic,strong)UIButton *actionButton;


@end

@implementation TSBopDetailViewTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.avatarImgView = [[TSImageView alloc]init];
    self.avatarImgView.backgroundColor = UURandomColor;
    self.avatarImgView.frame = CGRectMake(TSFloat(16), TSFloat(22), TSFloat(44), TSFloat(44));
    self.avatarImgView.layer.cornerRadius = self.avatarImgView.size_width / 2.;
    self.avatarImgView.clipsToBounds = YES;
    [self addSubview:self.avatarImgView];
    
    // label
    self.titleLabel = [TSViewTool createLabelFont:@"标题" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    // score
    self.scoreLabel = [TSViewTool createLabelFont:@"标题" textColor:@"橙"];
    self.scoreLabel.font = [self.scoreLabel.font boldFont];
    [self addSubview:self.scoreLabel];
    
    self.avgFixedLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    self.avgFixedLabel.font = [UIFont systemFontOfCustomeSize:11.];
    [self addSubview:self.avgFixedLabel];
    
    // arrow
    self.arrowImgView = [[TSImageView alloc]init];
    self.arrowImgView.backgroundColor = [UIColor clearColor];
    self.arrowImgView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    [self addSubview:self.arrowImgView];
    
    // btn
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    [self addSubview:self.actionButton];
}

-(void)setTransferBopDetailModel:(TSTeachBopDetailItemsModel *)transferBopDetailModel{
    _transferBopDetailModel = transferBopDetailModel;
    
    // 1.  头像
    [self.avatarImgView uploadImageWithURL:transferBopDetailModel.img placeholder:nil callback:NULL];
    
    // 2. 箭头
    self.arrowImgView.frame = CGRectMake(kScreenBounds.size.width - TSFloat(20) - TSFloat(6), 0, TSFloat(6), TSFloat(10));
    self.arrowImgView.center_y = [TSBopDetailViewTableViewCell calculationCellHeight] / 2.;
    
    // 2.
    self.avgFixedLabel.text = @"小节平均分";
    CGSize avgSize = [TSTool makeSizeWithLabel:self.avgFixedLabel];

    // 3. 创建分数
    self.scoreLabel.text = [NSString stringWithFormat:@"%li",(long)transferBopDetailModel.score];
    self.scoreLabel.textColor = [UIColor colorWithCustomerName:@"橙"];
    self.scoreLabel.font = [UIFont systemBopFontOfBopCustomeSize:24.];
    CGSize scoreSize = [TSTool makeSizeWithLabel:self.scoreLabel];
    self.scoreLabel.frame = CGRectMake(self.arrowImgView.orgin_x - scoreSize.width - TSFloat(3), TSFloat(35), scoreSize.width, scoreSize.height);
    self.scoreLabel.center_y = self.arrowImgView.center_y;

    self.avgFixedLabel.frame = CGRectMake(self.arrowImgView.orgin_x - avgSize.width - TSFloat(3), CGRectGetMaxY(self.scoreLabel.frame), avgSize.width, avgSize.height);
    
    self.titleLabel.text = transferBopDetailModel.name;
    CGFloat titleWidth = self.avgFixedLabel.orgin_x - CGRectGetMaxX(self.avatarImgView.frame) - 2 * TSFloat(11);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(titleWidth, CGFLOAT_MAX)];
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + TSFloat(11),([TSBopDetailViewTableViewCell calculationCellHeight] - 2 * [NSString contentofHeightWithFont:self.titleLabel.font]) / 2., titleWidth, titleSize.height);
        self.titleLabel.numberOfLines = 2;
    } else {
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.avatarImgView.frame) + TSFloat(11),([TSBopDetailViewTableViewCell calculationCellHeight] - 1 * [NSString contentofHeightWithFont:self.titleLabel.font]) / 2., titleWidth, titleSize.height);
        self.titleLabel.numberOfLines = 1;
    }
    
    // btn
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &readyButtonActionClickBlockKey);
        if (block){
            block();
        }
    }];
    self.actionButton.frame = CGRectMake(self.arrowImgView.orgin_x - TSFloat(7) - TSFloat(66), 0, TSFloat(66), TSFloat(33));
    self.actionButton.center_y = [TSBopDetailViewTableViewCell calculationCellHeight] / 2.;

    if(!transferBopDetailModel.unlock && transferBopDetailModel.iswatch){
        self.actionButton.hidden = YES;
        self.arrowImgView.hidden = NO;
        self.avgFixedLabel.hidden = NO;
        self.scoreLabel.hidden = NO;
        self.scoreLabel.text = [NSString stringWithFormat:@"%li",(long)transferBopDetailModel.score];
    } else {
        if (transferBopDetailModel.unlock){
            [self.actionButton setBackgroundImage:[UIImage imageNamed:@"icon_teach_go"] forState:UIControlStateNormal];
            self.actionButton.hidden = NO;
            self.actionButton.enabled = YES;
            self.actionButton.userInteractionEnabled = YES;
            self.arrowImgView.hidden = NO;
            self.avgFixedLabel.hidden = YES;
            self.scoreLabel.hidden = YES;
        } else {
            [self.actionButton setBackgroundImage:[UIImage imageNamed:@"icon_teach_ready"] forState:UIControlStateNormal];
            self.actionButton.hidden = NO;
            self.actionButton.enabled = YES;
            self.arrowImgView.hidden = YES;
            self.actionButton.userInteractionEnabled = NO;
            self.avgFixedLabel.hidden = YES;
            self.scoreLabel.hidden = YES;
        }
    }
}

+(CGFloat)calculationCellHeight{
    return TSFloat(90);
}

-(void)loadContentWithIndex:(NSInteger)index{
    NSString *fullStirng = [NSString stringWithFormat:@"%01ld. %@", (long)index, self.transferBopDetailModel.name];
    NSMutableAttributedString *richString = [[NSMutableAttributedString alloc] initWithString:fullStirng];
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [[UIFont HeitiSCWithFontSize:16.f] boldFont];
        fontAttribute.effectRange    = NSMakeRange(0, richString.length);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        FontAttribute *fontAttribute = [FontAttribute new];
        fontAttribute.font           = [UIFont fontWithName:@"GillSans-Italic" size:16.f];
        fontAttribute.effectRange    = NSMakeRange(0, 3);
        [richString addStringAttribute:fontAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [[UIColor blackColor] colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, richString.length);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    
    {
        ForegroundColorAttribute *foregroundColorAttribute = [ForegroundColorAttribute new];
        foregroundColorAttribute.color                     = [UUBlue colorWithAlphaComponent:0.65f];
        foregroundColorAttribute.effectRange               = NSMakeRange(0, 3);
        [richString addStringAttribute:foregroundColorAttribute];
    }
    self.titleLabel.attributedText = richString;
}


-(void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated{
    [super setHighlighted:highlighted animated:animated];
    if (self.highlighted){
        POPBasicAnimation *scaleAnimation = [POPBasicAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.duration           = 0.1f;
        scaleAnimation.toValue            = [NSValue valueWithCGPoint:CGPointMake(0.95, 0.95)];
        [self.titleLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    } else {
        POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPViewScaleXY];
        scaleAnimation.toValue             = [NSValue valueWithCGPoint:CGPointMake(1, 1)];
        scaleAnimation.velocity            = [NSValue valueWithCGPoint:CGPointMake(2, 2)];
        scaleAnimation.springBounciness    = 20.f;
        [self.titleLabel pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
    }
}


#pragma mark 飘动过去Animation
- (void)startAnimationWithDelay:(CGFloat)delayTime {
    self.titleLabel.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    self.avatarImgView.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    self.avgFixedLabel.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    self.scoreLabel.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    self.arrowImgView.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    self.actionButton.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    [UIView animateWithDuration:1. delay:delayTime usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        self.titleLabel.transform = CGAffineTransformIdentity;
        self.avatarImgView.transform = CGAffineTransformIdentity;
        self.avgFixedLabel.transform =  CGAffineTransformIdentity;
        self.scoreLabel.transform =  CGAffineTransformIdentity;
        self.arrowImgView.transform =  CGAffineTransformIdentity;
        self.actionButton.transform =  CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
    }];
}

-(void)readyButtonActionClickBlock:(void(^)())block{
    objc_setAssociatedObject(self, &readyButtonActionClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
