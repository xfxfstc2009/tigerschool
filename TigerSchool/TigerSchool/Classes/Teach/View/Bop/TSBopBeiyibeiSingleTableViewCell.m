//
//  TSBopBeiyibeiSingleTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/2.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBopBeiyibeiSingleTableViewCell.h"
#import "FBShimmeringView.h"
#import "NSAttributedString_Encapsulation.h"

@interface TSBopBeiyibeiSingleTableViewCell()
@property (nonatomic,strong)UILabel *mainLabel;
//@property (nonatomic,strong)FBShimmeringView *shimmeringView;
@end

@implementation TSBopBeiyibeiSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if(self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.mainLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"红"];
    self.mainLabel.font = [UIFont systemFontOfCustomeSize:14.];
    self.mainLabel.textAlignment = NSTextAlignmentCenter;
    self.mainLabel.numberOfLines = 0;
    [self addSubview:self.mainLabel];
    
//    self.shimmeringView = [[FBShimmeringView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, [NSString contentofHeightWithFont:self.mainLabel.font])];
//    [self addSubview:self.shimmeringView];
}

-(void)setTransferBopResultModel:(TSTeachBopReadedInfo *)transferBopResultModel{
    _transferBopResultModel = transferBopResultModel;
    
    if (!transferBopResultModel.hasRead){
        self.mainLabel.text = transferBopResultModel.mainTempInfoArrow;
    } else {
        self.mainLabel.text = transferBopResultModel.mainInfo;
    }
    
    CGFloat width = kScreenBounds.size.width - 4 * TSFloat(25);
    CGSize mainSize = [transferBopResultModel.mainInfo sizeWithCalcFont:self.mainLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.mainLabel.frame = CGRectMake(0, 0, width, mainSize.height);

    if (transferBopResultModel.bopLrcType == BopBeiyibeiTypeSuccess){
        self.mainLabel.textColor = [UIColor redColor];
    } else if (transferBopResultModel.bopLrcType == BopBeiyibeiTypeFail){
        self.mainLabel.textColor = [UIColor yellowColor];
    } else {
        self.mainLabel.textColor = [UIColor blackColor];
    }
    
    
//    // shimmeringView
//    self.shimmeringView.frame = CGRectMake(0, 0, width, [TSBopBeiyibeiSingleTableViewCell calculationCellHeightWithModel:transferBopResultModel]);
//    self.shimmeringView.contentView = self.mainLabel;
}

#pragma mark 提交百度识别的内容
-(void)setTransferBaiduEcodeArr:(NSArray *)transferBaiduEcodeArr{
    _transferBaiduEcodeArr = transferBaiduEcodeArr;
    
    self.mainLabel.attributedText = [NSAttributedString_Encapsulation changeFontAndColor:[UIFont fontWithCustomerSizeName:@"小正文"] Color:[UIColor colorWithCustomerName:@"红"] TotalString:self.transferBopResultModel.mainInfo SubStringArray:transferBaiduEcodeArr];
}


+(CGFloat)calculationCellHeightWithModel:(TSTeachBopReadedInfo *)model{
    CGFloat width = kScreenBounds.size.width - 4 * TSFloat(25);
    CGFloat cellHeight = 0;
    CGSize mainSize = [model.mainInfo sizeWithCalcFont:[UIFont systemFontOfCustomeSize:20] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += mainSize.height;
    return cellHeight;
}

-(void)animationWithSuccess{
    self.mainLabel.text = self.transferBopResultModel.mainInfo;
    self.mainLabel.textColor = [UIColor redColor];
}

-(void)animationWithFail{
    self.mainLabel.textColor = [UIColor yellowColor];
}



#pragma mark - 用来创建测评结果的内容
-(void)setTransferBopEndResultModel:(TSTeachBopReadedInfo *)transferBopEndResultModel{
    _transferBopEndResultModel = transferBopEndResultModel;
    
    CGFloat width = kScreenBounds.size.width - 4 * TSFloat(25);
    CGSize mainSize = [transferBopEndResultModel.mainInfo sizeWithCalcFont:self.mainLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.mainLabel.frame = CGRectMake(0, 0, width, mainSize.height);
    
    self.mainLabel.attributedText = [NSAttributedString_Encapsulation changeFontAndColor:[UIFont systemFontOfCustomeSize:14.] Color:[UIColor hexChangeFloat:@"6D7984"] TotalString:transferBopEndResultModel.mainInfo SubStringArray:transferBopEndResultModel.needMainTagArr];
}


@end
