//
//  TSBopClassLrcTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/24.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBopClassLrcTableViewCell.h"

@implementation TSBopClassLrcTableViewCellLabel

- (void)setProgress:(CGFloat)progress {
    _progress = progress;
    //重绘
    [self setNeedsDisplay];
}

- (void)drawRect:(CGRect)rect {
    [super drawRect:rect];
    
    CGRect fillRect = CGRectMake(0, 0, self.bounds.size.width * _progress, self.bounds.size.height);
    
    [[UIColor hexChangeFloat:@"F9A401"] set];
    
    UIRectFillUsingBlendMode(fillRect, kCGBlendModeSourceIn);
}
@end





@interface TSBopClassLrcTableViewCell()

@end

@implementation TSBopClassLrcTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.lrcLabel = [[TSBopClassLrcTableViewCellLabel alloc]init];
    self.lrcLabel.backgroundColor = [UIColor clearColor];
    self.lrcLabel.textAlignment = NSTextAlignmentCenter;
    self.lrcLabel.numberOfLines = 0;
    [self addSubview:self.lrcLabel];
}

-(void)setTransferLrcModel:(TSLyricModel *)transferLrcModel{
    _transferLrcModel = transferLrcModel;
    self.lrcLabel.text = transferLrcModel.content;
    
    CGFloat width = kScreenBounds.size.width - 4 * TSFloat(25);
    CGSize lrcSize = [self.lrcLabel.text sizeWithCalcFont:[UIFont systemFontOfCustomeSize:17.] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.lrcLabel.frame = CGRectMake(0, TSFloat(7.), width, lrcSize.height);
   
}


- (void)reloadCellForSelect:(BOOL)select {
    if (select) {
        self.lrcLabel.font = [UIFont systemFontOfCustomeSize:17.];
    }else{
        self.lrcLabel.font = [UIFont systemFontOfCustomeSize:14.];
        self.lrcLabel.progress = 0;
    }
}

//你好，我是百e国际的一名创业资讯顾问，
//百e国际是一家大健康电商创业平台
//它专注于大健康领域的创新创业，
//只在让家庭更健康，让创业更简单

+(CGFloat)calculationCellHeightWithText:(TSLyricModel *)transferLrcModel{
    CGFloat width = kScreenBounds.size.width - 4 * TSFloat(25);
    
    CGSize lrcSize = [transferLrcModel.content sizeWithCalcFont:[UIFont systemFontOfCustomeSize:17.] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    lrcSize.height += TSFloat(7.) * 2;
    return lrcSize.height;
}

@end
