//
//  TSTeachMaterialRootSingleTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/30.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachMaterialRootSingleTableViewCell.h"


@interface TSTeachMaterialRootSingleTableViewCell()
@property (nonatomic,strong)TSImageView *imgView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *descLabel;
@end

@implementation TSTeachMaterialRootSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.imgView = [[TSImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.imgView];
    
    // 2. titlel
    self.titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    // 3. desc
    self.descLabel = [TSViewTool createLabelFont:@"小正文" textColor:@"浅灰"];
    [self addSubview:self.descLabel];
}

-(void)setTransferSingleModel:(TSTeachMaterialSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    [self.imgView uploadImageWithURL:transferSingleModel.img placeholder:nil callback:NULL];
    self.imgView.frame = CGRectMake(TSFloat(35) / 2., TSFloat(8), TSFloat(170) / 2., TSFloat(150)/ 2.);
    
    // 2.title
    self.titleLabel.text = transferSingleModel.name;
    CGFloat width = kScreenBounds.size.width - CGRectGetMaxX(self.imgView.frame) - TSFloat(30);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.imgView.frame) + TSFloat(15), 0, width, 2 * [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.titleLabel.numberOfLines = 2;
    } else {
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.imgView.frame) + TSFloat(15), 0, width, 1 * [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.titleLabel.numberOfLines = 1;
    }
    
    // 3. 创建desc
    self.descLabel.text = transferSingleModel.desc;
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (descSize.height >= 2 * [NSString contentofHeightWithFont:self.descLabel.font]){
        self.descLabel.frame = CGRectMake(self.titleLabel.orgin_x, 0, width, descSize.height);
        self.descLabel.numberOfLines = 2;
    } else {
        self.descLabel.frame = CGRectMake(self.titleLabel.orgin_x, 0, width, descSize.height);
        self.descLabel.numberOfLines = 1;
    }
    
    CGFloat margin = ([TSTeachMaterialRootSingleTableViewCell calculationCellHeight] - descSize.height - titleSize.height) / 3.;
    self.titleLabel.orgin_y = margin;
    self.descLabel.orgin_y = CGRectGetMaxY(self.titleLabel.frame) + margin;
}

-(void)setTransferBopSingleModel:(TSTeachRootSingleModel *)transferBopSingleModel{
    _transferBopSingleModel = transferBopSingleModel;

    
    [self.imgView uploadImageWithURL:transferBopSingleModel.img placeholder:nil callback:NULL];
    self.imgView.frame = CGRectMake(TSFloat(35) / 2., TSFloat(8), TSFloat(170) / 2., TSFloat(150)/ 2.);
    
    // 2.title
    self.titleLabel.text = transferBopSingleModel.name;
    CGFloat width = kScreenBounds.size.width - CGRectGetMaxX(self.imgView.frame) - TSFloat(30);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.imgView.frame) + TSFloat(15), 0, width, 2 * [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.titleLabel.numberOfLines = 2;
    } else {
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.imgView.frame) + TSFloat(15), 0, width, 1 * [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.titleLabel.numberOfLines = 1;
    }
    
    // 3. 创建desc
    self.descLabel.text = @"";
    CGSize descSize = [self.descLabel.text sizeWithCalcFont:self.descLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (descSize.height >= 2 * [NSString contentofHeightWithFont:self.descLabel.font]){
        self.descLabel.frame = CGRectMake(self.titleLabel.orgin_x, 0, width, descSize.height);
        self.descLabel.numberOfLines = 2;
    } else {
        self.descLabel.frame = CGRectMake(self.titleLabel.orgin_x, 0, width, descSize.height);
        self.descLabel.numberOfLines = 1;
    }
    
    CGFloat margin = ([TSTeachMaterialRootSingleTableViewCell calculationCellHeight] - descSize.height - titleSize.height) / 3.;
    self.titleLabel.orgin_y = margin;
    self.descLabel.orgin_y = CGRectGetMaxY(self.titleLabel.frame) + margin;
}



+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += TSFloat(8);
    cellHeight += TSFloat(75);
    cellHeight += TSFloat(8);
    return cellHeight;
}
@end
