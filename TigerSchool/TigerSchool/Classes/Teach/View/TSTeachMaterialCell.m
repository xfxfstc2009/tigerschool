//
//  TSTeachMaterialCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachMaterialCell.h"
#import "TSTeachRootModel.h"

static char teachItemActionToClickKey;
@interface TSTeachMaterialCell()
@property (nonatomic,strong)UIScrollView *bgScrollView;

@end

@implementation TSTeachMaterialCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
    }
    return self;
}



-(void)setTransferMaterialArr:(NSArray *)transferMaterialArr{
    _transferMaterialArr = transferMaterialArr;
    if (!self.bgScrollView && transferMaterialArr.count){
        self.bgScrollView = [TSViewTool gwCreateScrollViewWithRect:CGRectMake(0, 0, kScreenBounds.size.width, [TSTeachMaterialCell calculationCellHeight])];
        self.bgScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * 10, self.bgScrollView.size_height);
        [self addSubview:self.bgScrollView];
        
        CGFloat width = TSFloat(120);
        CGFloat height = self.bgScrollView.size_height;
        __weak typeof(self)weakSelf = self;
        for (int i = 0 ; i < transferMaterialArr.count;i++){
            TSTeachMaterialSingleModel *singleModel = [transferMaterialArr objectAtIndex:i];
            CGFloat origin_x = TSFloat(35) / 2. + i * (TSFloat(120) + TSFloat(10));
            TSTeachMaterialSingleView *singleView = [[TSTeachMaterialSingleView alloc]initWithFrame:CGRectMake(origin_x, 0, width, height)];
            singleView.transferSingleModel = singleModel;
            [singleView actionClickToManagerBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                void(^block)(TSTeachMaterialSingleModel *singleModel) = objc_getAssociatedObject(strongSelf, &teachItemActionToClickKey);
                if (block){
                    block(singleModel);
                }
            }];
            [self.bgScrollView addSubview:singleView];
        }
    }
}

+(CGFloat)calculationCellHeight{
    return TSFloat(106) + TSFloat(14) + [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
}

-(void)teachItemActionToClick:(void(^)(TSTeachMaterialSingleModel *singleModel))block{
    objc_setAssociatedObject(self, &teachItemActionToClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



@end
