//
//  TSTeachMaterialSingleView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachMaterialSingleView.h"

static char actionClickToManagerBlockKey;
@implementation TSTeachMaterialSingleModel

-(NSDictionary *)modelKeyJSONKeyMapper{
    return @{@"desc":@"description",@"ID":@"id"};
}
@end

@interface TSTeachMaterialSingleView()
@property (nonatomic,strong)TSImageView *bgImageView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *actionButton;

@end

@implementation TSTeachMaterialSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建图片
    self.bgImageView = [[TSImageView alloc]init];
    self.bgImageView.backgroundColor = UURandomColor;
    self.bgImageView.frame = CGRectMake(0, 0, 120, 106);
    [self addSubview:self.bgImageView];
    
    // 2. 创建文字
    self.titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.text = @"百e国际";
    self.titleLabel.frame = CGRectMake(0, CGRectGetMaxY(self.bgImageView.frame), self.bgImageView.size_width, [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.frame = self.bounds;
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickToManagerBlockKey);
        if (block){
            block();
        }
    }];
}

-(void)setTransferSingleModel:(TSTeachMaterialSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    // img
    [self.bgImageView uploadImageWithURL:transferSingleModel.img placeholder:nil callback:NULL];
    
    // title
    self.titleLabel.text = transferSingleModel.name;
}

-(void)actionClickToManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickToManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
