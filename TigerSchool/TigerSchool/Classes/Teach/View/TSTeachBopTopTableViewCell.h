//
//  TSTeachBopTopTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSTeachRootModel.h"

@interface TSTeachBopTopTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)TSTeachRootSingleModel *transferSingleModel;

@end
