//
//  TSTeachRootTitleTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTeachRootTitleTableViewCell.h"

static char actionClickWithDymicBlockKey;
@interface TSTeachRootTitleTableViewCell()
@property (nonatomic,strong)TSImageView *iconImageView;
@property (nonatomic,strong)TSImageView *arrowImageView;

@end



@implementation TSTeachRootTitleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.backgroundColor = [UIColor whiteColor];
        [self createView];
    }
    return self;
}

-(void)createView{
    self.iconImageView = [[TSImageView alloc]init];
    self.iconImageView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImageView];
    
    // 2. 创建标题
    self.titleLabel = [[UILabel alloc]init];
    self.titleLabel.backgroundColor = [UIColor clearColor];
    self.titleLabel.font = [[UIFont systemFontOfCustomeSize:21.] boldFont];
    self.titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    [self addSubview:self.titleLabel];
    
    // 3. 创建dymicLabel
    self.dymicLabel = [[UILabel alloc]init];
    self.dymicLabel.backgroundColor = [UIColor clearColor];
    self.dymicLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    self.dymicLabel.adjustsFontSizeToFitWidth = YES;
    self.dymicLabel.textAlignment = NSTextAlignmentRight;
    [self addSubview:self.dymicLabel];
    
    // 4. 创建箭头
    self.arrowImageView = [[TSImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"icon_tool_arrow"];
    [self addSubview:self.arrowImageView];
    
    self.dymicButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dymicButton.hidden = YES;
    [self addSubview:self.dymicButton];
}

// icon
-(void)setTransferIcon:(UIImage *)transferIcon{
    _transferIcon = transferIcon;
    self.iconImageView.image = transferIcon;
    [self.iconImageView sizeToFit];
    
    self.iconImageView.orgin_x = TSFloat(16);
    self.iconImageView.center_y = self.transferCellHeight / 2.;
    
    [self aucalculationFrame];
}

//title
-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    self.titleLabel.text = transferTitle;
    self.titleLabel.textAlignment = NSTextAlignmentLeft;
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
    self.titleLabel.frame = CGRectMake(TSFloat(16), 0, titleSize.width, self.transferCellHeight);
    
    [self aucalculationFrame];
}

//desc
-(void)setTransferDesc:(NSString *)transferDesc{
    _transferDesc = transferDesc;
    self.dymicLabel.text = transferDesc;
    [self aucalculationFrame];
}

//arrow
-(void)setTransferHasArrow:(BOOL)transferHasArrow{
    _transferHasArrow = transferHasArrow;
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - TSFloat(16) - TSFloat(9), (self.transferCellHeight - TSFloat(15)) / 2., TSFloat(9), TSFloat(15));
    [self aucalculationFrame];
}

-(void)aucalculationFrame{
    
    // 1. 计算标题
    
    if(self.transferIcon){
        self.titleLabel.orgin_x = CGRectGetMaxX(self.iconImageView.frame) + TSFloat(5);
    } else {
        self.titleLabel.orgin_x = TSFloat(16);
    }
    
    // 2. 计算箭头
    CGFloat width = 0 ;
    if (self.transferHasArrow){         // 如果有箭头
        self.arrowImageView.hidden = NO;
        width = self.arrowImageView.orgin_x - CGRectGetMaxX(self.titleLabel.frame) - TSFloat(11) - TSFloat(5);
    } else{
        width = kScreenBounds.size.width - CGRectGetMaxX(self.titleLabel.frame) - 2 * TSFloat(11);
        self.arrowImageView.hidden = YES;
    }
    
    CGSize dymicSize = [self.dymicLabel.text sizeWithCalcFont:self.dymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.dymicLabel.font])];
    
    if (self.transferHasArrow){         // 如果有箭头
        self.dymicLabel.frame =CGRectMake(self.arrowImageView.orgin_x - TSFloat(11) - dymicSize.width, 0, dymicSize.width, self.transferCellHeight);
    } else {
        self.dymicLabel.frame =CGRectMake(kScreenBounds.size.width - TSFloat(11) - dymicSize.width, 0, dymicSize.width, self.transferCellHeight);
    }
    
    self.dymicButton.frame = self.dymicLabel.frame;
    __weak typeof(self)weakSelf = self;
    [self.dymicButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void (^block)() = objc_getAssociatedObject(strongSelf, &actionClickWithDymicBlockKey);
        if (block){
            block();
        }
    }];
    
}

-(void)setTextAlignmentCenter:(BOOL)textAlignmentCenter{
    _textAlignmentCenter = textAlignmentCenter;
    if (textAlignmentCenter){
        self.titleLabel.frame = CGRectMake(TSFloat(16), 0, kScreenBounds.size.width - TSFloat(16) * 2, self.transferCellHeight);
        self.titleLabel.textAlignment = NSTextAlignmentCenter;
    }
}

+(CGFloat)calculationCellHeight{
    return TSFloat(44);
}

-(void)actionClickWithDymicBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithDymicBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


@end
