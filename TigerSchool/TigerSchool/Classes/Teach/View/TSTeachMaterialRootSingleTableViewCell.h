//
//  TSTeachMaterialRootSingleTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/30.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSTeachMaterialSingleView.h"
#import "TSBaseTableViewCell.h"
//#import "TSTeachRootSingleModel."
#import "TSTeachRootModel.h"
@interface TSTeachMaterialRootSingleTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)TSTeachMaterialSingleModel *transferSingleModel;

@property (nonatomic,strong)TSTeachRootSingleModel *transferBopSingleModel;

@end
