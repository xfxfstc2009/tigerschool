//
//  TSTeachRootTitleTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"

@interface TSTeachRootTitleTableViewCell : TSBaseTableViewCell

@property (nonatomic,copy)NSString *transferTitle;                  /**< 传入的标题*/
@property (nonatomic,strong)UIImage *transferIcon;                  /**< 传入的icon*/
@property (nonatomic,copy)NSString *transferDesc;                   /**< 传入的内容*/
@property (nonatomic,assign)BOOL transferHasArrow;                  /**< 是否箭头*/
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *dymicLabel;
@property (nonatomic,assign)BOOL textAlignmentCenter;               /**< 是否设置中间*/
@property (nonatomic,strong)UIButton *dymicButton;

-(void)actionClickWithDymicBlock:(void(^)())block;

+(CGFloat)calculationCellHeight;

@end
