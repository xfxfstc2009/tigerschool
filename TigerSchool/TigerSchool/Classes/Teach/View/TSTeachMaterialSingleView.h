//
//  TSTeachMaterialSingleView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "FetchModel.h"

@protocol TSTeachMaterialSingleModel <NSObject>

@end


@interface TSTeachMaterialSingleModel : FetchModel

@property (nonatomic,copy)NSString *img;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *desc;
@property (nonatomic,copy)NSString *ID;


@end


@interface TSTeachMaterialSingleView : UIView

@property (nonatomic,strong)TSTeachMaterialSingleModel *transferSingleModel;

-(void)actionClickToManagerBlock:(void(^)())block;
@end
