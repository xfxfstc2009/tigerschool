//
//  TSTeachMaterialCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSTeachMaterialSingleView.h"

@interface TSTeachMaterialCell : TSBaseTableViewCell

@property (nonatomic,strong)NSArray *transferMaterialArr;

-(void)teachItemActionToClick:(void(^)(TSTeachMaterialSingleModel *singleModel))block;

@end
