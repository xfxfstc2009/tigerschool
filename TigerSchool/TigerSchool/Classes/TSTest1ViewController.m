//
//  TSTest1ViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/17.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTest1ViewController.h"
#import <SCSiriWaveformView.h>
@interface TSTest1ViewController ()
@property (nonatomic,strong)SCSiriWaveformView *waveView;
@end

@implementation TSTest1ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createView];
    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{
        
        [self.waveView updateWithLevel:.1f];
    }];
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES];
}

-(void)createView{
    self.waveView = [[SCSiriWaveformView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.width)];
    self.waveView.alpha = 1;
    self.waveView.numberOfWaves = 10;
    self.waveView.frame = CGRectMake(0, 0, kScreenBounds.size.width, kScreenBounds.size.width);
    self.waveView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.waveView];
}


@end
