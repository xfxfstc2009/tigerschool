//
//  TSTestViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSTestViewController.h"
#import <IAPShare.h>
@interface TSTestViewController ()

@end

@implementation TSTestViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self manager1];
    [self rightBarButtonWithTitle:@"123" barNorImage:nil barHltImage:nil action:^{
        [self smart];
    }];
}

-(void)manager1{
    if(![IAPShare sharedHelper].iap) {
        // ProductID_diamond1 我这里是宏，产品id一般为你工程的 Bundle ID+数字
        // 我这里是6个内购的商品
        NSSet* dataSet = [[NSSet alloc] initWithObjects:@"com.tigerschool.class88",nil];
        
        [IAPShare sharedHelper].iap = [[IAPHelper alloc] initWithProductIdentifiers:dataSet];
        //yes为生产环境  no为沙盒测试模式
        // 客户端做收据校验有用  服务器做收据校验忽略...
         [IAPShare sharedHelper].iap.production = NO;
    }
}

-(void)smart{
    // 请求商品信息
    [[IAPShare sharedHelper].iap requestProductsWithCompletion:^(SKProductsRequest* request,SKProductsResponse* response)
     {
          NSLog(@"无效的产品product ID = %@",response.invalidProductIdentifiers);
         
         if(response.products.count > 0 ) {
             //取出第一件商品id
             SKProduct *product = response.products[0];
             
             [[IAPShare sharedHelper].iap buyProduct:product onCompletion:^(SKPaymentTransaction* trans){
                 if(trans.error) {
                 
                 } else if(trans.transactionState == SKPaymentTransactionStatePurchased) {
                     NSLog(@"购买成功");
                     // 这个 receipt 就是内购成功 苹果返回的收据
                     NSData *receipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
//                     NSString *result = [[NSString alloc] initWithData:receipt  encoding:NSUTF8StringEncoding];
                     NSDictionary *dicWithRequestJson = [NSJSONSerialization JSONObjectWithData:receipt options:0 error:nil];
                     NSLog(@"%@",dicWithRequestJson);
//                    /******这里我将receipt base64加密，把加密的收据 和 产品id，一起发送到app服务器********/
//                    NSString *receiptBase64 = [NSString base64StringFromData:receipt length:[receipt length]];
//
//                    [self sendCheckReceiptWithBase64:receiptBase64 productID:product.productIdentifier];
//                    /*******上面的sendCheckReceipt请求成功了，会返回用户购买的钻石数量*******/

//                                                //客户端做收据验证 (不建议)
                     [[IAPShare sharedHelper].iap checkReceipt:receipt onCompletion:^(NSString *response, NSError *error) {
                         NSLog(@"购买验证---%@",response);
                     }];
//
                     NSLog(@"%@",receipt);
                 } else if(trans.transactionState == SKPaymentTransactionStateFailed) {
                     if (trans.error.code == SKErrorPaymentCancelled) {
                     
                     } else if (trans.error.code == SKErrorClientInvalid) {
                
                     } else if (trans.error.code == SKErrorPaymentInvalid) {
                     
                     } else if (trans.error.code == SKErrorPaymentNotAllowed) {
                     
                     } else if (trans.error.code == SKErrorStoreProductNotAvailable) {
                     
                     } else {
                }
            }
        }];
         } else {
             //  ..未获取到商品
             NSLog(@"..未获取到商品");
         }
     }];
}


@end
