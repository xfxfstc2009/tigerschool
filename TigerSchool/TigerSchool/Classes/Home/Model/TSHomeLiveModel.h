//
//  TSHomeLiveModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "FetchModel.h"


@protocol TSHomeLiveModel <NSObject>

@end


@interface TSHomeLiveModel : FetchModel

@property (nonatomic,copy)NSString *Timelong;       /**< 时长*/
@property (nonatomic,copy)NSString *appellation;    /**< 教师称谓*/
@property (nonatomic,copy)NSString *icon;           /**< 图标*/
@property (nonatomic,copy)NSString *ID;             /**< ID*/
@property (nonatomic,copy)NSString *img;            /**< 图片*/
@property (nonatomic,assign)BOOL isdirect;          /**< 是否在直播中*/
@property (nonatomic,assign)NSInteger levellimit; /**< 登记限制*/
@property (nonatomic,assign)CGFloat price;    /**< 价格*/
@property (nonatomic,copy)NSString *starttime;  /**< 开课时间*/
@property (nonatomic,copy)NSString *teacher;    /**< 到时*/
@property (nonatomic,copy)NSString *title;      /**< 标题*/

// 搜索处
@property (nonatomic,copy)NSString *name;      /**< 标题*/
@property (nonatomic,copy)NSString *url;
@property (nonatomic,assign)NSInteger type;



@end



@interface TSHomeLiveListModel : FetchModel

@property (nonatomic,strong)NSArray <TSHomeLiveModel>*items;

@end
