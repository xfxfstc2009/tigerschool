//
//  TSHomeDetailQuestionSingleModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/21.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@interface TSHomeDetailQuestionSingleModel : FetchModel

@property (nonatomic,assign)NSInteger score;

@end
