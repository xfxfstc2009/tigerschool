//
//  TSHomeBannerModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@protocol TSHomeBannerModel <NSObject>

@end

@interface TSHomeBannerModel : FetchModel

@property (nonatomic,assign)NSInteger courseid;
@property (nonatomic,assign)NSInteger levellimit;
@property (nonatomic,assign)NSInteger levelname;
@property (nonatomic,copy)NSString *url;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,assign)CGFloat price;

@end


@interface TSHomeBannerListModel:FetchModel

@property (nonatomic,strong)NSArray<TSHomeBannerModel> *items;

@end
