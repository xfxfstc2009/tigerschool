//
//  TSHomeCourseDetailClassModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@protocol TSHomeCourseDetailExamitemsSingleModel <NSObject>

@end


@interface TSHomeCourseDetailExamitemsSingleModel: FetchModel
@property (nonatomic,strong)NSArray *answers;               /**< 答案*/
@property (nonatomic,copy)NSString *answer;                 /**< 正确答案*/
@property (nonatomic,copy)NSString *ID;                     /**< 题目编号*/
@property (nonatomic,assign)BOOL ismul;                     /**< 是否多选题*/
@property (nonatomic,assign)NSInteger right;
@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *useranswer;
@property (nonatomic,assign)NSInteger wrong;

@property (nonatomic,assign)NSInteger questionNumber;       //
@property (nonatomic,copy)NSString *userChoose;

@property (nonatomic,strong)NSArray *userChoosedArr;        /**< 用户选择的答案*/

@end

@interface TSHomeCourseDetailExamitemsMainModel: FetchModel

@property (nonatomic,strong)NSArray <TSHomeCourseDetailExamitemsSingleModel> *items;

@end



@protocol TSHomeCourseDetailClassModel <NSObject>

@end

@interface TSHomeCourseDetailClassModel : FetchModel

@property (nonatomic,strong)NSArray *datums;
@property (nonatomic,copy)NSString *detailid;
@property (nonatomic,copy)NSString *endtime;
@property (nonatomic,strong)NSArray<TSHomeCourseDetailExamitemsSingleModel> *examitems;
@property (nonatomic,copy)NSString *img;
@property (nonatomic,assign)BOOL istest;
@property (nonatomic,assign)BOOL iswatch;
@property (nonatomic,copy)NSString *name;
@property (nonatomic,assign)CGFloat process;
@property (nonatomic,assign)CGFloat score;
@property (nonatomic,copy)NSString *starttime;
@property (nonatomic,copy)NSString *subtime;
@property (nonatomic,assign)NSInteger timelong;
@property (nonatomic,copy)NSString *url;

@end

@interface TSHomeCourseDetailListClassModel : FetchModel
@property (nonatomic,strong)NSArray <TSHomeCourseDetailClassModel> *items;
@end



