//
//  TSCoursedescModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@interface TSCoursedescModel : FetchModel

@property (nonatomic,copy)NSString *title;
@property (nonatomic,copy)NSString *cou_desc;   /**< 课程简介*/
@property (nonatomic,copy)NSString *tec_desc;   /**< 教师简介*/
@property (nonatomic,copy)NSString *buy_desc;   /**< 购买需知*/

@end




@protocol TSCoursedescDescModel <NSObject>

@end


@interface TSCoursedescDescModel:FetchModel

@property (nonatomic,assign)BOOL iswatch;           /**< 是否看过*/
@property (nonatomic,copy)NSString *logintime;      /**< 另一个设备登录时间格式 2006-01-02 15:04:05*/
@property (nonatomic,copy)NSString *devicename;     /**< 设备名称*/
@property (nonatomic,copy)NSString *detailid;       /**< 小节id*/
@property (nonatomic,copy)NSString *name;           /**< 名字*/
@property (nonatomic,copy)NSString *url;            /**< url*/
@property (nonatomic,copy)NSString *img;            /**< 图片*/
@property (nonatomic,assign)NSInteger timelong;     /**< 时间长度*/
@property (nonatomic,copy)NSString *starttime;      /**< 开课时间*/
@property (nonatomic,copy)NSString *endtime;        /**< 结束时间*/
@property (nonatomic,copy)NSString *subtime;        /**< 最晚自测提交时间*/
@property (nonatomic,assign)BOOL istest;            /**< 是否已完成测试*/
@property (nonatomic,assign)NSInteger score;        /**< 答题分数*/
@property (nonatomic,assign)NSInteger process;      /**< 小节观看进度  单位秒*/
@property (nonatomic,strong)NSArray *datums;        /**< 课程资料 数组内为地址*/
@property (nonatomic,strong)NSArray *examitems;     /**< 课后自测题目信息*/
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *title;          /**< 标题*/
@property (nonatomic,strong)NSArray *answers;       /**< 答案*/
@property (nonatomic,assign)NSInteger answer;       /**< 正确答案1代表A 2B 3C 4D 5e 6 f*/
@property (nonatomic,assign)BOOL ismul;             /**< 是否多选题*/
@end

@interface TSCoursedescDescListModel:FetchModel

@property (nonatomic,strong)NSArray<TSCoursedescDescModel> *items;

@end
