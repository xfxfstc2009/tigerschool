//
//  TSHomeAllClassTagsModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@protocol TSHomeAllClassSingleTagsModel <NSObject>

@end

@interface TSHomeAllClassSingleTagsModel : FetchModel

@property (nonatomic,copy)NSString *name;
@property (nonatomic,copy)NSString *url;
@property (nonatomic,copy)NSString *ID;

@end

@interface TSHomeAllClassTagsModel : FetchModel

@property (nonatomic,strong)NSArray <TSHomeAllClassSingleTagsModel>*items;

@end



