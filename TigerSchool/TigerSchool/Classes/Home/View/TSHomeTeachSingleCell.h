//
//  TSHomeTeachSingleCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSHomeLiveModel.h"

@interface TSHomeTeachSingleCell : TSBaseTableViewCell

@property (nonatomic,strong)TSHomeLiveModel *transferHomeLiveModel;

@end
