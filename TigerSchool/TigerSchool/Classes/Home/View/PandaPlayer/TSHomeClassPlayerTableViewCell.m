//
//  TSHomeClassPlayerTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassPlayerTableViewCell.h"


static char playerButtonClickKey;
@interface TSHomeClassPlayerTableViewCell()
@property (nonatomic,strong)UIButton *playButton;

@end


@implementation TSHomeClassPlayerTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    // 1. 创建头部图片
    self.playerImageView = [[TSImageView alloc]init];
    self.playerImageView.backgroundColor = UURandomColor;
    self.playerImageView.tag = 102;
    self.playerImageView.userInteractionEnabled = YES;
    [self addSubview:self.playerImageView];
    self.playerImageView.layer.shadowColor = [[UIColor colorWithCustomerName:@"浅灰"] CGColor];
    self.playerImageView.layer.shadowOpacity = 2.0;
    self.playerImageView.layer.shadowOffset = CGSizeMake(2.0f, 2.0f);
    
    // 2. 创建按钮
    self.playButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.playButton.backgroundColor = UURandomColor;
    self.playButton.userInteractionEnabled = YES;
//    [self.playButton setImage:[UIImage imageNamed:@"ic_radio"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [weakSelf.playButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &playerButtonClickKey);
        if (block){
            block();
        }
        
    }];
//    self.playButton.backgroundColor = [UIColor clearColor];
    [self.playerImageView addSubview:self.playButton];
}


-(void)setTransferCellheight:(CGFloat)transferCellheight{
    _transferCellheight = transferCellheight;
    self.playerImageView.frame = CGRectMake(TSFloat(11), TSFloat(11), kScreenBounds.size.width - 2 * TSFloat(11), self.transferCellheight - 2 * TSFloat(11));
    
    self.playButton.frame = CGRectMake((kScreenBounds.size.width - TSFloat(50)) / 2., (self.playerImageView.size_height - TSFloat(50)) / 2., TSFloat(50), TSFloat(50));
}

//-(void)setTransferLiveRootSingleModel:(PDLiveRoomSingleModel *)transferLiveRootSingleModel{
//    _transferLiveRootSingleModel = transferLiveRootSingleModel;
//    [self.playerImageView uploadImageWithURL:transferLiveRootSingleModel.coverImg placeholder:nil callback:NULL];
//}

-(void)playerViewClickManagerWithBlock:(void(^)())block{
    objc_setAssociatedObject(self, &playerButtonClickKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    return TSFloat(460) / 2.;
}
@end
