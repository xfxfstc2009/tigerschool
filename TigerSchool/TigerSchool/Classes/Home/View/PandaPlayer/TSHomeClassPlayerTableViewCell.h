//
//  TSHomeClassPlayerTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"

@interface TSHomeClassPlayerTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)TSImageView *playerImageView;

@property (nonatomic,assign) CGFloat transferCellheight;

-(void)playerViewClickManagerWithBlock:(void(^)())block;


@end
