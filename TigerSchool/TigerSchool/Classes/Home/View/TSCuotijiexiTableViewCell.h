//
//  TSCuotijiexiTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/21.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"

@interface TSCuotijiexiTableViewCell : TSBaseTableViewCell


-(void)showAnimationWithScore:(NSInteger)score;

-(void)actionClickToGetCuotijiexiBlock:(void(^)())block;
@end
