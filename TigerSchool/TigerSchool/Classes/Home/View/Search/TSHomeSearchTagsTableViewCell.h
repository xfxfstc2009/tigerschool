//
//  TSHomeSearchTagsTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/21.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHomeSearchTagsTableViewCell : UITableViewCell

@property (nonatomic,strong)NSArray *transferArr;
@property (nonatomic,copy)NSString *barTitle;

-(void)actionClickWithTagsBlock:(void(^)(NSString *info))block;

+(CGFloat)calculationCellHeightWithArr:(NSArray *)transferArr;



@end
