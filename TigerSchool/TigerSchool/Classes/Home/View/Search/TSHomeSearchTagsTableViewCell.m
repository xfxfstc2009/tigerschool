//
//  TSHomeSearchTagsTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/21.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeSearchTagsTableViewCell.h"

@interface TSHomeSearchTagsTableViewCell()
@property (nonatomic,strong)UIView *bgView;             /**< 背景图*/

@end

static char actionClickWithTagsBlockKey;
@implementation TSHomeSearchTagsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.userInteractionEnabled = YES;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 100);
    [self addSubview:self.bgView];
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    self.bgView.size_height = transferCellHeight;
}

-(void)setTransferArr:(NSArray *)transferArr{
    _transferArr = transferArr;
    
    CGFloat flagWidth = kScreenBounds.size.width - 2 * TSFloat(12);
    CGFloat flagOriginX = TSFloat(12);
    CGFloat flagOriginY = TSFloat(10);
    CGFloat flagMargin = TSFloat(10);

    if (self.bgView.subviews.count){
        [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }

    for (int i = 0 ; i < transferArr.count;i++){
        NSString *info = [transferArr objectAtIndex:i];
        UIView *flagImageView = [self createTagViewWithTitle:info];
        if (flagImageView.size_width < flagWidth){                      // 不换行
            flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
            flagWidth -= (flagImageView.size_width + flagMargin);
            flagOriginX += (flagImageView.size_width + flagMargin);
        } else {
            flagOriginY += [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13.]] + 2 * TSFloat(4) + flagMargin;
            flagOriginX =  flagMargin;
            flagWidth = kScreenBounds.size.width - 2 * flagMargin;
            flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
            flagWidth -= (flagImageView.size_width + flagMargin);
            flagOriginX += (flagImageView.size_width + flagMargin);
        }
        [self.bgView addSubview:flagImageView];
    }

}

//-(void)setTransferSingleModel:(NSArray<TSHomeAllClassSingleTagsModel> *)transferSingleModel{
//    _transferSingleModel = transferSingleModel;

//}
//
//
#pragma mark - createTagView
-(UIView *)createTagViewWithTitle:(NSString *)info{
    TSImageView *tagView = [[TSImageView alloc]init];
    tagView.backgroundColor = [UIColor clearColor];
    tagView.image = [TSTool stretchImageWithName:@"img_frame_search"];
    tagView.userInteractionEnabled = YES;

    //1. 创建文字
    UILabel *flagLabel = [[UILabel alloc]init];
    flagLabel.backgroundColor = [UIColor clearColor];
    flagLabel.font = [UIFont systemFontOfCustomeSize:13.];
    flagLabel.userInteractionEnabled = YES;
    flagLabel.textColor = [UIColor colorWithCustomerName:@"浅灰"];
    flagLabel.text = info;
    CGSize flagLabelSize = [flagLabel.text sizeWithCalcFont:flagLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:flagLabel.font])];
    flagLabel.frame = CGRectMake(TSFloat(11), 0, flagLabelSize.width, 2 * TSFloat(4) + [NSString contentofHeightWithFont:flagLabel.font]);
    [tagView addSubview:flagLabel];

    // 2. 创建按钮
    UIButton *selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectedButton.backgroundColor = [UIColor clearColor];
    selectedButton.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [selectedButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(NSString *info) = objc_getAssociatedObject(strongSelf, &actionClickWithTagsBlockKey);
        if (block){
            block(info);
        }
    }];
    [tagView addSubview:selectedButton];

    tagView.bounds = CGRectMake(0, 0, flagLabel.size_width + 2 * TSFloat(11), 2 * TSFloat(4) + [NSString contentofHeightWithFont:flagLabel.font] );
    selectedButton.frame = tagView.bounds;
    return tagView;
}

+(CGFloat)calculationCellHeightWithArr:(NSArray *)transferArr{

    CGFloat cellHeight = TSFloat(11);

    CGFloat flagWidth = kScreenBounds.size.width - 2 * TSFloat(12);
    CGFloat flagOriginX = TSFloat(12);
    CGFloat flagOriginY = TSFloat(10);
    CGFloat flagMargin = TSFloat(10);

    for (int i = 0 ; i < transferArr.count;i++){
        NSString *info = [transferArr objectAtIndex:i];
        CGSize floagSize = [info sizeWithCalcFont:[UIFont systemFontOfCustomeSize:13.] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13.]])];
        CGFloat floagWidth = floagSize.width + 2 * TSFloat(11);

        if (floagWidth < flagWidth){                      // 不换行
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
        } else {
            flagOriginY += [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13.]] + 2 * TSFloat(4) + flagMargin;
            flagOriginX =  TSFloat(10);
            flagWidth = kScreenBounds.size.width - 2 * flagMargin;
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
            cellHeight = flagOriginY;
        }
    
    }
    cellHeight += [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13.]] + 2 * TSFloat(4) + TSFloat(11);

    return cellHeight;
}


-(void)actionClickWithTagsBlock:(void(^)(NSString *info))block{
    objc_setAssociatedObject(self, &actionClickWithTagsBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
