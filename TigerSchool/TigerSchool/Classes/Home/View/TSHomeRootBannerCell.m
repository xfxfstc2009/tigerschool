//
//  TSHomeRootBannerCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeRootBannerCell.h"
#import "TSHomeBannerView.h"


static char actionClickWithBlockKey;
@interface TSHomeRootBannerCell()<TSHomeBannerViewDelegate>
@property (nonatomic,strong)TSHomeBannerView *homeBannerView;
@property (nonatomic,strong)NSArray *imgArr;

@end

@implementation TSHomeRootBannerCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        
    }
    return self;
}

-(void)setTransferBannerArr:(NSArray<TSHomeBannerModel> *)transferBannerArr{
    _transferBannerArr = transferBannerArr;
    
    NSMutableArray *bannerArr = [NSMutableArray array];
    for (int i = 0 ; i < transferBannerArr.count;i++){
        TSHomeBannerModel *bannerSingleModel = [transferBannerArr objectAtIndex:i];
        [bannerArr addObject:bannerSingleModel.url];
    }
    
    if (!self.homeBannerView && transferBannerArr.count){
        self.homeBannerView = [TSHomeBannerView cycleScrollViewWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, TSFloat(170)) shouldInfiniteLoop:YES imageGroups:bannerArr];
        self.homeBannerView.autoScrollTimeInterval = 5;
        self.homeBannerView.autoScroll = YES;
        self.homeBannerView.isZoom = YES;
        self.homeBannerView.itemSpace = -TSFloat(16);
        self.homeBannerView.imgCornerRadius = 0;
        self.homeBannerView.itemWidth = TSFloat(580) / 2.;
        self.homeBannerView.pageControl.hidden = YES;
        self.homeBannerView.delegate = self;
        [self addSubview:self.homeBannerView];
    }
}


-(void)bannerScrollView:(TSHomeBannerView *)cycleScrollView didSelectItemAtIndex:(NSInteger)index{
    TSHomeBannerModel *homeBannerSingleModel = [self.transferBannerArr objectAtIndex:index];
    
    void(^block)(TSHomeBannerModel *singleModel) = objc_getAssociatedObject(self, &actionClickWithBlockKey);
    if (block){
        block(homeBannerSingleModel);
    }
}


-(void)actionClickWithBlock:(void(^)(TSHomeBannerModel *singleModel))block{
    objc_setAssociatedObject(self, &actionClickWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += TSFloat(11);
    cellHeight += TSFloat(170);
    cellHeight += TSFloat(11);
    return cellHeight;
    
}

@end
