//
//  TSHomeTagsViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//
// 热门标签

#import "TSBaseTableViewCell.h"
#import "TSHomeAllClassTagsModel.h"
@interface TSHomeTagsViewCell : TSBaseTableViewCell

@property (nonatomic,strong)NSArray<TSHomeAllClassSingleTagsModel> *tansferTagsArr;            /**< 上个页面传递过来的tag*/

-(void)homeTagClickBlock:(void(^)(TSHomeAllClassSingleTagsModel *singleModel))block;

+(CGFloat)calculationCellHeightTagsArr:(NSArray<TSHomeAllClassSingleTagsModel> *)tansferTagsArr;

@end
