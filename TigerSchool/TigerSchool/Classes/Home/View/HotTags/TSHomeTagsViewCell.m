//
//  TSHomeTagsViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeTagsViewCell.h"
#import "TSHomeTagsSingleView.h"

static char homeTagClickBlockKey;
@interface TSHomeTagsViewCell()
@property (nonatomic,strong)UIView *baseView;


@end

@implementation TSHomeTagsViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark createView
-(void)createView{
    self.baseView = [[UIView alloc]init];
    self.baseView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.baseView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    self.transferCellHeight = transferCellHeight;
}

-(void)setTansferTagsArr:(NSArray<TSHomeAllClassSingleTagsModel> *)tansferTagsArr{
    _tansferTagsArr = tansferTagsArr;
    
    CGFloat width = (kScreenBounds.size.width - 2 * TSFloat(16) - 2 * TSFloat(14)) / 3.;
    
    if (self.baseView.subviews.count){
        [self.baseView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < self.tansferTagsArr.count;i++){
        NSInteger numberOfRows = i / 3;
        NSInteger index = i % 3;
        CGFloat origin_x = TSFloat(16) + index *(TSFloat(14) + width);
        CGFloat origin_y = 0 + numberOfRows * (TSFloat(38) + TSFloat(14));
        CGRect frame = CGRectMake(origin_x, origin_y, width, TSFloat(38));
        TSHomeTagsSingleView *singleview = [[TSHomeTagsSingleView alloc]initWithFrame:frame];
        singleview.backgroundColor = UURandomColor;
        TSHomeAllClassSingleTagsModel *singleModel = [self.tansferTagsArr objectAtIndex:i];
        singleview.transferSingleModel = singleModel;
        
        [singleview tapManagerWithBlock:^(TSHomeAllClassSingleTagsModel *singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(TSHomeAllClassSingleTagsModel *tempInfoModel) = objc_getAssociatedObject(strongSelf, &homeTagClickBlockKey);
            if (block){
                block(singleModel);
            }
        }];
        [self.baseView addSubview:singleview];
    }
    
    self.baseView.frame = CGRectMake(0, 0, kScreenBounds.size.width, [TSHomeTagsViewCell calculationCellHeightTagsArr:tansferTagsArr]);
}

-(void)homeTagClickBlock:(void(^)(TSHomeAllClassSingleTagsModel *singleModel))block{
    objc_setAssociatedObject(self, &homeTagClickBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeightTagsArr:(NSArray<TSHomeAllClassSingleTagsModel> *)tansferTagsArr{
    CGFloat cellHeight = 10;
 
    if (tansferTagsArr.count){
        NSInteger numberOfLine = tansferTagsArr.count % 3 == 0 ? tansferTagsArr.count / 3: tansferTagsArr.count / 3 + 1;
        
        cellHeight = TSFloat(38) * numberOfLine + TSFloat(14) * (numberOfLine - 1);
    }
    
    return cellHeight;
}







@end
