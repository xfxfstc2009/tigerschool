//
//  TSHomeTagsSingleView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeTagsSingleView.h"

static char tapManagerWithBlockKey;
@interface TSHomeTagsSingleView()
@property (nonatomic,strong)TSImageView *backgroundImageView;
@property (nonatomic,strong)TSImageView *topalphaImageView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UITapGestureRecognizer *tapGesture;

@end

@implementation TSHomeTagsSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self= [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.backgroundImageView = [[TSImageView alloc]init];
    self.backgroundImageView.backgroundColor = [UIColor clearColor];
    self.backgroundImageView.frame = self.bounds;
    [self addSubview:self.backgroundImageView];
    
    // 创建上层蒙层
    self.topalphaImageView = [[TSImageView alloc]init];
    self.topalphaImageView.backgroundColor = [UIColor clearColor];
    self.topalphaImageView.frame = self.bounds;
    self.topalphaImageView.image = [TSTool stretchImageWithName:@"icon_home_tags_alpha"];
    [self.backgroundImageView addSubview:self.topalphaImageView];
    
    // 创建上层label
    self.titleLabel = [TSViewTool createLabelFont:@"小正文" textColor:@"白"];
    self.titleLabel.textAlignment = NSTextAlignmentCenter;
    self.titleLabel.frame = self.bounds;
    [self addSubview:self.titleLabel];
    
    // tap
    self.tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(actionManager)];
    [self addGestureRecognizer:self.tapGesture];
}


-(void)setTransferSingleModel:(TSHomeAllClassSingleTagsModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    [self.backgroundImageView uploadImageWithURL:transferSingleModel.url placeholder:nil callback:NULL];
    
    self.titleLabel.text = transferSingleModel.name;
}

-(void)tapManagerWithBlock:(void(^)(TSHomeAllClassSingleTagsModel *singleModel))block{
    objc_setAssociatedObject(self, &tapManagerWithBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionManager{
    void(^block)(TSHomeAllClassSingleTagsModel *singleModel) = objc_getAssociatedObject(self, &tapManagerWithBlockKey);
    if (block){
        block(self.transferSingleModel);
    }
}



@end
