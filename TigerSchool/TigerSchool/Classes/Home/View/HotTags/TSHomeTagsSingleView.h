//
//  TSHomeTagsSingleView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHomeAllClassTagsModel.h"

@interface TSHomeTagsSingleView : UIView

@property (nonatomic,strong)TSHomeAllClassSingleTagsModel *transferSingleModel;

-(void)tapManagerWithBlock:(void(^)(TSHomeAllClassSingleTagsModel *singleModel))block;


@end
