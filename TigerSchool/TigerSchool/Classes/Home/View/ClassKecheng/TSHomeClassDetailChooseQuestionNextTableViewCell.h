//
//  TSHomeClassDetailChooseQuestionNextTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/5.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"

@interface TSHomeClassDetailChooseQuestionNextTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)UIButton *lastButton;               /**< 上一题按钮*/
@property (nonatomic,strong)UIButton *sendButton;               /**< 提交按钮*/

// 第一题的方法
-(void)firstQuestionManager;
-(void)lastQuestionManager;
-(void)normalQuestionManager;

-(void)actionButtonClickWithLast:(void(^)())lastBlock nextBlock:(void(^)())nextBlock;

@end
