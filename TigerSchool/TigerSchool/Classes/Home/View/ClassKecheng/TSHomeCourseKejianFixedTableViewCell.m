//
//  TSHomeCourseKejianFixedTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeCourseKejianFixedTableViewCell.h"

@interface TSHomeCourseKejianFixedTableViewCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *allLabel;

@end

@implementation TSHomeCourseKejianFixedTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.fixedLabel = [TSViewTool createLabelFont:@"标题" textColor:@"黑"];
    self.fixedLabel.font = [self.fixedLabel.font boldFont];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    [self addSubview:self.fixedLabel];
    self.fixedLabel.text = @"课件";
    CGSize fixedSize = [TSTool makeSizeWithLabel:self.fixedLabel];
    self.fixedLabel.frame = CGRectMake(TSFloat(19), 0, fixedSize.width, [TSHomeCourseKejianFixedTableViewCell calculationCellHeight]);
    
    // 2. 全部
    self.allLabel = [TSViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.allLabel.text = @"全部>";
    [self addSubview:self.allLabel];
    CGSize allSize = [TSTool makeSizeWithLabel:self.allLabel];
    self.allLabel.frame = CGRectMake(kScreenBounds.size.width - TSFloat(20) - allSize.width, 0, allSize.width, allSize.height);
    self.allLabel.center_y = self.fixedLabel.center_y;
    
    // 3. 创建文字
    self.titleLabel = [TSViewTool createLabelFont:@"小正文" textColor:@"黑"];
    [self addSubview:self.titleLabel];
}

-(void)setTransferCourseDetailModel:(TSHomeCourseDetailClassModel *)transferCourseDetailModel{
    _transferCourseDetailModel = transferCourseDetailModel;
    CGFloat width = self.allLabel.orgin_x - TSFloat(11) - CGRectGetMaxX(self.fixedLabel.frame) - TSFloat(11);
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.fixedLabel.frame) + TSFloat(11), 0, width,[TSHomeCourseKejianFixedTableViewCell calculationCellHeight]);
    self.titleLabel.text = transferCourseDetailModel.name;
}

+(CGFloat)calculationCellHeight{
    return TSFloat(50);
}

@end
