//
//  TSHomeClassImgScrollCollectionViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHomeClassImgScrollCollectionViewCell : UICollectionViewCell

@property (nonatomic,copy)NSString *transferTitle;
@property (nonatomic,strong)TSImageView *imgView;

@end
