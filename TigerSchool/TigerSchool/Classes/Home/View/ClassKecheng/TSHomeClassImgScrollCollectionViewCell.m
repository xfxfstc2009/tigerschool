//
//  TSHomeClassImgScrollCollectionViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassImgScrollCollectionViewCell.h"
#import "TSV2View.h"
#define xWidth  [UIScreen mainScreen].bounds.size.width * 0.6
#define xHeight [UIScreen mainScreen].bounds.size.height * 0.2


@interface TSHomeClassImgScrollCollectionViewCell()
@property (nonatomic,strong)UIView *containerView;
@property (nonatomic,strong)TSV2View *v2view;
@end

@implementation TSHomeClassImgScrollCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

-(void)createView{
    self.imgView = [[TSImageView alloc]init];
    self.imgView.backgroundColor = [UIColor clearColor];
}

-(void)setTransferTitle:(NSString *)transferTitle{
    _transferTitle = transferTitle;
    __weak typeof(self)weakSelf = self;
    [self.imgView uploadImageWithURL:transferTitle placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.imgView.image = image;
        
        // 2. 判断那条边比较短
        CGFloat margin_w = kScreenBounds.size.width - 2 * TSFloat(70);
        CGFloat marign_r_h = strongSelf.size_height - 2 * TSFloat(20);
        if (image.size.width > image.size.height){      // 宽度大于高度
            CGFloat margin_h = margin_w * image.size.height / image.size.width;
            strongSelf.imgView.frame = CGRectMake(0, 0, margin_w, margin_h);
            strongSelf.imgView.center = CGPointMake(kScreenBounds.size.width / 2., strongSelf.size_height / 2.);
        } else {                                        // 高度大于宽度
            CGFloat margin_w = image.size.width * marign_r_h / image.size.height;
            strongSelf.imgView.frame = CGRectMake(0, 0, margin_w, marign_r_h);
            strongSelf.imgView.center = CGPointMake(kScreenBounds.size.width / 2., strongSelf.size_height / 2.);
        }
        
        self.v2view = [[TSV2View alloc]initWithFrame:CGRectMake(0, 0, self.imgView.size_width, self.imgView.size_height)];
        [self.v2view addSubview:strongSelf.imgView];
//        [self.v2view setInvertedImage];
        [self.contentView addSubview:self.v2view];
    }];
}

@end
