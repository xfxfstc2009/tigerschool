//
//  TSHomeClassTopItemsSingleTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassTopItemsSingleTableViewCell.h"

@interface TSHomeClassTopItemsSingleTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *descLabel;
@property (nonatomic,strong)UILabel *timeLabel;
@end

@implementation TSHomeClassTopItemsSingleTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 标题
    self.titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"红"];
    [self addSubview:self.titleLabel];
    
    // 2. 创建desc
    self.descLabel = [TSViewTool createLabelFont:@"时长" textColor:@"浅灰"];
    [self addSubview:self.descLabel];
    
    // 3. 创建时长
    self.timeLabel =[TSViewTool createLabelFont:@"时长" textColor:@""];
    [self addSubview:self.timeLabel];
}

-(void)setTransferDetailModel:(TSHomeCourseDetailClassModel *)transferDetailModel{
    _transferDetailModel = transferDetailModel;
    
    // 1.title
    self.titleLabel.text = transferDetailModel.name;
//    CGSize titleSize = [TSTool makeSizeWithLabel:self.titleLabel];
    self.titleLabel.frame = CGRectMake(TSFloat(20), TSFloat(18), kScreenBounds.size.width - 2 * TSFloat(20), [NSString contentofHeightWithFont:self.titleLabel.font]);
    
    // 2. 创建时间
    self.descLabel.text = transferDetailModel.starttime.length?transferDetailModel.starttime:@"没有开课时间";
    self.descLabel.frame = CGRectMake(TSFloat(20), CGRectGetMaxY(self.titleLabel.frame) + TSFloat(9), kScreenBounds.size.width - 2 * TSFloat(20), [NSString contentofHeightWithFont:self.descLabel.font]);
    
    // 3. 创建时长
    self.timeLabel.text = [NSString stringWithFormat:@"时长：%li",(long)transferDetailModel.timelong];
    CGSize timeSize = [TSTool makeSizeWithLabel:self.timeLabel];
    self.timeLabel.frame = CGRectMake(kScreenBounds.size.width - TSFloat(21) - timeSize.width, CGRectGetMaxY(self.titleLabel.frame) + TSFloat(9), timeSize.width, [NSString contentofHeightWithFont:self.timeLabel.font]);
}

+(CGFloat)calculationCellHeight:(TSHomeCourseDetailClassModel *)transferDetailModel{
    CGFloat cellHeight = 44;
    return cellHeight;
}

@end
