//
//  TSHomeClassTopItemsTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSHomeCourseDetailClassModel.h"
@interface TSHomeClassTopItemsTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)TSHomeCourseDetailListClassModel *transferDetailItemsModel;         /**< 传递进来的数据源*/
@property (nonatomic,strong)TSHomeCourseDetailClassModel *transferUserSelectedDetailModel;      /**< 用户传递进来的model*/



+(CGFloat)calculationCellHeightWithModel:(TSHomeCourseDetailListClassModel *)transferDetailItemsModel;

-(void)actionClickWithSelectedModel:(void(^)(TSHomeCourseDetailClassModel *singleModel))block;

@end

