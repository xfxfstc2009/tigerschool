//
//  TSHomeClassDetailJieshaoWebTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/26.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSBaseTableViewCell.h"

@interface TSHomeClassDetailJieshaoWebTableViewCellModel : NSObject
@property (nonatomic,assign)CGFloat transferCellHeight;
@property (nonatomic,copy)NSString *ID;
@property (nonatomic,copy)NSString *html;
@property (nonatomic,assign)BOOL hasLoad;
@end

@interface TSHomeClassDetailJieshaoWebTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)WKWebView *webView;

@property (nonatomic,strong)TSHomeClassDetailJieshaoWebTableViewCellModel *transferModel;;          // 传递的model

+(CGFloat)calculationCellHeightWithModel:(TSHomeClassDetailJieshaoWebTableViewCellModel *)transferModel;



@end
