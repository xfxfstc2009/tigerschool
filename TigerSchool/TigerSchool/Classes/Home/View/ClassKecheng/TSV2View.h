//
//  TSV2View.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSV2View : UIView

-(void)setInvertedImage;

@property (nonatomic,strong)CAGradientLayer *gradientLayer;//处理颜色渐变

@end
