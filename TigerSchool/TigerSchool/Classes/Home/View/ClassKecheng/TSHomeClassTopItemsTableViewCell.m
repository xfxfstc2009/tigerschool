//
//  TSHomeClassTopItemsTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassTopItemsTableViewCell.h"

@interface TSHomeClassTopItemsTableViewCell()
@property (nonatomic,strong)UIView *bgView;
@end

static char actionClickWithSelectedModelKey;
@implementation TSHomeClassTopItemsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    [self addSubview:_bgView];
}

-(void)setTransferDetailItemsModel:(TSHomeCourseDetailListClassModel *)transferDetailItemsModel{
    _transferDetailItemsModel = transferDetailItemsModel;
    __weak typeof(self)weakSelf = self;
    if (self.bgView.subviews.count){
        [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    CGFloat flagOriginX = TSFloat(20);
    CGFloat flagWidth = kScreenBounds.size.width - 2 * flagOriginX;
    CGFloat flagOriginY = TSFloat(11);
    CGFloat flagMargin = TSFloat(6);
    CGFloat lastOrigin_Y = 0;
    
    for (int i = 0 ; i < transferDetailItemsModel.items.count;i++){
        TSHomeCourseDetailClassModel *singleModel = [transferDetailItemsModel.items objectAtIndex:i];
        UIView *flagImageView = [self createItemsWithIndex:i + 1 model:singleModel actionBlock:^(TSHomeCourseDetailClassModel *singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            void(^block)(TSHomeCourseDetailClassModel *model) = objc_getAssociatedObject(strongSelf, &actionClickWithSelectedModelKey);
            if (block){
                block(singleModel);
            }
        }];
        
        if (flagImageView.size_width < flagWidth){                      // 不换行
            flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
            flagWidth -= (flagImageView.size_width + flagMargin);
            flagOriginX += (flagImageView.size_width + flagMargin);
        } else {
            flagOriginY += TSFloat(33) + TSFloat(11);
            flagOriginX =  TSFloat(20);
            flagWidth = kScreenBounds.size.width - 2 * flagOriginX;
            flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
            flagWidth -= (flagImageView.size_width + flagMargin);
            flagOriginX += (flagImageView.size_width + flagMargin);
        }
        
        if (i == transferDetailItemsModel.items.count - 1){
            lastOrigin_Y = CGRectGetMaxY(flagImageView.frame) + TSFloat(11);
        }
        
        [self.bgView addSubview:flagImageView];
    }
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, lastOrigin_Y);
}


-(UIView *)createItemsWithIndex:(NSInteger)index model:(TSHomeCourseDetailClassModel *)model actionBlock:(void(^)(TSHomeCourseDetailClassModel *singleModel))block{
    CGFloat width = TSFloat(66) / 2.;
    CGFloat height = TSFloat(66) / 2.;
    
    UIView *itemView = [[UIView alloc]init];
    itemView.backgroundColor = RGB(237, 237, 237, 1);
    itemView.frame = CGRectMake(0, 0, width, height);
    
    UILabel *titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    titleLabel.textAlignment = NSTextAlignmentCenter;
    titleLabel.frame = itemView.bounds;
    titleLabel.text = [NSString stringWithFormat:@"%li",(long)index];
    [itemView addSubview:titleLabel];
    if (self.transferUserSelectedDetailModel == model){
        titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    } else {
        titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    }
    
    UIButton *actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    actionButton.backgroundColor = [UIColor clearColor];
    actionButton.frame = itemView.bounds;
    __weak typeof(self)weakSelf = self;
    [actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        if (block){
            block(model);
        }
    }];
    [itemView addSubview:actionButton];
    
    return itemView;
}

+(CGFloat)calculationCellHeightWithModel:(TSHomeCourseDetailListClassModel *)transferDetailItemsModel{
    
    CGFloat flagOriginX = TSFloat(20);
    CGFloat flagWidth = kScreenBounds.size.width - 2 * flagOriginX;
    CGFloat flagOriginY = TSFloat(11);
    CGFloat flagMargin = TSFloat(6);
    
    CGFloat cellHeight = flagOriginY;
    for (int i = 0 ; i < transferDetailItemsModel.items.count;i++){
        
        CGFloat floagWidth = TSFloat(33);
        
        if (floagWidth < flagWidth){                      // 不换行
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
        } else {
            flagOriginY += TSFloat(33) + TSFloat(11);
            flagOriginX =  TSFloat(20);
            flagWidth = kScreenBounds.size.width - 2 * flagOriginX;
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
        }
    }
    cellHeight = flagOriginY + TSFloat(33) + TSFloat(11);
    
    return cellHeight;
}

-(void)setTransferUserSelectedDetailModel:(TSHomeCourseDetailClassModel *)transferUserSelectedDetailModel{
    _transferUserSelectedDetailModel = transferUserSelectedDetailModel;
}


-(void)actionClickWithSelectedModel:(void(^)(TSHomeCourseDetailClassModel *singleModel))block{
    objc_setAssociatedObject(self, &actionClickWithSelectedModelKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

@end
