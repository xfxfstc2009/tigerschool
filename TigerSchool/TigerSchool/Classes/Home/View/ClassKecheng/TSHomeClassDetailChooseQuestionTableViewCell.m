//
//  TSHomeClassDetailChooseQuestionTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/4.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailChooseQuestionTableViewCell.h"
#import "NSAttributedString_Encapsulation.h"

@interface TSHomeClassDetailChooseQuestionTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation TSHomeClassDetailChooseQuestionTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
}

-(void)setTransferQuestionModel:(TSHomeCourseDetailExamitemsSingleModel *)transferQuestionModel{
    _transferQuestionModel = transferQuestionModel;
    
    NSString *info = [NSString stringWithFormat:@"%li.[%@]%@",(long)transferQuestionModel.questionNumber,transferQuestionModel.ismul?@"多选":@"单选",transferQuestionModel.title];
    NSString *hltStr = [NSString stringWithFormat:@"[%@]",transferQuestionModel.ismul?@"多选":@"单选"];
    
    self.titleLabel.attributedText = [NSAttributedString_Encapsulation changeTextColorWithColor:[UIColor colorWithCustomerName:@"蓝"] string:info andSubString:@[hltStr]];
    
    CGSize titleSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * TSFloat(11), CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(TSFloat(11), TSFloat(11), kScreenBounds.size.width - 2 * TSFloat(11), titleSize.height);
}

+(CGFloat)calculationCellHeightWithHeight:(TSHomeCourseDetailExamitemsSingleModel *)transferQuestionModel{
    CGFloat cellHeight = 0;
    cellHeight += TSFloat(11);
    NSString *info = [NSString stringWithFormat:@"%li.[%@]%@",(long)transferQuestionModel.questionNumber,transferQuestionModel.ismul?@"多选":@"单选",transferQuestionModel.title];
    CGSize titleSize = [info sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(kScreenBounds.size.width - 2 * TSFloat(11), CGFLOAT_MAX)];
    cellHeight += titleSize.height;
    cellHeight += TSFloat(11);
    return cellHeight;
}



@end
