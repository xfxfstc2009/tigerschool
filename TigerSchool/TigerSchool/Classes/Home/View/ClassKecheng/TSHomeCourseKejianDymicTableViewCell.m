//
//  TSHomeCourseKejianDymicTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeCourseKejianDymicTableViewCell.h"
#import "TSHomeClassImgScrollLayout.h"
#import "TSV2View.h"

static char actionClickWithShowImgsManagerBlockKey;
@interface TSHomeCourseKejianDymicTableViewCell()<UICollectionViewDataSource,UICollectionViewDelegate>{
    
}
@property (nonatomic,strong)UICollectionView *imgsCollectionView;
@property (nonatomic,strong)NSMutableArray *imgsArr;
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIButton *rightButton;
@property (nonatomic,strong)UILabel *pageLabel;
@property (nonatomic,strong)TSImageView *alphaImgView;

@end

@implementation TSHomeCourseKejianDymicTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self arrayWithInit];
        [self createView];
        
        self.backgroundColor = RGB(241, 241, 241, 1);
    }
    return self;
}

#pragma mark - arrayWithinit
-(void)arrayWithInit{
    self.imgsArr = [NSMutableArray array];
    self.currentRow = 0;
}

#pragma mark - createView
-(void)createView{
    TSHomeClassImgScrollLayout *layout = [[TSHomeClassImgScrollLayout alloc] init];
    self.imgsCollectionView = [[UICollectionView alloc] initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width , [TSHomeCourseKejianDymicTableViewCell calculationCellHeight]) collectionViewLayout:layout];
    self.imgsCollectionView.backgroundColor = [UIColor clearColor];
    self.imgsCollectionView.showsVerticalScrollIndicator = NO;
    self.imgsCollectionView.showsHorizontalScrollIndicator = NO;
    self.imgsCollectionView.delegate = self;
    self.imgsCollectionView.dataSource = self;
    [self addSubview:self.imgsCollectionView];
    
    [self.imgsCollectionView registerClass:[TSHomeClassImgScrollCollectionViewCell class] forCellWithReuseIdentifier:@"cell"];
    
    // 创建左边按钮
    self.leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.leftButton.frame = CGRectMake(TSFloat(9), ([TSHomeCourseKejianDymicTableViewCell calculationCellHeight] - TSFloat(40)) / 2., TSFloat(40), TSFloat(40));
    [self.leftButton setBackgroundImage:[UIImage imageNamed:@"icon_classDetail_left"] forState:UIControlStateNormal];
    [self addSubview:self.leftButton];
    __weak typeof(self)weakSelf = self;
    [self.leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
       strongSelf.currentRow -= 1;
        if(strongSelf.currentRow < 0 ){
            strongSelf.currentRow = 0;
        }
        [strongSelf.imgsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:strongSelf.currentRow inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        strongSelf.pageLabel.text = [NSString stringWithFormat:@"%li/%li",(long)strongSelf.currentRow + 1,(long)strongSelf.transferSingleModel.datums.count];
    }];
    
    // 创建右侧按钮
    self.rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.rightButton.frame = CGRectMake(kScreenBounds.size.width - TSFloat(9) - TSFloat(40), self.leftButton.orgin_y, TSFloat(40), TSFloat(40));
    [self.rightButton setBackgroundImage:[UIImage imageNamed:@"icon_classDetail_right"] forState:UIControlStateNormal];
    [self addSubview:self.rightButton];
    [self.rightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.currentRow += 1;
        if(strongSelf.currentRow > strongSelf.imgsArr.count - 1){
            strongSelf.currentRow = strongSelf.imgsArr.count - 1;
        }
        [strongSelf.imgsCollectionView scrollToItemAtIndexPath:[NSIndexPath indexPathForRow:strongSelf.currentRow inSection:0] atScrollPosition:UICollectionViewScrollPositionNone animated:YES];
        strongSelf.pageLabel.text = [NSString stringWithFormat:@"%li/%li",(long)strongSelf.currentRow + 1,(long)strongSelf.transferSingleModel.datums.count];
    }];
    

    // 创建底部阴影
    self.alphaImgView = [[TSImageView alloc]init];
    self.alphaImgView.backgroundColor = [UIColor clearColor];
    self.alphaImgView.frame = CGRectMake(0,[TSHomeCourseKejianDymicTableViewCell calculationCellHeight] - TSFloat(34), kScreenBounds.size.width, TSFloat(34));
    self.alphaImgView.image = [TSTool stretchImageWithName:@"img_video_shadow"];
    [self addSubview:self.alphaImgView];
    
    // 创建底部页面
    self.pageLabel = [TSViewTool createLabelFont:@"正文" textColor:@"白"];
    self.pageLabel.backgroundColor = [UIColor clearColor];
    self.pageLabel.frame = self.alphaImgView.bounds;
    self.pageLabel.textAlignment = NSTextAlignmentCenter;
    
    self.pageLabel.layer.shadowOpacity = 1.0;                           //阴影透明度
    self.pageLabel.layer.shadowRadius = 1.0;                            //阴影宽度
    self.pageLabel.layer.shadowColor = [UIColor grayColor].CGColor;     //阴影颜色
    self.pageLabel.layer.shadowOffset = CGSizeMake(1, 1);               //映影偏移
    [self.alphaImgView addSubview:self.pageLabel];
}

#pragma mark cell的数量
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return self.imgsArr.count;
}

#pragma mark cell的视图
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *cellIdentifier = @"cell";
    TSHomeClassImgScrollCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    NSString *img = [self.imgsArr objectAtIndex:indexPath.row];
    cell.transferTitle = img;

    if (indexPath.row == 0){
        self.tempCell = cell;
    }
    
    return cell;
}

#pragma mark cell的大小
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(kScreenBounds.size.width, [TSHomeCourseKejianDymicTableViewCell calculationCellHeight]);
}

#pragma mark cell的点击事件
- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    NSString *img = [self.imgsArr objectAtIndex:indexPath.row];
    void(^block)(NSString *imgUrl) = objc_getAssociatedObject(self, &actionClickWithShowImgsManagerBlockKey);
    if (block){
        block(img);
    }
}

-(void)setTransferSingleModel:(TSHomeCourseDetailClassModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    if (self.imgsArr.count){
        [self.imgsArr removeAllObjects];
    }
    [self.imgsArr addObjectsFromArray:transferSingleModel.datums];
    [self.imgsCollectionView reloadData];
    
    self.pageLabel.text = [NSString stringWithFormat:@"%li/%li",(long)self.currentRow + 1,(long)transferSingleModel.datums.count];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.imgsCollectionView){
        CGFloat origin_x = scrollView.contentOffset.x;
        NSInteger index = origin_x / kScreenBounds.size.width ;
        self.currentRow = index;
        self.pageLabel.text = [NSString stringWithFormat:@"%li/%li",(long)self.currentRow + 1,(long)self.transferSingleModel.datums.count];
        self.tempCell = (TSHomeClassImgScrollCollectionViewCell *)[self.imgsCollectionView cellForItemAtIndexPath:[NSIndexPath indexPathForRow:index inSection:0]];
    }
}

+(CGFloat)calculationCellHeight{
    return TSFloat(610) / 2.;
}

-(void)actionClickWithShowImgsManagerBlock:(void(^)(NSString *imgUrl))block{
    objc_setAssociatedObject(self, &actionClickWithShowImgsManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
