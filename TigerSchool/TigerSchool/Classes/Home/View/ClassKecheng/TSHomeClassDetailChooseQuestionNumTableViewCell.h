//
//  TSHomeClassDetailChooseQuestionNumTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/5.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"

@interface TSHomeClassDetailChooseQuestionNumTableViewCell : TSBaseTableViewCell

@property (nonatomic,copy)NSString *transferQuestionChoose;         /**< 传递过来的选项*/

@property (nonatomic,strong)NSArray *userSelectedArr;                   /**< 用户选中内容*/

+(CGFloat)calculationCellHeightWithQuestion:(NSString *)transferQuestionChoose;

@end
