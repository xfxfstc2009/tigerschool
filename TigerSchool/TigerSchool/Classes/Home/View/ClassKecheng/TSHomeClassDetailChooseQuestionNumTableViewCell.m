//
//  TSHomeClassDetailChooseQuestionNumTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/5.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailChooseQuestionNumTableViewCell.h"

@interface TSHomeClassDetailChooseQuestionNumTableViewCell()
@property (nonatomic,strong)TSImageView *chooseImageView;
@property (nonatomic,strong)UILabel *titleLabel;

@end

@implementation TSHomeClassDetailChooseQuestionNumTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.chooseImageView = [[TSImageView alloc]init];
    self.chooseImageView.backgroundColor = [UIColor clearColor];
    self.chooseImageView.image = [UIImage imageNamed:@"icon_question_choose_nor"];
    [self addSubview:self.chooseImageView];
    
    self.titleLabel = [TSViewTool createLabelFont:@"小正文" textColor:@"黑"];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
}

-(void)setTransferQuestionChoose:(NSString *)transferQuestionChoose{
    _transferQuestionChoose = transferQuestionChoose;
    
    self.chooseImageView.frame = CGRectMake(TSFloat(30), TSFloat(7), TSFloat(18), TSFloat(18));
    
    if ([self.userSelectedArr containsObject:transferQuestionChoose]){
        self.chooseImageView.image = [UIImage imageNamed:@"icon_question_choose_hlt"];
    } else {
        self.chooseImageView.image = [UIImage imageNamed:@"icon_question_choose_nor"];
    }
    
    self.titleLabel.text = transferQuestionChoose;
    CGFloat width = kScreenBounds.size.width - 2 * TSFloat(30) - TSFloat(18) - TSFloat(11);
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    self.titleLabel.frame = CGRectMake(TSFloat(30) + TSFloat(18) + TSFloat(11), TSFloat(7), width, titleSize.height);
    self.chooseImageView.center_y = self.titleLabel.center_y;
}

-(void)setUserSelectedArr:(NSArray *)userSelectedArr{
    _userSelectedArr = userSelectedArr;
}

+(CGFloat)calculationCellHeightWithQuestion:(NSString *)transferQuestionChoose{
    CGFloat cellHeight = 0;
    CGFloat width = kScreenBounds.size.width - 2 * TSFloat(30) - TSFloat(18) - TSFloat(11);
    CGSize titleSize = [transferQuestionChoose sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"小正文"] constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    cellHeight += TSFloat(7);
    cellHeight += titleSize.height;
    cellHeight += TSFloat(7);
    return cellHeight;
}

@end
