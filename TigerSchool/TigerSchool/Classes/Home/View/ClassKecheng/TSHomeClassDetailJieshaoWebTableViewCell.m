//
//  TSHomeClassDetailJieshaoWebTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/26.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailJieshaoWebTableViewCell.h"

@implementation TSHomeClassDetailJieshaoWebTableViewCellModel

@end

@interface TSHomeClassDetailJieshaoWebTableViewCell()

@end

@implementation TSHomeClassDetailJieshaoWebTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    [self addSubview:self.titleLabel];
    [self creatSubviews];
}

- (void)creatSubviews {
    WKWebViewConfiguration *confifg = [[WKWebViewConfiguration alloc] init];
    confifg.selectionGranularity = WKSelectionGranularityCharacter;
    _webView = [[WKWebView alloc] initWithFrame:CGRectMake(14, 0, kScreenBounds.size.width - 28, 1) configuration:confifg];
    self.contentView.backgroundColor = [UIColor clearColor];
    // _webView.scalesPageToFit = NO;
    _webView.scrollView.scrollEnabled=NO;
    _webView.userInteractionEnabled = NO;
    _webView.opaque = NO;
    _webView.scrollView.bounces=NO;
    _webView.backgroundColor=[UIColor clearColor];
    _webView.scrollView.decelerationRate=UIScrollViewDecelerationRateNormal;
    
    [self.contentView addSubview:_webView];
}


-(void)setTransferModel:(TSHomeClassDetailJieshaoWebTableViewCellModel *)transferModel{
    _transferModel = transferModel;

    if ([transferModel.html rangeOfString:@"<img src"].location != NSNotFound) {
        
        [_webView loadHTMLString:[NSString stringWithFormat:@"<meta content=\"width=device-width, initial-scale=1.0, maximum-scale=3.0, user-scalable=0;\" name=\"viewport\" />%@<div id=\"testDiv\" style = \"height:100px; width:100px\"></div>",transferModel.html] baseURL:[NSURL fileURLWithPath: [[NSBundle mainBundle]  bundlePath]]];
    
    } else {
        NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[transferModel.html dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
        
        self.titleLabel.attributedText = attrStr;
        self.titleLabel.numberOfLines = 0;
        
        //判断是否有style="font-size:
        UIFont *tempFont;
        if ([transferModel.html containsString:@"font-size:"]){
            NSRange startRange = [transferModel.html rangeOfString:@"font-size:"];
            NSRange endRange = [transferModel.html rangeOfString:@"px;"];
            NSRange range = NSMakeRange(startRange.location + startRange.length, endRange.location - startRange.location - startRange.length);
            NSString *result = [transferModel.html substringWithRange:range];
            tempFont = [UIFont systemFontOfCustomeSize:[result integerValue]];
        } else {
            tempFont = [UIFont systemFontOfCustomeSize:15.];
        }
        
        CGFloat contHeight = [transferModel.html getSpaceLabelHeightwithSpeace:6 withFont:tempFont withWidth:(kScreenBounds.size.width - 2 * TSFloat(20))];
        self.titleLabel.frame = CGRectMake(TSFloat(20), 0, (kScreenBounds.size.width - 2 * TSFloat(20)), contHeight);
    }
}







+(CGFloat)calculationCellHeightWithModel:(TSHomeClassDetailJieshaoWebTableViewCellModel *)transferModel{
    //判断是否有style="font-size:
    UIFont *tempFont;
    if ([transferModel.html containsString:@"font-size:"]){
        NSRange startRange = [transferModel.html rangeOfString:@"font-size:"];
        NSRange endRange = [transferModel.html rangeOfString:@"px;"];
        NSRange range = NSMakeRange(startRange.location + startRange.length, endRange.location - startRange.location - startRange.length);
        NSString *result = [transferModel.html substringWithRange:range];
        tempFont = [UIFont systemFontOfCustomeSize:[result integerValue]];
    } else {
        tempFont = [UIFont systemFontOfCustomeSize:15.];
    }
    
    CGFloat contHeight = [transferModel.html getSpaceLabelHeightwithSpeace:6 withFont:tempFont withWidth:(kScreenBounds.size.width - 2 * TSFloat(20))];
    
    return contHeight;
}

@end
