//
//  TSHomeCourseKejianDymicTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSHomeCourseDetailClassModel.h"
#import "TSHomeClassImgScrollCollectionViewCell.h"

@interface TSHomeCourseKejianDymicTableViewCell : TSBaseTableViewCell



@property (nonatomic,strong)TSHomeCourseDetailClassModel *transferSingleModel;


// temp
@property (nonatomic,assign)NSInteger currentRow;
@property (nonatomic,strong)TSHomeClassImgScrollCollectionViewCell *tempCell;

-(void)actionClickWithShowImgsManagerBlock:(void(^)(NSString *imgUrl))block;

@end
