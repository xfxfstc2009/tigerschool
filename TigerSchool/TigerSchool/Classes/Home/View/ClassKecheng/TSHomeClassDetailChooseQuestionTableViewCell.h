//
//  TSHomeClassDetailChooseQuestionTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/4.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSHomeCourseDetailClassModel.h"

@interface TSHomeClassDetailChooseQuestionTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)TSHomeCourseDetailExamitemsSingleModel *transferQuestionModel;

+(CGFloat)calculationCellHeightWithHeight:(TSHomeCourseDetailExamitemsSingleModel *)transferQuestionModel;

@end
