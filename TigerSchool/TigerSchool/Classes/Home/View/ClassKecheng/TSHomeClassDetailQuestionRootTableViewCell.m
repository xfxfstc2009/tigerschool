//
//  TSHomeClassDetailQuestionRootTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/5.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailQuestionRootTableViewCell.h"
#import "NSAttributedString_Encapsulation.h"

@interface TSHomeClassDetailQuestionRootTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)TSImageView *iconImgView;
@end

@implementation TSHomeClassDetailQuestionRootTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.frame = CGRectMake(TSFloat(20), TSFloat(11), kScreenBounds.size.width - 2 * TSFloat(20), [NSString contentofHeightWithFont:self.titleLabel.font]);
    [self addSubview:self.titleLabel];
    
    // 创建图标
    self.iconImgView = [[TSImageView alloc]init];
    self.iconImgView.backgroundColor = [UIColor clearColor];
    [self addSubview:self.iconImgView];
}


// 信息内容
-(void)setTransferSingleModel:(TSHomeCourseDetailExamitemsSingleModel *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    // 1. 传递选项
    NSString *str = [NSString stringWithFormat:@"%li",(long)self.transferIndex];
    
    str = [str stringByReplacingOccurrencesOfString:@"1" withString:@"A"];
    str = [str stringByReplacingOccurrencesOfString:@"2" withString:@"B"];
    str = [str stringByReplacingOccurrencesOfString:@"3" withString:@"C"];
    str = [str stringByReplacingOccurrencesOfString:@"4" withString:@"D"];
    str = [str stringByReplacingOccurrencesOfString:@"5" withString:@"E"];
    str = [str stringByReplacingOccurrencesOfString:@"6" withString:@"F"];
    str = [str stringByReplacingOccurrencesOfString:@"7" withString:@"G"];
    str = [str stringByReplacingOccurrencesOfString:@"8" withString:@"H"];
    
    // 2. 拼接内容
    str = [NSString stringWithFormat:@"%@、%@",str,self.transferChooseInfo];
    
    self.titleLabel.text = str;
    
    // 3. 创建图标
    CGFloat maxWidth = kScreenBounds.size.width - TSFloat(11) - TSFloat(16) - TSFloat(20) * 2;
    // 3.1 判断几行
    CGSize titleSize = [str sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(maxWidth, CGFLOAT_MAX)];
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){               // 多行
        self.titleLabel.frame = CGRectMake(TSFloat(20), TSFloat(11), maxWidth, titleSize.height);
        self.titleLabel.numberOfLines = 0;
        self.iconImgView.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + TSFloat(11), TSFloat(11), TSFloat(16), TSFloat(16));
        self.iconImgView.center_y = self.titleLabel.center_y;
    } else {                                                                                            // 一行
        CGSize contentOfSize = [str sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.titleLabel.font])];
        self.titleLabel.frame = CGRectMake(TSFloat(20), TSFloat(11), contentOfSize.width, [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.iconImgView.frame = CGRectMake(CGRectGetMaxX(self.titleLabel.frame) + TSFloat(11), TSFloat(11), TSFloat(16), TSFloat(16));
        self.iconImgView.center_y = self.titleLabel.center_y;
        self.titleLabel.numberOfLines = 1;
    }
    
    // 1. 获取到答案数组
//    NSArray *infoArr = [transferSingleModel.answer componentsSeparatedByString:@","];                       // 所有的选项
    
//    self.transferChooseInfo               // 当前选择的
//    infoArr                               // 所有的选项
//    userChooseArr                         // 用户选择的内容

    
    // 第一步 获取所有的正确选项
    NSArray *successChooseIndexArr = [transferSingleModel.answer componentsSeparatedByString:@","];         // 选中的index数组
    NSMutableArray *successChooseStrArr = [NSMutableArray array];                                           // 选中的
    
    for (int j = 0 ; j < successChooseIndexArr.count;j++){
        NSInteger successIndex = [[successChooseIndexArr objectAtIndex:j] integerValue] - 1;
        NSString *chooseStr = [transferSingleModel.answers objectAtIndex:successIndex];
        [successChooseStrArr addObject:chooseStr];
    }
    
    // 正确答案
    NSLog(@"%@",successChooseStrArr);
    // 第二部 获取用户所选择的答案
    NSArray *userChooseArr;
    if (transferSingleModel.userChoosedArr.count){
        userChooseArr = transferSingleModel.userChoosedArr;
    } else {
        NSArray *tempUserChooseArr = [transferSingleModel.useranswer componentsSeparatedByString:@","];
        NSMutableArray *tempMutableArr = [NSMutableArray array];
        for (int i = 0 ; i < tempUserChooseArr.count;i++){
            NSString *tempUserChooseStr = [tempUserChooseArr objectAtIndex:i];
            NSInteger tempUserChooseStrIndex = [tempUserChooseStr integerValue] - 1 < 0 ? 0 : [tempUserChooseStr integerValue] - 1;
            NSString *infoStr = [transferSingleModel.answers objectAtIndex:tempUserChooseStrIndex];
            [tempMutableArr addObject:infoStr];
        }
        userChooseArr = [tempMutableArr copy];
    }
    
    // 第三步获取当前选项
    NSLog(@"%@",self.transferChooseInfo);
    
    // 第四部,如果用户选择对的答案
    if ([userChooseArr containsObject:self.transferChooseInfo] && [successChooseStrArr containsObject:self.transferChooseInfo]){
        self.titleLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
        self.iconImgView.image = [UIImage imageNamed:@"icon_question_success"];
    }  else {
        if ([successChooseStrArr containsObject:self.transferChooseInfo]){
            self.titleLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
            self.iconImgView.image = [UIImage imageNamed:@"icon_question_success"];
        } else if ([userChooseArr containsObject:self.transferChooseInfo] && (![successChooseStrArr containsObject:self.transferChooseInfo])){
            self.iconImgView.image = [UIImage imageNamed:@"icon_question_fail"];
            self.titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
        } else if (![userChooseArr containsObject:self.transferChooseInfo] && [successChooseStrArr containsObject:self.transferChooseInfo]){
            self.iconImgView.image = [UIImage imageNamed:@"icon_question_fail"];
            self.titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
        } else {
            self.titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            self.iconImgView.image = nil;
        }
    }
}

// 选项位置
-(void)setTransferIndex:(NSInteger)transferIndex{
    _transferIndex = transferIndex;
}

// 选项答案
-(void)setTransferChooseInfo:(NSString *)transferChooseInfo{
    _transferChooseInfo = transferChooseInfo;
}

+(CGFloat)calculationCellHeightWithStr:(NSString *)str{
    CGFloat cellHeight = 0;
    cellHeight += TSFloat(11);
    
    CGFloat maxWidth = kScreenBounds.size.width - TSFloat(11) - TSFloat(16) - TSFloat(20) * 2;
    CGSize contentOfSize = [str sizeWithCalcFont:[UIFont fontWithCustomerSizeName:@"正文"] constrainedToSize:CGSizeMake(maxWidth, CGFLOAT_MAX)];
    cellHeight += contentOfSize.height;
    cellHeight += TSFloat(11);
    return cellHeight;
}


@end



@interface TSHomeClassDetailQuestionRootEndTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@end

@implementation TSHomeClassDetailQuestionRootEndTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"绿"];
    [self addSubview:self.titleLabel];
}


-(void)setTransferEndInfo:(NSString *)transferEndInfo{
    _transferEndInfo = transferEndInfo;

    NSString *str = [transferEndInfo stringByReplacingOccurrencesOfString:@"1" withString:@"A"];
    str = [str stringByReplacingOccurrencesOfString:@"2" withString:@"B"];
    str = [str stringByReplacingOccurrencesOfString:@"3" withString:@"C"];
    str = [str stringByReplacingOccurrencesOfString:@"4" withString:@"D"];
    str = [str stringByReplacingOccurrencesOfString:@"5" withString:@"E"];
    str = [str stringByReplacingOccurrencesOfString:@"6" withString:@"F"];
    str = [str stringByReplacingOccurrencesOfString:@"7" withString:@"G"];
    str = [str stringByReplacingOccurrencesOfString:@"8" withString:@"H"];
    
    NSString *mainInfo = [NSString stringWithFormat:@"正确答案：%@",str];
    
    self.titleLabel.attributedText = [NSAttributedString_Encapsulation changeTextColorWithColor:[UIColor colorWithCustomerName:@"黑"] string:mainInfo andSubString:@[@"正确答案："]];
    
    self.titleLabel.frame = CGRectMake(TSFloat(20), TSFloat(11), kScreenBounds.size.width - 2 * TSFloat(20), [NSString contentofHeightWithFont:self.titleLabel.font]);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += TSFloat(11);
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"正文"]];
    cellHeight += TSFloat(11);
    return cellHeight;
}

@end
