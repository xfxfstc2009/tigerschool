//
//  TSHomeClassDetailChooseQuestionNextTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/5.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailChooseQuestionNextTableViewCell.h"

static char actionButtonClickWithLastKey;
static char actionButtonClickWithNextKey;
@interface TSHomeClassDetailChooseQuestionNextTableViewCell()


@end
@implementation TSHomeClassDetailChooseQuestionNextTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.lastButton = [self createCustomerButton:@"上一题"];
    [self addSubview:self.lastButton];
    __weak typeof(self)weakSelf = self;
    [self.lastButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionButtonClickWithLastKey);
        if (block){
            block();
        }
    }];
    self.lastButton.frame = CGRectMake(0, TSFloat(11), TSFloat(70), TSFloat(28));
    
    self.sendButton = [self createCustomerButton:@"下一题"];
    [self addSubview:self.sendButton];
    [self.sendButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionButtonClickWithNextKey);
        if (block){
            block();
        }
    }];
    
    CGFloat margin = TSFloat(10);
    CGFloat margin_x = (kScreenBounds.size.width - 2 * TSFloat(70) - TSFloat(10)) / 2.;
    self.lastButton.orgin_x = margin_x;
    self.sendButton.frame = CGRectMake(CGRectGetMaxX(self.lastButton.frame) + margin, self.lastButton.orgin_y, self.lastButton.size_width, self.lastButton.size_height);
}

-(UIButton *)createCustomerButton:(NSString *)title{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    button.backgroundColor = [UIColor hexChangeFloat:@"353439"];
    [button setTitle:title forState:UIControlStateNormal];
    button.titleLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [button setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    return button;
}


-(void)actionButtonClickWithLast:(void(^)())lastBlock nextBlock:(void(^)())nextBlock{
    objc_setAssociatedObject(self, &actionButtonClickWithLastKey, lastBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
    objc_setAssociatedObject(self, &actionButtonClickWithNextKey, nextBlock, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = TSFloat(11);
    cellHeight += TSFloat(40);
    cellHeight += TSFloat(11);
    return cellHeight;
}


// 第一题的方法
-(void)firstQuestionManager{
    self.lastButton.hidden = YES;
    self.sendButton.hidden = NO;
    self.sendButton.center_x = kScreenBounds.size.width / 2.;
    [self.sendButton setTitle:@"下一题" forState:UIControlStateNormal];
}

-(void)lastQuestionManager{
    self.lastButton.hidden = NO;
    self.sendButton.hidden = NO;
    
    
    [self.sendButton setTitle:@"提交" forState:UIControlStateNormal];
    
    CGFloat margin = TSFloat(10);
    CGFloat margin_x = (kScreenBounds.size.width - 2 * TSFloat(70) - TSFloat(10)) / 2.;
    self.lastButton.orgin_x = margin_x;
    self.sendButton.frame = CGRectMake(CGRectGetMaxX(self.lastButton.frame) + margin, self.lastButton.orgin_y, self.lastButton.size_width, self.lastButton.size_height);
}

-(void)normalQuestionManager{
    self.lastButton.hidden = NO;
    self.sendButton.hidden = NO;
    
    [self.sendButton setTitle:@"下一题" forState:UIControlStateNormal];
    CGFloat margin = TSFloat(10);
    CGFloat margin_x = (kScreenBounds.size.width - 2 * TSFloat(70) - TSFloat(10)) / 2.;
    self.lastButton.orgin_x = margin_x;
    self.sendButton.frame = CGRectMake(CGRectGetMaxX(self.lastButton.frame) + margin, self.lastButton.orgin_y, self.lastButton.size_width, self.lastButton.size_height);
}

@end
