//
//  TSHomeClassDetailQuestionRootTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/5.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSHomeCourseDetailClassModel.h"
@interface TSHomeClassDetailQuestionRootTableViewCell : TSBaseTableViewCell

@property (nonatomic,copy)NSString *transferChooseInfo;             /**< 选项*/
@property (nonatomic,assign)NSInteger transferIndex;                /**< 选项位置*/
@property (nonatomic,strong)TSHomeCourseDetailExamitemsSingleModel *transferSingleModel;

+(CGFloat)calculationCellHeightWithStr:(NSString *)str;

@end


@interface TSHomeClassDetailQuestionRootEndTableViewCell : TSBaseTableViewCell

@property (nonatomic,copy)NSString *transferEndInfo;

@end
