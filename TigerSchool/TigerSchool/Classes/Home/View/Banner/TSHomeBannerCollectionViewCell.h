//
//  TSHomeBannerCollectionViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHomeBannerCollectionViewCell : UICollectionViewCell
@property (nonatomic,strong) TSImageView *imageView;
@property (nonatomic,assign) CGFloat imgCornerRadius;

@end
