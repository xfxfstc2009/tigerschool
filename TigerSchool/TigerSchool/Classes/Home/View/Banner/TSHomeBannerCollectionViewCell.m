//
//  TSHomeBannerCollectionViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeBannerCollectionViewCell.h"
#import <AVFoundation/AVFoundation.h>

@interface TSHomeBannerCollectionViewCell()
@property (nonatomic,strong)TSImageView *alphaImgView;


@end

@implementation TSHomeBannerCollectionViewCell

-(instancetype)initWithFrame:(CGRect)frame {
    if(self = [super initWithFrame:frame]) {
        
        
        
        
        
        
        [self.contentView addSubview:self.imageView];
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.alphaImgView = [[TSImageView alloc]init];
    self.alphaImgView.frame = self.bounds;
    self.alphaImgView.image = [TSTool stretchImageWithName:@"icon_home_tags_alpha"];
    [self addSubview:self.alphaImgView];
}


-(void)layoutSubviews {
    self.imageView.frame = self.bounds;
    
    UIBezierPath *maskPath = [UIBezierPath bezierPathWithRoundedRect:self.imageView.bounds cornerRadius:self.imgCornerRadius];
    CAShapeLayer *maskLayer = [[CAShapeLayer alloc]init];
    //设置大小
    maskLayer.frame = self.bounds;
    //设置图形样子
    maskLayer.path = maskPath.CGPath;
    _imageView.layer.mask = maskLayer;
    
}

-(UIImageView *)imageView {
    if(_imageView == nil) {
        _imageView = [[TSImageView alloc]init];
    }
    return _imageView;
}

@end
