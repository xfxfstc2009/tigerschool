//
//  TSCuotijiexiTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/21.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSCuotijiexiTableViewCell.h"

static char actionClickToGetCuotijiexiBlockKey;
@interface TSCuotijiexiTableViewCell()
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)TSImageView *bgView;
@property (nonatomic,strong)UILabel *scoreLabel;
@property (nonatomic,strong)UILabel *scoreFixedLabel;
@property (nonatomic,strong)UILabel *jiexiFixedLabel;
@property (nonatomic,strong)UIButton *actionButton;
@end

@implementation TSCuotijiexiTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.titleLabel = [TSViewTool createLabelFont:@"标题" textColor:@"黑"];
    self.titleLabel.text = @"本节自测";
    self.titleLabel.frame = CGRectMake(TSFloat(17), 0, kScreenBounds.size.width - 2 * TSFloat(17), [NSString contentofHeightWithFont:self.titleLabel.font]);
    self.titleLabel.font = [self.titleLabel.font boldFont];
    [self addSubview:self.titleLabel];
    
    self.bgView = [[TSImageView alloc]init];
    self.bgView.backgroundColor = [UIColor hexChangeFloat:@"FFF8F3"];
    self.bgView.frame = CGRectMake(TSFloat(17), CGRectGetMaxY(self.titleLabel.frame) + TSFloat(18), kScreenBounds.size.width - 2 * TSFloat(17), TSFloat(572) / 2.);
    self.bgView.userInteractionEnabled = YES;
    self.bgView.image = [UIImage imageNamed:@"bg_dati_"];
    [self addSubview:self.bgView];
    
    self.scoreLabel = [TSViewTool createLabelFont:@"正文" textColor:@"橙"];
    self.scoreLabel.font = [[UIFont systemFontOfCustomeSize:50.]boldFont];
    self.scoreLabel.backgroundColor = [UIColor clearColor];
    self.scoreLabel.alpha = .3f;
    self.scoreLabel.textAlignment = NSTextAlignmentCenter;
    self.scoreLabel.text = @"50";
    self.scoreLabel.frame = CGRectMake(0, TSFloat(65), self.bgView.size_width, [NSString contentofHeightWithFont:self.scoreLabel.font]);
    [self.bgView addSubview:self.scoreLabel];
    self.scoreLabel.center_y = TSFloat(105) + TSFloat(75) / 4.;
    
    self.scoreFixedLabel = [TSViewTool createLabelFont:@"标题" textColor:@"橙"];
    self.scoreFixedLabel.text = @"分";
    [self.bgView addSubview:self.scoreFixedLabel];
    
    
    self.jiexiFixedLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.jiexiFixedLabel.text = @"查看错题解析>";
    self.jiexiFixedLabel.frame = CGRectMake(0, CGRectGetMaxY(self.scoreLabel.frame) + TSFloat(25), self.bgView.size_width, [NSString contentofHeightWithFont:self.jiexiFixedLabel.font]);
    self.jiexiFixedLabel.textAlignment = NSTextAlignmentCenter;
    [self.bgView addSubview:self.jiexiFixedLabel];
    
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.backgroundColor = [UIColor clearColor];
    [self.bgView addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickToGetCuotijiexiBlockKey);
        if (block){
            block();
        }
    }];
}

-(void)actionClickToGetCuotijiexiBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickToGetCuotijiexiBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
}

-(void)showAnimationWithScore:(NSInteger)score{
    // 1. score
    self.scoreLabel.text = [NSString stringWithFormat:@"%li",(long)score];
    CGSize scoreSize = [self.scoreLabel.text sizeWithCalcFont:self.scoreLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.scoreLabel.font])];
    self.scoreLabel.frame = CGRectMake((self.bgView.size_width - scoreSize.width) / 2., self.scoreLabel.orgin_y, scoreSize.width, [NSString contentofHeightWithFont:self.scoreLabel.font]);
    
    // 2.
    CGSize scoreFixedLabelSize = [self.scoreFixedLabel.text sizeWithCalcFont:self.scoreFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.scoreFixedLabel.font])];
    self.scoreFixedLabel.frame = CGRectMake(CGRectGetMaxX(self.scoreLabel.frame), self.scoreLabel.center_y, scoreFixedLabelSize.width, [NSString contentofHeightWithFont:self.scoreFixedLabel.font]);
    
    self.jiexiFixedLabel.center_y = self.scoreLabel.center_y + TSFloat(25) + TSFloat(75) / 2. + [NSString contentofHeightWithFont:self.jiexiFixedLabel.font] / 2.;
    
    //
    self.actionButton.frame = CGRectMake(0, 0, self.bgView.size_width, 3 * self.jiexiFixedLabel.size_height);
    self.actionButton.center_y = self.jiexiFixedLabel.center_y;
}

+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight += [NSString contentofHeightWithFont:[UIFont fontWithCustomerSizeName:@"标题"]];
    cellHeight += TSFloat(18);
    cellHeight += TSFloat(572) / 2.;
    cellHeight += TSFloat(11);
    return cellHeight;
}

@end
