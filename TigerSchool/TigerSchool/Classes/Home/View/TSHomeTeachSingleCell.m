//
//  TSHomeTeachSingleCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeTeachSingleCell.h"
#import "TSHomeTeachLiveSingleView.h"

@interface TSHomeTeachSingleCell()


@property (nonatomic,strong)TSHomeTeachLiveSingleView *liveImgView;

@property (nonatomic,strong)UILabel *liveLabel;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UILabel *countLabel;
@property (nonatomic,strong)UILabel *teacherLabel;
@property (nonatomic,strong)UILabel *teacherTagLabel;               /**< 讲师的标签*/
@property (nonatomic,strong)UILabel *timeLabel;
@property (nonatomic,strong)UILabel *timeLongLabel;                 /**< 时长*/

@end

@implementation TSHomeTeachSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.liveImgView = [[TSHomeTeachLiveSingleView alloc]initWithFrame:CGRectMake(TSFloat(11), TSFloat(8), TSFloat(120), TSFloat(70))];
    [self addSubview:self.liveImgView];

    // 2. 创建label
    self.titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    self.titleLabel.font = [self.titleLabel.font boldFont];
    self.titleLabel.numberOfLines = 0;
    [self addSubview:self.titleLabel];
    
    // 3. countLabel
    self.countLabel = [TSViewTool createLabelFont:@"时长" textColor:@"红"];
    [self addSubview:self.countLabel];
    
    // 4. 创建讲师
    self.teacherLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"灰"];
    [self addSubview:self.teacherLabel];
    
    // 5. 创建讲师标签
    self.teacherTagLabel = [TSViewTool createLabelFont:@"时长" textColor:@"浅灰"];
    [self addSubview:self.teacherTagLabel];
    
    // 6. 创建开课时间
    self.timeLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"灰"];
    self.timeLabel.textColor = [UIColor hexChangeFloat:@"FFFFFF"];
    self.timeLabel.adjustsFontSizeToFitWidth = YES;
    self.timeLabel.textAlignment = NSTextAlignmentCenter;
    [self.liveImgView addSubview:self.timeLabel];
    
    // 7.创建时长
    self.timeLongLabel = [TSViewTool createLabelFont:@"时长" textColor:@"红"];
    self.timeLongLabel.backgroundColor = [UIColor colorWithCustomerName:@"红高亮"];
    self.timeLongLabel.textAlignment = NSTextAlignmentCenter;
    
    [self addSubview:self.timeLongLabel];
}

-(void)setTransferHomeLiveModel:(TSHomeLiveModel *)transferHomeLiveModel{
    _transferHomeLiveModel = transferHomeLiveModel;
    
    // 1.liveImgView
    self.liveImgView.frame = CGRectMake(TSFloat(16), TSFloat(8), self.liveImgView.size_width, self.liveImgView.size_height);
    self.liveImgView.transferHomeLiveModel = transferHomeLiveModel;
    
    // 2. title
    self.titleLabel.text = transferHomeLiveModel.title;
    
    // 3. 创建是否有讲师
    self.teacherLabel.text = [NSString stringWithFormat:@"特约嘉宾:%@",transferHomeLiveModel.teacher];
    CGSize teacherSize = [self.teacherLabel.text sizeWithCalcFont:self.teacherLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.teacherLabel.font])];
    
    // 4. 创建开课时间

    self.timeLabel.text = transferHomeLiveModel.starttime.length?[NSString stringWithFormat:@"%@",transferHomeLiveModel.starttime]:@"";
    CGFloat timeSizeHeight = 0;
    if (self.timeLabel.text.length){
        timeSizeHeight = [NSString contentofHeightWithFont:self.timeLabel.font];
    } else {
        timeSizeHeight = 0;
    }
    
    // 5. 计算时间长
    self.timeLongLabel.text = transferHomeLiveModel.Timelong;
    CGSize timeLongSize = [transferHomeLiveModel.Timelong sizeWithCalcFont:self.timeLongLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.timeLongLabel.font])];
    CGFloat timeLongWidth = timeLongSize.width + 2 * TSFloat(2);
    
    // 6. 计算
    CGFloat width = kScreenBounds.size.width - TSFloat(10) - CGRectGetMaxX(self.liveImgView.frame) - TSFloat(10) * 2;
    
    // 计算title
    CGSize titleSize = [self.titleLabel.text sizeWithCalcFont:self.titleLabel.font constrainedToSize:CGSizeMake(width, CGFLOAT_MAX)];
    if (titleSize.height >= 2 * [NSString contentofHeightWithFont:self.titleLabel.font]){
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.liveImgView.frame) + TSFloat(10), 0, width, 1 * [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.titleLabel.numberOfLines = 1;
    } else {
        self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.liveImgView.frame) + TSFloat(10), 0, width, [NSString contentofHeightWithFont:self.titleLabel.font]);
        self.titleLabel.numberOfLines = 1;
    }
    
    CGFloat margin = (self.liveImgView.size_height - self.titleLabel.size_height - teacherSize.height - [NSString contentofHeightWithFont:self.timeLongLabel.font]) / 2.;
    
    // 7. 计算title
    self.titleLabel.frame = CGRectMake(CGRectGetMaxX(self.liveImgView.frame) + TSFloat(10), self.liveImgView.orgin_y, self.titleLabel.size_width,self.titleLabel.size_height);
    
    // 8.teacher
    self.teacherLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.titleLabel.frame) + margin, teacherSize.width, [NSString contentofHeightWithFont:self.teacherLabel.font]);
    
    self.timeLabel.frame = CGRectMake(0,self.liveImgView.size_height - [NSString contentofHeightWithFont:self.timeLabel.font] , self.liveImgView.size_width, [NSString contentofHeightWithFont:self.timeLabel.font]);
    
    self.teacherTagLabel.text = transferHomeLiveModel.appellation;
    CGSize teacherTagSize = [self.teacherTagLabel.text sizeWithCalcFont:self.teacherTagLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:self.teacherTagLabel.font])];
    self.teacherTagLabel.frame= CGRectMake(CGRectGetMaxX(self.teacherLabel.frame) + TSFloat(10), CGRectGetMaxY(self.teacherLabel.frame) - [NSString contentofHeightWithFont:self.teacherTagLabel.font], teacherTagSize.width, [NSString contentofHeightWithFont:self.teacherTagLabel.font]);
    
    self.timeLongLabel.frame = CGRectMake(self.titleLabel.orgin_x, CGRectGetMaxY(self.teacherLabel.frame) + margin, timeLongWidth, [NSString contentofHeightWithFont:self.timeLongLabel.font]);
}


+(CGFloat)calculationCellHeight{
    CGFloat cellHeight = 0;
    cellHeight +=TSFloat(8);
    cellHeight += TSFloat(70);
    cellHeight += TSFloat(8);
    return cellHeight;
}


@end
