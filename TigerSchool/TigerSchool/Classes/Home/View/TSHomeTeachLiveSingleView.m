//
//  TSHomeTeachLiveSingleView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeTeachLiveSingleView.h"
#import "GWMusicWaveView.h"

@interface TSHomeTeachLiveSingleView()

@property (nonatomic,strong)TSImageView *imageBackgroundView;
@property (nonatomic,strong)TSImageView *tagsImageView;
@property (nonatomic,strong)TSImageView *alphaImageView;
@property (nonatomic,strong)TSImageView *bottomAlphaImgView;
@property (nonatomic,strong)GWMusicWaveView *musicWaveView;
@property (nonatomic,strong)UILabel *liveLabel;
@end

@implementation TSHomeTeachLiveSingleView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. bg
    self.imageBackgroundView = [[TSImageView alloc]initWithFrame:self.bounds];
    [self addSubview:self.imageBackgroundView];
    
    // 2. 创建tagsImageView
    self.tagsImageView = [[TSImageView alloc]init];
    self.tagsImageView.frame = CGRectMake(0, TSFloat(5), TSFloat(110) / 2., TSFloat(36) / 2.);
    [self addSubview:self.tagsImageView];
    
    // 3. 创建alphaView
    self.alphaImageView = [[TSImageView alloc]init];
    self.alphaImageView.frame = self.bounds;
    self.alphaImageView.image = [TSTool stretchImageWithName:@"icon_home_tags_alpha"];
    [self addSubview:self.alphaImageView];
    
    // 4. 创建bottom
    self.bottomAlphaImgView = [[TSImageView alloc]init];
    self.bottomAlphaImgView.frame = CGRectMake(0, self.size_height - TSFloat(18), self.size_width, TSFloat(18));
    self.bottomAlphaImgView.image = [TSTool stretchImageWithName:@"icon_home_live_alpha"];
    [self addSubview:self.bottomAlphaImgView];
    
    // 创建音乐波浪
    self.musicWaveView = [[GWMusicWaveView alloc]init];
    self.musicWaveView.backgroundColor = [UIColor clearColor];
    self.musicWaveView.pillarColor = [UIColor whiteColor];
    self.musicWaveView.pillarWidth = 3;
    [self addSubview:self.musicWaveView];
    
    // label
    self.liveLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"白"];
    self.liveLabel.font = [UIFont systemFontOfCustomeSize:10.];
    [self addSubview:self.liveLabel];
}

-(void)setTransferHomeLiveModel:(TSHomeLiveModel *)transferHomeLiveModel{
    _transferHomeLiveModel = transferHomeLiveModel;
    
    // 背景图片
    [self.imageBackgroundView uploadImageWithURL:transferHomeLiveModel.img placeholder:nil callback:NULL];
    
    // 标签
    if (transferHomeLiveModel.icon.length){
        [self.tagsImageView uploadImageWithURL:transferHomeLiveModel.icon placeholder:nil callback:NULL];
        self.tagsImageView.hidden = NO;
    } else {
        self.tagsImageView.hidden = YES;
    }
    

    if(transferHomeLiveModel.isdirect){         // 直播中
        self.musicWaveView.frame = CGRectMake(TSFloat(4),self.size_height - TSFloat(14) , TSFloat(20), TSFloat(14));
        [self.musicWaveView startAnimation];
        self.liveLabel.text = @"直播中";
        CGSize liveSize = [TSTool makeSizeWithLabel:self.liveLabel];
        self.liveLabel.frame = CGRectMake(CGRectGetMaxX(self.musicWaveView.frame) , 0, liveSize.width, liveSize.height);
        self.liveLabel.center_y = self.musicWaveView.center_y;
        self.musicWaveView.hidden = NO;
        self.liveLabel.hidden = NO;
    } else {
        self.musicWaveView.hidden = YES;
        self.liveLabel.hidden = YES;
    }
    
}

@end
