//
//  TSHomeAllClassTagsTopView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeAllClassTagsTopView.h"

static char actionClickWithTagsBlockKey;
@interface TSHomeAllClassTagsTopView(){
    
}
@property (nonatomic,strong)UIView *bgView;             /**< 背景图*/


@end

@implementation TSHomeAllClassTagsTopView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        
    }
    return self;
}

-(instancetype)init{
    self = [super init];
    if (self){
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 100);
    [self addSubview:self.bgView];
}

-(void)setTransferSingleModel:(NSArray<TSHomeAllClassSingleTagsModel> *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    
    CGFloat origin_margin_x = TSFloat(16);
    CGFloat origin_margin_y = TSFloat(16);
    CGFloat origin_margin_h = TSFloat(6);
    CGFloat margin = (kScreenBounds.size.width - 2 * origin_margin_x - 4 * TSFloat(75)) / 3.;
    
    if (self.bgView.subviews.count){
        [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    for (int i = 0 ; i < transferSingleModel.count;i++){
        TSHomeAllClassSingleTagsModel *tagSingleModel = [transferSingleModel objectAtIndex:i];
        UIView *flagImageView = [self createTagViewWithTitle:tagSingleModel];
        CGFloat origin_x = origin_margin_x + (i % 4) * (flagImageView.size_width + margin);
        NSInteger numberOfLine = i / 4;
        
        CGFloat origin_y = origin_margin_y + numberOfLine * (origin_margin_h + flagImageView.size_height);
        flagImageView.frame = CGRectMake(origin_x, origin_y, flagImageView.size_width, flagImageView.size_height);
        [self.bgView addSubview:flagImageView];
        
        if (i == transferSingleModel.count - 1){
            self.bgView.size_height = CGRectGetMaxY(flagImageView.frame) + origin_margin_y;
            self.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.bgView.size_height);
        }
    }
}

#pragma mark - createTagView
-(UIView *)createTagViewWithTitle:(TSHomeAllClassSingleTagsModel *)singleModel{
    TSImageView *tagView = [[TSImageView alloc]init];
    tagView.frame = CGRectMake(0, 0, TSFloat(150) / 2., TSFloat(56) / 2.);
    tagView.userInteractionEnabled = YES;
    
    //1. 创建文字
    UILabel *flagLabel = [[UILabel alloc]init];
    flagLabel.backgroundColor = [UIColor clearColor];
    flagLabel.font = [UIFont systemFontOfCustomeSize:13.];
    flagLabel.userInteractionEnabled = YES;
    flagLabel.textAlignment = NSTextAlignmentCenter;
    flagLabel.text = singleModel.name;
    flagLabel.frame = tagView.bounds;
    [tagView addSubview:flagLabel];
    
    if ([singleModel.ID isEqualToString:self.selectedSingleModel.ID]){
        tagView.image = [TSTool stretchImageWithName:@"icon_home_circular_hlt"];
        flagLabel.textColor = [UIColor colorWithCustomerName:@"白"];
    } else {
        tagView.image = [TSTool stretchImageWithName:@"icon_home_circular_nor"];
        flagLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    }
    
    // 2. 创建按钮
    UIButton *selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectedButton.backgroundColor = [UIColor clearColor];
    selectedButton.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [selectedButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.selectedSingleModel = singleModel;
        
        void(^block)(TSHomeAllClassSingleTagsModel *singleModel) = objc_getAssociatedObject(strongSelf, &actionClickWithTagsBlockKey);
        if (block){
            block(singleModel);
        }
    }];
    [tagView addSubview:selectedButton];
    
    selectedButton.frame = tagView.bounds;
    return tagView;
}

-(void)actionClickWithTagsBlock:(void(^)(TSHomeAllClassSingleTagsModel *singleModel))block{
    objc_setAssociatedObject(self, &actionClickWithTagsBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
