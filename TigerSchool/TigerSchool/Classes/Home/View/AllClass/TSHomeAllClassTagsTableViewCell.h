//
//  TSHomeAllClassTagsTableViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaseTableViewCell.h"
#import "TSHomeAllClassTagsModel.h"

@interface TSHomeAllClassTagsTableViewCell : TSBaseTableViewCell

@property (nonatomic,strong)NSArray <TSHomeAllClassSingleTagsModel>*transferSingleModel;

-(void)actionClickWithTagsBlock:(void(^)(TSHomeAllClassSingleTagsModel *singleModel))block;

+(CGFloat)calculationCellHeightWithArr:(NSArray *)transferArr;

@end
