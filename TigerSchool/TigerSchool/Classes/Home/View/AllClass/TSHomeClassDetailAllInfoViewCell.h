//
//  TSHomeClassDetailAllInfoViewCell.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TSHomeClassDetailAllInfoViewCell : UICollectionViewCell

@property (nonatomic,strong)UIView *containerView;
@property (nonatomic,strong)TSImageView *assetsImageView;
@property (nonatomic,copy)NSString *transferImgUrl;



@end
