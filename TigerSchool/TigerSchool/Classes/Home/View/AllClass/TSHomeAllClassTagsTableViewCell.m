//
//  TSHomeAllClassTagsTableViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeAllClassTagsTableViewCell.h"

static char actionClickWithTagsBlockKey;
@interface TSHomeAllClassTagsTableViewCell()
@property (nonatomic,strong)UIView *bgView;             /**< 背景图*/
@property (nonatomic,assign)CGRect lastViewRect;        /**< 最后一个view */
@end

@implementation TSHomeAllClassTagsTableViewCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        self.userInteractionEnabled = YES;
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.bgView = [[UIView alloc]init];
    self.bgView.backgroundColor = [UIColor clearColor];
    self.bgView.userInteractionEnabled = YES;
    self.bgView.frame = CGRectMake(0, 0, kScreenBounds.size.width, 100);
    [self addSubview:self.bgView];
}


-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    self.bgView.size_height = transferCellHeight;
}


-(void)setTransferSingleModel:(NSArray<TSHomeAllClassSingleTagsModel> *)transferSingleModel{
    _transferSingleModel = transferSingleModel;
    CGFloat flagWidth = kScreenBounds.size.width - 2 * TSFloat(16);
    CGFloat flagOriginX = TSFloat(16);
    CGFloat flagOriginY = TSFloat(17);
    CGFloat flagMargin = TSFloat(14);
    
    if (self.bgView.subviews.count){
        [self.bgView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }
    
    for (int i = 0 ; i < transferSingleModel.count;i++){
        TSHomeAllClassSingleTagsModel *tagSingleModel = [transferSingleModel objectAtIndex:i];
        UIView *flagImageView = [self createTagViewWithTitle:tagSingleModel];
        if (flagImageView.size_width < flagWidth){                      // 不换行
            flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
            flagWidth -= (flagImageView.size_width + flagMargin);
            flagOriginX += (flagImageView.size_width + flagMargin);
        } else {
            flagOriginY += [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13.]] + 2 * TSFloat(12) + TSFloat(17);
            flagOriginX =  TSFloat(14);
            flagWidth = kScreenBounds.size.width - 2 * flagMargin;
            flagImageView.frame = CGRectMake(flagOriginX, flagOriginY, flagImageView.size_width, flagImageView.size_height);
            flagWidth -= (flagImageView.size_width + flagMargin);
            flagOriginX += (flagImageView.size_width + flagMargin);
        }
        [self.bgView addSubview:flagImageView];
        
        if (i == transferSingleModel.count - 1){
            self.bgView.size_height = CGRectGetMaxY(flagImageView.frame) + TSFloat(17);
        }
    }
}


#pragma mark - createTagView
-(TSImageView *)createTagViewWithTitle:(TSHomeAllClassSingleTagsModel *)singleModel{
    TSImageView *tagView = [[TSImageView alloc]init];
    tagView.image = [TSTool stretchImageWithName:@"icon_home_circular_nor"];
    tagView.backgroundColor = [UIColor clearColor];
    tagView.userInteractionEnabled = YES;
    
    //1. 创建文字
    UILabel *flagLabel = [[UILabel alloc]init];
    flagLabel.backgroundColor = [UIColor clearColor];
    flagLabel.font = [UIFont systemFontOfCustomeSize:13.];
    flagLabel.userInteractionEnabled = YES;
    flagLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
    flagLabel.text = singleModel.name;
    CGSize flagLabelSize = [flagLabel.text sizeWithCalcFont:flagLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:flagLabel.font])];
    flagLabel.frame = CGRectMake(TSFloat(12), 0, flagLabelSize.width, 2 * TSFloat(12) + [NSString contentofHeightWithFont:flagLabel.font]);
    [tagView addSubview:flagLabel];
    
    // 2. 创建按钮
    UIButton *selectedButton = [UIButton buttonWithType:UIButtonTypeCustom];
    selectedButton.backgroundColor = [UIColor clearColor];
    selectedButton.userInteractionEnabled = YES;
    __weak typeof(self)weakSelf = self;
    [selectedButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)(TSHomeAllClassSingleTagsModel *singleModel) = objc_getAssociatedObject(strongSelf, &actionClickWithTagsBlockKey);
        if (block){
            block(singleModel);
        }
    }];
    [tagView addSubview:selectedButton];
    
    tagView.bounds = CGRectMake(0, 0, flagLabel.size_width + 2 * TSFloat(12), 2 * TSFloat(4) + [NSString contentofHeightWithFont:flagLabel.font] );
    selectedButton.frame = tagView.bounds;
    return tagView;
}

+(CGFloat)calculationCellHeightWithArr:(NSArray *)transferArr{
    
    CGFloat cellHeight = TSFloat(11);
    
    CGFloat flagWidth = kScreenBounds.size.width - 2 * TSFloat(16);
    CGFloat flagOriginX = TSFloat(16);
    CGFloat flagOriginY = TSFloat(17);
    CGFloat flagMargin = TSFloat(14);
    
    for (int i = 0 ; i < transferArr.count;i++){
        TSHomeAllClassSingleTagsModel *productTagSingleModel = [transferArr objectAtIndex:i];
        CGSize floagSize = [productTagSingleModel.name sizeWithCalcFont:[UIFont systemFontOfCustomeSize:13.] constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13.]])];
        CGFloat floagWidth = floagSize.width + 2 * TSFloat(4);
        
        if (floagWidth < flagWidth){                      // 不换行
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
        } else {
            flagOriginY += [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13.]] + 2 * TSFloat(12) + TSFloat(17);
            flagOriginX =  TSFloat(14);
            flagWidth = kScreenBounds.size.width - 2 * flagMargin;
            flagWidth -= (floagWidth + flagMargin);
            flagOriginX += (floagWidth + flagMargin);
            cellHeight = flagOriginY;
        }
    }
    cellHeight += [NSString contentofHeightWithFont:[UIFont systemFontOfCustomeSize:13.]] + 2 * TSFloat(4) + TSFloat(11);
    
    return cellHeight;
}


-(void)actionClickWithTagsBlock:(void(^)(TSHomeAllClassSingleTagsModel *singleModel))block{
    objc_setAssociatedObject(self, &actionClickWithTagsBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
