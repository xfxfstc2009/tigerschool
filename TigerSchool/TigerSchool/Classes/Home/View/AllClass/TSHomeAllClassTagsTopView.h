//
//  TSHomeAllClassTagsTopView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHomeAllClassTagsModel.h"

@interface TSHomeAllClassTagsTopView : UIView

@property (nonatomic,strong)TSHomeAllClassSingleTagsModel *selectedSingleModel;         /**< 选中的按钮*/

@property (nonatomic,strong)NSArray <TSHomeAllClassSingleTagsModel>*transferSingleModel;

-(void)actionClickWithTagsBlock:(void(^)(TSHomeAllClassSingleTagsModel *singleModel))block;

@end
