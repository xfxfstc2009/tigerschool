//
//  TSHomeClassDetailAllInfoViewCell.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailAllInfoViewCell.h"

//static char actionClickWithAllSucaiBlockKey;
@interface TSHomeClassDetailAllInfoViewCell()
@end

@implementation TSHomeClassDetailAllInfoViewCell

#define kThumbnailLength    100

-(id)initWithFrame:(CGRect)frame{
    if (self = [super initWithFrame:frame]){
        [self createView];
        self.clipsToBounds = YES;
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    self.containerView = [[UIView alloc]init];
    self.containerView.backgroundColor = [UIColor clearColor];
    self.containerView.frame = self.bounds;
    self.containerView.clipsToBounds = YES;
    [self addSubview:self.containerView];
    
    self.assetsImageView = [[TSImageView alloc]init];
    self.assetsImageView.backgroundColor = [UIColor clearColor];
    self.assetsImageView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.assetsImageView.frame = CGRectMake(0, 0, self.bounds.size.width, self.bounds.size.height);
    [self.containerView addSubview:self.assetsImageView];
}


-(void)setTransferImgUrl:(NSString *)transferImgUrl{
    _transferImgUrl = transferImgUrl;
    __weak typeof(self)weakSelf = self;
    [self.assetsImageView uploadImageWithURL:transferImgUrl placeholder:nil callback:^(UIImage *image) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
//        CGFloat scale = image.size.height / kThumbnailLength;
        self.assetsImageView.image = image;
        
        // 基本尺寸参数
        CGSize boundsSize = strongSelf.bounds.size;
        CGFloat boundsWidth = boundsSize.width;
        CGFloat boundsHeight = boundsSize.height;
        
        CGSize imageSize = strongSelf.assetsImageView.image.size;
        CGFloat imageWidth = imageSize.width;
        CGFloat imageHeight = imageSize.height;
        
        CGRect imageFrame = CGRectMake(0, (boundsHeight - boundsWidth) / 2., boundsWidth, imageHeight * boundsWidth / imageWidth);
        
        if (imageHeight > imageWidth){          // 图片高度大于图片宽度
            if (imageFrame.size.height < boundsHeight) {
                imageFrame.origin.y = floorf((boundsHeight - imageFrame.size.height) / 2.0);
            } else {
                imageFrame.origin.y = 0;
            }
        } else {                                // 图片宽度大于高度
            if (imageFrame.size.width < boundsWidth){
                imageFrame.origin.x = 0;
                imageFrame.size.width = (boundsHeight * 1.00 / imageFrame.size.height) *imageFrame.size.width;
                imageFrame.size.height = boundsHeight;
            } else {
                imageFrame.size.width = (boundsHeight / imageFrame.size.height) * imageFrame.size.width;
                imageFrame.size.height = boundsHeight;
                imageFrame.origin.x = - floorf((imageFrame.size.width - boundsWidth) / 2.0);
            }
        }
        strongSelf.assetsImageView.frame = imageFrame;
    }];
}



@end
