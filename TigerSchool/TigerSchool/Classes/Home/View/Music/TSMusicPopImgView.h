//
//  TSMusicPopImgView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/26.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSTeachBopDetailItemsModel.h"

@interface TSMusicPopImgView : UIControl

#pragma mark - 创建单利
+(instancetype)sharedMusicView;
@property (nonatomic,strong)TSImageView *flagImageView;             /**< 上面的图片*/
@property (nonatomic,assign)BOOL hasAnimation;                      /**< 判断是否动画*/

@property (nonatomic,strong)TSTeachBopDetailItemsModel *bopSingleModel;



-(void)removeFromWindow;

-(void)imageUploadWithBlock:(void(^)())block;
// 显示的时候弹簧动画
-(void)showWithAnimationWithFinishBlock:(void(^)())block;

-(void)actionClickWithShowBopViewControllerBlock:(void(^)())block;

-(void)actionClickWithhidenBlock:(void(^)())block;

@end
