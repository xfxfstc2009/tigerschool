//
//  TSMusicPopImgView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/26.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSMusicPopImgView.h"
#import <pop/POP.h>
#import "GWMusicWaveView.h"

typedef struct {
    CGFloat progress;
    CGFloat toValue;
    CGFloat currentValue;
} AnimationInfo;

static char actionClickWithShowBopViewControllerBlockKey;
@interface TSMusicPopImgView()
@property (nonatomic,retain)CABasicAnimation *basicAnimation;       /**< 动画*/
@property (nonatomic,strong)TSImageView *bgImgView;
@property (nonatomic,strong)GWMusicWaveView *musicWaveView;
@property (nonatomic,strong)TSImageView *bg_alphaView;;
@end

@implementation TSMusicPopImgView

+(instancetype)sharedMusicView{
    static TSMusicPopImgView *_sharedMusicView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedMusicView = [[TSMusicPopImgView alloc] initWithFrame:CGRectMake(kScreenBounds.size.width - TSFloat(100) - TSFloat(13), kScreenBounds.size.height - [PDMainTabbarViewController sharedController].tabBarHeight - TSFloat(13) - TSFloat(100), TSFloat(100), TSFloat(100))];
    });
    return _sharedMusicView;
}


- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self createView];
    }
    return self;
}


#pragma mark - createView
-(void)createView{
    self.bgImgView = [[TSImageView alloc]init];
    self.bgImgView.frame = CGRectMake(0, 0, self.size_width, self.size_height);
    self.bgImgView.image = [UIImage imageNamed:@"icon_top_music_alpha"];
    [self addSubview:self.bgImgView];
    
    self.flagImageView = [[TSImageView alloc]init];
    self.flagImageView.frame = CGRectMake(TSFloat(15), TSFloat(15), self.size_height - 2 * TSFloat(15), self.size_height - 2 * TSFloat(15));
    self.flagImageView.center = self.bgImgView.center;
    [self addSubview:self.flagImageView];
    
    UIPanGestureRecognizer *recognizer = [[UIPanGestureRecognizer alloc]initWithTarget:self action:@selector(handlePan:)];
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(tapManager)];
    [self addGestureRecognizer:tapGesture];
    [self addGestureRecognizer:recognizer];
    
    // [alpha]
    self.bg_alphaView = [[TSImageView alloc]init];
    self.bg_alphaView.frame = self.flagImageView.frame;
    self.bg_alphaView.image = [TSTool stretchImageWithName:@"bg_alpha"];
    self.bg_alphaView.layer.cornerRadius = self.bg_alphaView.size_height / 2.;
    self.bg_alphaView.clipsToBounds = YES;
    [self addSubview:self.bg_alphaView];
    
    // 创建音乐波浪
    self.musicWaveView = [[GWMusicWaveView alloc]init];
    self.musicWaveView.backgroundColor = [UIColor clearColor];
    self.musicWaveView.pillarColor = [UIColor whiteColor];
    self.musicWaveView.pillarWidth = 5;
    self.musicWaveView.frame = CGRectMake((self.size_width - TSFloat(30)) / 2. , (self.size_width - TSFloat(30)) / 2., TSFloat(30), TSFloat(30));
    [self addSubview:self.musicWaveView];
    
    [self scaleDownView:self];
}

#pragma mark - 手势
- (void)handlePan:(UIPanGestureRecognizer *)recognizer {
    // 1. 进行抖动
    [self scaleDownView:recognizer.view];
    
    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
    CGPoint translation = [recognizer translationInView:keywindow];
    recognizer.view.center = CGPointMake(recognizer.view.center.x + translation.x,
                                         recognizer.view.center.y + translation.y);
    [recognizer setTranslation:CGPointMake(0, 0) inView:keywindow];
    
    if(recognizer.state == UIGestureRecognizerStateEnded) {

        
        CGPoint velocity = [recognizer velocityInView:keywindow];

        POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
        positionAnimation.velocity = [NSValue valueWithCGPoint:velocity];
        positionAnimation.dynamicsTension = 10.f;
        positionAnimation.dynamicsFriction = 1.0f;
        positionAnimation.springBounciness = 12.0f;
        [recognizer.view.layer pop_addAnimation:positionAnimation forKey:@"layerPositionAnimation"];


        // 弹性
        if (recognizer.view.orgin_x < 0){
            recognizer.view.orgin_x = 0;
        }
        if (recognizer.view.orgin_x > kScreenBounds.size.width - recognizer.view.size_width){
            recognizer.view.orgin_x = kScreenBounds.size.width - recognizer.view.size_width;
        }

        if (recognizer.view.orgin_y < 64){
            recognizer.view.orgin_y = 64;
        }

        if (recognizer.view.orgin_y > kScreenBounds.size.height - [PDMainTabbarViewController sharedController].tabBarHeight - recognizer.view.size_height){
            recognizer.view.orgin_y = kScreenBounds.size.height - [PDMainTabbarViewController sharedController].tabBarHeight - recognizer.view.size_height;
        }
    }

    
    
}

- (void)touchDown:(UIControl *)sender {
    [self pauseAllAnimations:YES forLayer:sender.layer];
}

- (void)touchUpInside:(UIControl *)sender {
    AnimationInfo animationInfo = [self animationInfoForLayer:sender.layer];
    BOOL hasAnimations = sender.layer.pop_animationKeys.count?YES:NO;

    if (hasAnimations && animationInfo.progress < 0.98) {
        [self pauseAllAnimations:NO forLayer:sender.layer];
        return;
    }

    [sender.layer pop_removeAllAnimations];
    if (animationInfo.toValue == 1 || sender.layer.affineTransform.a == 1) {
        [self scaleDownView:sender];
        return;
    }
    [self scaleUpView:sender];
}


-(void)scaleUpView:(UIView *)view {
    POPSpringAnimation *positionAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerPosition];
    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
    positionAnimation.toValue = [NSValue valueWithCGPoint:keywindow.center];
    [view.layer pop_addAnimation:positionAnimation forKey:@"layerPositionAnimation"];
    
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(1, 1)];
    scaleAnimation.springBounciness = 10.f;
    [view.layer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)scaleDownView:(UIView *)view
{
    POPSpringAnimation *scaleAnimation = [POPSpringAnimation animationWithPropertyNamed:kPOPLayerScaleXY];
    scaleAnimation.toValue = [NSValue valueWithCGSize:CGSizeMake(0.5, 0.5)];
    scaleAnimation.springBounciness = 10.f;
    [view.layer pop_addAnimation:scaleAnimation forKey:@"scaleAnimation"];
}

- (void)pauseAllAnimations:(BOOL)pause forLayer:(CALayer *)layer
{
    for (NSString *key in layer.pop_animationKeys) {
        POPAnimation *animation = [layer pop_animationForKey:key];
        [animation setPaused:pause];
    }
}

- (AnimationInfo)animationInfoForLayer:(CALayer *)layer {
    POPSpringAnimation *animation = [layer pop_animationForKey:@"scaleAnimation"];
    CGPoint toValue = [animation.toValue CGPointValue];
    CGPoint currentValue = [[animation valueForKey:@"currentValue"] CGPointValue];
    
    CGFloat min = MIN(toValue.x, currentValue.x);
    CGFloat max = MAX(toValue.x, currentValue.x);
    
    AnimationInfo info;
    info.toValue = toValue.x;
    info.currentValue = currentValue.x;
    info.progress = min / max;
    return info;
}


#pragma mark - 动画
-(void)imageUploadWithBlock:(void(^)())block{
    self.flagImageView.image =  [TSTool imageByComposingImage:[UIImage imageNamed:@"icon_music_avatar_nor"] withMaskImage:[UIImage imageNamed:@"round.png"]];
}

- (CABasicAnimation *)createBasicAnimation {
    if (_basicAnimation == nil) {
        self.basicAnimation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        self.basicAnimation.duration = 20;
        self.basicAnimation.fromValue = [NSNumber numberWithInt:0];
        self.basicAnimation.toValue = [NSNumber numberWithInt:M_PI * 2];
        [self.basicAnimation setRepeatCount:NSIntegerMax];
        [self.basicAnimation setAutoreverses:NO];
        [self.basicAnimation setCumulative:YES];
        self.flagImageView.layer.speed = 1;
        [self.flagImageView.layer addAnimation:self.basicAnimation forKey:@"basicAnimation"];
    } else {
        [self.flagImageView.layer addAnimation:self.basicAnimation forKey:@"basicAnimation"];
    }
    return _basicAnimation;
}

// 【view 跳出来】
-(void)showWithAnimationWithFinishBlock:(void(^)())block{
    self.transform =  CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:1. delay:.2f usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.transform = CGAffineTransformIdentity;
    } completion:^(BOOL finished) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf animationWithRoundBlock:^{
            if (block){
                block();
            }
        }];
    }];
}

// 【view进行旋转】
-(void)animationWithRoundBlock:(void(^)())block{
    [self.musicWaveView startAnimation];
    [self createBasicAnimation];
    if (block){
        block();
    }
}

-(void)removeFromWindow{
    [self removeFromSuperview];
}

-(void)actionClickWithShowBopViewControllerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &actionClickWithShowBopViewControllerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)tapManager{
    void(^block)() = objc_getAssociatedObject(self, &actionClickWithShowBopViewControllerBlockKey);
    if (block){
        block();
    }
}

-(void)actionClickWithhidenBlock:(void(^)())block{

    // 2.
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:1. delay:.4f usingSpringWithDamping:0.6 initialSpringVelocity:0 options:0 animations:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.transform = CGAffineTransformMakeTranslation(kScreenBounds.size.width, 0);
    } completion:^(BOOL finished) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 1. 移除转动动画
        [strongSelf.musicWaveView stopAnimation];
        if (block){
            block();
        }
    }];
}
@end
