//
//  TSHomeAllClassViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//
// 【全部课程】
#import "AbstractViewController.h"
#import "NetworkAdapter+TSHome.h"

@interface TSHomeAllClassViewController : AbstractViewController

@property (nonatomic,strong)TSHomeAllClassSingleTagsModel *selectedSingleModel;         /**< 选中的model*/

@end
