//
//  ImgSelectorViewController.h
//  17Live
//
//  Created by 裴烨烽 on 2017/12/11.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHomeClassDetailAllInfoViewCell.h"
#import "TSHomeCourseKejianDymicTableViewCell.h"

@interface ImgSelectorViewController : UIViewController

-(void)showInView:(UIViewController *)viewController imgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentIndex cell:(TSHomeClassDetailAllInfoViewCell *)cell;

- (void)dismissFromView:(UIViewController *)viewController;


// 课件cell
-(void)showInViewWithCell:(UIViewController *)viewController imgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentIndex cell:(TSHomeCourseKejianDymicTableViewCell *)cell;

@end
