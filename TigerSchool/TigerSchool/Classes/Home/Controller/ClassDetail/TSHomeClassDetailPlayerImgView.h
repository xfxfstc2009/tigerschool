//
//  TSHomeClassDetailPlayerImgView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSHomeCourseDetailClassModel.h"
#import "TSMaterialSegmentModel.h"
#import "ZFPlayerView.h"

@interface TSHomeClassDetailPlayerImgView : UIView
@property (nonatomic,strong)TSImageView *imgView;
@property (nonatomic,strong)UIButton *actionButton;
@property (nonatomic,strong)TSHomeCourseDetailClassModel *selectedCourseDetailModel;
@property (nonatomic,assign)BOOL hasNonePlay;           /**< 是否可以播放，Yes表示不能播放,NO 表示可以播放*/

// 点击播放按钮
-(void)actionClickToPlayer:(void(^)(TSHomeCourseDetailClassModel *selectedCourseDetailModel))block;
// 进行播放数据
-(void)actionToPlayerManager:(TSHomeCourseDetailClassModel *)selectedCourseDetailModel;
// 释放播放窗口
-(void)resetPlayerView;
// 播放结束
-(void)playViewPlayedEndManagerBlock:(void(^)())block;
// 播放记录时间
-(void)playViewPlayedUploadTimeManagerBlock:(void(^)(NSInteger time))block;





#pragma mark - 进行素材播放
@property (nonatomic,strong)TSMaterialSegmentModel *selectedMaterialModel;
-(void)actionPlayer:(TSMaterialSegmentModel *)transferStudyInfoSingleModel;
// 点击播放按钮
-(void)actionClickToPlayerWithSucai:(void(^)(TSMaterialSegmentModel *selectedCourseDetailModel))block;
-(void)changeAblumImgWithModel:(TSHomeCourseDetailClassModel *)transferModel;

@end
