//
//  TSHomeClassDetailClassViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"
#import "TSHomeCourseDetailClassModel.h"

@interface TSHomeClassDetailClassViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferCourseId;
@property (nonatomic,assign)BOOL hasQihangban;              /**< 判断是否是其航班*/

-(void)actionClickWithShowChangeModelBlock:(void(^)(TSHomeCourseDetailClassModel *model))block;

-(void)playerEndWithModel:(TSHomeCourseDetailClassModel *)detailClassModel;

@end
