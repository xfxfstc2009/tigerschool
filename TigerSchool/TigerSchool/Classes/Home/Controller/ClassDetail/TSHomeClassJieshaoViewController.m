//
//  TSHomeClassJieshaoViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassJieshaoViewController.h"
#import "TSHomeClassDetailJieshaoWebTableViewCell.h"
#import "NetworkAdapter+TSHome.h"

static char requestBackGetHasQihangbanKey;
@interface TSHomeClassJieshaoViewController ()<UITableViewDelegate,UITableViewDataSource,WKNavigationDelegate,WKUIDelegate>{
    TSCoursedescModel *tempCourseDescModel;
    CGFloat webHeight;
}
@property (nonatomic,strong)UITableView *jieshaoTableView;
@property (nonatomic,strong)NSMutableArray *jieshaoMutableArr;
@property (nonatomic,strong)NSMutableArray *jieshaoModelMutableArr;
@end

@implementation TSHomeClassJieshaoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetInfo];
}

-(void)pageSetting{
    self.barMainTitle = @"介绍";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.jieshaoMutableArr = [NSMutableArray array];
    self.jieshaoModelMutableArr = [NSMutableArray array];
}

#pragma mark - UITableView
-(void)createTableView{
    self.jieshaoTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
    self.jieshaoTableView.dataSource = self;
    self.jieshaoTableView.delegate = self;
    [self.view addSubview:self.jieshaoTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.jieshaoMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.jieshaoMutableArr objectAtIndex:section];
    return  sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.row == 0){
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        GWNormalTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
            cellWithRowZero.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowZero.transferCellHeight = cellHeight;
        if (indexPath.section == 0){
            cellWithRowZero.transferTitle = tempCourseDescModel.title;
        } else {
            cellWithRowZero.transferTitle = [[self.jieshaoMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
        }
        
        return cellWithRowZero;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        TSHomeClassDetailJieshaoWebTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[TSHomeClassDetailJieshaoWebTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowOne.webView.UIDelegate = self;
        cellWithRowOne.webView.navigationDelegate = self;
        TSHomeClassDetailJieshaoWebTableViewCellModel *model3 = [self.jieshaoModelMutableArr objectAtIndex:indexPath.section - 1];
        cellWithRowOne.transferModel = model3;
        
        if ([model3.html rangeOfString:@"<img src"].location != NSNotFound) {
            cellWithRowOne.webView.hidden = NO;
            cellWithRowOne.titleLabel.hidden = YES;
        } else {
            cellWithRowOne.webView.hidden = YES;
            cellWithRowOne.titleLabel.hidden = NO;
        }
        
        return cellWithRowOne;
    }
}

- (void)webView:(WKWebView *)webView didFinishNavigation:(null_unspecified WKNavigation *)navigation {
    
    [webView evaluateJavaScript:@"document.getElementById(\"testDiv\").offsetTop"completionHandler:^(id _Nullable result,NSError * _Nullable error) {
        //获取页面高度，并重置webview的frame
        CGFloat lastHeight  = [result doubleValue];
        webView.frame = CGRectMake(0, 0, kScreenBounds.size.width , lastHeight);
        webHeight = lastHeight;
        [self.jieshaoTableView beginUpdates];
        [self.jieshaoTableView endUpdates];
    }];
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    // 判断webView所在的cell是否可见，如果可见就layout
    NSArray *cells = self.jieshaoTableView.visibleCells;
    for (UITableViewCell *cell in cells) {
        if ([cell isKindOfClass:[TSHomeClassDetailJieshaoWebTableViewCell class]]) {
            TSHomeClassDetailJieshaoWebTableViewCell *webCell = (TSHomeClassDetailJieshaoWebTableViewCell *)cell;
            [webCell.webView setNeedsLayout];
        }
    }
}


#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0){
        return [GWNormalTableViewCell calculationCellHeight];
    } else {
      TSHomeClassDetailJieshaoWebTableViewCellModel *model3 = [self.jieshaoModelMutableArr objectAtIndex:indexPath.section - 1];
        if ([model3.html rangeOfString:@"<img src"].location != NSNotFound) {
            return webHeight;
        } else {
            return [TSHomeClassDetailJieshaoWebTableViewCell calculationCellHeightWithModel:model3];
        }
    }
}

#pragma mark - interfaceManager
-(void)sendRequestToGetInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeCoursedescWithId:self.transferCourseId block:^(TSCoursedescModel *courseDescModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->tempCourseDescModel = courseDescModel;
        
        void(^block)(BOOL hasQihangban) = objc_getAssociatedObject(strongSelf, &requestBackGetHasQihangbanKey);
        if ([courseDescModel.title containsString:@"航班"]){
            if (block){
                block(YES);
            }
        } else {
            if (block){
                block(NO);
            }
        }
        
        // 1.传递播放信息
        if (courseDescModel.title.length){
            [strongSelf.jieshaoMutableArr addObject:@[@"标题"]];
        }
        if (courseDescModel.cou_desc.length){
            [strongSelf.jieshaoMutableArr addObject:@[@"课程简介",@"课程简介详情"]];
            TSHomeClassDetailJieshaoWebTableViewCellModel *model1 = [[TSHomeClassDetailJieshaoWebTableViewCellModel alloc]init];
            model1.html = courseDescModel.cou_desc;
            [strongSelf.jieshaoModelMutableArr addObject:model1];
        }
        if (courseDescModel.tec_desc.length){
            [strongSelf.jieshaoMutableArr addObject:@[@"讲师简介",@"讲师简介详情"]];
            TSHomeClassDetailJieshaoWebTableViewCellModel *model2 = [[TSHomeClassDetailJieshaoWebTableViewCellModel alloc]init];
            model2.html = courseDescModel.tec_desc;
            [strongSelf.jieshaoModelMutableArr addObject:model2];
        }
        if (courseDescModel.buy_desc.length){
            [strongSelf.jieshaoMutableArr addObject:@[@"购买须知",@"购买须知详情"]];
            TSHomeClassDetailJieshaoWebTableViewCellModel *model3 = [[TSHomeClassDetailJieshaoWebTableViewCellModel alloc]init];
            model3.html = courseDescModel.buy_desc;
            [strongSelf.jieshaoModelMutableArr addObject:model3];
        }
        [strongSelf.jieshaoTableView reloadData];
    }];
}


-(void)requestBackGetHasQihangban:(void(^)(BOOL hasQihangban))block{
    objc_setAssociatedObject(self, &requestBackGetHasQihangbanKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

@end
