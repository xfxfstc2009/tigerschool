//
//  TSHomeClassDetailPlayerImgView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailPlayerImgView.h"

static char playViewPlayedEndManagerBlockKey;
static char actionClickToPlayerKey;
static char actionClickToPlayerWithSucaiKey;
static char playViewPlayedUploadTimeManagerBlock;
@interface TSHomeClassDetailPlayerImgView()<ZFPlayerViewDelegate>
@property (nonatomic,strong)ZFPlayerView *playerView;
@end

@implementation TSHomeClassDetailPlayerImgView

-(instancetype)initWithFrame:(CGRect)frame{
    self = [super initWithFrame:frame];
    if (self){
        [self createPlayer];
    }
    return self;
}

-(void)createPlayer{
    // 1.
    self.imgView = [[TSImageView alloc]init];
    self.imgView.frame = self.bounds;
    [self addSubview:self.imgView];
    
    // 2.playerButton
    self.actionButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.actionButton.frame = CGRectMake((kScreenBounds.size.width - TSFloat(50)) / 2., (self.size_height - TSFloat(50)) / 2., TSFloat(50), TSFloat(50));
    [self.actionButton setImage:[UIImage imageNamed:@"icon_home_player"] forState:UIControlStateNormal];
    [self addSubview:self.actionButton];
    __weak typeof(self)weakSelf = self;
    [self.actionButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        void(^block)() = objc_getAssociatedObject(strongSelf, &actionClickToPlayerKey);
        if (block){
            block(strongSelf.selectedCourseDetailModel);
        }
        
        void(^sucaiBlock)(TSMaterialSegmentModel *model) = objc_getAssociatedObject(strongSelf, &actionClickToPlayerWithSucaiKey);
        if (sucaiBlock){
            sucaiBlock(strongSelf.selectedMaterialModel);
        }
    }];
}

-(void)setSelectedCourseDetailModel:(TSHomeCourseDetailClassModel *)selectedCourseDetailModel{
    _selectedCourseDetailModel = selectedCourseDetailModel;
    // 1. img
    if (self.hasNonePlay){
        self.imgView.image = [UIImage imageNamed:@"img_homeDetail_bg_black"];
        self.actionButton.hidden = YES;
    } else {
        [self.imgView uploadImageWithURL:selectedCourseDetailModel.img placeholder:nil callback:NULL];
        self.actionButton.hidden = NO;
    }
}

// 点击播放按钮
-(void)actionClickToPlayer:(void(^)(TSHomeCourseDetailClassModel *selectedCourseDetailModel))block{
    objc_setAssociatedObject(self, &actionClickToPlayerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)actionToPlayerManager:(TSHomeCourseDetailClassModel *)selectedCourseDetailModel{
    NSURL *videoURL = [NSURL URLWithString:selectedCourseDetailModel.url];

    __block TSHomeClassDetailPlayerImgView *topDetailPlayerView = self;
    [[ZFPlayerView sharedPlayerView] setVideoURL:videoURL];                                                   // 设置url
    [ZFPlayerView sharedPlayerView].delegate = self;
    [[ZFPlayerView sharedPlayerView] addPlayerToCellImageView:topDetailPlayerView.imgView];                   // 设置info
    [ZFPlayerView sharedPlayerView].playerLayerGravity = ZFPlayerLayerGravityResize;
    
    if ([selectedCourseDetailModel.url hasSuffix:@"mp3"]){
        [ZFPlayerView sharedPlayerView].placeholderImg = selectedCourseDetailModel.img;
    } else {
        [ZFPlayerView sharedPlayerView].placeholderImg = @"";
    }
}

-(void)resetPlayerView{
    [[ZFPlayerView sharedPlayerView] resetPlayer];
}


#pragma mark - 播放素材
-(void)actionPlayer:(TSMaterialSegmentModel *)transferStudyInfoSingleModel{
    NSURL *videoURL = [NSURL URLWithString:transferStudyInfoSingleModel.url];
    
    __block TSHomeClassDetailPlayerImgView *topDetailPlayerView = self;
    [[ZFPlayerView sharedPlayerView] setVideoURL:videoURL];                                                   // 设置url
    [[ZFPlayerView sharedPlayerView] addPlayerToCellImageView:topDetailPlayerView.imgView];                   // 设置info
    [ZFPlayerView sharedPlayerView].playerLayerGravity = ZFPlayerLayerGravityResize;
    
    if ([transferStudyInfoSingleModel.url hasSuffix:@"mp3"]){
        [ZFPlayerView sharedPlayerView].placeholderImg = transferStudyInfoSingleModel.img;
    } else {
        [ZFPlayerView sharedPlayerView].placeholderImg = @"";
    }
}

// 点击播放按钮
-(void)actionClickToPlayerWithSucai:(void(^)(TSMaterialSegmentModel *selectedCourseDetailModel))block{
    objc_setAssociatedObject(self, &actionClickToPlayerWithSucaiKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)setSelectedMaterialModel:(TSMaterialSegmentModel *)selectedMaterialModel{
    _selectedMaterialModel = selectedMaterialModel;
    // 1. img
    [self.imgView uploadImageWithURL:selectedMaterialModel.img placeholder:nil callback:NULL];
}



-(void)changeAblumImgWithModel:(TSHomeCourseDetailClassModel *)transferModel{
    [[ZFPlayerView sharedPlayerView] resetPlayer];
    
    self.selectedCourseDetailModel = transferModel;
}

-(void)setHasNonePlay:(BOOL)hasNonePlay{
    _hasNonePlay = hasNonePlay;
    if (hasNonePlay){
        self.imgView.image = [UIImage imageNamed:@"img_homeDetail_bg_black"];
        self.actionButton.hidden = YES;
    } else {
        [self.imgView uploadImageWithURL:self.selectedCourseDetailModel.img placeholder:nil callback:NULL];
        self.actionButton.hidden = NO;
    }
}

#pragma mark - 播放结束
-(void)playerEndManager{
    void(^block)() = objc_getAssociatedObject(self, &playViewPlayedEndManagerBlockKey);
    if (block){
        block();
    }
}

-(void)playViewPlayedEndManagerBlock:(void(^)())block{
    objc_setAssociatedObject(self, &playViewPlayedEndManagerBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)playerTime:(NSInteger)time{
    if (time % 15 == 0){
        void(^block)(NSInteger time) = objc_getAssociatedObject(self, &playViewPlayedUploadTimeManagerBlock);
        if (block){
            block(time);
        }
    }
}

-(void)playViewPlayedUploadTimeManagerBlock:(void(^)(NSInteger time))block{
    objc_setAssociatedObject(self, &playViewPlayedUploadTimeManagerBlock, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}
@end
