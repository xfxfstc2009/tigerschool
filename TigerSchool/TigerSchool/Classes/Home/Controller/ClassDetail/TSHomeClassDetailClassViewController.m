//
//  TSHomeClassDetailClassViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/20.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailClassViewController.h"
#import "NetworkAdapter+TSHome.h"

#import "TSHomeClassTopItemsTableViewCell.h"            // 第一行
#import "TSHomeCourseKejianFixedTableViewCell.h"
#import "TSHomeCourseKejianDymicTableViewCell.h"
#import "TSHomeClassDetailAllInfoViewController.h"
#import "ImgSelectorViewController.h"
#import "TSHomeClassDetailChooseQuestionTableViewCell.h"
#import "TSHomeClassDetailChooseQuestionNumTableViewCell.h"
#import "TSHomeClassDetailChooseQuestionNextTableViewCell.h"
#import "TSHomeClassDetailQuestionRootViewController.h"
#import "TSCuotijiexiTableViewCell.h"

static char actionClickWithShowChangeModelBlockKey;
@interface TSHomeClassDetailClassViewController ()<UITableViewDelegate,UITableViewDataSource> {
    TSHomeCourseDetailListClassModel *tempListModel;
    TSHomeCourseDetailClassModel *userSelectedSingleModel;              /**< 用户选中的model*/
    NSInteger currentQuestionIndex;                                     /**< 当前题目的标签位置*/
}
@property (nonatomic,strong)UITableView *classTableView;
@property (nonatomic,strong)NSMutableArray *classRootMutableArr;
@property (nonatomic,strong)NSMutableArray *classMutableArr;
@property (nonatomic,strong)NSMutableArray *userLastChoosedMutableArr;
@property (nonatomic,strong)NSMutableArray *questionMutableArr;
@property (nonatomic,strong)NSMutableArray *questionChooseMutableArr;               /**< 问题选择的数组*/
@property (nonatomic,strong)NSArray *coursewareArr;                         /**< 课件数组*/


@end

@implementation TSHomeClassDetailClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self sendRequestToGetHomeClassInfo];
    
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"课程";
}

#pragma mark - ArrayWithInit
-(void)arrayWithInit{
    self.classRootMutableArr = [NSMutableArray array];                  // 主tableView
    self.classMutableArr = [NSMutableArray array];                      // 课程列表
    self.coursewareArr = @[@"课件fixed",@"课件"];                         // 课件数组
    self.questionMutableArr = [NSMutableArray array];                   // 当前的题目
    
    // 上一波选中的内容
    self.userLastChoosedMutableArr = [NSMutableArray array];
    
    // 用户选择问题的数组
    self.questionChooseMutableArr = [NSMutableArray array];
    
    // 初始化当前选中问题
    currentQuestionIndex = 0;
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.classTableView){
        self.classTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.classTableView.dataSource = self;
        self.classTableView.delegate = self;
        [self.view addSubview:self.classTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.classRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.classRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"课程列表" sourceArr:self.classRootMutableArr]){
        if (indexPath.row == [self cellIndexPathRowWithcellData:@"课程列表" sourceArr:self.classRootMutableArr]){
            static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
            TSHomeClassTopItemsTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
            if (!cellWithRowOne){
                cellWithRowOne = [[TSHomeClassTopItemsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
                cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowOne.transferCellHeight = cellHeight;
            cellWithRowOne.transferUserSelectedDetailModel = userSelectedSingleModel;
            cellWithRowOne.transferDetailItemsModel = tempListModel;
            __weak typeof(self)weakSelf = self;
            [cellWithRowOne actionClickWithSelectedModel:^(TSHomeCourseDetailClassModel *singleModel) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf chooseItemsManager:singleModel];
            }];

            return cellWithRowOne;
        } else {
            static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
            GWNormalTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
            if (!cellWithRowTwo){
                cellWithRowTwo = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
                cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowTwo.transferCellHeight = cellHeight;
            TSHomeCourseDetailClassModel *singleModel = [[self.classRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            cellWithRowTwo.transferTitle = [NSString stringWithFormat:@"%li、%@",(long)indexPath.row,singleModel.name];
            cellWithRowTwo.transferDesc = [NSString stringWithFormat:@"时长:%li分",(long)singleModel.timelong];
            cellWithRowTwo.titleLabel.font = [UIFont systemFontOfCustomeSize:14.];
            cellWithRowTwo.dymicLabel.font = [UIFont systemFontOfCustomeSize:10.];
            
            if (singleModel == userSelectedSingleModel){
                cellWithRowTwo.titleLabel.textColor = [UIColor colorWithCustomerName:@"红"];
            } else {
                cellWithRowTwo.titleLabel.textColor = [UIColor colorWithCustomerName:@"黑"];
            }
            
            return cellWithRowTwo;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"课件" sourceArr:self.classRootMutableArr]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
            TSHomeCourseKejianFixedTableViewCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
            if (!cellWithRowThr){
                cellWithRowThr = [[TSHomeCourseKejianFixedTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
                cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowThr.transferCellHeight = cellHeight;
            cellWithRowThr.transferCourseDetailModel = userSelectedSingleModel;
            return cellWithRowThr;
        } else {
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            TSHomeCourseKejianDymicTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[TSHomeCourseKejianDymicTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFour.transferSingleModel = userSelectedSingleModel;
            __weak typeof(self)weakSelf = self;
            [cellWithRowFour actionClickWithShowImgsManagerBlock:^(NSString *imgUrl) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                // 显示大图片
                ImgSelectorViewController *imgSelected = [[ImgSelectorViewController alloc]init];
                [imgSelected showInViewWithCell:strongSelf.parentViewController imgArr:strongSelf->userSelectedSingleModel.datums  currentIndex:cellWithRowFour.currentRow cell:cellWithRowFour];
            }];
            return cellWithRowFour;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"自测fixed" sourceArr:self.classRootMutableArr]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
            GWNormalTableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
            if (!cellWithRowSix){
                cellWithRowSix = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
                cellWithRowSix.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSix.transferCellHeight = cellHeight;
            cellWithRowSix.titleLabel.font = [[UIFont systemFontOfCustomeSize:17.] boldFont];
            cellWithRowSix.transferTitle = @"本节自测";
            
            return cellWithRowSix;
        } else {
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"题目" sourceArr:self.classRootMutableArr]){
                static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
                TSHomeClassDetailChooseQuestionTableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
                if (!cellWithRowFiv){
                    cellWithRowFiv = [[TSHomeClassDetailChooseQuestionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
                    cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                if (userSelectedSingleModel.examitems.count){
                    cellWithRowFiv.transferQuestionModel = [userSelectedSingleModel.examitems objectAtIndex:currentQuestionIndex];
                }
                
                return cellWithRowFiv;
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"提交" sourceArr:self.classRootMutableArr]){
                static NSString *cellIdentifyWithRowFiv2 = @"cellIdentifyWithRowFiv2";
                TSHomeClassDetailChooseQuestionNextTableViewCell *cellWithRowFiv2 = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv2];
                if (!cellWithRowFiv2){
                    cellWithRowFiv2 = [[TSHomeClassDetailChooseQuestionNextTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv2];
                    cellWithRowFiv2.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                if (currentQuestionIndex == 0){
                    [cellWithRowFiv2 firstQuestionManager];
                } else if (currentQuestionIndex == userSelectedSingleModel.examitems.count - 1){
                    [cellWithRowFiv2 lastQuestionManager];
                } else {
                    [cellWithRowFiv2 normalQuestionManager];
                }

                __weak typeof(self)weakSelf = self;
                [cellWithRowFiv2 actionButtonClickWithLast:^{               // 上一题
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    
                    // 第一步 获取到当前的问题Model
                    // 1. 获取当前的是否单选
                    if (strongSelf->currentQuestionIndex >= strongSelf->userSelectedSingleModel.examitems.count){
                        strongSelf->currentQuestionIndex = strongSelf->userSelectedSingleModel.examitems.count - 1;
                    }
                    
                    TSHomeCourseDetailExamitemsSingleModel *questionModel = [userSelectedSingleModel.examitems objectAtIndex:strongSelf->currentQuestionIndex];
                    questionModel.userChoosedArr = [strongSelf.questionChooseMutableArr copy];
                    // 第二步，当前的数组清除
                    [strongSelf.questionChooseMutableArr removeAllObjects];
                    // 第三步，题号减
                    strongSelf->currentQuestionIndex --;
                    // 第四步
                    [strongSelf.questionMutableArr removeAllObjects];
                    [strongSelf.questionMutableArr addObject:@"自测fixed"];
                    [strongSelf.questionMutableArr addObject:@"题目"];
                    TSHomeCourseDetailExamitemsSingleModel *examitemsModel = [userSelectedSingleModel.examitems objectAtIndex:currentQuestionIndex];
                    [strongSelf.questionMutableArr addObjectsFromArray:examitemsModel.answers];
                    [strongSelf.questionMutableArr addObject:@"提交"];
                    
                    // 第五步
                    [strongSelf.questionChooseMutableArr addObjectsFromArray:examitemsModel.userChoosedArr];
                    
                    [strongSelf.classTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationRight];

                } nextBlock:^{                                              // 下一题&&提交
                    if (!weakSelf){
                        return ;
                    }
                    __strong typeof(weakSelf)strongSelf = weakSelf;
                    if (!strongSelf.questionChooseMutableArr.count){
                        [StatusBarManager statusHiddenWithString:@"您当前题目没有选择哦"];
                        return;
                    }
                    
                    if (strongSelf->userSelectedSingleModel.examitems.count){
                        TSHomeCourseDetailExamitemsSingleModel *questionModel = [userSelectedSingleModel.examitems objectAtIndex:strongSelf->currentQuestionIndex];
                        questionModel.userChoosedArr = [self.questionChooseMutableArr copy];
                        [self.questionChooseMutableArr removeAllObjects];
                    }
                    
                    // 进行切换到下一个
                    currentQuestionIndex++;
                    // 提交
                    if (strongSelf->currentQuestionIndex == strongSelf->userSelectedSingleModel.examitems.count){           // 最后一题
                        [[UIAlertView alertViewWithTitle:@"提示" message:@"题目已全部回答完毕，是否提交结果" buttonTitles:@[@"确定",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                            if (!weakSelf){
                                return ;
                            }
                            __strong typeof(weakSelf)strongSelf = weakSelf;
                            if(buttonIndex == 0){
                                [TSMTAManager MTAevent:MTAType_Class_ZCTJ str:strongSelf.transferCourseId];                 // 埋点
                                [strongSelf uploadQuestionAnswerManager];
                            }
                        }]show];
                        return;
                    }
                    
                    // 获取到下一题的内容
                    [strongSelf.questionMutableArr removeAllObjects];
                    [strongSelf.questionMutableArr addObject:@"自测fixed"];
                    [strongSelf.questionMutableArr addObject:@"题目"];
                    TSHomeCourseDetailExamitemsSingleModel *examitemsModel = [userSelectedSingleModel.examitems objectAtIndex:currentQuestionIndex];
                    [strongSelf.questionMutableArr addObjectsFromArray:examitemsModel.answers];
                    [strongSelf.questionMutableArr addObject:@"提交"];
                    [strongSelf.classTableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationLeft];
                }];
                return cellWithRowFiv2;
            } else {
                static NSString *cellIdentifyWithRowFiv1 = @"cellIdentifyWithRowFiv1";
                TSHomeClassDetailChooseQuestionNumTableViewCell *cellWithRowFiv1 = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv1];
                if (!cellWithRowFiv1){
                    cellWithRowFiv1 = [[TSHomeClassDetailChooseQuestionNumTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv1];
                    cellWithRowFiv1.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                cellWithRowFiv1.transferCellHeight = cellHeight;
                if (userSelectedSingleModel.examitems.count){
                    TSHomeCourseDetailExamitemsSingleModel *questionModel = [userSelectedSingleModel.examitems objectAtIndex:currentQuestionIndex];
                    NSInteger index = indexPath.row - 2;
                    cellWithRowFiv1.userSelectedArr = self.questionChooseMutableArr;
                    cellWithRowFiv1.transferQuestionChoose = [questionModel.answers objectAtIndex:index];
                }
                
                return cellWithRowFiv1;
            }
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"错题解析" sourceArr:self.classRootMutableArr]){
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowSix = @"cellIdentifyWithRowSix";
            GWNormalTableViewCell *cellWithRowSix = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowSix];
            if (!cellWithRowSix){
                cellWithRowSix = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowSix];
                cellWithRowSix.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowSix.transferCellHeight = cellHeight;
            cellWithRowSix.titleLabel.font = [[UIFont systemFontOfCustomeSize:17.] boldFont];
            cellWithRowSix.transferTitle = @"本节自测";
            
            return cellWithRowSix;
        } else {
            static NSString *cellIdentifyWithRowEig = @"cellIdentifyWithRowEig";
            TSCuotijiexiTableViewCell *cellWithRowEig = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowEig];
            if (!cellWithRowEig){
                cellWithRowEig = [[TSCuotijiexiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowEig];
                cellWithRowEig.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowEig.transferCellHeight = cellHeight;
            cellWithRowEig.backgroundColor = UURandomColor;
            return cellWithRowEig;
        }
        
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"结算分数" sourceArr:self.classRootMutableArr]){
        static NSString *cellIdentifyWithRowNie = @"cellIdentifyWithRowNie";
        TSCuotijiexiTableViewCell *cellWithRowNie = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowNie];
        if (!cellWithRowNie){
            cellWithRowNie = [[TSCuotijiexiTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowNie];
            cellWithRowNie.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowNie.transferCellHeight = cellHeight;
        [cellWithRowNie showAnimationWithScore:(long)userSelectedSingleModel.score];
        __weak typeof(self)weakSelf = self;
        [cellWithRowNie actionClickToGetCuotijiexiBlock:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            TSHomeClassDetailQuestionRootViewController *homeClassDetailQuestionViewController = [[TSHomeClassDetailQuestionRootViewController alloc]init];
            homeClassDetailQuestionViewController.transferDetailid = strongSelf->userSelectedSingleModel.detailid;
            [strongSelf.navigationController pushViewController:homeClassDetailQuestionViewController animated:YES];
            
            [TSMTAManager MTAevent:MTAType_Class_ZC_Result str:strongSelf.transferCourseId];                 // 埋点
        }];
        return cellWithRowNie;
    } else {
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        UITableViewCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return cellWithRowFiv;
    }
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == [self cellIndexPathSectionWithcellData:@"课程列表" sourceArr:self.classRootMutableArr]){
        if (indexPath.row == 0){
            return [TSHomeClassTopItemsTableViewCell calculationCellHeightWithModel:tempListModel];
        } else {
            return 44;
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"课件" sourceArr:self.classRootMutableArr]){
        if (indexPath.row == 0){
            return [TSHomeCourseKejianFixedTableViewCell calculationCellHeight];
        } else {
            return [TSHomeCourseKejianDymicTableViewCell calculationCellHeight];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"自测fixed" sourceArr:self.classRootMutableArr]){
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            if (indexPath.row == [self cellIndexPathRowWithcellData:@"题目" sourceArr:self.classRootMutableArr]){
                TSHomeCourseDetailExamitemsSingleModel *questionModel = [userSelectedSingleModel.examitems objectAtIndex:currentQuestionIndex];
                return [TSHomeClassDetailChooseQuestionTableViewCell calculationCellHeightWithHeight:questionModel];
            } else if (indexPath.row == [self cellIndexPathRowWithcellData:@"提交" sourceArr:self.classRootMutableArr]){
                return [TSHomeClassDetailChooseQuestionNextTableViewCell calculationCellHeight];
            } else {
                TSHomeCourseDetailExamitemsSingleModel *questionModel = [userSelectedSingleModel.examitems objectAtIndex:currentQuestionIndex];
                NSInteger index = indexPath.row - 2;
                NSString *chooseInfo = [questionModel.answers objectAtIndex:index];
                return [TSHomeClassDetailChooseQuestionNumTableViewCell calculationCellHeightWithQuestion:chooseInfo];
            }
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"结算分数" sourceArr:self.classRootMutableArr]){
        return [TSCuotijiexiTableViewCell calculationCellHeight];
    } else {
        return 44;
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    if (section == [self cellIndexPathSectionWithcellData:@"课件" sourceArr:self.classRootMutableArr]){
        return TSFloat(6);
    } else {
        return 0;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if(indexPath.section == [self cellIndexPathSectionWithcellData:@"课件fixed" sourceArr:self.classRootMutableArr] && indexPath.row == [self cellIndexPathRowWithcellData:@"课件fixed" sourceArr:self.classRootMutableArr]){
        TSHomeClassDetailAllInfoViewController *classDetailInfoVC = [[TSHomeClassDetailAllInfoViewController alloc]init];
        classDetailInfoVC.transferCourseDetailClassModel = [tempListModel.items firstObject];
        [self.navigationController pushViewController:classDetailInfoVC animated:YES];
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"课程列表" sourceArr:self.classRootMutableArr]){
        if (indexPath.row != 0){
            TSHomeCourseDetailClassModel *singleModel = [[self.classRootMutableArr objectAtIndex:indexPath.section] objectAtIndex:indexPath.row];
            [self chooseItemsManager:singleModel];
        }
    } else if (indexPath.section == [self cellIndexPathSectionWithcellData:@"自测fixed" sourceArr:self.classRootMutableArr]){
        if (indexPath.row != [self cellIndexPathRowWithcellData:@"题目" sourceArr:self.classRootMutableArr] && indexPath.row != [self cellIndexPathRowWithcellData:@"提交" sourceArr:self.classRootMutableArr]){

            // 1. 获取当前的是否单选
            if (currentQuestionIndex >= userSelectedSingleModel.examitems.count){
                currentQuestionIndex = userSelectedSingleModel.examitems.count - 1;
            }
            TSHomeCourseDetailExamitemsSingleModel *questionModel = [userSelectedSingleModel.examitems objectAtIndex:currentQuestionIndex];
            NSInteger index = indexPath.row - 2;
            NSString *chooseInfo = [questionModel.answers objectAtIndex:index];
            if (!questionModel.ismul){           // 如果是单选
                if (self.questionChooseMutableArr.count){
                    [self.questionChooseMutableArr removeAllObjects];
                }
                [self.questionChooseMutableArr addObject:chooseInfo];
                // 2. 刷新
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:indexPath.section];
                [tableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationNone];
            } else {                            // 多选
                if (![self.questionChooseMutableArr containsObject:chooseInfo]){
                    [self.questionChooseMutableArr addObject:chooseInfo];
                } else {
                    [self.questionChooseMutableArr removeObject:chooseInfo];
                }
                // 2. 刷新
                [tableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
            }
        }
    }
}

#pragma mark - Interface
-(void)sendRequestToGetHomeClassInfo{
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] homeCourseDetailWithId:self.transferCourseId block:^(TSHomeCourseDetailListClassModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->tempListModel = listModel;
        if (listModel.items.count){
            [strongSelf.classMutableArr addObject:@"课程列表"];
            [strongSelf.classMutableArr addObjectsFromArray:listModel.items];
            [strongSelf.classRootMutableArr addObject:strongSelf.classMutableArr];
            if (!strongSelf.userLastChoosedMutableArr.count){
                [strongSelf.userLastChoosedMutableArr addObject:[listModel.items firstObject]];
                strongSelf-> userSelectedSingleModel = [listModel.items firstObject];
                [strongSelf.userLastChoosedMutableArr addObject:[listModel.items firstObject]];
            }
            [strongSelf chooseItemsManager:[listModel.items firstObject]];
        }
        
        [strongSelf.classTableView reloadData];
    }];
}



#pragma mark - 选择数组内容
-(void)chooseItemsManager:(TSHomeCourseDetailClassModel *)model{
    if (self.hasQihangban){         // 表示是其航班
        // 获取当前model 前面的所有的model
        NSInteger currentChooseIndex = [tempListModel.items indexOfObject:model];
        NSInteger targetIndex = -1;
        for (int i = 0 ; i < tempListModel.items.count;i++){
            TSHomeCourseDetailClassModel *singleModel = [tempListModel.items objectAtIndex:i];
            if (!singleModel.iswatch){
                targetIndex = i;
                break;
            }
        }
        
        // 获取当前在看的内容
        if (targetIndex != -1){
            if (currentChooseIndex < targetIndex + 1){          // 选中的items 小于目标items
                TSHomeCourseDetailClassModel *lastModel = [self.userLastChoosedMutableArr lastObject];
                if (![lastModel.detailid isEqualToString:model.detailid]){
                    if (!lastModel.iswatch){
                        if (!model.iswatch){
                            [StatusBarManager statusHiddenWithString:[NSString stringWithFormat:@"当前课程为起航班课程,必须看完第%li节课后解锁该课程",(long)targetIndex + 1]];
                            return;
                        }
                    }
                }
            } else {
                [StatusBarManager statusHiddenWithString:[NSString stringWithFormat:@"当前课程为起航班课程,必须看完第%li节课后解锁该课程",(long)targetIndex + 1]];
                return;
            }
        }
    }
    currentQuestionIndex = 0;
    
    void(^block)(TSHomeCourseDetailClassModel *model) = objc_getAssociatedObject(self, &actionClickWithShowChangeModelBlockKey);
    if (block){
        block(model);
    }
    // 1. 判断是否上一个model
    if (self.userLastChoosedMutableArr.count){
        [self.userLastChoosedMutableArr removeAllObjects];
        userSelectedSingleModel = model;
        [self.userLastChoosedMutableArr addObject:model];
        
        // 2. 修改课件 方法
        [self coursewareManager];
        // 3. 获取题目列表
        [self actionGetQuestionNumber];
        //4.
        [self questionInfoChooseManagerWithReload:NO selectedStr:@""];
        
    } else {
        [self.userLastChoosedMutableArr addObject:model];
        
        // 2.1 获取当前的indexpath
        NSInteger currentIndex = [self.classMutableArr indexOfObject:model];
        NSIndexPath *currentPath = [NSIndexPath indexPathForRow:currentIndex inSection:[self cellIndexPathSectionWithcellData:@"课程列表" sourceArr:self.classRootMutableArr]];
        
        userSelectedSingleModel = model;
        [self.userLastChoosedMutableArr addObject:model];
        [self.classTableView reloadRowsAtIndexPaths:@[currentPath] withRowAnimation:UITableViewRowAnimationNone];
    }
    
    // 7. 进行刷新
    [self.classTableView reloadData];
}

#pragma mark 课件方法
-(void)coursewareManager{
    // 5.1 判断是否有课件
    if (userSelectedSingleModel.datums.count){              // 如果有课件,需要加入
        if (![self.classRootMutableArr containsObject:self.coursewareArr]){
            NSInteger insertIndex = [self cellIndexPathSectionWithcellData:@"课程列表" sourceArr:self.classRootMutableArr];
            [self.classRootMutableArr insertObject:self.coursewareArr atIndex:insertIndex + 1];
        }
    } else {
        if ([self.classRootMutableArr containsObject:self.coursewareArr]){
            [self.classRootMutableArr removeObject:self.coursewareArr];
        }
    }
}

#pragma mark 题目方法
-(void)questionInfoChooseManagerWithReload:(BOOL)reload selectedStr:(NSString *)str{
    [self.questionMutableArr removeAllObjects];
    if (str.length){
        // 1. 记录当前的model
        TSHomeCourseDetailExamitemsSingleModel *examitemsModel = [userSelectedSingleModel.examitems objectAtIndex:currentQuestionIndex];
        examitemsModel.userChoose = str;
    }
    
    if (userSelectedSingleModel.examitems.count){
        [self.questionMutableArr addObject:@"自测fixed"];
        [self.questionMutableArr addObject:@"题目"];
        // 1. 刷新题目
        if(str.length){
            currentQuestionIndex ++;
        }
        if (userSelectedSingleModel.examitems.count <= currentQuestionIndex){
            currentQuestionIndex = userSelectedSingleModel.examitems.count - 1;
            __weak typeof(self)weakSelf = self;
            [[UIAlertView alertViewWithTitle:@"提示" message:@"题目已全部回答完毕，是否提交结果" buttonTitles:@[@"确定",@"取消"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                [strongSelf uploadQuestionAnswerManager];
            }] show];
            return;
        }
        TSHomeCourseDetailExamitemsSingleModel *examitemsModel = [userSelectedSingleModel.examitems objectAtIndex:currentQuestionIndex];
        [self.questionMutableArr addObjectsFromArray:examitemsModel.answers];
        [self.questionMutableArr addObject:@"提交"];
        
        if (reload || userSelectedSingleModel.iswatch){
            if (![self.classRootMutableArr containsObject:self.questionMutableArr]){
                [self.classRootMutableArr addObject:self.questionMutableArr];
                [self.classTableView reloadData];
            } else {
                NSInteger index = [self.classRootMutableArr indexOfObject:self.questionMutableArr];
                NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
                [self.classTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
            }
        }
        return;
    } else {
        if ([self.classRootMutableArr containsObject:self.questionMutableArr]){
            [self.classRootMutableArr removeObject:self.questionMutableArr];
        }
        
        if (userSelectedSingleModel.istest){            // 表示已经做了题目了
            if (![self.classRootMutableArr containsObject:@[@"结算分数"]]){
                [self.classRootMutableArr addObject:@[@"结算分数"]];
            }
        } else {
            if ([self.classRootMutableArr containsObject:@[@"结算分数"]]){
                [self.classRootMutableArr removeObject:@[@"结算分数"]];
            }
        }
    }
    
    if (reload){
        [self.classTableView reloadData];
//        NSInteger index = [self cellIndexPathSectionWithcellData:@"自测fixed" sourceArr:self.classRootMutableArr];
//        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndex:index];
//        [self.classTableView reloadSections:indexSet withRowAnimation:UITableViewRowAnimationLeft];
    }
}

#pragma mark 获取问题编号
-(void)actionGetQuestionNumber{
    for (int i = 0 ; i < userSelectedSingleModel.examitems.count;i++){
        TSHomeCourseDetailExamitemsSingleModel *examitemsModel = [userSelectedSingleModel.examitems objectAtIndex:i];
        examitemsModel.questionNumber = i + 1;
    }
}

-(void)actionClickWithShowChangeModelBlock:(void(^)(TSHomeCourseDetailClassModel *model))block{
    objc_setAssociatedObject(self, &actionClickWithShowChangeModelBlockKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}



#pragma mark - 提交问题信息
-(void)uploadQuestionAnswerManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] sendRequestToQuestionManagerWithDetailId:userSelectedSingleModel.detailid questionArr:userSelectedSingleModel.examitems block:^(NSInteger score) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        TSHomeClassDetailQuestionRootViewController *homeClassDetailQuestionViewController = [[TSHomeClassDetailQuestionRootViewController alloc]init];
        homeClassDetailQuestionViewController.transferDetailid = strongSelf->userSelectedSingleModel.detailid;
        [strongSelf.navigationController pushViewController:homeClassDetailQuestionViewController animated:YES];
    }];
}

-(void)playerEndWithModel:(TSHomeCourseDetailClassModel *)detailClassModel{
    if ([self.classMutableArr lastObject] == detailClassModel){
        // 4. 修改题目
        detailClassModel.iswatch = YES;
        [StatusBarManager statusHiddenWithString:@"当前起航班已查看完毕，已开启测评。"];
        [self questionInfoChooseManagerWithReload:YES selectedStr:@""];
        return;
    }
    for (int i = 0 ; i < tempListModel.items.count;i++){
        TSHomeCourseDetailClassModel *tempModel = [tempListModel.items objectAtIndex:i];
        if ([tempModel.detailid isEqualToString:detailClassModel.detailid]){
            tempModel.iswatch = YES;
        }
    }
    detailClassModel.iswatch = YES;
}

@end
