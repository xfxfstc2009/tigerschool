//
//  TSHomeClassDetailQuestionRootViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/5.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailQuestionRootViewController.h"
#import "TSHomeClassDetailQuestionRootTableViewCell.h"
#import "TSHomeClassDetailChooseQuestionTableViewCell.h"

@interface TSHomeClassDetailQuestionRootViewController ()<UITableViewDelegate,UITableViewDataSource>

@property (nonatomic,strong)UITableView *questionRootTableView;
@property (nonatomic,strong)UIView *topView;
@property (nonatomic,strong)UIView *leftView;
@property (nonatomic,strong)UIView *rightView;
@property (nonatomic,strong)NSMutableArray *dataSourceMutableArr;

@end

@implementation TSHomeClassDetailQuestionRootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTopView];
    
    [self createTableView];
    if (!self.transferExamitems){
        [self sendRequestToGetZicejiexi];
    }
}

-(void)pageSetting{
    self.barMainTitle = @"错题解析";
}

-(void)arrayWithInit{
    self.dataSourceMutableArr = [NSMutableArray array];
    if (self.transferExamitems.count){
        [self.dataSourceMutableArr addObjectsFromArray:self.transferExamitems];
    }
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.questionRootTableView){
        self.questionRootTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.questionRootTableView.dataSource = self;
        self.questionRootTableView.delegate = self;
        self.questionRootTableView.orgin_y = CGRectGetMaxY(self.topView.frame);
        self.questionRootTableView.size_height -= CGRectGetMaxY(self.topView.frame);
        [self.view addSubview:self.questionRootTableView];
    }
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.dataSourceMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    TSHomeCourseDetailExamitemsSingleModel *singleModel = [self.dataSourceMutableArr objectAtIndex:section];
    return singleModel.answers.count + 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TSHomeCourseDetailExamitemsSingleModel *singleModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.row == 0){            // 【题目标题】
        static NSString *cellIdentifyWithRowZero = @"cellIdentifyWithRowZero";
        TSHomeClassDetailChooseQuestionTableViewCell *cellWithRowZero = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowZero];
        if (!cellWithRowZero){
            cellWithRowZero = [[TSHomeClassDetailChooseQuestionTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowZero];
        }
        cellWithRowZero.transferCellHeight = cellHeight;
        if (singleModel.questionNumber == 0){
            singleModel.questionNumber = indexPath.row + 1;
        }
        cellWithRowZero.transferQuestionModel = singleModel;

        return cellWithRowZero;
    } else if (indexPath.row == singleModel.answers.count + 2 - 1){         // 答案
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        TSHomeClassDetailQuestionRootEndTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[TSHomeClassDetailQuestionRootEndTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
            cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowTwo.transferEndInfo = singleModel.answer;
        return cellWithRowTwo;
    } else {
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        TSHomeClassDetailQuestionRootTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){                                               // 
            cellWithRowOne = [[TSHomeClassDetailQuestionRootTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        NSInteger index = indexPath.row - 1;

        cellWithRowOne.transferIndex = indexPath.row;
        cellWithRowOne.transferChooseInfo = [singleModel.answers objectAtIndex:index];             // 提交题目
        cellWithRowOne.transferSingleModel = singleModel;                                          // 提交用户信息
        
        return cellWithRowOne;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    TSHomeCourseDetailExamitemsSingleModel *singleModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];

    if (indexPath.row == 0){
        return [TSHomeClassDetailChooseQuestionTableViewCell calculationCellHeightWithHeight:singleModel];
    } else if (indexPath.row == singleModel.answers.count + 2 - 1){
        return [TSHomeClassDetailQuestionRootEndTableViewCell calculationCellHeight];
    } else {
        TSHomeCourseDetailExamitemsSingleModel *singleModel = [self.dataSourceMutableArr objectAtIndex:indexPath.section];
        NSInteger index = indexPath.row - 1;
        return [TSHomeClassDetailQuestionRootTableViewCell calculationCellHeightWithStr:[singleModel.answers objectAtIndex:index]];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return TSFloat(10);
}

#pragma mark - topView
-(void)createTopView{
    self.topView = [[UIView alloc]init];
    self.topView.backgroundColor = [UIColor hexChangeFloat:@"FFF8F3"];
    self.topView.frame = CGRectMake(0, 0, kScreenBounds.size.width, TSFloat(36));
    [self.view addSubview:self.topView];
    
    // 计算
    NSInteger success = 0;
    NSInteger fail = 0;
    for (int i = 0 ; i < self.dataSourceMutableArr.count;i++){
        TSHomeCourseDetailExamitemsSingleModel *singleModel = [self.dataSourceMutableArr objectAtIndex:i];
        NSArray *successAnsArr = [singleModel.answer componentsSeparatedByString:@","];
        
        for (int j = 0 ; j < singleModel.userChoosedArr.count;j++){
            NSString *info = [singleModel.userChoosedArr objectAtIndex:j];
            NSInteger index = [singleModel.answers indexOfObject:info] + 1;     // 分别获取用户选择的答案
            if (successAnsArr.count == singleModel.userChoosedArr.count){
                if (![successAnsArr containsObject:[NSString stringWithFormat:@"%li",(long)index]]){
                    fail++;
                    break;
                }
            }
        }
    }
    
    success = self.dataSourceMutableArr.count - fail;
    
    if (!self.leftView){
        self.leftView = [self createTitle:YES dymic:[NSString stringWithFormat:@"%li",(long)success]];
        self.leftView.frame = CGRectMake(0, 0, self.topView.size_width /2., self.topView.size_height);
        [self.topView addSubview:self.leftView];
    }
    
    if (!self.rightView){
        self.rightView = [self createTitle:NO dymic:[NSString stringWithFormat:@"%li",(long)fail]];
        self.rightView.frame = CGRectMake(kScreenBounds.size.width / 2., 0, kScreenBounds.size.width / 2., self.topView.size_height);
        [self.topView addSubview:self.rightView];
    }
}

-(UIView *)createTitle:(BOOL)check dymic:(NSString *)dymic{
    UIView *view = [[UIView alloc]init];
    view.frame = CGRectMake(0, 0, kScreenBounds.size.width / 2., self.topView.size_height);
    
    
    UILabel *titleLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    titleLabel.font = [titleLabel.font boldFont];
    if (check){
        titleLabel.text = @"答对";
    } else {
        titleLabel.text = @"答错";
    }
    
    [view addSubview:titleLabel];
    CGSize titleSize = [TSTool makeSizeWithLabel:titleLabel];
    titleLabel.frame = CGRectMake(TSFloat(18), 0, titleSize.width, self.topView.size_height);
    
    // 多少题
    UILabel *successDymicLabel = [TSViewTool createLabelFont:@"正文" textColor:@"浅灰"];
    successDymicLabel.backgroundColor = [UIColor clearColor];
    successDymicLabel.text = dymic;
    successDymicLabel.stringTag = @"successDymicLabel";
    if (check){
        successDymicLabel.textColor = [UIColor colorWithCustomerName:@"绿"];
    } else {
        successDymicLabel.textColor = [UIColor colorWithCustomerName:@"红"];
    }
    [view addSubview:successDymicLabel];
    
    // 题固定字
    UILabel *tiFixedLabel = [TSViewTool createLabelFont:@"小正文" textColor:@"灰"];
    tiFixedLabel.textColor = [UIColor hexChangeFloat:@"999999"];
    tiFixedLabel.text = @"题";
    [view addSubview:tiFixedLabel];
    
    CGSize tiDymicSize = [@"100" sizeWithCalcFont:successDymicLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:successDymicLabel.font])];
    successDymicLabel.frame = CGRectMake(CGRectGetMaxX(titleLabel.frame) + TSFloat(5), 0, tiDymicSize.width, [NSString contentofHeightWithFont:successDymicLabel.font]);
    successDymicLabel.center_y = titleLabel.center_y;
    
    // 题固定字
    CGSize tiFixedSize = [tiFixedLabel.text sizeWithCalcFont:tiFixedLabel.font constrainedToSize:CGSizeMake(CGFLOAT_MAX, [NSString contentofHeightWithFont:tiFixedLabel.font])];
    tiFixedLabel.frame = CGRectMake(CGRectGetMaxX(successDymicLabel.frame) + TSFloat(5), 0, tiFixedSize.width, [NSString contentofHeightWithFont:tiFixedLabel.font]);
    tiFixedLabel.center_y = titleLabel.center_y;
    return view;
}

-(void)sendRequestToGetZicejiexi{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"detailid":self.transferDetailid};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:@"examdetail" requestParams:params responseObjectClass:[TSHomeCourseDetailExamitemsMainModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        TSHomeCourseDetailExamitemsMainModel *itemsModel = (TSHomeCourseDetailExamitemsMainModel *)responseObject;
        [strongSelf.dataSourceMutableArr addObjectsFromArray:itemsModel.items];
        
        [strongSelf.questionRootTableView reloadData];
        
        TSHomeCourseDetailExamitemsSingleModel *singleModel = [itemsModel.items lastObject];
       
        UILabel *successLabel = (UILabel *)[strongSelf.leftView viewWithStringTag:@"successDymicLabel"];
        successLabel.text = [NSString stringWithFormat:@"%li",(long)singleModel.right];

        UILabel *wrongLabel = (UILabel *)[strongSelf.rightView viewWithStringTag:@"successDymicLabel"];
        wrongLabel.text = [NSString stringWithFormat:@"%li",(long)singleModel.wrong];
        
    }];
}

@end
