//
//  TSHomeClassDetailAllInfoViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"
#import "TSHomeCourseDetailClassModel.h"

@interface TSHomeClassDetailAllInfoViewController : AbstractViewController

@property (nonatomic,strong)TSHomeCourseDetailClassModel *transferCourseDetailClassModel;

@end
