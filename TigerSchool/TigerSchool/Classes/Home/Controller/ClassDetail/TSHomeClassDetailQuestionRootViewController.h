//
//  TSHomeClassDetailQuestionRootViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/5.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"
#import "TSHomeCourseDetailClassModel.h"

@interface TSHomeClassDetailQuestionRootViewController : AbstractViewController

@property (nonatomic,strong)NSArray<TSHomeCourseDetailExamitemsSingleModel> *transferExamitems;         /**< 传递过来的数组内容*/

@property (nonatomic,copy)NSString *transferDetailid;

@end
