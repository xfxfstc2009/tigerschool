//
//  ImgSelectorViewController.m
//  17Live
//
//  Created by 裴烨烽 on 2017/12/11.
//  Copyright © 2017年 PPWhale. All rights reserved.
//

#import "ImgSelectorViewController.h"

@interface ImgSelectorViewController ()<UIScrollViewDelegate>
@property (nonatomic,weak)UIViewController *showViewController;                 /**< 显示的控制器*/
@property (nonatomic,strong)UIView *backView;                                   /**< 背景View*/
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)TSImageView *userAnimationImgView;;
@property (nonatomic,strong)NSArray *tempImgArr;
@property (nonatomic,assign)NSInteger tempCurrentIndex;
@property (nonatomic,strong)UIButton *downButton;
@property (nonatomic,strong)TSImageView *tempConvertView;
@property (nonatomic,strong)TSHomeClassDetailAllInfoViewCell *tempCell;
@property (nonatomic,strong)TSHomeCourseKejianDymicTableViewCell *kejianTempCell;
@property (nonatomic,strong)UIPageControl *pageControl;
@property (nonatomic,strong)UILabel *pageLabel;
@end

@implementation ImgSelectorViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor clearColor];
    [self pageSetting];
}

-(void)dealloc{
    NSLog(@"123");
}

-(void)pageSetting{
    if (!self.backView){
        self.backView = [[UIView alloc]init];
        self.backView.backgroundColor = [UIColor blackColor];
        self.backView.frame = self.view.bounds;
        self.backView.alpha = 1;
        self.backView.userInteractionEnabled = YES;
        [self.view addSubview:self.backView];
        // 2. 创建手势
        UITapGestureRecognizer *tapGestureRecognizer = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(sheetViewDismiss)];
        tapGestureRecognizer.numberOfTapsRequired = 1;
        [self.backView addGestureRecognizer:tapGestureRecognizer];
    }

    // 2. 创建scrollview
    if (!self.mainScrollView){
        self.mainScrollView = [[UIScrollView alloc]init];
        self.mainScrollView.backgroundColor = [UIColor blackColor];
        self.mainScrollView.showsHorizontalScrollIndicator = NO;
        self.mainScrollView.showsVerticalScrollIndicator = NO;
        self.mainScrollView.layer.zPosition = MAXFLOAT;
        self.mainScrollView.delegate = self;
        self.mainScrollView.userInteractionEnabled = YES;
        self.mainScrollView.maximumZoomScale=2.0;
        self.mainScrollView.minimumZoomScale=0.5;
        self.mainScrollView.bounces = YES;
        self.mainScrollView.pagingEnabled = YES;
        self.mainScrollView.bouncesZoom = YES;
        self.mainScrollView.frame = kScreenBounds;
        [self.backView addSubview:self.mainScrollView];
    }
    // 2. 创建pagecontrol
    if (!self.pageControl){
        self.pageControl = [[UIPageControl alloc] initWithFrame:CGRectMake(0, kScreenBounds.size.height - TSFloat(11) * 3,kScreenBounds.size.width,TSFloat(11))];
        self.pageControl.pageIndicatorTintColor = [UIColor lightGrayColor];
        self.pageControl.currentPageIndicatorTintColor = [UIColor whiteColor];
        [self.backView addSubview:self.pageControl];
    }
    
    if (!self.pageLabel){
        self.pageLabel = [TSViewTool createLabelFont:@"小正文" textColor:@"白"];
        self.pageLabel.frame = CGRectMake(0, kScreenBounds.size.height - TSFloat(11) - [NSString contentofHeightWithFont:self.pageLabel.font] - 40, kScreenBounds.size.width, [NSString contentofHeightWithFont:self.pageLabel.font]);
        if (IS_iPhoneX){
            self.pageLabel.orgin_y -= 34;
        }
        self.pageLabel.textAlignment = NSTextAlignmentCenter;
        [self.view addSubview:self.pageLabel];
    }
}

// 隐藏view
- (void)dismissFromView:(UIViewController *)viewController{
    __weak ImgSelectorViewController *weakVC = self;

    self.backView.alpha = 0;
    self.mainScrollView.alpha = 0;
    //背景色渐出
    [self backgroundColorFadeInOrOutFromValue:1.f toValue:0.f];
    // 1. 获取当前的index
    if (self.pageControl.currentPage + 1 > 2){
        [UIView animateWithDuration:.3f animations:^{

        } completion:^(BOOL finished) {
            if (!weakVC){
                return ;
            }
            __strong typeof(weakVC)strongSelf = weakVC;
            [strongSelf.userAnimationImgView removeFromSuperview];
            strongSelf.userAnimationImgView = nil;
            [strongSelf willMoveToParentViewController:nil];
            [strongSelf.view removeFromSuperview];
            [strongSelf removeFromParentViewController];
        }];
    } else {
        // 1. 找到当前的view
        TSImageView *targetImgView;
        if (self.pageControl.currentPage == 0){
            if (self.kejianTempCell){
                targetImgView = self.kejianTempCell.tempCell.imgView;
            } else if (self.tempCell) {
                targetImgView = self.tempCell.assetsImageView;
            }
        } else if (self.pageControl.currentPage == 1){
            if (self.kejianTempCell){
                targetImgView = self.kejianTempCell.tempCell.imgView;
            } else if (self.tempCell) {
                targetImgView = self.tempCell.assetsImageView;
            }
        } else if (self.pageControl.currentPage == 2){
            if (self.kejianTempCell){
                targetImgView = self.kejianTempCell.tempCell.imgView;
            } else if (self.tempCell) {
                targetImgView = self.tempCell.assetsImageView;
            }
        }
        // 1.
        self.userAnimationImgView.image = targetImgView.image;
        CGSize fromImgSize = [self convertToCalculationImg:self.userAnimationImgView.image];
        self.userAnimationImgView.frame = CGRectMake((kScreenBounds.size.width - fromImgSize.width) / 2., (kScreenBounds.size.height - fromImgSize.height) / 2., fromImgSize.width, fromImgSize.height);
        self.userAnimationImgView.alpha = 1;

        // 2. 寻找target frame
        CGRect targetRect;
        if (self.tempCell){
            targetRect = [self.tempCell convertRect:targetImgView.frame toView:self.view.window];
        } else if (self.kejianTempCell){
           targetRect = [self.kejianTempCell convertRect:targetImgView.frame toView:self.view.window];
        } else {
            targetRect = CGRectMake(0, 0, 0, 0);
        }
        
        __weak typeof(self)weakSelf = self;
        [UIView animateWithDuration:.3f animations:^{
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.userAnimationImgView.frame = targetRect;
        } completion:^(BOOL finished) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            [strongSelf.userAnimationImgView removeFromSuperview];
            strongSelf.userAnimationImgView = nil;
            [strongSelf willMoveToParentViewController:nil];
            [strongSelf.view removeFromSuperview];
            [strongSelf removeFromParentViewController];
        }];
    }
}

// 显示view
- (void)showInView:(UIViewController *)viewController{
    _showViewController = viewController;
    [viewController.view.window addSubview:self.view];
    [viewController addChildViewController:self];
    [self didMoveToParentViewController:viewController];

    // 1. 计算将要变成的frame
    CGSize maxSize = [self convertToCalculationImg:self.userAnimationImgView.image];
    __weak typeof(self)weakSelf = self;
    [UIView animateWithDuration:.3f animations:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.backView.alpha = 1;
        strongSelf.userAnimationImgView.frame = CGRectMake((kScreenBounds.size.width - maxSize.width) / 2., (kScreenBounds.size.height - maxSize.height ) / 2., maxSize.width, maxSize.height);
    } completion:^(BOOL finished) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf addOtherImgView];
        [strongSelf.view addSubview:self.pageLabel];
    }];
}

#pragma mark 背景色渐入效果
- (void)backgroundColorFadeInOrOutFromValue:(NSInteger)fromValue toValue:(NSInteger)toValue{
    CABasicAnimation *theAnimation = [CABasicAnimation animationWithKeyPath:@"opacity"];
    theAnimation.duration = 1.1;
    theAnimation.fromValue = [NSNumber numberWithFloat:fromValue];
    theAnimation.toValue = [NSNumber numberWithFloat:toValue];
    [self.backView.layer addAnimation:theAnimation forKey:@"anumateOpacity"];
}

-(void)sheetViewDismiss{
    [self dismissFromView:_showViewController];
}

-(void)showInView:(UIViewController *)viewController imgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentIndex cell:(TSHomeClassDetailAllInfoViewCell *)cell{
    // 1. 创建view
    [self pageSetting];
    // 2. img
    self.tempImgArr = imgArr;
    // 3.label
    self.pageLabel.text = [NSString stringWithFormat:@"%li/%li",(long)currentIndex + 1,(long)imgArr.count];
    // 4.frame
    CGRect convertImgFrame = [cell convertRect:cell.assetsImageView.frame toView:self.view.window];

    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.tempImgArr.count, kScreenBounds.size.height);
    [self.mainScrollView setContentOffset:CGPointMake(currentIndex * kScreenBounds.size.width, 0)];
    // 3. cell
    self.tempCell = cell;

    if (!self.userAnimationImgView){
        self.userAnimationImgView = [[TSImageView alloc]init];
        self.userAnimationImgView.backgroundColor = [UIColor clearColor];
        self.userAnimationImgView.frame = convertImgFrame;
        self.userAnimationImgView.userInteractionEnabled = NO;
        [self.view addSubview:self.userAnimationImgView];
        self.userAnimationImgView.image = cell.assetsImageView.image;
    }

    [self showInView:viewController];
}


-(CGSize)convertToCalculationImg:(UIImage *)image{
    UIImage *originalImage = [UIImage scaleDown:image withSize:CGSizeMake(self.view.size_width, self.view.size_width * image.size.height/image.size.width)];

    // 设置比例
    CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
    CGFloat screenRadio = CGRectGetWidth(self.view.window.frame)/CGRectGetHeight(self.view.window.frame);
    if (imageRadio >= screenRadio) {
        CGFloat currentImageHeight = CGRectGetWidth(self.view.window.frame)/imageRadio;
        self.mainScrollView.maximumZoomScale = CGRectGetHeight(self.view.window.frame)/currentImageHeight;
        return CGSizeMake(kScreenBounds.size.width, currentImageHeight);
    } else {
        CGFloat currentImageWidth = CGRectGetWidth(self.view.window.frame)*imageRadio;
        self.mainScrollView.maximumZoomScale = CGRectGetWidth(self.view.window.frame)/currentImageWidth;
        return CGSizeMake(currentImageWidth, kScreenBounds.size.height);
    }
}

-(void)addOtherImgView{                 //    // 1. 创建多张图片
    __weak typeof(self)weakSelf = self;
    for (int i = 0 ; i < self.tempImgArr.count;i++){
        TSImageView *originalImageView = [[TSImageView alloc] init];
        originalImageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.mainScrollView addSubview:originalImageView];
        NSString *imgUrl = [self.tempImgArr objectAtIndex:i];
        [originalImageView uploadImageWithURL:imgUrl placeholder:nil callback:^(UIImage *image) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            UIImage *originalImage = [UIImage scaleDown:image withSize:CGSizeMake(strongSelf.view.size_width, strongSelf.view.size_width * image.size.height/image.size.width)];
            originalImageView.image = originalImage;
            
            // 设置比例
            CGFloat imageRadio = originalImage.size.width/originalImage.size.height;
            CGFloat screenRadio = CGRectGetWidth(strongSelf.view.window.frame)/CGRectGetHeight(strongSelf.view.window.frame);
            if (imageRadio >= screenRadio) {
                CGFloat currentImageHeight = CGRectGetWidth(strongSelf.view.window.frame)/imageRadio;
                strongSelf.mainScrollView.maximumZoomScale = CGRectGetHeight(strongSelf.view.window.frame)/currentImageHeight;
            } else {
                CGFloat currentImageWidth = CGRectGetWidth(strongSelf.view.window.frame)*imageRadio;
                strongSelf.mainScrollView.maximumZoomScale = CGRectGetWidth(strongSelf.view.window.frame)/currentImageWidth;
            }
            
            if (i == strongSelf.tempCurrentIndex){
                strongSelf.userAnimationImgView.alpha = 0;
            }
        }];

        originalImageView.frame =   kScreenBounds;
        originalImageView.orgin_x = kScreenBounds.size.width * i;
    }
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    if (scrollView == self.mainScrollView){
        NSInteger index = self.mainScrollView.contentOffset.x / kScreenBounds.size.width;
        self.pageControl.currentPage = index;
        self.pageLabel.text = [NSString stringWithFormat:@"%li/%li",(long)index + 1,(long)self.tempImgArr.count];
    }
}







-(void)showInViewWithCell:(UIViewController *)viewController imgArr:(NSArray *)imgArr currentIndex:(NSInteger)currentIndex cell:(TSHomeCourseKejianDymicTableViewCell *)cell{
    // 1. 创建view
    [self pageSetting];
    // 2. img
    self.tempImgArr = imgArr;
    // 3.label
    self.pageLabel.text = [NSString stringWithFormat:@"%li/%li",(long)currentIndex + 1,(long)imgArr.count];
    // 4.frame
    CGRect convertImgFrame = [cell convertRect:cell.contentView.frame toView:self.view.window];
    
    self.mainScrollView.contentSize = CGSizeMake(kScreenBounds.size.width * self.tempImgArr.count, kScreenBounds.size.height);
    [self.mainScrollView setContentOffset:CGPointMake(currentIndex * kScreenBounds.size.width, 0)];
    // 3. cell
    self.kejianTempCell = cell;
    
    if (!self.userAnimationImgView){
        self.userAnimationImgView = [[TSImageView alloc]init];
        self.userAnimationImgView.backgroundColor = [UIColor clearColor];
        self.userAnimationImgView.frame = convertImgFrame;
        self.userAnimationImgView.userInteractionEnabled = NO;
        [self.view addSubview:self.userAnimationImgView];
        self.userAnimationImgView.image = cell.tempCell.imgView.image;
    }
    
    [self showInView:viewController];
}

@end
