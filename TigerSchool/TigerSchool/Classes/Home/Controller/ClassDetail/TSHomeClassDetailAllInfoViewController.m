//
//  TSHomeClassDetailAllInfoViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailAllInfoViewController.h"
#import "TSHomeClassDetailAllInfoViewCell.h"
#import "ImgSelectorViewController.h"

#define k_AllSucai_margin TSFloat(10)

@interface TSHomeClassDetailAllInfoViewController ()<UICollectionViewDelegate,UICollectionViewDataSource>
@property (nonatomic,strong)UICollectionView *allInfoCollectionView;
@property (nonatomic,strong)NSMutableArray *allInfoMutablearr;
@end

@implementation TSHomeClassDetailAllInfoViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createCollectionView];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"所有资料";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.allInfoMutablearr = [NSMutableArray array];
    [self.allInfoMutablearr addObjectsFromArray:self.transferCourseDetailClassModel.datums];
}

#pragma mark - 【UICollectionView】
-(void)createCollectionView{
    UICollectionViewFlowLayout *flowLayout= [[UICollectionViewFlowLayout alloc]init];
    self.allInfoCollectionView = [[UICollectionView alloc] initWithFrame:self.view.bounds collectionViewLayout:flowLayout];
    self.allInfoCollectionView.backgroundColor = [UIColor clearColor];
    self.allInfoCollectionView.showsVerticalScrollIndicator = NO;
    [self.allInfoCollectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:@"myCell"];
    self.allInfoCollectionView.delegate = self;
    self.allInfoCollectionView.dataSource = self;
    self.allInfoCollectionView.showsHorizontalScrollIndicator = NO;
    self.allInfoCollectionView.scrollsToTop = YES;
    self.allInfoCollectionView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    [self.view addSubview:self.allInfoCollectionView];
    
    [self.allInfoCollectionView registerClass:[TSHomeClassDetailAllInfoViewCell class] forCellWithReuseIdentifier:@"collectionIdentify"];
    
    __weak typeof(self)weakSelf = self;
    [self.allInfoCollectionView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        NSLog(@"123");
    }];
    
    [self.allInfoCollectionView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        
    }];
    
}

#pragma mark - UICollectionViewDataSource
-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return self.allInfoMutablearr.count;
}

#pragma mark - UICollectionViewDelegate
-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    TSHomeClassDetailAllInfoViewCell *cell = (TSHomeClassDetailAllInfoViewCell *)[collectionView dequeueReusableCellWithReuseIdentifier:@"collectionIdentify"  forIndexPath:indexPath];
    cell.transferImgUrl = [self.transferCourseDetailClassModel.datums objectAtIndex:indexPath.row];
    
    return cell;
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    ImgSelectorViewController *imgSelected = [[ImgSelectorViewController alloc]init];
    TSHomeClassDetailAllInfoViewCell * cell = (TSHomeClassDetailAllInfoViewCell *)[self.allInfoCollectionView cellForItemAtIndexPath:indexPath];
    [imgSelected showInView:self.parentViewController imgArr:self.allInfoMutablearr currentIndex:indexPath.row cell:cell];
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake((kScreenBounds.size.width - 4 * k_AllSucai_margin) / 3., (kScreenBounds.size.width - 4 * k_AllSucai_margin) / 3.);
}

#pragma mark - UICollectionViewDelegate
-(UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    return UIEdgeInsetsMake(0,TSFloat(7),0,TSFloat(7));
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForFooterInSection:(NSInteger)section {
    CGSize size = {kScreenBounds.size.width,TSFloat(20)};
    return size;
}



@end
