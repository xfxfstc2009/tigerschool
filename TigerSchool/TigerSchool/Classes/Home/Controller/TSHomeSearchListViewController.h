//
//  TSHomeSearchListViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//
// 首页搜索列表
#import "AbstractViewController.h"

@interface TSHomeSearchListViewController : AbstractViewController

@end
