//
//  TSHomeAllClassViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/13.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeAllClassViewController.h"
#import "TSHomeAllClassTagsTableViewCell.h"
#import "TSHomeTeachSingleCell.h"
#import "TSHomeAllClassTagsTopView.h"
#import "TSHomeClassDetailViewController.h"

@interface TSHomeAllClassViewController ()<UITableViewDelegate,UITableViewDataSource>{
    TSHomeAllClassSingleTagsModel *lastSelectedSingleModel;         /**< 选中的model*/
}
@property (nonatomic,strong)UITableView *allClassTableView;
@property (nonatomic,strong)NSMutableArray *allClassTagsMuarbleArr;         /**<所有的标签*/
@property (nonatomic,strong)NSMutableArray *allClassMuarbleArr;             /**<所有的内容*/
@property (nonatomic,strong)TSHomeAllClassTagsTopView *topView;
@end

@implementation TSHomeAllClassViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self baseSetting];
    [self arrayWithInit];
    [self createView];
    [self sendRequestToGetAllTags];
}

-(void)baseSetting{
    self.barMainTitle = @"全部课程";
}

-(void)arrayWithInit{
    self.allClassMuarbleArr = [NSMutableArray array];
    self.allClassTagsMuarbleArr = [NSMutableArray array];
}

-(void)createView{
    self.topView = [[TSHomeAllClassTagsTopView alloc]init];
    self.topView.selectedSingleModel = self.selectedSingleModel;
    __weak typeof(self)weakSelf = self;
    [self.topView actionClickWithTagsBlock:^(TSHomeAllClassSingleTagsModel *singleModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.topView.transferSingleModel = [strongSelf.allClassTagsMuarbleArr copy];
        strongSelf.selectedSingleModel = singleModel;
        if ([strongSelf ->lastSelectedSingleModel.ID isEqualToString:strongSelf.selectedSingleModel.ID]){
            return;
        } else {
            strongSelf.allClassTableView.currentPage = 1;
            [strongSelf sendRequestToGetAllClassInfo];
        }
    }];
    [self.view addSubview:self.topView];
}

#pragma mark -UITableView
-(void)createTableView{
    if (!self.allClassTableView){
        self.allClassTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.allClassTableView.frame = CGRectMake(0, CGRectGetMaxY(self.topView.frame), kScreenBounds.size.width, self.view.size_height - CGRectGetMaxY(self.topView.frame));
        self.allClassTableView.dataSource = self;
        self.allClassTableView.delegate = self;
        [self.view addSubview:self.allClassTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.allClassTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetAllClassInfo];
    }];
    
    [self.allClassTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToGetAllClassInfo];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.allClassMuarbleArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
    TSHomeTeachSingleCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
    if (!cellWithRowTwo){
        cellWithRowTwo = [[TSHomeTeachSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        cellWithRowTwo.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cellWithRowTwo.transferCellHeight = cellHeight;
    cellWithRowTwo.transferHomeLiveModel = [self.allClassMuarbleArr objectAtIndex:indexPath.row];
    return cellWithRowTwo;
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    TSHomeClassDetailViewController *homeClassDetailViewController = [[TSHomeClassDetailViewController alloc]init];
    TSHomeLiveModel *singleModel = [self.allClassMuarbleArr objectAtIndex:indexPath.row];
    homeClassDetailViewController.transferCourseId = singleModel.ID;
    homeClassDetailViewController.hasCancelPanDismiss = YES;
    [self.navigationController pushViewController:homeClassDetailViewController animated:YES];
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return TSFloat(11);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return [TSHomeTeachSingleCell calculationCellHeight];
}

#pragma mark - 获取所有的标签
-(void)sendRequestToGetAllTags{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeAllClassTagsWithBlock:^(TSHomeAllClassTagsModel *singleModle) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.allClassTagsMuarbleArr.count){
            [strongSelf.allClassTagsMuarbleArr removeAllObjects];
        }
        [strongSelf.allClassTagsMuarbleArr addObjectsFromArray:singleModle.items];
        
        // 如果有传递过来选中按钮就进行执行数据请求
        if (!strongSelf.selectedSingleModel){
            strongSelf.selectedSingleModel = [strongSelf.allClassTagsMuarbleArr firstObject];
        }
        
        strongSelf.topView.selectedSingleModel = strongSelf.selectedSingleModel;
        strongSelf.topView.transferSingleModel = [strongSelf.allClassTagsMuarbleArr copy];
        
        [strongSelf createTableView];

        [strongSelf sendRequestToGetAllClassInfo];

    }];
}

#pragma mark - 获取所有课程
-(void)sendRequestToGetAllClassInfo{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeCourselimitWithPage:self.allClassTableView.currentPage type:self.selectedSingleModel.ID block:^(TSHomeLiveListModel *list) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.allClassTableView.isXiaLa){
            [strongSelf.allClassMuarbleArr removeAllObjects];
        }
        
        if (![strongSelf->lastSelectedSingleModel.ID isEqualToString:strongSelf.selectedSingleModel.ID]){
            [strongSelf.allClassMuarbleArr removeAllObjects];
        }
        
        [strongSelf.allClassMuarbleArr addObjectsFromArray:list.items];
        [strongSelf.allClassTableView reloadData];
        
        if (!strongSelf.allClassMuarbleArr.count){
            [strongSelf.allClassTableView showPrompt:@"当前没有课程哦" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        } else {
            [strongSelf.allClassTableView dismissPrompt];
        }
        
        strongSelf->lastSelectedSingleModel = strongSelf.selectedSingleModel;
        [strongSelf.allClassTableView stopPullToRefresh];
        [strongSelf.allClassTableView stopFinishScrollingRefresh];
    }];
}

@end
