//
//  TSHomeClassDetailViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeClassDetailViewController.h"
#import "TSHomeClassPlayerTableViewCell.h"
#import "TSHomeClassDetailPlayerImgView.h"

#import "TSHomeClassDetailClassViewController.h"
#import "TSHomeClassJieshaoViewController.h"
#import "TSIMConversationViewController.h"

#import "NetworkAdapter+TSHome.h"
#import "PDGuideRootViewController.h"
#import "NetworkAdapter+TSTeach.h"
#import "TSHomeClassDetailQuestionRootViewController.h"

@interface TSHomeClassDetailViewController (){
    TSHomeCourseDetailClassModel *selectedCourseDetailModel;                /**< 用户选中的课程*/
    TSTeachBopHasPayModel *hasPayModel;
    BOOL hasShow;
    BOOL hasQihangban;                            /**< 如果说是其航班，就不能进行拖拉*/
}
@property (nonatomic,strong)TSHomeClassDetailPlayerImgView *playerImgView;
@property (nonatomic,strong)UIScrollView *mainScrollView;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,strong)TSHomeClassJieshaoViewController *jieshaoViewController;
@property (nonatomic,strong)TSHomeClassDetailClassViewController *classViewController;
@property (nonatomic,strong)UIButton *payButton;

@end

@implementation TSHomeClassDetailViewController

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)viewWillDisappear:(BOOL)animated{
    [super viewWillDisappear:animated];
    [self.playerImgView resetPlayerView];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createScrollView];
    [self createBottomView];
    [self sendRequestToGetHasPay];
//    [self showNewFeature];
}

#pragma mark - pageSetting
-(void)pageSetting{
    self.barMainTitle = @"课程详情";
    __weak typeof(self)weakSelf = self;
    [self leftBarButtonWithTitle:nil barNorImage:[UIImage imageNamed:@"icon_main_back"] barHltImage:[UIImage imageNamed:@"icon_main_back"] action:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.navigationController popViewControllerAnimated:YES];
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    hasShow = YES;
}

#pragma mark - createMainScrollView
-(void)createScrollView{
    __weak typeof(self)weakSelf = self;
    // 1. player
    self.playerImgView = [[TSHomeClassDetailPlayerImgView alloc]initWithFrame:CGRectMake(0, 0, kScreenBounds.size.width, TSFloat(230))];
    [self.playerImgView actionClickToPlayer:^(TSHomeCourseDetailClassModel *selectedCourseDetailModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([GWMusicPlayerMethod sharedLocationManager].streamer.status == DOUAudioStreamerPlaying){        // 正在播放中
            [[GWMusicPlayerMethod sharedLocationManager] actionStopAndRemoveFromWindow];
        }
        
        [strongSelf.playerImgView actionToPlayerManager:selectedCourseDetailModel];
    }];
    
    [self.playerImgView playViewPlayedEndManagerBlock:^{
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        // 播放结束进行发送结束信息
        [strongSelf sendRequestToWatchLog];
    }];
    
    [self.playerImgView playViewPlayedUploadTimeManagerBlock:^(NSInteger time) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (time > 10){
            [ZFPlayerView sharedPlayerView].currentProcess = time;
            [strongSelf sendRequestToUpload:time];
        }
    }];

    [self.view addSubview:self.playerImgView];
    
    // 2. 创建segment
    self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"介绍",@"课程"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (index == 1 && strongSelf->hasShow == NO){
            [strongSelf.segmentList setSelectedButtonIndex:0 animated:YES];
            [StatusBarManager statusHiddenWithString:@"请购买后观看"];
            return;
        }
        if (index == 1){
            [TSMTAManager MTAevent:MTAType_Class_SegmentClass str:strongSelf.transferCourseId];                 // 埋点
        } else if (index == 0){
            [TSMTAManager MTAevent:MTAType_Class_SegmentDetail str:strongSelf.transferCourseId];                 // 埋点
        }
        [strongSelf.mainScrollView setContentOffset:CGPointMake(kScreenBounds.size.width * index, 0) animated:YES];
    }];
    self.segmentList.shotLine = YES;
    self.segmentList.frame = CGRectMake(0, CGRectGetMaxY(self.playerImgView.frame), kScreenBounds.size.width, TSFloat(44));
    [self.view addSubview:self.segmentList];
    
    UIView *lineView = [[UIView alloc]init];
    lineView.frame = CGRectMake(0, self.segmentList.size_height - .5f, kScreenBounds.size.width, .5f);
    lineView.backgroundColor = [UIColor colorWithCustomerName:@"分割线"];
    [self.segmentList addSubview:lineView];
    
    if (!self.mainScrollView){
        self.mainScrollView = [UIScrollView createScrollViewScrollEndBlock:^(UIScrollView *scrollView) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            NSInteger index = scrollView.contentOffset.x / kScreenBounds.size.width;
            [strongSelf.segmentList setSelectedButtonIndex:index animated:YES];
        }];
        self.mainScrollView.backgroundColor = [UIColor clearColor];
        self.mainScrollView.scrollEnabled = NO;
        self.mainScrollView.frame = CGRectMake(0, CGRectGetMaxY(self.segmentList.frame), kScreenBounds.size.width, self.view.size_height - CGRectGetMaxY(self.segmentList.frame) - 10 - [PDMainTabbarViewController sharedController].tabBarHeight);
        self.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.size_width * 2, self.mainScrollView.size_height );
        [self.view addSubview:self.mainScrollView];
    }
    
    [self createJieshaoViewController];         // 介绍
    [self createClassViewController];           // 课程
}

#pragma mark - createBottomView
-(void)createBottomView{
    __weak typeof(self)weakSelf = self;
    self.payButton = [UIButton buttonWithType:UIButtonTypeCustom];
    self.payButton.backgroundColor = [UIColor colorWithCustomerName:@"红"];
    [self.view addSubview:self.payButton];
    if (IS_iPhoneX){
        self.payButton.frame = CGRectMake(0, kScreenBounds.size.height - [PDMainTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width, TSFloat(50) + 34);
    } else {
        self.payButton.frame = CGRectMake(0, kScreenBounds.size.height - [PDMainTabbarViewController sharedController].navBarHeight, kScreenBounds.size.width, TSFloat(50));
    }

    [self.payButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf payManager];
        [TSMTAManager MTAevent:MTAType_Class_Buy str:strongSelf.transferCourseId];                 // 埋点
    }];
    
    [self.payButton setTitle:[NSString stringWithFormat:@"￥%.2f",self.transferPrice] forState:UIControlStateNormal];
    [self.payButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}





#pragma mark - 介绍
- (void)createJieshaoViewController{
    self.jieshaoViewController = [[TSHomeClassJieshaoViewController alloc] init];
    self.jieshaoViewController.transferCourseId = self.transferCourseId;
    self.jieshaoViewController.view.frame = CGRectMake(0, 0, kScreenBounds.size.width, self.mainScrollView.size_height);
    __weak typeof(self)weakSelf = self;
    [self.jieshaoViewController requestBackGetHasQihangban:^(BOOL hasQihangban) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->hasQihangban = hasQihangban;
        if (strongSelf.classViewController){
            strongSelf.classViewController.hasQihangban = hasQihangban;
        }
    }];
    [self.mainScrollView addSubview:self.jieshaoViewController.view];
    [self addChildViewController:self.jieshaoViewController];
}

#pragma mark - 课程
- (void)createClassViewController{
    self.classViewController = [[TSHomeClassDetailClassViewController alloc] init];
    self.classViewController.transferCourseId = self.transferCourseId;
    self.classViewController.view.frame = CGRectMake(kScreenBounds.size.width, 0, kScreenBounds.size.width, self.mainScrollView.size_height);
    [self.mainScrollView addSubview:self.classViewController.view];
    __weak typeof(self)weakSelf = self;
    [self.classViewController actionClickWithShowChangeModelBlock:^(TSHomeCourseDetailClassModel *model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->selectedCourseDetailModel = model;              // 获取到用户获取的model
        [ZFPlayerView sharedPlayerView].currentProcess = model.process;
        
        [strongSelf.playerImgView changeAblumImgWithModel:model];
        [strongSelf qihangbanManager];                // 设置是否可以拖拉《其航班》
    }];
    [self addChildViewController:self.classViewController];
}


#pragma mark - Interface
-(void)sendRequestToGetHomeClassDetailInfo{
    __weak typeof(self)weakSelf = self;
    
    [[NetworkAdapter sharedAdapter] homeCourseDetailWithId:self.transferCourseId block:^(TSHomeCourseDetailListClassModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf ->selectedCourseDetailModel = [listModel.items firstObject];
        // player播放器
        strongSelf.playerImgView.selectedCourseDetailModel = strongSelf->selectedCourseDetailModel;
        [strongSelf qihangbanManager];
        
        // 5.判断是否支付过
        if (strongSelf-> hasPayModel.isfree){            // 表示免费的
            hasShow = YES;
            strongSelf.playerImgView.hasNonePlay = NO;
        } else {                                        // 表示不免费
            if (strongSelf->hasPayModel.ispay){             // 已经支付
                strongSelf->hasShow = YES;
                strongSelf.playerImgView.hasNonePlay = NO;
                strongSelf.mainScrollView.size_height = kScreenBounds.size.height - CGRectGetMaxY(strongSelf.segmentList.frame) - [PDMainTabbarViewController sharedController].navBarHeight;
                [UIView animateWithDuration:1.f animations:^{
                    strongSelf.payButton.orgin_y =  kScreenBounds.size.height;
                } completion:^(BOOL finished) {
                    strongSelf.payButton.orgin_y =  kScreenBounds.size.height;
                }];
            } else {                                        // 没有支付
                strongSelf->hasShow = NO;
                strongSelf.playerImgView.hasNonePlay = YES;
                strongSelf.mainScrollView.size_height = kScreenBounds.size.height - [PDMainTabbarViewController sharedController].navBarHeight - strongSelf.payButton.size_height - CGRectGetMaxY(strongSelf.segmentList.frame);
                [UIView animateWithDuration:1.f animations:^{
                    strongSelf.payButton.orgin_y =  kScreenBounds.size.height - [PDMainTabbarViewController sharedController].navBarHeight - strongSelf.payButton.size_height;
                } completion:^(BOOL finished) {
                    strongSelf.payButton.orgin_y =  kScreenBounds.size.height - [PDMainTabbarViewController sharedController].navBarHeight - strongSelf.payButton.size_height;
                    strongSelf.classViewController.view.size_height = strongSelf.mainScrollView.size_height;
                    strongSelf.jieshaoViewController.view.size_height = strongSelf.mainScrollView.size_height;
                    strongSelf.mainScrollView.contentSize = CGSizeMake(self.mainScrollView.size_width * 2, self.mainScrollView.size_height);
                }];
            }
        }
        
        if (self.transferPrice == 8.8 || self.transferPrice == 9.9){
            self.transferPrice = 12.0;
        }
        [strongSelf.payButton setTitle:[NSString stringWithFormat:@"￥%.2f / %li节",self.transferPrice,(long)listModel.items.count] forState:UIControlStateNormal];
    }];
}

-(void)sendRequestToGetHasPay{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachBopHasPayId:self.transferCourseId block:^(TSTeachBopHasPayModel *model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf->hasPayModel = model;
        [strongSelf sendRequestToGetHomeClassDetailInfo];
    }];
}

#pragma mark - 显示课程详情的介绍
-(void)showNewFeature {         // 添加收费信息
    if (![AccountModel sharedAccountModel].homeDetail){
        [self actionToShowGuide];
    }
}

-(void)actionToShowGuide{
    PDGuideRootViewController *guideRootViewController = [[PDGuideRootViewController alloc]init];
    // 1. 添加消息
    PDGuideSingleModel *remindGuideSingleModel = [[PDGuideSingleModel alloc]init];
    remindGuideSingleModel.text = @"点击这里可以进行播放老师课程哦";
    remindGuideSingleModel.foundationType = foundationTypeCircle;
    remindGuideSingleModel.guideType = GuideGuesterTypeTap;
    UIWindow *keyWindow = (UIWindow *)[[UIApplication sharedApplication].delegate window];
    CGRect convertFrame = [self.playerImgView convertRect:self.playerImgView.actionButton.frame toView:keyWindow];
    remindGuideSingleModel.transferShowFrame = CGRectMake(convertFrame.origin.x, convertFrame.origin.y + self.navigationController.navigationBar.size_height + [PDMainTabbarViewController sharedController].statusBarHeight, convertFrame.size.width, convertFrame.size.height);
    
    [guideRootViewController showInView:self.parentViewController withViewActionArr:@[remindGuideSingleModel]];
    [guideRootViewController homeActionClickToPlayerBlock:^{
        [[AccountModel sharedAccountModel] guestSuccessWithType:GuestTypeHomeDetail block:NULL];
    }];
}

#pragma mark - 提交观看视频的结束符
-(void)sendRequestToWatchLog{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachToolWatchLog:selectedCourseDetailModel.detailid courseid:self.transferCourseId itemid:@"" score:-1 str:@"" recite:NO block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            strongSelf-> selectedCourseDetailModel.iswatch = YES;
            [strongSelf.classViewController playerEndWithModel:strongSelf->selectedCourseDetailModel];
        }
    }];
}

#pragma mark - 当前进度
-(void)sendRequestToUpload:(NSInteger)time{
    [[NetworkAdapter sharedAdapter] teachPlayerCourseprocess:self.transferCourseId detailId:selectedCourseDetailModel.detailid process:time];
}

-(void)payManager{
    TSTeachBopDetailSingleModel *singleModel = [[TSTeachBopDetailSingleModel alloc]init];
    singleModel.price = self.transferPrice;
    singleModel.tempCourId = self.transferCourseId;
    __weak typeof(self)weakSelf = self;
    [[TSAlertView sharedAlertView] showAlertWithType:AlertTypePay model:singleModel block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [[UIAlertView alertViewWithTitle:@"支付成功" message:nil buttonTitles:@[@"确定"] callBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            strongSelf->hasPayModel.ispay = YES;
            if (strongSelf->hasPayModel.ispay){             // 已经支付
                strongSelf.playerImgView.hasNonePlay = NO;
                strongSelf->hasShow = YES;
                [UIView animateWithDuration:1.f animations:^{
                    strongSelf.payButton.orgin_y =  kScreenBounds.size.height;
                    strongSelf.mainScrollView.size_height = kScreenBounds.size.height - CGRectGetMaxY(strongSelf.segmentList.frame) - [PDMainTabbarViewController sharedController].navBarHeight;
                    
                } completion:^(BOOL finished) {
                    strongSelf.payButton.orgin_y =  kScreenBounds.size.height;
                    strongSelf.mainScrollView.size_height = kScreenBounds.size.height - CGRectGetMaxY(strongSelf.segmentList.frame) - [PDMainTabbarViewController sharedController].navBarHeight;
                }];
            } else {                                        // 没有支付
                strongSelf.playerImgView.hasNonePlay = YES;
                [UIView animateWithDuration:1.f animations:^{
                    strongSelf.mainScrollView.size_height -= strongSelf.payButton.size_height;
                    strongSelf.payButton.orgin_y =  kScreenBounds.size.height - [PDMainTabbarViewController sharedController].navBarHeight - strongSelf.payButton.size_height;
                } completion:^(BOOL finished) {
                    strongSelf.payButton.orgin_y =  kScreenBounds.size.height - [PDMainTabbarViewController sharedController].navBarHeight - strongSelf.payButton.size_height;
                    strongSelf.mainScrollView.size_height = kScreenBounds.size.height - [PDMainTabbarViewController sharedController].navBarHeight - [PDMainTabbarViewController sharedController].tabBarHeight - CGRectGetMaxY(strongSelf.segmentList.frame);
                }];
            }
        }] show];
    }];
}


#pragma mark - 如果是其航班的就有不能拖拉的逻辑
-(void)qihangbanManager{
    if (hasQihangban){
        if (selectedCourseDetailModel.iswatch){             // 表示已经看完
            [ZFPlayerView sharedPlayerView].transferTigerSchoolType = TigerSchoolSceneTypeNormal;
        } else {
            [ZFPlayerView sharedPlayerView].transferTigerSchoolType = TigerSchoolSceneTypeQihangban;
        }
    } else {
        [ZFPlayerView sharedPlayerView].transferTigerSchoolType = TigerSchoolSceneTypeNormal;
    }
}

@end
