//
//  TSHomeSearchListViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/15.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSHomeSearchListViewController.h"
#import "TSHomeSearchTagsTableViewCell.h"
#import "NetworkAdapter+TSHome.h"
#import "TSHomeSearchTagsTableViewCell.h"
#import "TSHomeSearchDetailViewController.h"
#import "GWDropListView.h"
#import "TSHomeClassDetailViewController.h"
#import "TSTeachBopDetailViewController.h"
#import "TSTeachMaterialDetailViewController.h"

@interface TSHomeSearchListViewController ()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (nonatomic,strong)UITableView *searchTableView;
@property (nonatomic,strong)NSMutableArray *hotMutableArr;              /**< 热门 */
@property (nonatomic,strong)NSMutableArray *historyMutableArr;          /**< 历史*/
@property (nonatomic,strong)UISearchBar *searchBar;
@property (nonatomic,strong)NSMutableArray *searchRootMutableArr;
@property (nonatomic,strong)GWDropListView *dropList;
@end

@implementation TSHomeSearchListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self pageSetting];
    [self arrayWithInit];
    [self createTableView];
    [self setBarButtonItem];
    [self sendRequestToGetSearchItems];
    [self createSearchController];
}

#pragma mark - createView
- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.dropList resignFirstResponder];
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(.3f * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        if (self.dropList){
            [self.dropList removeFromSuperview];
            self.dropList = nil;
        }
    });
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.searchBar becomeFirstResponder];
}

#pragma mark - pageSettig
-(void)pageSetting{
    self.barMainTitle = @"搜索历史";
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.searchRootMutableArr = [NSMutableArray array];
    self.hotMutableArr = [NSMutableArray array];
    self.historyMutableArr = [NSMutableArray array];
    [self.searchRootMutableArr addObject:self.hotMutableArr];
    [self.searchRootMutableArr addObject:self.historyMutableArr];
}

#pragma mark - UITableView
-(void)createTableView{
    self.searchTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
    self.searchTableView.dataSource = self;
    self.searchTableView.delegate = self;
    [self.view addSubview:self.searchTableView];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    NSInteger rowCount = 0;
    if (self.hotMutableArr.count){
        rowCount += 1;
    }
    if (self.historyMutableArr.count){
        rowCount += 1;
    }
    return rowCount;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
    TSHomeSearchTagsTableViewCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
    if (!cellWithRowOne){
        cellWithRowOne = [[TSHomeSearchTagsTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
        cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    if (indexPath.section == 0){
        cellWithRowOne.transferArr = [self.hotMutableArr copy];
    } else {
        cellWithRowOne.transferArr = [self.historyMutableArr copy];
    }
    __weak typeof(self)weakSelf = self;
    [cellWithRowOne actionClickWithTagsBlock:^(NSString *info) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        strongSelf.searchBar.text = info;
        [strongSelf searchWithText:info];
        if (indexPath.section == 0){
            [TSMTAManager MTAevent:MTAType_Search_Hot str:info];                 // 埋点
        } else {
            [TSMTAManager MTAevent:MTAType_Search_History str:info];                 // 埋点
        }
       
    }];
    
    return cellWithRowOne;
}

#pragma mark - UITableViewDelegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){
        return [TSHomeSearchTagsTableViewCell calculationCellHeightWithArr:self.hotMutableArr];
    } else {
        return [TSHomeSearchTagsTableViewCell calculationCellHeightWithArr:self.historyMutableArr];
    }
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor whiteColor];
    
    UILabel *titleLabel = [TSViewTool createLabelFont:@"小提示" textColor:@"浅灰"];
    [headerView addSubview:titleLabel];
    titleLabel.frame = CGRectMake(TSFloat(11), 0, kScreenBounds.size.width - 2 * TSFloat(11), TSFloat(40));
    if (section == 0){
        titleLabel.text = @"热门搜索";
    } else {
        titleLabel.text = @"历史搜索";
    }
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return TSFloat(40);
}



- (void)setBarButtonItem{
    [self.navigationItem setHidesBackButton:YES];
  
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    if(searchBar.text.length){
        if (!self.dropList){
            [self createDropList];
        }
        if (searchBar.text.length){
            [self.dropList interfaceWithSearch:searchBar.text];
        } else {
            [self.dropList removeFromSuperview];
            self.dropList = nil;
        }
    }
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {

}
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}
- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (!self.dropList){
        [self createDropList];
    }
    if (searchText.length){
        [self.dropList interfaceWithSearch:searchText];
    } else {
        [self.dropList removeFromSuperview];
        self.dropList = nil;
    }
}

-(void)searchWithText:(NSString *)text{
    if (!self.dropList){
        [self createDropList];
    }
    if (text.length){
        [self.dropList interfaceWithSearch:text];
    } else {
        [self.dropList removeFromSuperview];
        self.dropList = nil;
    }
}


#pragma mark - 创建searchController
-(void)createSearchController{
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(5, 7, kScreenBounds.size.width - TSFloat(60), 40)];
    titleView.backgroundColor = [UIColor whiteColor];
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(titleView.frame) - 15, titleView.size_height)];
    searchBar.placeholder = @"课程/案例/素材";
    searchBar.backgroundImage = [TSTool createImageWithColor:[UIColor clearColor] frame:titleView.bounds];
    searchBar.delegate = self;
    searchBar.showsCancelButton = NO;
    UITextField *searchTextField = [searchBar valueForKey:@"_searchField"];
    searchTextField.font = [UIFont systemFontOfSize:15];
    searchTextField.backgroundColor = [UIColor colorWithRed:234/255.0 green:235/255.0 blue:237/255.0 alpha:1];
    searchBar.backgroundColor = [UIColor whiteColor];
    [titleView addSubview:searchBar];
    self.searchBar = searchBar;
    self.navigationItem.titleView = titleView;
}


#pragma mark - Interface
-(void)sendRequestToGetSearchItems{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeSearchListWithBlock:^(TSHomeSearchModel *list) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.hotMutableArr removeAllObjects];
        
        [strongSelf.hotMutableArr addObjectsFromArray:list.hotsearch];

        [strongSelf.historyMutableArr removeAllObjects];
        [strongSelf.historyMutableArr addObjectsFromArray:list.historysearch];
        [strongSelf.searchTableView reloadData];
        
        if ((!strongSelf.historyMutableArr.count)&&(!strongSelf.hotMutableArr.count)){
            [strongSelf.searchTableView showPrompt:@"当前没有数据" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        } else {
            [strongSelf.searchTableView dismissPrompt];
        }
        
    }];
}

#pragma mark - createDropList
-(void)createDropList{
    if (!self.dropList){
        self.dropList = [[GWDropListView alloc]init];
    }
    
    CGRect convertFrame = [self.navigationItem.titleView convertRect:self.searchBar.frame toView:self.view.window];
    __weak typeof(self)weakSelf = self;
    CGRect newRect = CGRectMake(0, convertFrame.origin.y, kScreenBounds.size.width, convertFrame.size.height);

    [self.dropList showInViewController:self frame:newRect listArr:@[@"1",@"2",@"3",@"4",@"5"] searchType:SearchTypeClass block:^(SearchType searchType, id model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if ([model isKindOfClass:[TSHomeLiveModel class]]){         // 课程
            TSHomeClassDetailViewController *homeClassDetailViewController = [[TSHomeClassDetailViewController alloc]init];
            TSHomeLiveModel *singleModel = (TSHomeLiveModel *)model;
            homeClassDetailViewController.transferCourseId = singleModel.ID;
            homeClassDetailViewController.hasCancelPanDismiss = YES;
            [strongSelf.navigationController pushViewController:homeClassDetailViewController animated:YES];
        } else if ([model isKindOfClass:[TSTeachRootSingleModel class]]){   // bop
            TSTeachBopDetailViewController *bopClassViewController = [[TSTeachBopDetailViewController alloc]init];
            TSTeachRootSingleModel *singleModel = (TSTeachRootSingleModel *)model;
            bopClassViewController.transferBopDetailId = singleModel.ID;
            [self.navigationController pushViewController:bopClassViewController animated:YES];
        } else if ([model isKindOfClass:[TSMaterialSegmentModel class]]){          // 素材
            TSMaterialSegmentModel *studyInfoSingleModel = (TSMaterialSegmentModel *)model;
            TSTeachMaterialDetailViewController *materialDetailViewController = [[TSTeachMaterialDetailViewController alloc]init];
            materialDetailViewController.transferStudyInfoSingleModel = studyInfoSingleModel;
            [self.navigationController pushViewController:materialDetailViewController animated:YES];
        }
    }];
    

    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
    [keywindow addSubview:self.dropList];
    [self.dropList actionDropListClickWithTapManager:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.searchBar resignFirstResponder];
        if (strongSelf.dropList){
            [strongSelf.dropList removeFromSuperview];
            strongSelf.dropList = nil;
        }
    }];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
   [self.searchBar resignFirstResponder];
}


@end
