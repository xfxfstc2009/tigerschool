//
//  HomeRootViewController.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/12.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "HomeRootViewController.h"
// view
#import "TSHomeTeachSingleCell.h"                   // 执行的内容
#import "TSHomeTagsViewCell.h"                      //
#import "TSHomeRootBannerCell.h"

// controller
#import "TSHomeAllClassViewController.h"            //
#import "TSHomeClassDetailViewController.h"
#import "TSHomeSearchListViewController.h"

// Net
#import "NetworkAdapter+TSHome.h"

// test
#import "TNSexyImageUploadProgress.h"


@interface HomeRootViewController()<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
@property (nonatomic,strong)UITableView *homeTableView;
@property (nonatomic,strong)NSMutableArray *homeRootMutableArr;                 /**< 首页总体数组*/

@property (nonatomic,strong)NSMutableArray *homeBannerMutableArr;               /**< 首页banner*/
@property (nonatomic,strong)NSMutableArray *homeActivityMutableArr;             /**< 活动页面*/
@property (nonatomic,strong)NSMutableArray *homeTagsMutableArr;                 /**< 首页标签数组*/
@property (nonatomic,strong)NSMutableArray *homePagesMutableArr;                /**< 首页分页数组*/
@property (nonatomic,strong)UISearchBar *searchBar;
@property (nonatomic,strong)TNSexyImageUploadProgress *imageUploadProgress;

@end

@implementation HomeRootViewController

+(instancetype)sharedCenterController{
    static HomeRootViewController *_sharedCenterController = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedCenterController = [[HomeRootViewController alloc] init];
    });
    return _sharedCenterController;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.searchBar resignFirstResponder];
}

-(void)viewDidLoad{
    [super viewDidLoad];
    [self arrayWithInit];
    [self createTableView];
    [self setBarButtonItem];
}

- (void)setBarButtonItem {
    [self.navigationItem setHidesBackButton:YES];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(TSFloat(5), 7, kScreenBounds.size.width - 2 * TSFloat(5), 40)];
    titleView.backgroundColor = [UIColor whiteColor];
    UISearchBar *searchBar = [[UISearchBar alloc] initWithFrame:CGRectMake(0, 0, CGRectGetWidth(titleView.frame) - 15, titleView.size_height)];
    searchBar.placeholder = @"课程/案例/素材";
    searchBar.backgroundImage = [TSTool createImageWithColor:[UIColor clearColor] frame:titleView.bounds];
    searchBar.delegate = self;
    searchBar.showsCancelButton = NO;
    UITextField *searchTextField = [searchBar valueForKey:@"_searchField"];
    searchTextField.font = [UIFont systemFontOfSize:15];
    searchTextField.backgroundColor = [UIColor colorWithRed:234/255.0 green:235/255.0 blue:237/255.0 alpha:1];
    searchBar.backgroundColor = [UIColor whiteColor];
    [titleView addSubview:searchBar];
    self.searchBar = searchBar;
    self.navigationItem.titleView = titleView;
    
    
}

#pragma mark - UISearchBarDelegate
- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar{
    TSHomeSearchListViewController *searchListController = [[TSHomeSearchListViewController alloc]init];
    [self.navigationController pushViewController:searchListController animated:YES];
    [TSMTAManager MTAevent:MTAType_Search_back str:@""];                 // 埋点
    return YES;
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    NSLog(@"SearchButton");
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self.searchBar resignFirstResponder];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    searchBar.showsCancelButton = NO;
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    NSLog(@"%@",searchText);
}

-(void)homeRootInterfaceManagerWithLogin{
    __weak typeof(self)weakSelf = self;
    [self authorizeWithCompletionHandler:^(BOOL successed) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (successed){
            [strongSelf homeRootInterfaceManager];
        }
    }];
}

#pragma mark - arrayWithInit
-(void)arrayWithInit{
    self.homeRootMutableArr = [NSMutableArray array];
    [self.homeRootMutableArr addObject:@[@"banner"]];
    
    self.homeBannerMutableArr = [NSMutableArray array];
    self.homeTagsMutableArr = [NSMutableArray array];
    self.homeActivityMutableArr = [NSMutableArray array];
    
    [self.homeRootMutableArr addObject:self.homeActivityMutableArr];
    [self.homeRootMutableArr addObject:@[@"标签fixed",@"标签"]];
    self.homePagesMutableArr = [NSMutableArray array];
    
    [self.homeRootMutableArr addObject:self.homePagesMutableArr];
}

#pragma mark - UITableView
-(void)createTableView{
    if (!self.homeTableView){
        self.homeTableView = [TSViewTool gwCreateTableViewRect:self.view.bounds];
        self.homeTableView.dataSource = self;
        self.homeTableView.delegate = self;
        [self.view addSubview:self.homeTableView];
    }
    __weak typeof(self)weakSelf = self;
    [self.homeTableView appendingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf homeRootInterfaceManager];
    }];
    [self.homeTableView appendingFiniteScrollingPullToRefreshHandler:^{
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf homePageInterfaceManagerWithPage];
    }];
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return self.homeRootMutableArr.count;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    NSArray *sectionOfArr = [self.homeRootMutableArr objectAtIndex:section];
    return sectionOfArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    CGFloat cellHeight = [tableView rectForRowAtIndexPath:indexPath].size.height;
    if (indexPath.section == 0){
        static NSString *cellIdentifyWithRowBanner = @"cellIdentifyWithRowBanner";
        TSHomeRootBannerCell *cellWithRowBanner = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowBanner];
        if (!cellWithRowBanner){
            cellWithRowBanner = [[TSHomeRootBannerCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowBanner];
            cellWithRowBanner.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        cellWithRowBanner.transferBannerArr = [self.homeBannerMutableArr copy];
        __weak typeof(self)weakSelf = self;
        [cellWithRowBanner actionClickWithBlock:^(TSHomeBannerModel *singleModel) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            TSHomeClassDetailViewController *homeClassDetailViewController = [[TSHomeClassDetailViewController alloc]init];
            homeClassDetailViewController.hasCancelPanDismiss = YES;
            homeClassDetailViewController.transferCourseId = singleModel.ID;
            [TSMTAManager MTAevent:MTAType_Home_Banner str:singleModel.ID];                 // 埋点
            homeClassDetailViewController.transferPrice = singleModel.price;
            [strongSelf.navigationController pushViewController:homeClassDetailViewController animated:YES];
        }];
        
        return cellWithRowBanner;
    } else if (indexPath.section == 1){         // 课程内容
        static NSString *cellIdentifyWithRowThr = @"cellIdentifyWithRowThr";
        TSHomeTeachSingleCell *cellWithRowThr = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowThr];
        if (!cellWithRowThr){
            cellWithRowThr = [[TSHomeTeachSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowThr];
            cellWithRowThr.selectionStyle = UITableViewCellSelectionStyleNone;
        }

        TSHomeLiveModel *singleModel = [self.homeActivityMutableArr objectAtIndex:indexPath.row];
        cellWithRowThr.transferHomeLiveModel = singleModel;
        
        return cellWithRowThr;
    } else if (indexPath.section == 2){     // 标签
        if (indexPath.row == 0){
            static NSString *cellIdentifyWithRowFour1 = @"cellIdentifyWithRowFour1";
            GWNormalTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour1];
            if (!cellWithRowFour){
                cellWithRowFour = [[GWNormalTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour1];
                cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFour.transferCellHeight = cellHeight;
            cellWithRowFour.transferTitle = @"热门标签";
            cellWithRowFour.transferDesc = @"全部";
            cellWithRowFour.transferHasArrow = YES;
            cellWithRowFour.dymicButton.hidden = NO;
//            cellWithRowFour.dymicButton.backgroundColor = UURandomColor;
            __weak typeof(self)weakSelf = self;
            [cellWithRowFour actionClickWithDymicBlock:^{
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                TSHomeAllClassViewController *allClassViewController = [[TSHomeAllClassViewController alloc]init];
                [strongSelf.navigationController pushViewController:allClassViewController animated:YES];
                [TSMTAManager MTAevent:MTAType_Home_TagsALL str:@""];                 // 埋点
            }];
            
            return cellWithRowFour;
        } else {
            static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
            TSHomeTagsViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
            if (!cellWithRowFour){
                cellWithRowFour = [[TSHomeTagsViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
                cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            cellWithRowFour.tansferTagsArr = [self.homeTagsMutableArr copy];
            __weak typeof(self)weakSelf = self;
            [cellWithRowFour homeTagClickBlock:^(TSHomeAllClassSingleTagsModel *singleModel) {
                if (!weakSelf){
                    return ;
                }
                __strong typeof(weakSelf)strongSelf = weakSelf;
                TSHomeAllClassViewController *allClassViewController = [[TSHomeAllClassViewController alloc]init];
                allClassViewController.selectedSingleModel = singleModel;
                [TSMTAManager MTAevent:MTAType_Home_Tags str:singleModel.ID];                 // 埋点
                [strongSelf.navigationController pushViewController:allClassViewController animated:YES];
            }];
            return cellWithRowFour;
        }
    } else {
        static NSString *cellIdentifyWithRowFiv = @"cellIdentifyWithRowFiv";
        TSHomeTeachSingleCell *cellWithRowFiv = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFiv];
        if (!cellWithRowFiv){
            cellWithRowFiv = [[TSHomeTeachSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFiv];
            cellWithRowFiv.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        
        TSHomeLiveModel *singleModel = [self.homePagesMutableArr objectAtIndex:indexPath.row];
        cellWithRowFiv.transferHomeLiveModel = singleModel;
        
        return cellWithRowFiv;
    }
}

#pragma mark - UITableViewDelegate
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section == 1){
        TSHomeClassDetailViewController *homeClassDetailViewController = [[TSHomeClassDetailViewController alloc]init];
        TSHomeLiveModel *singleModel = [self.homeActivityMutableArr objectAtIndex:indexPath.row];
        homeClassDetailViewController.transferCourseId = singleModel.ID;
        homeClassDetailViewController.transferPrice = singleModel.price;
        homeClassDetailViewController.hasCancelPanDismiss = YES;
        [self.navigationController pushViewController:homeClassDetailViewController animated:YES];
    } else if (indexPath.section == 2){
        if (indexPath.row == 0){

        }
    } else {
        TSHomeClassDetailViewController *homeClassDetailViewController = [[TSHomeClassDetailViewController alloc]init];
        TSHomeLiveModel *singleModel = [self.homePagesMutableArr objectAtIndex:indexPath.row];
        homeClassDetailViewController.hasCancelPanDismiss = YES;
        homeClassDetailViewController.transferCourseId = singleModel.ID;
        homeClassDetailViewController.transferPrice = singleModel.price;
        [self.navigationController pushViewController:homeClassDetailViewController animated:YES];
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return TSFloat(11);
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView = [[UIView alloc]init];
    headerView.backgroundColor = [UIColor clearColor];
    return headerView;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0){            // banner
        return [TSHomeRootBannerCell calculationCellHeight];
    } else if (indexPath.section == 1){
        return [TSHomeTeachSingleCell calculationCellHeight];
    } else if (indexPath.section == 2){
        if (indexPath.row == 0){
            return [GWNormalTableViewCell calculationCellHeight];
        } else {
            return [TSHomeTagsViewCell calculationCellHeightTagsArr:[self.homeTagsMutableArr copy]];
        }
    } else {
        return [TSHomeTeachSingleCell calculationCellHeight];
    }
    
    return [TSHomeTeachSingleCell calculationCellHeight];
}




#pragma mark - Interface
-(void)homeRootInterfaceManager{
    [self homeBannerInterfaceManger];                   // 获取banner
    [self homeActivityInterfaceManager];                // 获取活动
    [self homeTagsInterfaceManager];                    // 获取标签
    [self homePageInterfaceManagerWithPage];            // 获取分页信息
}

// 获取banner
-(void)homeBannerInterfaceManger{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeBannerFetchWithBlock:^(TSHomeBannerListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.homeBannerMutableArr.count){
            [strongSelf.homeBannerMutableArr removeAllObjects];
        }
        [strongSelf.homeBannerMutableArr addObjectsFromArray:listModel.items];
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:0];
        [strongSelf.homeTableView reloadRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationNone];
    }];
}

// 获取活动信息
-(void)homeActivityInterfaceManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeCourselistWithBlock:^(TSHomeLiveListModel *list) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.homeActivityMutableArr.count){
            [strongSelf.homeActivityMutableArr removeAllObjects];
        }
        [strongSelf.homeActivityMutableArr addObjectsFromArray:list.items];
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:1];
        [strongSelf.homeTableView reloadData];
    }];
}

// 获取活动标签
-(void)homeTagsInterfaceManager{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeTypelimitWithBlock:^(TSHomeAllClassTagsModel *singleModle) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.homeTagsMutableArr.count){
            [strongSelf.homeTagsMutableArr removeAllObjects];
        }
        [strongSelf.homeTagsMutableArr addObjectsFromArray:singleModle.items];
//        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:2];
        [strongSelf.homeTableView reloadData];
    }];
}

// 获取分页
-(void)homePageInterfaceManagerWithPage{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeCourselimitWithHomePage:self.homeTableView.currentPage block:^(TSHomeLiveListModel *list) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (strongSelf.homeTableView.isXiaLa){
           [strongSelf.homePagesMutableArr removeAllObjects];
        }

        [strongSelf.homePagesMutableArr addObjectsFromArray:list.items];
        
        [strongSelf.homeTableView reloadData];
        [strongSelf.homeTableView stopPullToRefresh];
        [strongSelf.homeTableView stopFinishScrollingRefresh];
    }];
}






@end
