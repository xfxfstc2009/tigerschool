//
//  TSHomeClassDetailViewController.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/16.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AbstractViewController.h"

@interface TSHomeClassDetailViewController : AbstractViewController

@property (nonatomic,copy)NSString *transferCourseId;           /**< 传递过来的id*/
@property (nonatomic,assign)CGFloat transferPrice;              /**< 传递过来的单价*/


@end
