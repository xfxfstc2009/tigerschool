//
//  NetworkAdapter+TSHome.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "NetworkAdapter+TSHome.h"

@implementation NetworkAdapter (TSHome)
// 获取用户banner
-(void)homeBannerFetchWithBlock:(void(^)(TSHomeBannerListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_banner requestParams:nil responseObjectClass:[TSHomeBannerListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeBannerListModel *singleModel = (TSHomeBannerListModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}

// 【获取所有的课程-标签】
-(void)homeAllClassTagsWithBlock:(void(^)(TSHomeAllClassTagsModel *singleModle))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_typeall requestParams:nil responseObjectClass:[TSHomeAllClassTagsModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeAllClassTagsModel *singleModle = (TSHomeAllClassTagsModel *)responseObject;
            if (block){
                block(singleModle);
            }
        }
    }];
}

// 【获取前三节课的推荐】
-(void)homeCourselistWithBlock:(void(^)(TSHomeLiveListModel *list))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:courselist requestParams:nil responseObjectClass:[TSHomeLiveListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeLiveListModel *list = (TSHomeLiveListModel *)responseObject;
            if (block){
                block(list);
            }
        }
    }];
}

// 【课程分类】
-(void)homeTypelimitWithBlock:(void(^)(TSHomeAllClassTagsModel *singleModle))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter]fetchWithGetPath:home_typelimit requestParams:nil responseObjectClass:[TSHomeAllClassTagsModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return;
        }
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeAllClassTagsModel *singleModle = (TSHomeAllClassTagsModel *)responseObject;
            if (block){
                block(singleModle);
            }
        }
    }];
}

// 【课程简介】
-(void)homeCoursedescWithId:(NSString *)courId block:(void(^)(TSCoursedescModel *courseDescModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"courseid":courId};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_coursedesc requestParams:params responseObjectClass:[TSCoursedescModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if(isSucceeded){
            TSCoursedescModel *courseDescModel = (TSCoursedescModel *)responseObject;
            if (block){
                block(courseDescModel);
            }
        }
    }];
}

// 【课程内容】
-(void)homeCoursedetailWithBlock:(void(^)(TSCoursedescModel *courseDescModel))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_coursedesc requestParams:nil responseObjectClass:[TSCoursedescModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if(isSucceeded){
            TSCoursedescModel *courseDescModel = (TSCoursedescModel *)responseObject;
            if (block){
                block(courseDescModel);
            }
        }
    }];
}

// 【获取课程列表分页】
-(void)homeCourselimitWithHomePage:(NSInteger)page block:(void(^)(TSHomeLiveListModel *list))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page":@(page),@"limit":@"10",@"type":@"0",@"isall":@"0"};
    
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_courselimit requestParams:params responseObjectClass:[TSHomeLiveListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeLiveListModel *list = (TSHomeLiveListModel *)responseObject;
            if (block){
                block(list);
            }
        }
    }];
}

// 【获取课程列表分页】
-(void)homeCourselimitWithPage:(NSInteger)page type:(NSString *)type block:(void(^)(TSHomeLiveListModel *list))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"page":@(page),@"limit":@"10",@"type":type,@"isall":@"1"};
    
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_courselimit requestParams:params responseObjectClass:[TSHomeLiveListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeLiveListModel *list = (TSHomeLiveListModel *)responseObject;
            if (block){
                block(list);
            }
        }
    }];
}

// 获取搜索历史
-(void)homeSearchListWithBlock:(void(^)(TSHomeSearchModel *list))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_searchlist requestParams:nil responseObjectClass:[TSHomeSearchModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
//        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSucceeded){
            TSHomeSearchModel *searchList = (TSHomeSearchModel *)responseObject;
            if (block){
                block(searchList);
            }
        }
    }];
}

// 进行搜索
-(void)homeSearchDetailInfoWithType:(SearchType)type key:(NSString *)key block:(void(^)(TSHomeLiveListModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"type":@(type),@"search":key,@"page":@"1",@"limit":@(20)};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_searchDetail requestParams:params responseObjectClass:[TSHomeLiveListModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeLiveListModel *singleModel = (TSHomeLiveListModel *)responseObject;
            if (block){
                block(singleModel);
            }
        } else {
            TSHomeLiveListModel *singleModel = [[TSHomeLiveListModel alloc]init];
            if (block){
                block(singleModel);
            }
        }
    }];
}

// 获取课程内容
-(void)homeCourseDetailWithId:(NSString *)courseId block:(void(^)(TSHomeCourseDetailListClassModel *listModel))block{
    __weak typeof(self)weakSelf = self;
    NSDictionary *params = @{@"courseid":courseId};
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_coursedetail requestParams:params responseObjectClass:[TSHomeCourseDetailListClassModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeCourseDetailListClassModel *singleModel = (TSHomeCourseDetailListClassModel *)responseObject;
            if (block){
                block(singleModel);
            }
        }
    }];
}


#pragma mark - 提交信息
-(void)sendRequestToQuestionManagerWithDetailId:(NSString *)detailId questionArr:(NSArray<TSHomeCourseDetailExamitemsSingleModel> *)questionArr block:(void(^)(NSInteger score))block{
    __weak typeof(self)weakSelf = self;
    NSString *error = @"";
    if (!detailId.length){
        error = @"丢失小节ID，请退出重试";
    } else if (!questionArr.count){
        error = @"没有收到回答信息,请退出重试";
    }
    if (error.length){
        [StatusBarManager statusHiddenWithString:error];
        return;
    }
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setValue:detailId forKey:@"detailid"];
    
    NSString *questionStr = @"";
    for (int i = 0 ;i < questionArr.count;i++){
        TSHomeCourseDetailExamitemsSingleModel *singleModel = [questionArr objectAtIndex:i];
        NSString *singleQuestionStr = @"";
        for (int j = 0 ; j < singleModel.userChoosedArr.count;j++){
            NSString *info = [singleModel.userChoosedArr objectAtIndex:j];
            NSInteger index = [singleModel.answers indexOfObject:info] + 1;
            if (j == singleModel.userChoosedArr.count - 1){
                singleQuestionStr = [singleQuestionStr stringByAppendingString:[NSString stringWithFormat:@"%li",(long)index]];
            } else {
                singleQuestionStr = [singleQuestionStr stringByAppendingString:[NSString stringWithFormat:@"%li,",(long)index]];
            }
        }
        
        if (i == questionArr.count - 1){
            questionStr = [questionStr stringByAppendingString:[NSString stringWithFormat:@"%@",singleQuestionStr]];
        } else {
            questionStr = [questionStr stringByAppendingString:[NSString stringWithFormat:@"%@|",singleQuestionStr]];
        }
    }
    
    if (questionStr.length){
        [params setValue:questionStr forKey:@"answer"];
    }
    [[NetworkAdapter sharedAdapter] fetchWithGetPath:home_subexam requestParams:params responseObjectClass:[TSHomeDetailQuestionSingleModel class] succeededBlock:^(BOOL isSucceeded, id responseObject, NSError *error) {
        if (!weakSelf){
            return ;
        }
        if (isSucceeded){
            TSHomeDetailQuestionSingleModel *detailModel = (TSHomeDetailQuestionSingleModel *)responseObject;
            if (block){
                block(detailModel.score);
            }
        }
    }];
}


@end
