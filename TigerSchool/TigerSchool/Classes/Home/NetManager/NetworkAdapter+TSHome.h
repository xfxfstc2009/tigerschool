//
//  NetworkAdapter+TSHome.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/14.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "NetworkAdapter.h"
#import "TSHomeAllClassTagsModel.h"
#import "TSHomeLiveModel.h"                     /**< 内容*/
#import "TSCoursedescModel.h"                   /**< 课程简介*/
#import "TSHomeBannerModel.h"
#import "TSHomeSearchModel.h"
#import "TSHomeCourseDetailClassModel.h"
#import "GWDropListView.h"
#import "TSHomeDetailQuestionSingleModel.h"

@interface NetworkAdapter (TSHome)
// 1. 获取首页banner
-(void)homeBannerFetchWithBlock:(void(^)(TSHomeBannerListModel *listModel))block;
// 【获取所有的课程-标签】
-(void)homeAllClassTagsWithBlock:(void(^)(TSHomeAllClassTagsModel *singleModle))block;
// 【获取前三节课的推荐】
-(void)homeCourselistWithBlock:(void(^)(TSHomeLiveListModel *list))block;
// 【课程分类】
-(void)homeTypelimitWithBlock:(void(^)(TSHomeAllClassTagsModel *singleModle))block;
// 【课程简介】
-(void)homeCoursedescWithId:(NSString *)courId block:(void(^)(TSCoursedescModel *courseDescModel))block;
// 【获取首页课程列表分页】
-(void)homeCourselimitWithHomePage:(NSInteger)page block:(void(^)(TSHomeLiveListModel *list))block;
// 【获取课程列表分页】
-(void)homeCourselimitWithPage:(NSInteger)page type:(NSString *)type block:(void(^)(TSHomeLiveListModel *list))block;
// 获取搜索历史
-(void)homeSearchListWithBlock:(void(^)(TSHomeSearchModel *list))block;
// 获取课程内容
-(void)homeCourseDetailWithId:(NSString *)courseId block:(void(^)(TSHomeCourseDetailListClassModel *listModel))block;
// 进行搜索
-(void)homeSearchDetailInfoWithType:(SearchType)type key:(NSString *)key block:(void(^)(TSHomeLiveListModel *listModel))block;
// 提交答案信息
-(void)sendRequestToQuestionManagerWithDetailId:(NSString *)detailId questionArr:(NSArray<TSHomeCourseDetailExamitemsSingleModel> *)questionArr block:(void(^)(NSInteger score))block;
@end
