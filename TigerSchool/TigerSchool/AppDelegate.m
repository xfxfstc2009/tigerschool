//
//  AppDelegate.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/12.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "AppDelegate.h"
#import "TSLoginSuccessAnimationViewController.h"
#import "TSMTAManager.h"

@interface AppDelegate ()

@end

@implementation AppDelegate


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self mainSetUp];               // 1. 主视图创建
    [[UITextField appearance] setTintColor:[UIColor colorWithCustomerName:@"红"]];
    [TSMTAManager MTAInit];                 // 友盟埋点

#ifdef DEBUG        // 测试
    
    #else               // 线上
    
    #endif
    return YES;
}


- (void)applicationWillResignActive:(UIApplication *)application {
//    [[NSNotificationCenter defaultCenter]postNotificationName:PDAppdelegateWillResignNotification object:nil userInfo:nil];

    NSLog(@"将要进入后台");
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    NSLog(@"已经进入后台");
}


- (void)applicationWillEnterForeground:(UIApplication *)application {

}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}


#pragma mark - 1.主视图
-(void)mainSetUp{
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor clearColor];
    [self.window makeKeyAndVisible];
    [UIApplication sharedApplication].statusBarHidden = NO;
    PDSideMenu *meni = [[PDSideMenu alloc]init];
    self.window.rootViewController = [meni setupSlider];
    
    
    if ([[AccountModel sharedAccountModel] hasMemberLogined]){
        [self createLaungch];
    } else {
        [[HomeRootViewController sharedCenterController] homeRootInterfaceManagerWithLogin];
    }
}

#pragma mark - 2. 创建laungch视图
-(void)createLaungch{
    TSLoginSuccessAnimationViewController *launchVC = [[TSLoginSuccessAnimationViewController alloc] init];
    [self.window addSubview:launchVC.view];
}


@end
