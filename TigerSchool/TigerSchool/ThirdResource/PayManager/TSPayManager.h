//
//  TSPayManager.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/18.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"
#import "TSApplePayModel.h"


typedef NS_ENUM(NSInteger,PayType) {
    PayTypeBop,         /**< bop 支付*/
};

@interface TSPayManager : FetchModel

+(instancetype)sharedPayManager;

-(void)applePayManagerWithType:(PayType)payType identify:(NSString *)identify block:(void(^)(BOOL isSuccessed,TSApplePayModel *successedModel))block;
@end
