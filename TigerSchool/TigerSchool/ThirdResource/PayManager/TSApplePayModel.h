//
//  TSApplePayModel.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/19.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "FetchModel.h"

@protocol TSApplePayReceiptIn_AppModel <NSObject>

@end

@interface TSApplePayReceiptIn_AppModel : FetchModel

@property (nonatomic,copy)NSString *quantity;
@property (nonatomic,copy)NSString *transaction_id;

@end

@interface TSApplePayModel : FetchModel

@property (nonatomic,strong)NSArray<TSApplePayReceiptIn_AppModel> *in_app;

@end







