//
//  TSPayManager.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/18.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSPayManager.h"
#import <IAPShare.h>
#import "SBJSON.h"
@implementation TSPayManager

+(instancetype)sharedPayManager{
    static TSPayManager *_sharedPayManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedPayManager = [[TSPayManager alloc] init];
    });
    return _sharedPayManager;
}


-(void)applePayManagerWithType:(PayType)payType identify:(NSString *)identify block:(void(^)(BOOL isSuccessed,TSApplePayModel *successedModel))block{
    NSSet* dataSet = [[NSSet alloc] initWithObjects:identify,@"com.tigerschool.class88", nil];
    
    [IAPShare sharedHelper].iap = [[IAPHelper alloc] initWithProductIdentifiers:dataSet];
    // 客户端做收据校验有用  服务器做收据校验忽略...
#ifdef DEBUG        // 测试
    [IAPShare sharedHelper].iap.production = NO;
#else               // 线上
    [IAPShare sharedHelper].iap.production = YES;
#endif
    
    [[IAPShare sharedHelper].iap requestProductsWithCompletion:^(SKProductsRequest *request, SKProductsResponse *response) {
        NSLog(@"无效的产品product ID = %@",response.invalidProductIdentifiers);
        if(response.products.count > 0) {
            SKProduct *product = [response.products objectAtIndex:0];
            [[IAPShare sharedHelper].iap buyProduct:product onCompletion:^(SKPaymentTransaction *transcation) {
                if (transcation.error){         // 表示有报错
                    switch (transcation.error.code) {
                        case
                        SKErrorUnknown:
                            [StatusBarManager statusHiddenWithString:@"未知的错误，您可能正在使用越狱手机。"];
                            break;
                        case
                        SKErrorClientInvalid:
                            [StatusBarManager statusHiddenWithString:@"当前苹果账户无法购买商品(如有疑问，可以询问苹果客服)"];
                            break;
                        case
                        SKErrorPaymentCancelled:
                            [StatusBarManager statusHiddenWithString:@"订单已取消"];
                            break;
                        case
                        SKErrorPaymentInvalid:
                            [StatusBarManager statusHiddenWithString:@"订单无效(如有疑问，可以询问苹果客服)"];
                            break;
                        case
                        SKErrorPaymentNotAllowed:
                            [StatusBarManager statusHiddenWithString:@"当前苹果设备无法购买商品(如有疑问，可以询问苹果客服)"];
                            break;
                        default:
                            [StatusBarManager statusHiddenWithString:@"出现未知错误(如有疑问，可以询问苹果客服)"];
                            break;
                    }
                    
                    if (block){
                        block(NO,nil);
                    }
                } else {
                    if (transcation.transactionState == SKPaymentTransactionStatePurchased){
                        [StatusBarManager statusShowWithString:@"正在请求苹果服务器拉取支付结果"];
                        NSData *receipt = [NSData dataWithContentsOfURL:[[NSBundle mainBundle] appStoreReceiptURL]];
                        [[IAPShare sharedHelper].iap checkReceipt:receipt onCompletion:^(NSString *response, NSError *error) {
                            NSLog(@"购买验证---%@",response);
                            SBJSON *json = [[SBJSON alloc] init];
                            NSDictionary *responseDic = [json objectWithString:response error:nil];
                            TSApplePayModel *model = [(TSApplePayModel *)[TSApplePayModel alloc]initWithJSONDict:[responseDic objectForKey:@"receipt"]];
                            
                            if (block){
                                block(YES,model);
                            }
                        }];
                    } else if (transcation.transactionState == SKPaymentTransactionStateFailed){
                        [StatusBarManager statusHiddenWithString:@"当前课程支付失败"];
                        if (block){
                            block(NO,nil);
                        }
                    }
                }
            }];
        }
    }];
}




@end
