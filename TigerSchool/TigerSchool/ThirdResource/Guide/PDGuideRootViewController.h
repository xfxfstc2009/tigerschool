//
//  PDGuideRootViewController.h
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/21.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "AbstractViewController.h"
#import "PDGuideSingleModel.h"


@protocol PDGuideRootViewControllerDelegate <NSObject>

-(void)actionClickWithGuide:(PDGuideSingleModel *)guideSingleModel;
-(void)actionDismissGuide;                                              /**< 停止引导*/
@end


@interface PDGuideRootViewController : AbstractViewController

// 相关属性

@property (nonatomic,weak)id<PDGuideRootViewControllerDelegate> delegate;


- (void)showInView:(UIViewController *)viewController withViewActionArr:(NSArray *)viewArr;
- (void)dismissFromView:(UIViewController *)viewController;
-(void)sheetViewDismiss;

#pragma mark - Home
-(void)homeActionClickToPlayerBlock:(void(^)())block;


@end
