//
//  PDGuideSingleModel.m
//  PandaKing
//
//  Created by 裴烨烽 on 2016/12/22.
//  Copyright © 2016年 PandaOL. All rights reserved.
//

#import "PDGuideSingleModel.h"

@implementation PDGuideSingleModel


-(void)actionClickWithBlock:(void(^)())block{
    if (block){
        block();
    }
}

@end
