//
//  TSMTAManager.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSInteger,MTAType) {
    MTAType_Wel_Register,                       /**< 欢迎页面注册*/
    MTAType_Wel_Login,                          /**< 欢迎页面登录*/
    MTAType_Login_Register_Reg_Next,            /**< 注册下一步*/
    MTAType_Login_Register_Reg_YZM,             /**< 验证码*/
    MTAType_Login_Register,                     /**< 注册*/
    MTAType_Login_PWDLogin,                     /**< 密码登录*/
    MTAType_Login_SMSLogin,                     /**< 验证码登录*/
    MTAType_Login_ResetPwd,                     /**< 忘记密码*/
    MTAType_Login_ResetPwdGetSMS,               /**< 获取验证码*/
    MTAType_Login_ForgetSMS,                    /**< */
    MTAType_Login_ForgetLogin,
    // 主界面
    MTAType_Home_Root,                          /**< 首页点击*/
    MTAType_Home_Teach,                          /**< 课程点击*/
    MTAType_Home_Center,                          /**< 个人中心点击*/
    MTAType_Home_Banner,                        /**< 首页点击banner*/
    MTAType_Home_TagsALL,                       /**< 首页点击热门标签全部*/
    MTAType_Home_Tags,                          /**< 首页点击热门标签*/
    MTAType_Home_JingpinClass,                  /**< 首页精品课程*/
    
    // 工具页
    MTAType_Tool_Bop,                           /**< Bop*/
    MTAType_Tool_BopSearch,                     /**< Bop搜索*/
    MTAType_Tool_Sucai_All,                     /**< 全部素材*/
    MTAType_Tool_Sucai_Type,                    /**< 素材类型*/
    MTAType_Tool_Sucai_Direct,                  /**< 素材跳转*/
    MTAType_Tool_BopBuy,                        /**< bop 购买*/
    MTAType_Tool_BopInClass,                    /**< bop 进入课程*/
    MTAType_Tool_BopDetail,                     /**< bop 介绍*/
    MTAType_Tool_BopClass,                      /**< bop 课程*/
    MTAType_Tool_Bop_Beiyibei,                  /**< bop 背一背*/
    MTAType_Tool_Bop_ReBeiyibei,                /**< bop 重新背一背*/
    MTAType_Tool_Bop_Next,                      /**< bop 下一段*/
    
    // 搜索
    MTAType_Search_back,                        /**< 搜索*/
    MTAType_Search_Hot,                         /**< 热门词汇*/
    MTAType_Search_History,                     /**< 历史*/
    
    // end
    MTAType_Center_ygkc,                        /**< 已购课程*/
    MTAType_Center_gkls,                        /**< 观看历史*/
    MTAType_Center_wdgj,                        /**< 我的工具*/
    MTAType_Center_gywm,                        /**< 关于我们*/
    MTAType_Center_Setting,                     /**< 设置*/
    
    // 课程
    MTAType_Class_SegmentClass,                 /**< 课程详情-点击课程*/
    MTAType_Class_SegmentDetail,                 /**< 课程详情-点击介绍*/
    MTAType_Class_Buy,                          /**< 课程点击购买*/
    MTAType_Class_ZC,                           /**< 课程自测*/
    MTAType_Class_ZCTJ,                         /**< 课程自测提交*/
    MTAType_Class_ZC_Result,                    /**< 课程结果*/
};


@interface TSMTAManager : NSObject

+(void)MTAInit;

+ (void)MTAevent:(MTAType)type str:(NSString *)str;

@end
