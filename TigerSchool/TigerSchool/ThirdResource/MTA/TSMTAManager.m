//
//  TSMTAManager.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/4/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSMTAManager.h"

static NSString *back = @"0_0_0_0";

/**< Welcom*/
static NSString *Wel_reg = @"1_1_1_1";                   /**< 启动页面的注册*/
static NSString *Wel_login = @"1_1_1_2";                 /**< 启动页面的登录*/
/** *注册 */
static NSString *Reg_Next=@"1_1_2_2";                    /**< 下一步*/
static NSString *Reg_YZM=@"1_1_2_3";                     /**< 获取验证码*/
static NSString *Reg_zc=@"1_1_2_4";                      /**< 注册*/
/**< 登录*/
static NSString *Log_mem_dl = @"1_1_3_1";                   /**< 密码登录*/
static NSString *Log_yzm_dl = @"1_1_3_2";                   /**< 验证码登录*/
static NSString *Log_forget__bt = @"1_1_3_3";               /**< 忘记密码*/
static NSString *Log_yzm=@"1_1_3_4";                        /**< 获取验证码*/
/**< 忘记密码*/
static NSString *Forget_yzm = @"1_1_4_1";                    /**< 忘记密码 验证码 第一个*/
static NSString *Forget_cz_dl = @"1_1_4_1";                  /**< 登录*/
/* * 主界面 * */
static NSString *Home_bottom_kc=@"1_2_1_1";                // 课程
static NSString *Home_bottom_gj=@"1_2_1_2";                // 工具
static NSString *Home_bottom_wd=@"1_2_1_3";                // 我的
static NSString *Home_bottom_banner=@"1_2_1_4";            // banner 推荐 第一页
static NSString *Home_hot_label_All=@"1_2_1_6";                // 全部
static NSString *Home_hot_label=@"1_2_1_7";                // 热门标签
static NSString *Home_tj=@"1_2_1_8";                       // 精品课推荐
static NSString *Home_bop_banner = @"1_2_1_9";             // Bop 工具banner 第二页
static NSString *Search_back=@"1_3_1_1";
static NSString *Search_Hot=@"1_3_1_3";                    //热词搜索_点击
static NSString *Search_History=@"1_3_1_5";                //历史搜索_点击
/**< Bop 相关*/
static NSString *Bop_search = @"1_4_1_0";                   // bop 搜索
/**< 全部素材*/
static NSString *Bop_sc_all=@"1_4_1_2";                     // 素材全部
static NSString *Bop_type=@"1_4_2_1";// 素材类型
static NSString *Bop_sc_jump=@"1_4_2_2";//素材跳转
/**Bop 购买*/
static NSString *Bop_buy=@"1_4_4_1";// bop 购买
static NSString *Bop_bug_next=@"1_4_4_2";// bop 进入bop课程
static NSString *Bop_nr_js=@"1_4_5_2";// Bop 课程内容介绍
static NSString *Bop_nr_kc=@"1_4_5_3";// 对应的bop 课程
/** <背一背*/
static NSString *Bop_byb=@"1_4_7_1";// 背一背
static NSString *Bop_cx_byb=@"1_4_7_2";// 重新背一背
static NSString *Bop_next_bop=@"1_4_7_3";//下一段

/**< 课程相关*/
static NSString *searcher=@"1_6_1_0";//搜索
static NSString *label= @"1_6_1_1";//课程类型
static NSString *kc_detail= @"1_6_1_2";//点击课程
static NSString *kc_bug = @"1_6_1_3";//课程购买
static NSString *kc_js= @"1_6_1_4";//介绍
    
static NSString *kc_zc = @"1_6_1_5";//自测_
static NSString *kc_zc_tj = @"1_6_1_6";//提交
static NSString *kc_zc_result = @"1_6_1_7";// 课程自测详情 查询按钮
static NSString *detail_kc = @"1_6_1_8"; //详情：课程选中
static NSString *detail_js=@""; //详情：介绍选种

static NSString *ygkc=@"1_5_1_1";               //易购课程
static NSString *gkls=@"1_5_1_2";               //观看历史
static NSString *wdgj=@"1_5_1_3";               //我的工具
static NSString *gywm=@"1_5_1_4";               //关于我们
static NSString *sz=@"1_5_1_4";                 //设置

@implementation TSMTAManager




+(void)MTAInit{
    
}

+ (void)MTAevent:(MTAType)type str:(NSString *)str{
   
}



@end
