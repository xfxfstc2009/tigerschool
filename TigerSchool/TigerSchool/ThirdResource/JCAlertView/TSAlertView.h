//
//  TSAlertView.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TSTeachBopDetailSingleModel.h"

typedef NS_ENUM(NSInteger ,AlertType) {
    AlertTypePay,
    AlertTypeNormal,
};

@interface TSAlertView : NSObject

+(instancetype)sharedAlertView;                                                               /**< 单例*/

-(void)showAlertWithType:(AlertType)type model:(TSTeachBopDetailSingleModel *)bopDetailModel block:(void(^)(BOOL isSuccessed))block;
@end
