//
//  TSAlertView.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/23.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSAlertView.h"
#import <objc/runtime.h>
#import "JCAlertView.h"
#import "NetworkAdapter+TSTeach.h"
#import "TSPayManager.h"

@interface TSAlertView()<UITextFieldDelegate>
@property (nonatomic,strong)JCAlertView *showAlert;
@end

@implementation TSAlertView

+(instancetype)sharedAlertView{
    static TSAlertView *_sharedAlertView = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _sharedAlertView = [[TSAlertView alloc] init];
    });
    return _sharedAlertView;
}

-(void)dismissAllWithBlock:(void(^)())block{
    __weak typeof(self)weakSelf = self;
    [JCAlertView dismissAllCompletion:^{
        if (!weakSelf){
            return ;
        }
        if (block){
            block();
        }
    }];
}

#pragma mark - 显示ppp
-(void)showAlertWithType:(AlertType)type model:(TSTeachBopDetailSingleModel *)bopDetailModel block:(void(^)(BOOL isSuccessed))block{
    bopDetailModel.currentPrice = bopDetailModel.price;
    
    // 添加键盘代理
    [self createNotifi];
    
    
    UIView *mainBgView = [[UIView alloc]init];
    mainBgView.frame = CGRectMake(0, 0, kScreenBounds.size.width - 2 * TSFloat(42) + TSFloat(15), TSFloat(710) / 2. + TSFloat(15));
    mainBgView.backgroundColor = [UIColor clearColor];
    
    UIView *bgView = [[UIView alloc]init];
    bgView.backgroundColor = [UIColor whiteColor];
    bgView.frame = CGRectMake(0, TSFloat(15), kScreenBounds.size.width - 2 * TSFloat(42), TSFloat(710) / 2.);
    bgView.layer.cornerRadius = TSFloat(10);
    bgView.clipsToBounds = YES;
    [mainBgView addSubview:bgView];
    
    // 1. 创建图片
    TSImageView *topImageView = [[TSImageView alloc]init];
    topImageView.backgroundColor = UURandomColor;
    topImageView.image = [UIImage imageNamed:@"bg_payTop"];
    topImageView.frame = CGRectMake(0, 0, bgView.size_width, TSFloat(290) / 2.);
    [bgView addSubview:topImageView];

    // 2. 创建scrollview
    UIScrollView *bottomScrollView = [UIScrollView createScrollViewScrollEndBlock:NULL];
    bottomScrollView.frame = CGRectMake(0, CGRectGetMaxY(topImageView.frame), bgView.size_width, bgView.size_height - CGRectGetMaxY(topImageView.frame));
    bottomScrollView.scrollEnabled = NO;
    bottomScrollView.contentSize = CGSizeMake(bottomScrollView.size_width * 2, bottomScrollView.size_height);
    [bgView addSubview:bottomScrollView];
    
    UIButton *cancelButton = [UIButton buttonWithType:UIButtonTypeCustom];
    cancelButton.frame = CGRectMake(bgView.size_width - TSFloat(20), TSFloat(5), TSFloat(30), TSFloat(30));
    [cancelButton setBackgroundImage:[UIImage imageNamed:@"icon_pay_cancel"] forState:UIControlStateNormal];
    __weak typeof(self)weakSelf = self;
    [cancelButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [[TSAlertView sharedAlertView] dismissAllWithBlock:NULL];
    }];
    [mainBgView addSubview:cancelButton];
    
    // 3. 创建view
    UIView *page1View = [[UIView alloc]init];
    page1View.frame = bottomScrollView.bounds;
    [bottomScrollView addSubview:page1View];
    
    // 3.1 创建标题
    UILabel *payFixedLabel = [TSViewTool createLabelFont:@"正文" textColor:@"黑"];
    payFixedLabel.textAlignment = NSTextAlignmentCenter;
    payFixedLabel.text = @"待支付金额";
    payFixedLabel.frame = CGRectMake(0, TSFloat(29), page1View.size_width, [NSString contentofHeightWithFont:payFixedLabel.font]);
    [page1View addSubview:payFixedLabel];

    // 3.2 创建原价
    UILabel *yuanLabel = [TSViewTool createLabelFont:@"正文" textColor:@"浅灰"];
    yuanLabel.textAlignment = NSTextAlignmentCenter;
    yuanLabel.text = [NSString stringWithFormat:@"￥%.2f",bopDetailModel.price];
    yuanLabel.frame = CGRectMake(0, CGRectGetMaxY(payFixedLabel.frame), bgView.size_width, [NSString contentofHeightWithFont:yuanLabel.font]);
    [page1View addSubview:yuanLabel];
    
    //3.3 创建现价
    UILabel *xianLabel = [TSViewTool createLabelFont:@"22" textColor:@"红"];
    xianLabel.font = [[UIFont systemFontOfCustomeSize:36.f]boldFont];
    xianLabel.textAlignment = NSTextAlignmentCenter;
    xianLabel.text = [NSString stringWithFormat:@"￥%.2f",bopDetailModel.currentPrice];
    xianLabel.frame = CGRectMake(0, CGRectGetMaxY(yuanLabel.frame) + TSFloat(12), bgView.size_width, [NSString contentofHeightWithFont:xianLabel.font]);
    xianLabel.adjustsFontSizeToFitWidth = YES;
    [page1View addSubview:xianLabel];
    
    // 3.4 创建
    UIButton *payButton = [UIButton buttonWithType:UIButtonTypeCustom];
    payButton.frame = CGRectMake(TSFloat(12), CGRectGetMaxY(xianLabel.frame) + TSFloat(20), bgView.size_width - 2 * TSFloat(12), TSFloat(44));
    [payButton setBackgroundImage:[TSTool stretchImageWithName:@"icon_button_hlt"] forState:UIControlStateNormal];
    
    [payButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [payButton setTitle:@"支付" forState:UIControlStateNormal];
    [payButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    payButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16.];
    [page1View addSubview:payButton];
    
    // 3.5 创建优惠吗
    UIButton *youhuiButton = [UIButton buttonWithType:UIButtonTypeCustom];
    youhuiButton.frame = CGRectMake(TSFloat(12), CGRectGetMaxY(payButton.frame) , bgView.size_width - 2 * TSFloat(12), page1View.size_height - CGRectGetMaxY(payButton.frame));
    [youhuiButton setTitle:@"输入优惠码>" forState:UIControlStateNormal];
    youhuiButton.titleLabel.font = [UIFont systemFontOfCustomeSize:14.];
    [youhuiButton setTitleColor:[UIColor colorWithCustomerName:@"浅灰"] forState:UIControlStateNormal];
    [youhuiButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [bottomScrollView setContentOffset:CGPointMake(bgView.size_width, 0) animated:YES];
    }];
    [page1View addSubview:youhuiButton];
    
    // 创建view 2
    UIView *pageView2 = [[UIView alloc]init];
    pageView2.frame = page1View.bounds;
    pageView2.orgin_x = bgView.size_width;
    [bottomScrollView addSubview:pageView2];
    
    // fixedLabel2
    UILabel *yaoqingFixedlabel = [TSViewTool createLabelFont:@"小正文" textColor:@"浅灰"];
    yaoqingFixedlabel.text = @"输入优惠码";
    yaoqingFixedlabel.frame = CGRectMake(TSFloat(13), TSFloat(20), pageView2.size_width - 2 * TSFloat(13), [NSString contentofHeightWithFont:yaoqingFixedlabel.font]);
    [pageView2 addSubview:yaoqingFixedlabel];

    // 创建bottomView
    UIView *bottomAlphaView = [[UIView alloc]init];
    bottomAlphaView.frame = CGRectMake(TSFloat(12),CGRectGetMaxY(yaoqingFixedlabel.frame) + TSFloat(12), pageView2.size_width - 2 * TSFloat(12), TSFloat(60));
    bottomAlphaView.layer.cornerRadius = TSFloat(3);
    bottomAlphaView.clipsToBounds = YES;
    bottomAlphaView.backgroundColor = [UIColor colorWithCustomerName:@"白灰"];
    [pageView2 addSubview:bottomAlphaView];
    
    // 2. 创建输入框
    UITextField *inputTextField = [[UITextField alloc]init];
    inputTextField.backgroundColor = [UIColor clearColor];
    inputTextField.userInteractionEnabled = YES;
    inputTextField.delegate = self;
    inputTextField.font = [[UIFont systemFontOfCustomeSize:24.]boldFont];
    inputTextField.textAlignment = NSTextAlignmentCenter;
    inputTextField.frame = bottomAlphaView.bounds;
    [bottomAlphaView addSubview:inputTextField];
 
    UIButton *leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [leftButton setTitle:@"取消" forState:UIControlStateNormal];;
    leftButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16.];
    [leftButton setTitleColor:[UIColor hexChangeFloat:@"666666"] forState:UIControlStateNormal];
    leftButton.frame = CGRectMake(TSFloat(22), CGRectGetMaxY(bottomAlphaView.frame) + TSFloat(19), TSFloat(113), TSFloat(44));
    leftButton.backgroundColor = [UIColor hexChangeFloat:@"F0F0F0"];
    leftButton.clipsToBounds = YES;
    leftButton.layer.cornerRadius = MIN(leftButton.size_width, leftButton.size_height) / 2.;
    [pageView2 addSubview:leftButton];
    [leftButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        [inputTextField resignFirstResponder];
        [bottomScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
    }];
    
    
    
    UIButton *rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
    rightButton.frame = CGRectMake(pageView2.size_width - TSFloat(22) - leftButton.size_width, CGRectGetMaxY(bottomAlphaView.frame) + TSFloat(19), TSFloat(113), TSFloat(44));
    [rightButton setTitle:@"确认" forState:UIControlStateNormal];;
    rightButton.titleLabel.font = [UIFont systemFontOfCustomeSize:16.];
    [rightButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    rightButton.backgroundColor = [UIColor hexChangeFloat:@"EB514B"];
    rightButton.clipsToBounds = YES;
    rightButton.layer.cornerRadius = MIN(leftButton.size_width, leftButton.size_height) / 2.;
    [pageView2 addSubview:rightButton];
    
    [rightButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [inputTextField resignFirstResponder];
        [strongSelf sendRequestToYanzhengCouponHasUsed:inputTextField.text courseId:bopDetailModel.tempCourId block:^(CGFloat price) {
            if (price != -1){
                bopDetailModel.currentPrice = price;
                xianLabel.text = [NSString stringWithFormat:@"￥%.2f",bopDetailModel.currentPrice];
                [bottomScrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            }
        }];
    }];
    
    
    // 支付按钮点击
    [payButton buttonWithBlock:^(UIButton *button) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf sendRequestToWillPay:inputTextField.text courseId:bopDetailModel.tempCourId block:^(BOOL isSuccessed, TSAddWillPayOrderModel *model) {
            if (block){
                block(isSuccessed);
            }
        }];
    }];
    
    self.showAlert =  [JCAlertView initWithCustomView:mainBgView dismissWhenTouchedBackground:NO];
}

#pragma mark - 验证优惠券是否可用
-(void)sendRequestToYanzhengCouponHasUsed:(NSString *)coupon courseId:(NSString *)courseId block:(void(^)(CGFloat price))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachCheckCouponHasUsedCoupon:coupon courseId:courseId block:^(BOOL isSuccessed, CGFloat newPrice) {
        if (!weakSelf){
            return ;
        }
        if (isSuccessed){
            if (block){
                block(newPrice);
            }
        } else {
            if (block){
                block(-1);
            }
        }
    }];
}

#pragma mark - 进行预支付
-(void)sendRequestToWillPay:(NSString *)coupon courseId:(NSString *)courseId block:(void(^)(BOOL isSuccessed,TSAddWillPayOrderModel *model))block{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachAddpayorderCourseId:courseId coupon:coupon block:^(TSAddWillPayOrderModel *model) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [PDHUD showHUDBindingProgress:@"正在向苹果服务器申请支付订单……" diary:0];
        if (model){
            [[TSAlertView sharedAlertView] dismissAllWithBlock:NULL];
            [strongSelf payManagerWithModel:model payBackBlock:^(BOOL successed, TSAddWillPayOrderModel *successedModel) {
                if (block){
                    block(successed,successedModel);
                }
            }];
        }
    }];
}

#pragma mark - 进行支付回调
-(void)sendRequestPayBack:(TSAddWillPayOrderModel *)model backBlock:(void(^)(BOOL isSuccessed))backBlock{
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] teachPayBackDevice:[AccountModel sharedAccountModel].keychain outtradeno:model.outtradeno transactionid:model.transactionid userid:[AccountModel sharedAccountModel].accountId block:^(BOOL isSuccessed) {
        if (!weakSelf){
            return ;
        }
        if (isSuccessed){
            if (backBlock){
                backBlock(YES);
            }
        } else {
            if (backBlock){
                backBlock(NO);
            }
        }
    }];
}

#pragma mark 进行支付
-(void)payManagerWithModel:(TSAddWillPayOrderModel *)model payBackBlock:(void(^)(BOOL successed, TSAddWillPayOrderModel *successedModel))block{
    __weak typeof(self)weakSelf = self;
    
    [[TSPayManager sharedPayManager] applePayManagerWithType:PayTypeBop identify:model.identify block:^(BOOL isSuccessed, TSApplePayModel *successedModel) {
        if(!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        if (isSuccessed){
            [PDHUD showHUDBindingProgress:@"支付成功" diary:2];
            TSApplePayReceiptIn_AppModel *in_AppModel = [successedModel.in_app lastObject];
            model.transactionid = in_AppModel.transaction_id;
            [strongSelf sendRequestPayBack:model backBlock:^(BOOL backIsSuccessed) {
                if (block){
                    block(backIsSuccessed,model);
                }
            }];
        }
    }];
}





#pragma mark - 通知
#pragma mark 手势通知
-(void)createNotifi{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}

#pragma mark 键盘通知
- (void)keyboardWillShow:(NSNotification *)notification {
    
    NSDictionary *userInfo = [notification userInfo];
    
    NSValue* aValue = [userInfo objectForKey:UIKeyboardFrameEndUserInfoKey];
    
    CGRect keyboardRect = [aValue CGRectValue];
    keyboardRect = [self.self.showAlert convertRect:keyboardRect fromView:nil];
    
    CGFloat keyboardTop = keyboardRect.origin.y;
    CGRect newTextViewFrame = self.showAlert.bounds;
    newTextViewFrame.size.height = keyboardTop - self.showAlert.bounds.origin.y;
    
    self.showAlert.center_y = kScreenBounds.size.height / 2. - 100;
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    [UIView commitAnimations];
}

#pragma mark 键盘通知
- (void)keyboardWillHide:(NSNotification *)notification {
    
    NSDictionary* userInfo = [notification userInfo];
    
    NSValue *animationDurationValue = [userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey];
    NSTimeInterval animationDuration;
    [animationDurationValue getValue:&animationDuration];
    
    self.showAlert.center_y = kScreenBounds.size.height / 2.;
    
    [UIView beginAnimations:nil context:NULL];
    [UIView setAnimationDuration:animationDuration];
    
    [UIView commitAnimations];
}

-(void)dealloc{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:nil object:nil];
}

-(void)viewDidUnload{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}


@end
