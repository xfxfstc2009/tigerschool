//
//  TSBaiduSoundsManager.h
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BDSEventManager.h"
#import "BDSASRDefines.h"
#import "BDSASRParameters.h"
#import "TSTeachBopResultModel.h"

@protocol TSBaiduSoundsManagerDelegate<NSObject>
-(void)soundsCallBackWithModelInfo:(TSTeachBopResultMainModel *)responseModelObject;
@end

@interface TSBaiduSoundsManager : NSObject
@property (strong, nonatomic) BDSEventManager *asrEventManager;
@property (nonatomic,weak)id<TSBaiduSoundsManagerDelegate> delegate;
-(void)soundsWithInit;              // 1.初始化

-(void)actionToRead;
-(void)actionToStop;
@end
