//
//  TSBaiduSoundsManager.m
//  TigerSchool
//
//  Created by 裴烨烽 on 2018/3/22.
//  Copyright © 2018年 百e国际. All rights reserved.
//

#import "TSBaiduSoundsManager.h"

const NSString* API_KEY = @"fjg25Aex4HbDWBjuBgox79jQ";
const NSString* SECRET_KEY = @"xiEK4Rk13FNFL3v1eMfyPxDtfE5325Ey";
const NSString* APP_ID = @"10957666";

@interface TSBaiduSoundsManager()
@property(nonatomic, assign) BOOL continueToVR;

@end
@implementation TSBaiduSoundsManager

#pragma mark 第一步初始化
-(void)soundsWithInit{
    self.asrEventManager = [BDSEventManager createEventManagerWithName:BDS_ASR_NAME];
    [self configVoiceRecognitionClient];
}

- (void)configVoiceRecognitionClient {
    // 1. 设置logo的级别
    [self.asrEventManager setParameter:@(EVRDebugLogLevelOff) forKey:BDS_ASR_DEBUG_LOG_LEVEL];
    // 2. 配置key
    [self.asrEventManager setParameter:@[API_KEY,SECRET_KEY] forKey:BDS_ASR_API_SECRET_KEYS];
    [self.asrEventManager setParameter:APP_ID forKey:BDS_ASR_OFFLINE_APP_CODE];
    // 3. 配置端点检测（二选一）
    [self configModelVAD];
    // 4. 开启语义理解
    [self enableNLU];
}

// 检测方式
- (void)configModelVAD {
    NSString *modelVAD_filepath = [[NSBundle mainBundle] pathForResource:@"bds_easr_basic_model" ofType:@"dat"];
    [self.asrEventManager setParameter:modelVAD_filepath forKey:BDS_ASR_MODEL_VAD_DAT_FILE];
    [self.asrEventManager setParameter:@(YES) forKey:BDS_ASR_ENABLE_MODEL_VAD];
}

// ---- 开启语义理解 -----
- (void) enableNLU {
    [self.asrEventManager setParameter:@(YES) forKey:BDS_ASR_ENABLE_NLU];
    [self.asrEventManager setParameter:@"1536" forKey:BDS_ASR_PRODUCT_ID];
}

-(void)actionToRead{
    
    [self.asrEventManager setParameter:@(YES) forKey:BDS_ASR_ENABLE_LONG_SPEECH];           // 长语音
    [self.asrEventManager setParameter:@(NO) forKey:BDS_ASR_NEED_CACHE_AUDIO];
    [self.asrEventManager setParameter:@"" forKey:BDS_ASR_OFFLINE_ENGINE_TRIGGERED_WAKEUP_WORD];
    [self voiceRecogButtonHelper];
}

-(void)actionToStop{
    [self.asrEventManager sendCommand:BDS_ASR_CMD_CANCEL];
}

- (void)voiceRecogButtonHelper {
    //    [self configFileHandler];
    [self.asrEventManager setDelegate:self];
    [self.asrEventManager setParameter:nil forKey:BDS_ASR_AUDIO_FILE_PATH];
    [self.asrEventManager setParameter:nil forKey:BDS_ASR_AUDIO_INPUT_STREAM];
    [self.asrEventManager sendCommand:BDS_ASR_CMD_START];
}



- (void)VoiceRecognitionClientWorkStatus:(int)workStatus obj:(id)aObj {
    switch (workStatus) {
        case EVoiceRecognitionClientWorkStatusNewRecordData: {
//            [self.fileHandler writeData:(NSData *)aObj];
            break;
        }
            
        case EVoiceRecognitionClientWorkStatusStartWorkIng: {
            NSDictionary *logDic = [self parseLogToDic:aObj];
            [self printLogTextView:[NSString stringWithFormat:@"CALLBACK: start vr, log: %@\n", logDic]];
            break;
        }
        case EVoiceRecognitionClientWorkStatusStart: {
            [self printLogTextView:@"CALLBACK: detect voice start point.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusEnd: {
            [self printLogTextView:@"CALLBACK: detect voice end point.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusFlushData: {
            [self printLogTextView:[NSString stringWithFormat:@"CALLBACK: partial result - %@.\n\n", [self getDescriptionForDic:aObj]]];
            break;
        }
        case EVoiceRecognitionClientWorkStatusFinish: {
            [self printLogTextView:[NSString stringWithFormat:@"CALLBACK: final result - %@.\n\n", [self getDescriptionForDic:aObj]]];
            if (aObj) {
//                NSLog(@"%@",[self getDescriptionForDic:aObj]);
            }
//            if (!self.longSpeechFlag) {
//                [self onEnd];
//            }
            break;
        }
        case EVoiceRecognitionClientWorkStatusMeterLevel: {
            break;
        }
        case EVoiceRecognitionClientWorkStatusCancel: {
//            [self printLogTextView:@"CALLBACK: user press cancel.\n"];
//            [self onEnd];
            break;
        }
        case EVoiceRecognitionClientWorkStatusError: {
//            [self printLogTextView:[NSString stringWithFormat:@"CALLBACK: encount error - %@.\n", (NSError *)aObj]];
//            [self onEnd];
            break;
        }
        case EVoiceRecognitionClientWorkStatusLoaded: {
//            [self printLogTextView:@"CALLBACK: offline engine loaded.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusUnLoaded: {
//            [self printLogTextView:@"CALLBACK: offline engine unLoaded.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusChunkThirdData: {
//            [self printLogTextView:[NSString stringWithFormat:@"CALLBACK: Chunk 3-party data length: %lu\n", (unsigned long)[(NSData *)aObj length]]];
            break;
        }
        case EVoiceRecognitionClientWorkStatusChunkNlu: {
//            NSString *nlu = [[NSString alloc] initWithData:(NSData *)aObj encoding:NSUTF8StringEncoding];
//            [self printLogTextView:[NSString stringWithFormat:@"CALLBACK: Chunk NLU data: %@\n", nlu]];
//            NSLog(@"%@", nlu);
            break;
        }
        case EVoiceRecognitionClientWorkStatusChunkEnd: {
//            [self printLogTextView:[NSString stringWithFormat:@"CALLBACK: Chunk end, sn: %@.\n", aObj]];
//            if (!self.longSpeechFlag) {
//                [self onEnd];
//            }
            break;
        }
        case EVoiceRecognitionClientWorkStatusFeedback: {
//            NSDictionary *logDic = [self parseLogToDic:aObj];
//            [self printLogTextView:[NSString stringWithFormat:@"CALLBACK Feedback: %@\n", logDic]];
            break;
        }
        case EVoiceRecognitionClientWorkStatusRecorderEnd: {
//            [self printLogTextView:@"CALLBACK: recorder closed.\n"];
            break;
        }
        case EVoiceRecognitionClientWorkStatusLongSpeechEnd: {
//            [self printLogTextView:@"CALLBACK: Long Speech end.\n"];
//            [self onEnd];
            break;
        }
        default:
            break;
    }
}

- (NSDictionary *)parseLogToDic:(NSString *)logString {
    NSArray *tmp = NULL;
    NSMutableDictionary *logDic = [[NSMutableDictionary alloc] initWithCapacity:3];
    NSArray *items = [logString componentsSeparatedByString:@"&"];
    for (NSString *item in items) {
        tmp = [item componentsSeparatedByString:@"="];
        if (tmp.count == 2) {
            [logDic setObject:tmp.lastObject forKey:tmp.firstObject];
        }
    }
    return logDic;
}

- (void)printLogTextView:(NSString *)logString {
//    NSLog(@"%@",logString);
}

- (NSString *)getDescriptionForDic:(NSDictionary *)dic {
    NSLog(@"%@",dic);
    if (self.delegate && [self.delegate respondsToSelector:@selector(soundsCallBackWithModelInfo:)]){
        TSTeachBopResultMainModel *responseModelObject = (TSTeachBopResultMainModel *)[[TSTeachBopResultMainModel alloc] initWithJSONDict:dic];
        [self.delegate soundsCallBackWithModelInfo:responseModelObject];
        return @"";
    }
    
//    if (dic) {
//        return [[NSString alloc] initWithData:[NSJSONSerialization dataWithJSONObject:dic
//                                                                              options:NSJSONWritingPrettyPrinted
//                                                                                error:nil] encoding:NSUTF8StringEncoding];
//    }
    return nil;
}
@end
