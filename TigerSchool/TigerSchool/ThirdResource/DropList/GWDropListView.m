//
//  GWDropListView.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "GWDropListView.h"
#import "DropListSingleCell.h"
#import <objc/runtime.h>
#import "TSHomeTeachSingleCell.h"
#import "NetworkAdapter+TSHome.h"

static char actionDropListClickWithTapManagerKey;
static char chooseKey;

@interface GWDropListView()<UITableViewDataSource,UITableViewDelegate>
@property (nonatomic,strong)NSMutableArray *listMutableArr;
@property (nonatomic,strong)UITableView *tableView;
@property (nonatomic,strong)HTHorizontalSelectionList *segmentList;
@property (nonatomic,assign)SearchType currentSearchType;                   /**< 当前搜索类型*/
@property (nonatomic,copy)NSString *searchKey;
@end

@implementation GWDropListView

-(instancetype)init{
    self = [super init];
    if (self){
        self.backgroundColor = [UIColor colorWithWhite:0. alpha:.1f];
    }
    return self;
}

-(void)showInViewController:(UIViewController *)controller frame:(CGRect)listFrame listArr:(NSArray *)listArr searchType:(SearchType)type block:(void(^)(SearchType searchType,id model))block{
    UIWindow *keywindow = [UIApplication sharedApplication].keyWindow;
    objc_setAssociatedObject(self, &chooseKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
    
    self.listMutableArr = [NSMutableArray array];
    _currentSearchType = type;
    
    self.frame = keywindow.bounds;
    
    CGFloat height = (listArr.count >= 3) ? 3 * [TSHomeTeachSingleCell calculationCellHeight] : listArr.count *  [TSHomeTeachSingleCell calculationCellHeight];
    CGFloat startY = listFrame.origin.y >= (keywindow.frame.size.height / 2) ? listFrame.origin.y : listFrame.origin.y + listFrame.size.height;
    CGFloat endY = listFrame.origin.y >= (keywindow.frame.size.height / 2) ? listFrame.origin.y - height : listFrame.origin.y + listFrame.size.height;
    CGFloat segmentEndY = endY;
    CGFloat tableEndY = endY += TSFloat(50);
    [self createSegmentListWithOrignY:segmentEndY];
    
    self.tableView = [[UITableView alloc] initWithFrame:CGRectMake(listFrame.origin.x, startY, listFrame.size.width, 0)  style:UITableViewStylePlain];
    [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        self.tableView.frame = CGRectMake(listFrame.origin.x, tableEndY, listFrame.size.width, height);
    } completion:nil];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    self.tableView.backgroundColor = [UIColor whiteColor];
    self.tableView.showsVerticalScrollIndicator = NO;
    self.tableView.scrollEnabled = YES;

    [self addSubview:self.tableView];
    
    
    if(self.listMutableArr.count){
        [self.tableView dismissPrompt];
    } else {
        [self.tableView showPrompt:@"当前没有搜索内容" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
    }
    
}

#pragma mark - UITableViewDataSource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return self.listMutableArr.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.currentSearchType == SearchTypeClass){             // 课程
        static NSString *cellIdentifyWithRowOne = @"cellIdentifyWithRowOne";
        TSHomeTeachSingleCell *cellWithRowOne = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowOne];
        if (!cellWithRowOne){
            cellWithRowOne = [[TSHomeTeachSingleCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowOne];
            cellWithRowOne.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        TSHomeLiveModel *listModel = [self.listMutableArr objectAtIndex:indexPath.row];
        cellWithRowOne.transferHomeLiveModel = listModel;
        return cellWithRowOne;
    } else if (self.currentSearchType == SearchTypeBop){        // bop
        static NSString *cellIdentifyWithRowTwo = @"cellIdentifyWithRowTwo";
        TSTeachMaterialRootSingleTableViewCell *cellWithRowTwo = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowTwo];
        if (!cellWithRowTwo){
            cellWithRowTwo = [[TSTeachMaterialRootSingleTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowTwo];
        }
        TSTeachRootSingleModel *singleModel = [self.listMutableArr objectAtIndex:indexPath.row];
        cellWithRowTwo.transferBopSingleModel = singleModel;
        return cellWithRowTwo;
    } else if (self.currentSearchType == SearchTypeMaterial){   // 素材
        static NSString *cellIdentifyWithRowFour = @"cellIdentifyWithRowFour";
        TSCenterMyToolTableViewCell *cellWithRowFour = [tableView dequeueReusableCellWithIdentifier:cellIdentifyWithRowFour];
        if (!cellWithRowFour){
            cellWithRowFour = [[TSCenterMyToolTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifyWithRowFour];
            cellWithRowFour.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        TSMaterialSegmentModel *singleModel = [self.listMutableArr objectAtIndex:indexPath.row];
        cellWithRowFour.transferSingleModel = singleModel;
        return cellWithRowFour;
    }
    return nil;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (self.currentSearchType == SearchTypeClass){             // 课程
        return [TSHomeTeachSingleCell calculationCellHeight];
    } else if (self.currentSearchType == SearchTypeBop){        // bop
        return [TSTeachMaterialRootSingleTableViewCell calculationCellHeight];
    } else if (self.currentSearchType == SearchTypeMaterial){   // 素材
        return [TSCenterMyToolTableViewCell calculationCellHeight];
    }
    return 44;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
  
    void(^block)(SearchType searchType,id model) = objc_getAssociatedObject(self, &chooseKey);
    if (block){
        block(self.currentSearchType,[self.listMutableArr objectAtIndex:indexPath.row]);
    }
    
    [NSTimer scheduledTimerWithTimeInterval:0.2 target:self selector:@selector(clickCell:) userInfo:nil repeats:NO];
}


-(void)clickCell:(NSTimer*) timer {
    [self removeFromSuperview];
    [timer invalidate];
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    void(^block)() = objc_getAssociatedObject(self, &actionDropListClickWithTapManagerKey);
    if (block){
        block();
    }
    [self removeFromSuperview];
}

-(void)createSegmentListWithOrignY:(CGFloat)originY{
    __weak typeof(self)weakSelf = self;
    if (!self.segmentList){
        self.segmentList = [HTHorizontalSelectionList createSegmentWithDataSource:@[@"课程",@"Bop",@"素材"] actionClickBlock:^(HTHorizontalSelectionList *segment, NSInteger index) {
            if (!weakSelf){
                return ;
            }
            __strong typeof(weakSelf)strongSelf = weakSelf;
            strongSelf.currentSearchType = (index + 1);
            [strongSelf interfaceWithSearch:strongSelf.searchKey.length?strongSelf.searchKey:@""];
        }];
    }
    self.segmentList.backgroundColor = [UIColor whiteColor];
    self.segmentList.frame = CGRectMake(0, originY, self.size_width, TSFloat(50));
    [self addSubview:self.segmentList];
}

-(void)actionDropListClickWithTapManager:(void(^)())block{
    objc_setAssociatedObject(self, &actionDropListClickWithTapManagerKey, block, OBJC_ASSOCIATION_COPY_NONATOMIC);
}

-(void)interfaceWithSearch:(NSString *)key{
    self.searchKey = key;
    __weak typeof(self)weakSelf = self;
    [[NetworkAdapter sharedAdapter] homeSearchDetailInfoWithType:self.currentSearchType key:key block:^(TSHomeLiveListModel *listModel) {
        if (!weakSelf){
            return ;
        }
        __strong typeof(weakSelf)strongSelf = weakSelf;
        [strongSelf.listMutableArr removeAllObjects];
        if (strongSelf.currentSearchType == SearchTypeClass){               // 课程
            [strongSelf.listMutableArr addObjectsFromArray:listModel.items];
        } else if (strongSelf.currentSearchType == SearchTypeBop){          // Bop
            for (TSHomeLiveModel *liveSingleModel in listModel.items){
                TSTeachRootSingleModel *bopModel = [[TSTeachRootSingleModel alloc]init];
                bopModel.ID = liveSingleModel.ID;
                bopModel.img = liveSingleModel.img;
                bopModel.name = liveSingleModel.name;
                [strongSelf.listMutableArr addObject:bopModel];
            }
        } else if (strongSelf.currentSearchType == SearchTypeMaterial){     // 素材
            for (TSHomeLiveModel *liveSingleModel in listModel.items){
                TSMaterialSegmentModel *bopModel = [[TSMaterialSegmentModel alloc]init];
                bopModel.ID = liveSingleModel.ID;
                bopModel.img = liveSingleModel.img;
                bopModel.name = liveSingleModel.name;
                bopModel.type = liveSingleModel.type;
                bopModel.url = liveSingleModel.url;
                [strongSelf.listMutableArr addObject:bopModel];
            }
        }
        
        [strongSelf.tableView reloadData];
        
        if(strongSelf.listMutableArr.count){
            [strongSelf.tableView dismissPrompt];
        } else {
            [strongSelf.tableView showPrompt:@"当前没有搜索内容" withImage:nil andImagePosition:PDPromptImagePositionNone tapBlock:NULL];
        }
    }];
}

@end
