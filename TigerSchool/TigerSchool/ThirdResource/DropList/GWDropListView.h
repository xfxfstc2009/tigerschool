//
//  GWDropListView.h
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TSTeachRootModel.h"
#import "TSTeachMaterialRootSingleTableViewCell.h"
#import "TSMaterialSegmentModel.h"
#import "TSCenterMyToolTableViewCell.h"

typedef NS_ENUM(NSInteger,SearchType) {
    SearchTypeClass = 1,                /**< 课程*/
    SearchTypeBop = 2,                  /**< Bop*/
    SearchTypeMaterial = 3,             /**< 素材*/
};


@interface GWDropListView : UIView

-(void)showInViewController:(UIViewController *)controller frame:(CGRect)listFrame listArr:(NSArray *)listArr searchType:(SearchType)type block:(void(^)(SearchType searchType,id model))block;

-(void)actionDropListClickWithTapManager:(void(^)())block;

-(void)interfaceWithSearch:(NSString *)key;

@end
