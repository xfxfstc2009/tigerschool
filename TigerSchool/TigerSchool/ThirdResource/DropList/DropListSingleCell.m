//
//  DropListSingleCell.m
//  GiganticWhale
//
//  Created by GiganticWhale on 16/9/8.
//  Copyright © 2016年 Gigantic. All rights reserved.
//

#import "DropListSingleCell.h"

@interface DropListSingleCell()
@property (nonatomic,strong)UILabel *fixedLabel;
@property (nonatomic,strong)TSImageView *arrowImageView;

@end

@implementation DropListSingleCell

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self){
        [self createView];
    }
    return self;
}

#pragma mark - createView
-(void)createView{
    // 1. 创建fixedLabel
    self.fixedLabel = [[UILabel alloc]init];
    self.fixedLabel.backgroundColor = [UIColor clearColor];
    self.fixedLabel.font = [UIFont fontWithCustomerSizeName:@"小正文"];
    [self addSubview:self.fixedLabel];
    
    // 2. 创建arrowImageView
    self.arrowImageView = [[TSImageView alloc]init];
    self.arrowImageView.backgroundColor = [UIColor clearColor];
    self.arrowImageView.image = [UIImage imageNamed:@"icon_tick"];
    [self addSubview:self.arrowImageView];
}

-(void)setTransferCellHeight:(CGFloat)transferCellHeight{
    _transferCellHeight = transferCellHeight;
}

-(void)setTransferFixedStr:(NSString *)transferFixedStr{
    _transferFixedStr = transferFixedStr;
    self.fixedLabel.text = transferFixedStr;
    self.fixedLabel.frame = CGRectMake(TSFloat(11), 0, kScreenBounds.size.width, self.transferCellHeight);
    
    self.arrowImageView.frame = CGRectMake(kScreenBounds.size.width - TSFloat(30) - 15, (self.transferCellHeight - 10) / 2., 15, 10);
}

-(void)setChecked:(BOOL)checked{
    if (checked) {
        self.arrowImageView.image = [UIImage imageNamed:@"icon_tick"];
        [TSTool clickZanWithView:self.arrowImageView block:NULL];
    } else {
        self.arrowImageView.image = nil;
    }
    isChecked = checked;
}

+(CGFloat)calculationCellHeight{
    return 44;
}
@end
