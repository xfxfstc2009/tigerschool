//
//  AliOSSManager.h
//  SmartMin
//
//  Created by 裴烨烽 on 16/1/22.
//  Copyright © 2016年 SmartMin. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OSSSingleFileModel.h"
#import <AliyunOSSiOS/OSSService.h>
#import <AliyunOSSiOS/OSSCompat.h>

typedef NS_ENUM(NSInteger,ImageUsingType) {
    ImageUsingTypeUser = 2,                     /**< User/Avatar 用户头像*/
};

@interface AliOSSManager : NSObject

@property (nonatomic,strong)OSSClient *oSSClient;

+(instancetype)sharedOssManager;


#pragma mark - 上传图片数组
-(void)uploadFileWithObjctype:(ImageUsingType)type name:(NSString *)objcName objImg:(UIImage *)img block:(void(^)(NSString *fileUrl))block progress:(void(^)(CGFloat progress))progressBlock;

-(void)uploadFileWithImgArr:(NSArray<OSSSingleFileModel> *)objcArr uploadType:(ImageUsingType)type compressionRatio:(CGFloat)compressionRatio block:(void(^)(NSArray *fileUrlArr))fileArrBlock;







#pragma mark - 需要上传图片后，然后上传接口
#pragma mark 上传我的相册
-(void)uploadFileWithImgArray:(NSArray *)imgArr successBlock:(void(^)(BOOL isSuccessed,NSArray *imgArr))block;

@end
