//
//  GWSlider.h
//  GiganticWhale
//
//  Created by 裴烨烽 on 16/2/8.
//  Copyright © 2016年 GiganticWhale. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol GWSliderDataSource;
@protocol GWSliderDelegate;

@interface GWSlider : UISlider

- (void)showPopUpView;
- (void)hidePopUpView;
@property (strong, nonatomic) UIColor *textColor;
@property (strong, nonatomic) UIFont *font;
@property (strong, nonatomic) UIColor *popUpViewColor;
@property (strong, nonatomic) NSArray *popUpViewAnimatedColors;
- (void)setPopUpViewAnimatedColors:(NSArray *)popUpViewAnimatedColors withPositions:(NSArray *)positions;
@property (nonatomic) CGFloat popUpViewCornerRadius;
@property (nonatomic) BOOL autoAdjustTrackColor;
- (void)setMaxFractionDigitsDisplayed:(NSUInteger)maxDigits;
@property (copy, nonatomic) NSNumberFormatter *numberFormatter;
@property (weak, nonatomic) id<GWSliderDataSource> dataSource;
@property (weak, nonatomic) id<GWSliderDelegate> delegate;
@property (nonatomic,strong)UIColor *bufferTrackColor;              /**< 缓冲跟踪颜色*/
@property (nonatomic,assign) float bufferValue; //0 - 1.            /**< 缓冲进度*/
@end

@protocol GWSliderDataSource <NSObject>
- (NSString *)slider:(GWSlider *)slider stringForValue:(float)value;
@end

@protocol GWSliderDelegate <NSObject>
- (void)sliderWillDisplayPopUpView:(GWSlider *)slider;

@optional
- (void)sliderDidHidePopUpView:(GWSlider *)slider;
@end

